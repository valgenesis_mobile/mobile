import { Component, OnInit, Injector } from '@angular/core';
import { PopoverController, NavParams } from '@ionic/angular';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { UserIdleService } from 'angular-user-idle';
import {LoadingService} from '../loading.service';
// import {LoginComponent} from '../login/login.component';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {

  modalTitle:string;
  modelId:number;
  timeInSeconds:any;
	time:any;
	runTimer:any;
	hasStarted:any;
	hasFinished:any;
	remainingTime:any;;
  displayTime:any;
  count:any;
  public loadingService: LoadingService;
  constructor(
    private modalController: PopoverController,
	private navParams: NavParams, private userIdle: UserIdleService,private _router: Router,injector:Injector
	, 
  ) { 
		setTimeout(() => this.loadingService = injector.get(LoadingService));
	}
	// public LoginComponent:LoginComponent,
  ngOnInit() {
    console.table(this.navParams);
    this.initTimer();
		this.startTimer();
    this.modalTitle = this.navParams.data.paramTitle;
    //this.userIdle.stopWatching();
    // this.userIdle.setConfigValues({idle: 1, timeout: 120, ping: 120});
	// 	this.userIdle.startWatching();
	// 	this.userIdle.onTimerStart().subscribe(count =>{ 
	// 	this.count = count;		
	// 	if(count == null){
	// 		this.userIdle.resetTimer();
	// 	}
	// 	console.log("modal ", count)});
		
	// 	// Start watch when time is up.
	// 	this.userIdle.onTimeout().subscribe(() => {
	// 		if(	this.count != null){
	// 		const onClosedData: string = "Wrapped Up!";
	// 		this.modalController.dismiss(null, onClosedData);
	// 		const url = localStorage.getItem('valgen_settingUrl');
	// 		localStorage.clear();
	// 		localStorage.setItem('valgen_settingUrl', url);
      
      
	// 		this.userIdle.stopWatching();
	// 		this._router.navigate(['login']);
	// 		}
	// 		else{
	// 			this.ngOnInit();
	// 		}
	// 	});	
	}
	

 
   logout() {
    const url = localStorage.getItem('valgen_settingUrl');
		localStorage.clear();
		localStorage.setItem('valgen_settingUrl', url);
		// this._localpdb.deleteDatabase().subscribe();
	this._router.navigate(['login']);
	//location.reload();
	// this.LoginComponent.load();
    const onClosedData: string = "Wrapped Up!";
    this.modalController.dismiss(null, onClosedData);
  }

   Activate()
  {  
		const onClosedData: string = "Wrapped Up!";
		this.loadingService.reset();
	
    localStorage.setItem('valgen_validTimeStamp', moment().format('YYYY-MM-DD, h:mm:ss'));
     this.modalController.dismiss(null, onClosedData);
  }

  initTimer() {
		// Pomodoro is usually for 25 minutes
	   if (!this.timeInSeconds) { 
		 this.timeInSeconds = 120; 
	   }
	 
	   this.time = this.timeInSeconds;
	   this.runTimer = false;
	   this.hasStarted = false;
	   this.hasFinished = false;
	   this.remainingTime = this.timeInSeconds;
	   
		 this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
		 console.log("this.displayTime  ", this.displayTime)
	 }
	 
	 startTimer() {
		this.runTimer = true;
	   this.hasStarted = true;
	   this.timerTick();
	 }
	 
	 pauseTimer() {
	   this.runTimer = false;
	 }
	 
	 resumeTimer() {
	   this.startTimer();
	 }
	 
	 timerTick() {
	   setTimeout(() => {
	 
		 if (!this.runTimer) { return; }
		 this.remainingTime--;
		 this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
		 if (this.remainingTime > 0) {
		   this.timerTick();
		 }
		 else {
		   this.hasFinished = true;
		   const onClosedData: string = "Wrapped Up!";
		   this.modalController.dismiss(null, onClosedData);
		   const url = localStorage.getItem('valgen_settingUrl');
			localStorage.clear();
			localStorage.setItem('valgen_settingUrl', url);
      
      
			this.userIdle.stopWatching();
			this._router.navigate(['login']);
			// this.LoginComponent.load();
		//	location.reload();
		 }
	   }, 1000);
	 }
	 
	 getSecondsAsDigitalClock(inputSeconds: number) {
	   var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
	   var hours = Math.floor(sec_num / 3600);
	   var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
	   var seconds = sec_num - (hours * 3600) - (minutes * 60);
	   var hoursString = '';
	   var minutesString = '';
	   var secondsString = '';
	   hoursString = (hours < 10) ? "0" + hours : hours.toString();
	   minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
	   secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
	   return  minutesString + ':' + secondsString;
	 }

}
