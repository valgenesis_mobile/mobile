import { Component, OnInit } from '@angular/core';
import { PopoverController, NavParams } from '@ionic/angular';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-task-filter',
  templateUrl: './task-filter.component.html',
  styleUrls: ['./task-filter.component.scss'],
})
export class TaskFilterComponent implements OnInit {
  modalTitle:string;
  modelId:number;
  filterArray = [];
  filterList = [];
  isenabled:boolean=false;
 
  sortValue:any = 'nil';

  customAlertOptions: any = {
    header: 'Select Sorting',
    translucent: true
  };
  constructor(private popoverController: PopoverController,
    private navParams: NavParams) { }

  ngOnInit() {
    console.table(this.navParams);
    this.modelId = this.navParams.data.paramID;
    this.modalTitle = this.navParams.data.paramTitle;
    console.log("localStorage.taskFilter  ", localStorage.taskFilter)
     if(localStorage.taskFilter)
     {
       this.filterList = JSON.parse(localStorage.taskFilter);
       console.log("this.filterList   ", this.filterList)
     }
     
    // if (this.filterList != null || ""){
    //   //checkboxValues = {};
      //  var checkboxValues = JSON.parse(localStorage.getItem('checkboxValues'));
      //  if(checkboxValues != null || ""){
      
        
      //  }
    //   document.getElementById(checkboxValues).setAttribute('checked',"true");
    // }
    this.filterArray = [{
      id:1, name:'Execution', checked:false
    },
    {
      id:2, name:'Split Execution', checked:false
    },{
      id:3, name:'Project', checked:false
    },{
      id:4, name:'Change Control', checked:false
    },{
      id:5, name:'Reviewed', checked:false
    },{
      id:6, name:'Peer', checked:false
    },{
      id:7, name:'Over Due', checked:false
    }];
      
   console.log("this.filterArray  ", this.filterArray)
    if(localStorage.taskFilter){
      this.filterList.forEach((fList) => {
        this.filterArray.forEach((fArray) => {
          if(fList.id == fArray.id){
            fArray.checked = true;
            this.isenabled =true;
          }
        })
      })
      console.log("this.filterArray  inside", this.filterArray)
    }
    // if( this.filterArray.id ==   this.filterList.id)
    //          {
    //            checkValue.ListItems.forEach((list) =>{
    //              if(checkData.Value == list.name){
    //                list.checked = true;
    //              }
    //            })
    //          }
  
    
  }
  async closeModal() {
    const onClosedData: string = "closePopover";
     this.popoverController.dismiss(onClosedData, "DATA");
    
  }

 


  public filterChange(e,check,id){
   if(e.detail.checked == true){
     if(this.sortValue == 1 || 2){
       this.isenabled =true;
     }
        const tempListItem = {
             id: id,
             name: check.name,
             checked: false
           }
       this.filterList.push(tempListItem);
       localStorage.taskFilter = JSON.stringify(this.filterList);
   }
   else
   {

     for(var i=0; i< this.filterList.length; i++){
         if(this.filterList[i].id == id){
          this.filterList.splice(i,1);
         }
        
     }

     this.filterArray.forEach((fArray) => {
      if(id == fArray.id){
        fArray.checked = false;
      }
    })
     localStorage.taskFilter = JSON.stringify(this.filterList);
     console.log("FilterList else ", JSON.stringify(this.filterList));
     if(this.filterList.length == 0)
{
  this.isenabled =false;
} 
else{
  this.isenabled =true;
} 
 }
  }

  public sortChange(e){

    console.log(e.target.value)
    this.sortValue = e.target.value;
    if(this.sortValue =='nil') {
      this.isenabled =false;
        }else{
      
       this.isenabled = true;
    
      }
    
  }

  public filterReset(){
    this.filterList = [];
    this.sortValue = 'nil';
  }
  
  public filterApply() {
    let data = JSON.stringify(this.filterList);
    this.popoverController.dismiss(this.sortValue,data);
  }


}
