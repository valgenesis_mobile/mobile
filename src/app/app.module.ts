import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Network } from '@ionic-native/network/ngx';
// import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { MediaCapture } from '@ionic-native/media-capture/ngx';
// import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { Base64 } from '@ionic-native/base64/ngx';

import { OrderModule } from 'ngx-order-pipe';

import { Pipe, PipeTransform } from '@angular/core';

 //import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Toast } from '@ionic-native/toast/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { UserIdleModule } from 'angular-user-idle';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive'; 
import {Keepalive} from '@ng-idle/keepalive';
import {FileTransfer,  FileTransferObject  } from '@ionic-native/file-transfer';  
import {File} from '@ionic-native/file';
import { Device } from '@ionic-native/device/ngx';
// import { ToastrModule } from 'ng6-toastr-notifications';
//import { ToastrModule } from 'ngx-toastr';
 import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 //import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CommonServiceModule } from './common/http.service';
import { LocalPouchdbProvider } from './common/local-pouchdb';
import { LiveModeService } from './common/livemode.service';
import { ElogModeService } from './common/elogmode.service';
import { ElogPouchdbProvider } from './common/elog-pouchdb';
import { CommonAuthServiceModule } from './common/auth';
import { EncrDecrService } from './common/encr-decr.service';
//import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoadingService } from './loading.service';
import { MenuComponent } from './menu/menu.component';
import { HomeService } from './menu/home/home.service';
import { TaskFilterComponent } from './task-filter/task-filter.component';
import { ElogFilterComponent } from './menu/home/elog/elog-filter/elog-filter.component';
import { ModalPageModule } from './modal/modal.module';

@NgModule({
	declarations: [AppComponent, TaskFilterComponent, ElogFilterComponent],
	entryComponents: [TaskFilterComponent, ElogFilterComponent],
	imports: [
		BrowserModule,OrderModule, 
		IonicModule.forRoot({
			backButtonText: '',
			swipeBackEnabled: true
		}, ),
		AppRoutingModule,
		HttpClientModule,
		CommonServiceModule,
		CommonAuthServiceModule,
		HttpModule,
		OrderModule,
		
		//Pipe,
		ModalPageModule,
		//NgbModule.forRoot(),
		NgIdleKeepaliveModule,
		//ToastrModule.forRoot(),
		BrowserAnimationsModule,
		
		
		
		
	],
	providers: [
		StatusBar,
		SplashScreen,
		{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
		Camera,
		Network,
		// QRScanner,
		EncrDecrService,
		LocalPouchdbProvider,
		ElogPouchdbProvider,
		LiveModeService,
		ElogModeService,
		LoadingService,
		HomeService,
		MenuComponent,
		MediaCapture,
		// EmailComposer,
		Base64,
		// InAppBrowser,
		Toast,
		BarcodeScanner,
		UserIdleModule,
		Keepalive,
		DatePipe,
		// FileTransfer,  
		FileTransferObject,   
		Device,
	],
	bootstrap: [AppComponent],
	schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
