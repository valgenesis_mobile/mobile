import { Component, OnInit  } from '@angular/core';
import { Router } from '@angular/router';
import { Events } from '@ionic/angular';
import { AlertController, LoadingController, MenuController } from '@ionic/angular';
import { LocalPouchdbProvider } from '../common/local-pouchdb';
import { ElogPouchdbProvider } from '../common/elog-pouchdb';
import async from 'async';
import { interval } from 'rxjs/observable/interval';
import { HomeService } from '../menu/home/home.service';
import { Network } from '@ionic-native/network/ngx';
import {LoadingService} from '../loading.service';
import * as momenttz from 'moment-timezone';
import * as moment from 'moment';


@Component({
	templateUrl: 'menu.component.html',
	styleUrls: ['menu.component.scss'],
	providers: [HomeService]
})
export class MenuComponent implements OnInit{

	public loggedInUser: string;
	public UserRole:string;
	public loading: any;
	public siteList: any = [];
	public siteId: any;
	public selectedCodeValue:any;
	public vlmsMenu:boolean = false;
	public eLogMenu:boolean = false;
	public selectOptions = {
		header: 'Change Site'    
	  };
	public appPages = [
		{
			title: 'Home',
			url: '/menu/home',
			icon: 'home'
		},
		// {
		// 	title: 'VLMS',
		// 	url: '/menu/home/task',
		// 	icon: 'list-box'
		// },
		// {
		// 	title: 'E-Log',
		// 	url: '/menu/home/elog',
		// 	icon: 'home'
		// },
		{
			title: 'Profile',
			url: '/menu/profile',
			icon: 'person'
		},
		// {
		// 	title: 'Setting',
		// 	url: '/menu/setting',
		// 	icon: 'settings'
		// }
	];
	customAlertOptions: any = {
		header: 'Select Site',
	  };
	ClientZone:any;
	currentDataTime:any;
	constructor(private _router: Router, public _localpdb: LocalPouchdbProvider, public events: Events, private _homeService: HomeService,
		public _loadingCtrl: LoadingController, public menu: MenuController, public _alertCtrl: AlertController,
		 public _elogpdb: ElogPouchdbProvider, public loadingService: LoadingService, private network: Network) {

	}

	ionViewWillEnter() {
		this.vlmsMenu = false;
		this.eLogMenu = false;
		this.selectedCodeValue ="change Site";
		this.loggedInUser = localStorage.getItem('valgen_firstName');
		this.UserRole = localStorage.getItem('valgen_roleName');		
		this.menu.swipeEnable(true);
		if (localStorage.getItem('valgen_eLogMenu') == 'true') {
			this.eLogMenu = true;
		}
	   if (localStorage.getItem('valgen_vlmsMenu') == 'true') {
			this.vlmsMenu =true;
		}
		
	}
	ngOnInit(){
		this.vlmsMenu = false;
		this.eLogMenu = false;
		this.getSiteDetails();
		this.ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
		this.currentDataTime = moment(moment().format('YYYY-MM-DD, h:mm:ss')); //now
		this.currentDataTime  = new Date();
		if (localStorage.getItem('valgen_eLogMenu') == 'true') {		
			this.eLogMenu = true;
		}
		 if (localStorage.getItem('valgen_vlmsMenu') == 'true') {
			this.vlmsMenu =true;		
		}
	}
	
	
	toggleMenu() {
		this.menu.close();
	}

	public viewprofile(){
		this.menu.close();
		this._router.navigate(['menu', 'profile']);

	}
	public viewsetting(){
		this.menu.close();
		this._router.navigate(['menu', 'setting']);
	}

	async alertForLogout() {

		if (this.network.type != 'none' || navigator.onLine === false) {

			const alert = await this._alertCtrl.create({
				header: 'Logout',
				message: 'Do you want to Logout?',
				backdropDismiss: false,
				buttons: [
					
					{
						text: 'CANCEL',
						handler: () => {
							alert.dismiss();
						}
					},
					{
						text: 'OK',
						handler: () => {
							alert.dismiss();
							this._homeService.UserLogOff().subscribe((data) => {								
								const url = localStorage.getItem('valgen_settingUrl');
								const configData = localStorage.getItem('valgen_SecondaryPassword');
								const appPlatform = localStorage.getItem('valgen_myplatform');
								localStorage.clear(); 
								 localStorage.setItem('valgen_settingUrl', url);
								 localStorage.setItem('valgen_myplatform', appPlatform);
								 console.log("Menu url  ", url)
								 localStorage.setItem('valgen_SecondaryPassword', configData);
									this.menu.close();
									this._router.navigate(['login']);
							})
							
				
							
									
						}
					}
				]
			});
			await alert.present();

               
		}

		else{

			const alert = await this._alertCtrl.create({
				header: 'Logout',
				message: 'You are in Offline Mode. Do you want to Logout?',
				backdropDismiss: false,
				buttons: [
					
					{
						text: 'CANCEL',
						handler: () => {
							alert.dismiss();
						}
					},
					{
						text: 'OK',
						handler: () => {
							alert.dismiss();
							const url = localStorage.getItem('valgen_settingUrl');
							const configData = localStorage.getItem('valgen_SecondaryPassword');
							const appPlatform = localStorage.getItem('valgen_myplatform');
							localStorage.clear();							 
							localStorage.setItem('valgen_settingUrl', url);
							localStorage.setItem('valgen_SecondaryPassword', configData);
							localStorage.setItem('valgen_myplatform', appPlatform);
						    this.menu.close();
							this._router.navigate(['login']);															
						}
					}
				]
			});
			await alert.present();
		}

		
	}

	public getSiteDetails() {
		this._homeService.getSiteDetails().subscribe((data) => {
			this.siteList = [];
			//const siteId = localStorage.getItem('valgen_siteId');
			this.siteId = localStorage.getItem('valgen_siteId');
			console.log("this.siteId   ", this.siteId)
			if(data.TotalCount !== 0) {
				data.Result.forEach((list) => {
					this.siteList.push(list);
				})
				//this.siteList = siteList;
			} else {
				this.alertPopup('No Site List Found !');
			}
			}, (err) => {
				console.log(err, 'getSiteDetails')
			}
		);
	}
    public checkAuthorization(siteId) {
		console.log("Site called.....")
		this._router.navigate(['menu', 'home']);
			this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
				if (data.TransactionId === 0) {
					this.loadingService.pushAllLocalDataToServer(siteId);
					this.menu.close();
				} else {
					this.alertPopup(data.TransactionMessage);
				}
				}, (err) => {
					console.log(err, 'getSiteAuthorization')
				}
			);
		}


		public pushAllLocalDataToServer(siteId) {
			this._localpdb.getUnPushedTaskList().subscribe(
				(data) => {
					if(data.length) {
						this.alertPopup('You Have Unpushed Task List.Kindly Push and Change The Site..')
					} else {
						async.series([
							(done) => {
								this._localpdb.deleteAllEntiryDb(done);
							}
						], () => {
							localStorage.setItem('valgen_siteId', siteId);
							this.toggleMenu();
						});
					}
				},
				(err) => {
					console.log('data', err);
				}
			);
		}
	async alertPopup(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: ['OK']
		});
		await alert.present();
	}


	async loadingController(message) {
		this.loading = await this._loadingCtrl.create({
			message: message,
			spinner: 'lines'
		});
		await this.loading.present();
	}
}
