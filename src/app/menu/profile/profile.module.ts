// Angular Imports
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

// This Module's Components
import { ProfileComponent } from './profile.component';
import { HomeService } from '../home/home.service';

const routes: Routes = [
	{
		path: '',
		component: ProfileComponent
	}
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
		RouterModule.forChild(routes)
    ],
    declarations: [
        ProfileComponent,
    ],
	providers: [
		HomeService
	]
})
export class ProfileModule {

}
