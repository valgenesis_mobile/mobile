import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { LoadingService } from '../../loading.service'
import { LocalPouchdbProvider } from '../../common/local-pouchdb';
import { HomeService } from '../home/home.service';
declare let jQuery: any;
import async from 'async';

@Component({
	selector: 'profile',
	templateUrl: 'profile.component.html',
	styleUrls: ['profile.component.scss']
})
export class ProfileComponent implements OnInit {
	public userName: string;
	public userPostion: string;
	public userEmail: string;
	public siteList: any;
	public loginData: any;
	public appPlatform: any;

	constructor(public _alertCtrl: AlertController, private _homeService: HomeService, private _router: Router,
		public _localpdb: LocalPouchdbProvider, public loading: LoadingService) {
		this.appPlatform = localStorage.valgen_myplatform;
	}

	ionViewWillEnter() {
	//	this.checkSessionTimeOut();
	}

	ngOnInit() {
		this.loading.present('Loading..');
		this.userName = localStorage.getItem('valgen_firstName');
		this.userPostion = localStorage.getItem('valgen_roleName');
		this.userEmail = localStorage.getItem('valgen_emailId');
		this.loginData = JSON.parse(localStorage.valgen_login);
		console.log(this.loginData);
		this.loading.dismiss();
	}

	public checkSessionTimeOut() {
		const currentTime = moment(moment().format('YYYY-MM-DD, h:mm:ss'));//now
		const validTime = moment(localStorage.getItem('valgen_validTimeStamp'));
		const sessionTime = localStorage.getItem('valgen_sessionTimeOut');
		console.log(currentTime.diff(validTime, 'minutes'));
		if (currentTime.diff(validTime, 'minutes') > parseInt(sessionTime)) {
			this.alertSession('Session TimeOut');
		} else {
			localStorage.setItem('valgen_validTimeStamp', moment().format('YYYY-MM-DD, h:mm:ss'));
		}
	}

	public getSiteDetails() {
		this._homeService.getSiteDetails().subscribe((data) => {
			console.log('getSiteDetails', data);
			const siteList = [];
			const siteId = localStorage.getItem('valgen_siteId');
			if (data.TotalCount !== 0) {
				data.Result.forEach((list) => {
					if (list.SiteID != siteId) {
						siteList.push(list);
					}
				})
				this.siteList = siteList;
				this.openModalSiteList();
			} else {
				this.alertPopup('No Site List Found !');
			}
		}, (err) => {
			console.log(err, 'getSiteDetails')
		}
		);
	}

	public openModalSiteList() {
		jQuery('#siteListModalProfile').modal('show');
	}

	public closeSiteModal() {
		console.log('close');
		jQuery('#siteListModalProfile').modal('hide');
	}

	public checkAuthorization(siteId) {
		jQuery('#siteListModal').modal('hide');
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			console.log('getSiteAuthorization', data);
			if (data.TransactionId === 0) {
				this.loading.pushAllLocalDataToServer(siteId);
			} else {
				this.alertPopup(data.TransactionMessage);
			}
		}, (err) => {
			console.log(err, 'getSiteAuthorization')
		}
		);
	}



	async alertPopup(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: ['OK']
		});
		await alert.present();
	}

	async alertSession(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						this.sessionLogout();
					}
				}
			]
		});
		await alert.present();
	}

	public sessionLogout() {
		const url = localStorage.getItem('valgen_settingUrl');
		localStorage.clear();
		localStorage.setItem('valgen_settingUrl', url);
		// this._localpdb.deleteDatabase().subscribe(
		// 	(data) => {
		// 		console.log('delete', data);
		// 	}
		// );
		this._router.navigate(['login']);
	}
}
