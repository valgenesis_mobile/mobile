// Angular Imports
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
// This Module's Components
import { SettingComponent } from './setting.component';
import { HomeService } from '../home/home.service';

const routes: Routes = [
	{
		path: '',
		component: SettingComponent
	}
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule, 
        ReactiveFormsModule,
        IonicModule,
		RouterModule.forChild(routes)
    ],
    declarations: [
        SettingComponent,
    ],
	providers: [
		HomeService
	]
})
export class SettingModule {

}
