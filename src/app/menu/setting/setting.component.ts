import { Component } from '@angular/core';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { LoadingService } from '../../loading.service';
import { LocalPouchdbProvider } from '../../common/local-pouchdb';
import { HomeService } from '../home/home.service';
declare let jQuery: any;
import async from 'async';

@Component({
	selector: 'setting',
	templateUrl: 'setting.component.html',
	styleUrls: ['setting.component.scss']
})
export class SettingComponent {

	public vlmsValue: any;
	public elogValue: any;
	public siteList: any;
	public appPlatform: any;

	constructor(public _alertCtrl: AlertController, private _homeService: HomeService, private _router: Router,
		public _localpdb: LocalPouchdbProvider, public loadingService: LoadingService) {
		this.appPlatform = localStorage.valgen_myplatform;
	}

	ionViewWillEnter() {
		const vlmsMenu = localStorage.getItem('valgen_vlmsMenu');

		if (vlmsMenu === 'true') {
			this.vlmsValue = 'enable';
		} else {
			this.vlmsValue = 'disable';
		}

		const eLogMenu = localStorage.getItem('valgen_eLogMenu');
		if (eLogMenu === 'true') {
			this.elogValue = 'enable';
		} else {
			this.elogValue = 'disable';
		}

		//this.checkSessionTimeOut();
	}

	changeVlms(value) {
		if (value === 'enable') {
			localStorage.setItem('valgen_vlmsMenu', 'true');
		} else {
			localStorage.setItem('valgen_vlmsMenu', 'false');
		}
	}

	changeElog(value) {
		if (value === 'enable') {
			localStorage.setItem('valgen_eLogMenu', 'true');
		} else {
			localStorage.setItem('valgen_eLogMenu', 'false');
		}
	}

	public checkSessionTimeOut() {
		const currentTime = moment(moment().format('YYYY-MM-DD, h:mm:ss'));//now
		const validTime = moment(localStorage.getItem('valgen_validTimeStamp'));
		const sessionTime = localStorage.getItem('valgen_sessionTimeOut');
		console.log(currentTime.diff(validTime, 'minutes'));
		if (currentTime.diff(validTime, 'minutes') > parseInt(sessionTime)) {
			this.alertSession('Session TimeOut');
		} else {
			localStorage.setItem('valgen_validTimeStamp', moment().format('YYYY-MM-DD, h:mm:ss'));
		}
	}

	public getSiteDetails() {
		this._homeService.getSiteDetails().subscribe((data) => {
			console.log('getSiteDetails', data);
			const siteList = [];
			const siteId = localStorage.getItem('valgen_siteId');
			if (data.TotalCount !== 0) {
				data.Result.forEach((list) => {
					if (list.SiteID != siteId) {
						siteList.push(list);
					}
				})
				this.siteList = siteList;
				this.openModalSiteList();
			} else {
				this.alertPopup('No Site List Found !');
			}
		}, (err) => {
			console.log(err, 'getSiteDetails')
		}
		);
	}

	public openModalSiteList() {
		jQuery('#siteListModalSetting').modal('show');
	}

	public closeSiteModal() {
		console.log('close');
		jQuery('#siteListModalSetting').modal('hide');
	}

	public checkAuthorization(siteId) {
		jQuery('#siteListModal').modal('hide');
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			console.log('getSiteAuthorization', data);
			if (data.TransactionId === 0) {
				this.loadingService.pushAllLocalDataToServer(siteId);
			} else {
				this.alertPopup(data.TransactionMessage);
			}
		}, (err) => {
			console.log(err, 'getSiteAuthorization')
		}
		);
	}



	async alertPopup(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: ['OK']
		});
		await alert.present();
	}

	async alertSession(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						this.sessionLogout();
					}
				}
			]
		});
		await alert.present();
	}

	public sessionLogout() {
		const url = localStorage.getItem('valgen_settingUrl');
		localStorage.clear();
		localStorage.setItem('valgen_settingUrl', url);
		// this._localpdb.deleteDatabase().subscribe(
		// 	(data) => {
		// 		console.log('delete', data);
		// 	}
		// );
		this._router.navigate(['login']);
	}
}
