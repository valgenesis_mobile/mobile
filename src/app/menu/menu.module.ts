// Angular Imports
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { Routes, RouterModule } from '@angular/router';
import { CheckLoginAuthGuardService } from '../common/auth/authGuard';
import { CommonAuthServiceModule } from '../common/auth'
import { LocalDbServiceModule } from '../common/local-pouchdb';
import { FormsModule } from '@angular/forms';
// This Module's Components
import { MenuComponent } from './menu.component';

const routes: Routes = [
	{
		path: '', component: MenuComponent, children: [
			{
				path: '', redirectTo: 'home', pathMatch: 'full'
			},
			{
				path: 'home', loadChildren: './home/home.module#HomePageModule', canActivate: [CheckLoginAuthGuardService]
			},
			{ 
				path: 'profile', loadChildren: './profile/profile.module#ProfileModule', canActivate: [CheckLoginAuthGuardService]
			},
			{
				path: 'setting', loadChildren: './setting/setting.module#SettingModule', canActivate: [CheckLoginAuthGuardService]
			}
		]
	}
];
// , canActivate: [CheckLoginAuthGuardService]
@NgModule({
    imports: [
		IonicModule,
		CommonModule,
		FormsModule,
		CommonAuthServiceModule,
		LocalDbServiceModule,
		RouterModule.forChild(routes)
	],
    declarations: [
        MenuComponent,
    ],
    exports: [
        RouterModule,
    ]
})
export class MenuModule {

}
