import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

import { HomeService } from '../../home.service';
declare let jQuery: any;
import async from 'async';
import { LocalPouchdbProvider } from '../../../../common/local-pouchdb';
import {LoadingService} from '../../../../loading.service';
import * as moment from 'moment';

@Component({
    templateUrl: 'viewlog.component.html',
    styleUrls: ['viewlog.component.scss']
})
export class ViewlogComponent {

    public siteList: any;

	constructor( private _router: Router, private _homeService: HomeService, public _alertCtrl: AlertController, 
		private _localpdb: LocalPouchdbProvider, public loadingService: LoadingService) {
	}
	
	ionViewWillEnter() {
       // this.checkSessionTimeOut();
	}
	public goBack() {
		this._router.navigate([`/menu/home/elog`]);
	}
    
    public viewLogListClick (path) {
        this._router.navigate([`/menu/home/elog/viewLog/enterEntity`]);
    }

    public getSiteDetails() {
		this._homeService.getSiteDetails().subscribe((data) => {
			console.log('getSiteDetails', data);
			const siteList = [];
			const siteId = localStorage.getItem('valgen_siteId');
			if(data.TotalCount !== 0) {
				data.Result.forEach((list) => {
					if(list.SiteID != siteId) {
						siteList.push(list);
					}
				})
				this.siteList = siteList;
				this.openModalSiteList();
			} else {
				this.alertPopup('No Site List Found !');
			}
			}, (err) => {
				console.log(err, 'getSiteDetails')
			}
		);
	}

	public openModalSiteList() {
		jQuery('#siteListModalViewLog').modal('show');
	}

	public closeSiteModal() {
		console.log('close');
		jQuery('#siteListModalViewLog').modal('hide');
	}

	public checkAuthorization(siteId) {
		jQuery('#siteListModal').modal('hide');
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			console.log('getSiteAuthorization', data);
			if (data.TransactionId === 0) {
				this.loadingService.pushAllLocalDataToServer(siteId);
			} else {
				this.alertPopup(data.TransactionMessage);
			}
			}, (err) => {
				console.log(err, 'getSiteAuthorization')
			}
		);
	}

	

	public checkSessionTimeOut() {
		const currentTime = moment(moment().format('YYYY-MM-DD, h:mm:ss'));//now
		const validTime = moment(localStorage.getItem('valgen_validTimeStamp'));
		const sessionTime = localStorage.getItem('valgen_sessionTimeOut');
		console.log(currentTime.diff(validTime, 'minutes'));
		if(currentTime.diff(validTime, 'minutes') > parseInt(sessionTime)) {
			this.alertSession('Session TimeOut');
		} else {
			localStorage.setItem('valgen_validTimeStamp', moment().format('YYYY-MM-DD, h:mm:ss'));
		}
	}
	
	async alertSession(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						this.sessionLogout();
					}
				}
			]
		});
		await alert.present();
	}

	public sessionLogout () {
		const url = localStorage.getItem('valgen_settingUrl');
		localStorage.clear();
		localStorage.setItem('valgen_settingUrl', url);
		this._localpdb.deleteDatabase().subscribe(
			(data) => {
				console.log('delete', data);
			}
		);
		this._router.navigate(['login']);
	}

	async alertPopup(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
	}
}
