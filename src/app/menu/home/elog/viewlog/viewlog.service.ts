import { Injectable } from '@angular/core';
import { HttpService } from '../../../../common/http.service';

@Injectable()
export class VieLogService {

	private userId: any;
	private siteId: any;

	constructor(private _httpService: HttpService) {
		this.userId = localStorage.getItem('valgen_userId');
		this.siteId = localStorage.valgen_siteId;
	}

	public getEntityType() {
		const info = {
			url: `EntityType/GetEntityType?SiteID=${this.siteId}&Mode=1&MasterType=6`
		};
		return this._httpService.get(info);
	}

	public getLogType() {
		const info = {
			url: `eLogBook/GetLogType?SiteID=${this.siteId}`
		};
		return this._httpService.get(info);
	}

	public getEntityName(id) {
		const info = {
			url: `EntityType/GetEntity?SiteID=${this.siteId}&Mode=1&MasterType=6&EntityTypeID=${id}`
		};
		return this._httpService.get(info);
	}

	public GetLogBook(startDate, endDate, logTypeId, entityID) {
		const info = {
			url: `eLogBook/GetLogBook?Fromdate=${startDate} 00:00:00&Todate=${endDate} 00:00:00&SiteID=${this.siteId}&LmLogTypeID=${logTypeId}&EntityID=${entityID}&Title=`
		};
		return this._httpService.get(info);
	}

}
