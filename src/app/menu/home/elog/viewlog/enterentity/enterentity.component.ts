import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import * as moment from 'moment';
import { VieLogService } from '../viewlog.service';
import { Toast } from '@ionic-native/toast/ngx';
import { LocalPouchdbProvider } from '../../../../../common/local-pouchdb';
import { HomeService } from '../../../home.service';
import {LoadingService} from '../../../../../loading.service';
declare let jQuery: any;
import async from 'async';

@Component({
	templateUrl: 'enterentity.component.html',
	styleUrls: ['enterentity.component.scss']
})
export class EnterentityComponent implements OnInit {

	public endDate: any;
	public startDate: any;
	public siteList: any;
	public loading: any;
	public entityTypeList: any;
	public entityNameList: any;
	public entityNo: any = '- No Data -';
	public entityID: any;
	public entityLogType: any;
	public entityType: any;
	public entityName: any;
	public logBookList: any;
	public showLogBook = false;
	public logBookButton: boolean = true;
	public taskListButton: boolean = false;
	public disableEntitySelector: boolean = false;
	public entityErrorMsg: boolean = false;
	public elogDetails: any = {};
	public siteId:any;
    public entityTypeOptions: any = {
		header: 'Select Entity Type',
	  };
    public entityOptions: any = {
		header: 'Select Entity',
	  };
	constructor(private _router: Router, private _viewlogService: VieLogService, private _localpdb: LocalPouchdbProvider,
		public _loadingCtrl: LoadingController, public _alertCtrl: AlertController, private _homeService: HomeService
		, public loadingService: LoadingService, private toast: Toast) {
	}

	ionViewWillEnter() {
      //  this.checkSessionTimeOut();
    }

	ngOnInit() {
		this.getSiteDetails();
		this.getEntityTypeName();
		this.getLogType();
		this.logBookButton = true;
		this.taskListButton = false;
	}

	public getLogType () {
		this._viewlogService.getLogType().subscribe(
			(data) => {
				console.log('data', data);
			//this.loading.dismiss();
				this.entityLogType = data.Result;
			},
			(err) => {
				console.log('err', err);
			}
		);
	}


	public getEntityTypeName() {
		this.loadingService.present("Loading....");
		this._viewlogService.getEntityType().subscribe(
			(data) => {
				console.log('data', data);
				this.entityTypeList = data.Result;
				this.loadingService.dismiss();
			},
			(err) => {
				console.log('err', err);
				this.loadingService.dismiss();
			}
		);
	}

	

	public selectedEntityType(typeId) {
		this.entityName = '';
		this.entityNo = '';
		this._viewlogService.getEntityName(typeId).subscribe(
			(data) => {
				console.log('data', data);
				this.entityNameList = data.Result;
				this.disableEntitySelector = true;
				//this.checkLogBookShow();
			},
			(err) => {
				console.log('err', err);
			}
		);
	}

	public selectedEntityName (entityId) {
		console.log(name,'llllllllllllllllll')
		this.entityNameList.forEach((list) => {
			if(list.EntityID === parseInt(entityId)) {
				this.entityNo = list.EntityNo;
				this.entityID = list.EntityID;
				//this.checkLogBookShow();
			}
		});
	}

	public startDateChanged(date) {
		this.startDate = moment(date).format('YYYY-MM-DD');
	//	this.checkLogBookShow();
	}

	public endDateChanged(date) {
		this.endDate = moment(date).format('YYYY-MM-DD');
		//this.checkLogBookShow();
	}

	public checkLogBookShow() {
		console.log(this.startDate);
		if(this.entityType == ''){ 
			this.showLogBook = false;
			this.entityErrorMsg = true;
		} else if(this.entityNo == ''){
			this.showLogBook = false;
			this.entityErrorMsg = true;
		} else if(this.startDate == undefined){
			this.showLogBook = false;
			this.entityErrorMsg = true;
		}else if(this.endDate == undefined){
			this.showLogBook = false;
			this.entityErrorMsg = true;
		}
		 else {
			this.showLogBook = true;
			this.entityErrorMsg = false;
		}
	}

	public toggleSection(i, startDate, endDate, logTypeId) {
		this._viewlogService.GetLogBook(startDate, endDate, logTypeId, this.entityID).subscribe(
			(data) => {
				console.log('data', data);
				if(data.TotalCount === 0) {
					//this.alertController('No data found!');
					// });
					this.toast.show('No data found!', '10000', 'bottom').subscribe(
						toast => {
						}
					);
				} else {
					this.logBookList = data.Result;
					this.entityLogType.forEach((log, index) => {
						if (i === index) {
							log.open = !log.open;
						} else {
							log.open = false;
						}
					});
				}
			},
			(err) => {
				console.log('err', err);
			}
		);
	}
	public elogDetailsView(book,i){
	  console.log("Details  elog ", book);
	  this.elogDetails = book;
	  jQuery('#elogDetails').modal('show');
	}

	public closeSiteModal() {
		console.log('close');
		jQuery('#elogDetails').modal('hide');
	}

	public checkAuthorization(siteId) {
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			if (data.TransactionId === 0) {
				this.loadingService.pushAllLocalDataToServer(siteId);
			} else {
				this.alertController(data.TransactionMessage);
			}
		}, (err) => {
			console.log(err, 'getSiteAuthorization')
		}
		);
	}

	public getSiteDetails() {
		this._homeService.getSiteDetails().subscribe((data) => {
			console.log('getSiteDetails', data);
			const siteList = [];
			const siteId = localStorage.getItem('valgen_siteId');
			this.siteId = localStorage.getItem('valgen_siteId');
			if(data.TotalCount !== 0) {
				data.Result.forEach((list) => {
						siteList.push(list);
				})
				this.siteList = siteList;
			} else {
				this.alertPopup('No Site List Found !');
			}
			}, (err) => {
				console.log(err, 'getSiteDetails')
			}
		);
	}


	public checkSessionTimeOut() {
		const currentTime = moment(moment().format('YYYY-MM-DD, h:mm:ss'));//now
		const validTime = moment(localStorage.getItem('valgen_validTimeStamp'));
		const sessionTime = localStorage.getItem('valgen_sessionTimeOut');
		console.log(currentTime.diff(validTime, 'minutes'));
		if(currentTime.diff(validTime, 'minutes') > parseInt(sessionTime)) {
			this.alertSession('Session TimeOut');
		} else {
			localStorage.setItem('valgen_validTimeStamp', moment().format('YYYY-MM-DD, h:mm:ss'));
		}
	}
	
	async alertSession(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						this.sessionLogout();
					}
				}
			]
		});
		await alert.present();
	}

	public sessionLogout () {
		const url = localStorage.getItem('valgen_settingUrl');
		localStorage.clear();
		localStorage.setItem('valgen_settingUrl', url);
		// this._localpdb.deleteDatabase().subscribe(
		// 	(data) => {
		// 		console.log('delete', data);
		// 	}
		// );
		this._router.navigate(['login']);
	}

	async alertController(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
	}

	async loadingController(message) {
		this.loading = await this._loadingCtrl.create({
			message: message,
			spinner: 'lines'
		});
		await this.loading.present();
	}

	public logBookBtnClick() {
		this.logBookButton = true;
		this.taskListButton = false;
	}
	 public taskListBtnClick(event) {
		this.taskListButton = true;
		this.logBookButton = false;
		this._router.navigate([`/menu/home/elog/elogTask/0`]);
		
	}
	public goBack() {
		this._router.navigate([`/menu/home/elog`]);
	}

	async alertPopup(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
	}

}
