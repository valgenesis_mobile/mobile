import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { ViewlogComponent } from './viewlog.component';
import { EnterentityComponent } from './enterentity/enterentity.component';

import { VieLogService } from './viewlog.service';

const route = [

	{ path: '', component: ViewlogComponent},
	{ path: 'enterEntity', component: EnterentityComponent },
]

@NgModule({
	imports: [
		CommonModule,
		IonicModule,
		FormsModule, 
		ReactiveFormsModule,
		RouterModule.forChild(route)
	],
	declarations: [
		ViewlogComponent,
		EnterentityComponent
	], 
	providers: [
		VieLogService
	]
})
export class ViewlogModule {

}
