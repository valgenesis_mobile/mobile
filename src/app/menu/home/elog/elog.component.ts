import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { HomeService } from '../home.service';
import { AlertController } from '@ionic/angular';
// import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import {LoadingService} from '../../../loading.service';
import { MenuComponent } from '../../../menu/menu.component';
import { LocalPouchdbProvider } from '../../../common/local-pouchdb';
import * as moment from 'moment';
import async from 'async';
declare let jQuery: any;

@Component({
    templateUrl: 'elog.component.html',
    styleUrls: ['elog.component.scss']
})
export class ElogComponent {

	public siteList: any;
	public siteId: any;
	public appPlatform:any;
    
    constructor( private _router: Router, public _alertCtrl: AlertController, 
		private _homeService: HomeService, private _localpdb: LocalPouchdbProvider,
		 private barcode: BarcodeScanner,  public loadingService: LoadingService, public menucom: MenuComponent
		) {
	}
	
	ionViewWillEnter() {
	//	this.checkSessionTimeOut();
		this.getSiteDetails();
		this.appPlatform = localStorage.valgen_myplatform;
    }
    
    public elogListClick (path) {
        this._router.navigate([`/menu/home/elog/${path}`]);
	}
	public goHome()
	{
		this._router.navigate(['/menu/home']);
	}

	menuclick(){
		console.log("menu click")
		this.menucom.ngOnInit();
	}

	public getSiteDetails() {
		this._homeService.getSiteDetails().subscribe((data) => {
			console.log('getSiteDetails', data);
			const siteList = [];
			const siteId = localStorage.getItem('valgen_siteId');
			this.siteId = localStorage.getItem('valgen_siteId');
			if(data.TotalCount !== 0) {
				data.Result.forEach((list) => {
						siteList.push(list);
				})
				this.siteList = siteList;
			} else {
				this.alertPopup('No Site List Found !');
			}
			}, (err) => {
				console.log(err, 'getSiteDetails')
			}
		);
	}

	public checkAuthorization(siteId) {
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			if (data.TransactionId === 0) {
				this.loadingService.pushAllLocalDataToServer(siteId);
			} else {
				this.alertPopup(data.TransactionMessage);
			}
			}, (err) => {
				console.log(err, 'getSiteAuthorization')
			}
		);
	}

	

	public qrCodeScan() {
		

		this.barcode.scan().then(barcodeData => {
			this.loadingService.showLoading10("Loading");
			console.log('Barcode data', barcodeData);
			const qrcodeId = (barcodeData.text).substring(0, 3);
			console.log('Barcode data222', qrcodeId);
			this.gotoElogList(qrcodeId);
			this.loadingService.dismiss();
		//	this.alertScanPopupText(barcodeData.text);
		   }).catch(err => {
			this.loadingService.dismiss();
			   console.log('Error', err);
		   });

	}
	
	public gotoElogList(id){
		this._router.navigate([`/menu/home/elog/elogTask/${id}`]);
	}
	async alertScanPopupText(message) { 
		const alert = await this._alertCtrl.create({
			header: 'QR Scanner Info!',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
	}



	public checkSessionTimeOut() {
		const currentTime = moment(moment().format('YYYY-MM-DD, h:mm:ss'));//now
		const validTime = moment(localStorage.getItem('valgen_validTimeStamp'));
		const sessionTime = localStorage.getItem('valgen_sessionTimeOut');
		if(currentTime.diff(validTime, 'minutes') > parseInt(sessionTime)) {
			this.alertSession('Session TimeOut');
		} else {
			localStorage.setItem('valgen_validTimeStamp', moment().format('YYYY-MM-DD, h:mm:ss'));
		}
	}
	
	async alertSession(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						this.sessionLogout();
					}
				}
			]
		});
		await alert.present();
	}

	public sessionLogout () {
		const url = localStorage.getItem('valgen_settingUrl');
		localStorage.clear();
		localStorage.setItem('valgen_settingUrl', url);
		// this._localpdb.deleteDatabase().subscribe(
		// 	(data) => {
		// 		console.log('delete', data);
		// 	}
		// );
		this._router.navigate(['login']);
	}

	async alertPopup(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
	}
}
