import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
// This Module's Components
import { ElogComponent } from './elog.component';
import { FormsModule } from '@angular/forms';

const route = [

	{ path: '', component: ElogComponent },
	{ path: 'elogTask', loadChildren: './elogtask/elogtask.module#ElogtaskModule' },
	{ path: 'viewLog', loadChildren: './viewlog/viewlog.module#ViewlogModule' }
]

@NgModule({
	imports: [
		CommonModule,
		IonicModule,
		FormsModule,
		RouterModule.forChild(route)
	],
	declarations: [
		ElogComponent,
	]
})
export class ElogModule {

}
