import { Component, OnInit } from '@angular/core';
import { PopoverController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-elog-filter',
  templateUrl: './elog-filter.component.html',
  styleUrls: ['./elog-filter.component.scss'],
})
export class ElogFilterComponent implements OnInit {
  modalTitle:string;
  modelId:number;
  filterArray = [];
  filterList = [];
  sortValue:any = 'nil';
  taskDetails:any;
  
  customAlertOptions: any = {
    header: 'Select Sorting',
    translucent: true
  };
 
  constructor(private popoverController: PopoverController,
    private navParams: NavParams) { }

  ngOnInit() {

    console.table(this.navParams);
    this.modelId = this.navParams.data.paramID;
    this.modalTitle = this.navParams.data.paramTitle;
    if(this.modalTitle === "TaskInfo"){

      this.taskDetails = this.navParams.data.paramDetails;
    }
    this.filterArray = [{
      id:1, name:'Capture Log'
    },
    {
      id:2, name:'Verify Log'
    },{
      id:3, name:'Approve Log'
    },{
      id:4, name:'Reinitiate Log'
    }];

  }
  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
     this.popoverController.dismiss(onClosedData);
  }

  public filterChange(id, e){
   console.log("idddd ", id);
   console.log(e.detail.checked);

   if(e.detail.checked == true){
       this.filterList.push(id);
       console.log("FilterList ", this.filterList);
   }
   else
   {
     for(var i=0; i< this.filterList.length; i++){
         if(this.filterList[i] == id){
          this.filterList.splice(i,1);
         }
     }
     console.log("FilterList else ", this.filterList);
   }
  }

  public sortChange(e){

    console.log(e.target.value)
    this.sortValue = e.target.value;

  }

  
  // public filterApply(): Observable<any> {
	// 	return Observable.create((observer) => {
  //     observer.next(this.sortValue,this.filterList);
	// 		observer.complete();
	// 	});
  // }
  public filterReset(){
    this.filterList = [];
    this.sortValue = 'nil';
  }
  
  public filterApply() {
    let data = JSON.stringify(this.filterList);

    this.popoverController.dismiss(this.sortValue,data);
  }


}
