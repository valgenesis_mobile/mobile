import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonPipesModule } from '../../../../common/pipe';
import { ElogtaskComponent } from './elogtask.component';
import { ElogTaskService } from './elogtask.service';
 import {ElogverifyComponent} from './elogverify/elogverify.component';
 import {ElogapproveComponent} from './elogapprove/elogapprove.component';
 import {ElogformlistComponent} from './elogformlist/elogformlist.component';

import { ElogDbServiceModule } from '../../../../common/elog-pouchdb';

const route = [

	{ path: ':id', component: ElogtaskComponent },
	{ path: ':id/detail', loadChildren: './elogtaskdetail/elogtaskdetail.module#ElogtaskdetailModule' },
	{ path: 'elogverify/:id',  component: ElogverifyComponent},
	{ path: 'elogapprove/:id',  component: ElogapproveComponent},
	{ path: 'elogformlist/:id/:type',  component: ElogformlistComponent},
]

@NgModule({
	imports: [
		ElogDbServiceModule,
		CommonModule,
		FormsModule,
		IonicModule,
		CommonPipesModule,
		RouterModule.forChild(route)
	],
	declarations: [
		ElogtaskComponent,
		ElogverifyComponent,
		ElogapproveComponent,
		ElogformlistComponent
	],
	providers: [
		ElogTaskService
	]
})
export class ElogtaskModule {

}
