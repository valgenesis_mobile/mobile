import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElogformlistComponent } from './elogformlist.component';

describe('ElogformlistComponent', () => {
  let component: ElogformlistComponent;
  let fixture: ComponentFixture<ElogformlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElogformlistComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElogformlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
