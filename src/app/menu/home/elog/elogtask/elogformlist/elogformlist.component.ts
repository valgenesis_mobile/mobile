//import { forEach } from '@angular/router/src/utils/collection';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ElogPouchdbProvider } from '../../../../../common/elog-pouchdb';
import { LocalPouchdbProvider } from '../../../../../common/local-pouchdb';
import { AlertController } from '@ionic/angular';
import async from 'async';
import * as moment from 'moment';
import { HomeService } from '../../../home.service';
declare let jQuery: any;
import * as momenttz from 'moment-timezone';
import { Toast } from '@ionic-native/toast/ngx';
import { ElogtaskComponent } from '../elogtask.component';
import { LoadingService } from '../../../../../loading.service';
import { ElogModeService } from '../../../../../common/elogmode.service';

@Component({
  selector: 'app-elogformlist',
  templateUrl: './elogformlist.component.html',
  styleUrls: ['./elogformlist.component.scss'],
  providers: [ElogtaskComponent]
})
export class ElogformlistComponent implements OnInit {

 
 
	private taskId: string;
	public elogTaskDetail: any;
	public elogTaskLogSection: any;
	public siteList: any;
	public tableDetailsButton: boolean = true;
	public captureDataButton: boolean = false;
	public openSection: boolean = false;
	public siteId: any;
	public userId: any;
	public userName: any;
	public section: any;
	public loadDetails: any;
	public loadSection: any;
	public loadSectionData: any;
	public SecHeader: any;
  public SecDetail: any;
  public verifyFormList:any;
  public taskType:any;

	constructor(private _router: Router, public _elogpdb: ElogPouchdbProvider,
		private route: ActivatedRoute, private _homeService: HomeService, public _alertCtrl: AlertController,
		private _localpdb: LocalPouchdbProvider, private toast: Toast, public elogtaskcom: ElogtaskComponent,
		public loadingService: LoadingService, public _elogModeService: ElogModeService) {
		this.getParamValues();
		this.siteId = localStorage.getItem('valgen_siteId');
		this.userId = localStorage.getItem('valgen_userId');
		this.userName = localStorage.getItem('valgen_userName');
		this.loadDetails = false;
		this.loadSection = false;
    this.loadSectionData = false;
    

	}

	ionViewWillEnter() {
	// this.checkSessionTimeOut();
	}

	private getParamValues() {
		this.route.params.subscribe((params) => {
			console.log('Params', params)
      this.taskId = params['id'];
      this.taskType = params['type'];
		});
    console.log('taskId', this.taskId);
    console.log('taskType', this.taskType);
	}

	ngOnInit() {
	//	this.getElogTaskDetail();   // above label part in html
		this.getElogTaskLogForms();  // section list part in html
		this.tableDetailsButton = true;
		this.captureDataButton = false;
	}

	// private getElogTaskDetail() {
	
	// 	this.loadingService.present('Loading.....');
	// 	this._elogModeService.taskDetails(this.taskId).subscribe(
	// 		(data) => {
	// 			console.log("_elogModeService  ", data)
	// 			if (data.TransactionCode === 'SUCCESS') {
	// 				this.elogTaskDetail = data.Result[0];
	// 				this.loadDetails = true;
	// 				console.log("this.elogTaskDetail ", this.elogTaskDetail)

	// 			}
	// 		})
	// }


	private getElogTaskLogForms() {
    this.loadingService.present('Loading.....');
    this._elogModeService.GetTaskAlertCapture(this.taskId).subscribe(
      (data) => {

        console.log("GetTaskAlertCapture  ", data);

        this.verifyFormList = data.Result;
        this.loadSection = true;
        this.loadingService.dismiss();

      })

  }
  
  public viewForm(formId, form, index){
   if(this.taskType == 0){
    this._router.navigate([`/menu/home/elog/elogTask/elogverify/${formId}`]);
   }
   else
   {
    this._router.navigate([`/menu/home/elog/elogTask/elogapprove/${formId}`]);
   }
    

  }



}
