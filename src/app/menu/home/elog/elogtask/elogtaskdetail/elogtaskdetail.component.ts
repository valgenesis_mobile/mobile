import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ElogPouchdbProvider } from '../../../../../common/elog-pouchdb';
import { LocalPouchdbProvider } from '../../../../../common/local-pouchdb';
import { AlertController, LoadingController } from '@ionic/angular';
import { Network } from '@ionic-native/network/ngx';
import async from 'async';
import * as moment from 'moment';
import { HomeService } from '../../../home.service';
import { ElogModeService } from '../../../../../common/elogmode.service';
import { LoadingService } from '../../../../../loading.service';
declare let jQuery: any;

@Component({
	templateUrl: 'elogtaskdetail.component.html',
	styleUrls: ['elogtaskdetail.component.scss']
})
export class ElogtaskdetailComponent implements OnInit {

	private taskId: string;
	public elogTaskDetail: any;
	public elogTaskLogSection: any = [];
	public siteList: any;
	public tableDetailsButton: boolean = true;
	public captureDataButton: boolean = false;
	public openSection: boolean = false;
	public loading: any;
	public elogUserName: any;
	public loadDetails: boolean;
	public siteId:any;
	public entityId:any;

	constructor(private _router: Router, public _elogpdb: ElogPouchdbProvider,
		private route: ActivatedRoute, private _homeService: HomeService, public _alertCtrl: AlertController,
		private _localpdb: LocalPouchdbProvider, public _elogModeService: ElogModeService, private _network: Network,
		public _loadingCtrl: LoadingController, public loadingService: LoadingService) {
		this.getParamValues();
		this.elogUserName = localStorage.valgen_userElogName;
		this.loadDetails = false;

	}

	ionViewDidEnter() {
	

	}

	ionViewWillEnter() {
		this.getSiteDetails();
		//this.checkSessionTimeOut();
	}

	private getParamValues() {
		this.route.params.subscribe((params) => {
			console.log('Params', params)
			this.taskId = params['id'];
		});
		this.route.queryParams.subscribe((params) => {
			this.entityId = params['EntityId'];
			
	});
		console.log('taskId', this.taskId);
	}

	ngOnInit() {
		this.elogTaskDetail = {
			"Code": "",
			"Title": "",
			"LogFormId": "",
			"LogType": "",
			"Entity": "",
		}
		  //  label part in html
		  this.siteId = localStorage.getItem('valgen_siteId');
		  this.getElogTaskDetail(); 
		  this.getElogTaskLogSection();  // section list part in html
		  this.tableDetailsButton = true;
		  this.captureDataButton = false;
	}

	private getElogTaskDetail() {
		
		this.loadingService.present('Loading.....');
		// this.loadingService.present('Loading.....');
		// this._elogModeService.taskDetails(this.taskId).subscribe(
		// 	(data) => {
		// 		console.log("_elogModeService  ", data)
		// 		if (data.TransactionCode === 'SUCCESS') {
		// 			this.elogTaskDetail = data.Result[0];
		// 			this.loadDetails = true;
		// 			console.log("this.elogTaskDetail ", this.elogTaskDetail)
        //             this.loadingService.dismiss();	
		// 		}})


		this._elogpdb.getElogTaskDetail(this.taskId).subscribe(
			(data) => {
				console.log(data, 'getElogTaskDetail');
				this.elogTaskDetail = data;
				this.loadDetails = true;
				this.loadingService.dismiss();	
			},
			(err) => {
				console.log('data', err);
			}
		);
	}

	// private getElogTaskLogSection() {
	// 	this.elogTaskLogSection = [];
	// 	this._elogpdb.getElogTaskLogSection(this.taskId).subscribe(
	// 		(data) => {
	// 			console.log(data, 'getElogTaskLogSection');
	// 			for (let i = 0; i < data.length; i++) {
	// 				//  if(data[i].LockedStatus === "Locked" && data[i].LockedBy === this.elogUserName){
	// 				this.elogTaskLogSection.push(data[i]);
	// 				//  }
	// 			}
	// 		},
	// 		(err) => {
	// 			console.log('data', err);
	// 		}
	// 	);
	// }

	private getElogTaskLogSection() {

		this._elogpdb.getAllElogTaskDetailDetail(this.taskId).subscribe(
			(localFileData) => {
				console.log("FIND ME  ", localFileData);

				if (localFileData.length == 0) {
					async.series([
						(done) => {
							this._elogpdb.deleteLocalData(this.taskId, done);
						},
						// (done) => {
						// 	this._elogpdb.getAllElogTaskList().subscribe(
						// 		(data) => {}
						// 	);
						// },

						(done) => {
							this._elogpdb.updateElogTaskById(this.taskId,this.entityId, this.siteId,'statusPull').subscribe(
								(ldata) => {
									console.log('updateElogTaskById', ldata);

									done();
								},
								(err) => {
									console.log('data', err);
									done();
								}
							);
						}
					], () => {
						console.log('Completed Push');
						this._router.navigate([`/menu/home/elog/elogTask`]);

					});


				}
				else {
					this.elogTaskLogSection = [];
					this._elogpdb.getElogTaskLogSection(this.taskId).subscribe(
						(data) => {
							console.log(data, 'getElogTaskLogSection');
							for (let i = 0; i < data.length; i++) {
								//  if(data[i].LockedStatus === "Locked" && data[i].LockedBy === this.elogUserName){
								this.elogTaskLogSection.push(data[i]);
								console.log("this.elogTaskLogSection   ", this.elogTaskLogSection)
								//  }
							}
						},
						(err) => {
							console.log('data', err);
						}
					);
				}
				

			})

	}

	public onReleaseSolution(section) {
		//	console.log("Release Section", section)
		if (this._network.type === 'none' || navigator.onLine === false) {
			this.alertPopup('Check Internet Connection!');
		} else {
			// this.loadingController('Syncing..');
			const disconnectSubscription = this._network.onDisconnect().subscribe(() => {
				//	console.log('network was disconnected :-(');
				this.loading.dismiss();
				this.alertPopup('Check Internet Connection!');
			});
			const LockData = 'UnLocked';
			this._elogpdb.updateTaskLogSection(this.taskId, section.FormSectionId, section.LMlogDataSectionID, LockData).subscribe((uData) => {
				//	console.log(uData, 'updateTaskLogSection');
				this.getElogTaskLogSection();
				// this.loading.dismiss();
			}, (uErr) => {
				//console.log(uErr, 'updateTaskLogSection');
			})
			async.series([
				(done) => {
					this._elogpdb.pushSingleSectionLocalData(this.taskId, section.FormSectionId, section.LMlogDataSectionID, done);
				},
				(done) => {
					this._elogpdb.deleteSingleSectionLocalData(this.taskId, section.FormSectionId, section.LMlogDataSectionID, done);
				},

			], () => {
				//console.log('Completed Push');
				disconnectSubscription.unsubscribe();
				// this.loading.dismiss();
			});
		}

	}
	public onAcceptSolution(section) {
		if (this._network.type === 'none' || navigator.onLine === false) {
			this.alertPopup('Check Internet Connection!');
		} else {
			//	this.loadingController('Syncing..');
			const disconnectSubscription = this._network.onDisconnect().subscribe(() => {
				console.log('network was disconnected :-(');
				this.loading.dismiss();
				this.alertPopup('Check Internet Connection!');
			});
				this._elogModeService.checkLockStatus(this.taskId, section.FormSectionId, section.LmEntityPartID).subscribe(
					(data) => {
						console.log("Locked section ", data)
						async.series([
							(done) => {
								const LockData = 'Locked';
								this._elogpdb.updateTaskLogSection(this.taskId, section.FormSectionId, section.LMlogDataSectionID, LockData).subscribe((uData) => {
									console.log(uData, 'updateTaskLogSection');
									this.getElogTaskLogSection();
									done();
								}, (uErr) => {
									console.log(uErr, 'updateTaskLogSection');
									done();
								})
							},
							(done) => {
								this._elogpdb.addSingleElogTaskList(this.taskId,this.entityId,this.siteId, true, 0).subscribe(
									(ldata) => {
										done();
									},
									(err) => {
										done();
									})
							}
						], () => {
							
						})
						
					}, (err) =>{
						this.alertPopup('Check Internet Connection!');					
					}
				)		
		}
	}
	public onCaptureSolution(formsectionid, partId, logDataSectionID, section) {
		console.log("seeeeeeee   ", section)
		if(section.LockedBy === localStorage.valgen_userElogName){
			this._router.navigate([`/menu/home/elog/elogTask/${this.taskId}/detail/captureSolution`], { queryParams: { formsectionid: formsectionid, partId: partId, logDataSectionID: logDataSectionID } });
	
		}
		else
		{
			this.alertPopup('Section already Locked');
		}
		}
	public editSection(section, index) {
		this._router.navigate([`/menu/home/elog/elogTask/${this.taskId}/detail/captureSolution`]);

	}

	public checkLockStatus(taskId, formSectionId) {
		// this._elogModeService.checkLockStatus(taskId, formSectionId).subscribe((data) => {
		// 		console.log('checkLockStatus', data);
		// 		this._elogpdb.updateTaskLogSection(taskId, formSectionId, data).subscribe((uData) => {
		// 			console.log(uData, 'updateTaskLogSection');
		// 			this.getElogTaskLogSection();
		// 		}, (uErr) => {
		// 			console.log(uErr, 'updateTaskLogSection');
		// 		})
		// 	}, (err) => {
		// 		console.log(err, 'checkLockStatus')
		// 	}
		// );
	}

	public getSiteDetails() {
		const userName = localStorage.getItem('valgen_userName');
		// this.userName = {
		// 	userName
		// };
	//	const encryptValue = this._encrDecr.encryptURL(this.userName);
		this._homeService.getSiteDetails().subscribe((data) => {
			const siteList = [];
			const siteId = localStorage.getItem('valgen_siteId');
			this.siteId = localStorage.getItem('valgen_siteId');
			if (data.TotalCount !== 0) {
				data.Result.forEach((list) => {
					siteList.push(list);
				})
				this.siteList = siteList;
			} else {
				this.alertPopup('No Site List Found !');
			}
		}, (err) => {
			
		}
		);

		this._homeService.getRoleProfile().subscribe((data) => {
			if (data.TransactionId === 0) {
			}
		})
	}



	public checkAuthorization(siteId) {
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			if (data.TransactionId === 0) {
				this.loadingService.pushAllLocalDataToServer(siteId);
			} else {
				this.alertPopup(data.TransactionMessage);
				console.log(" My error")
			}
		}, (err) => {
		
		}
		);
	}


	public checkSessionTimeOut() {
		const currentTime = moment(moment().format('YYYY-MM-DD, h:mm:ss'));//now
		const validTime = moment(localStorage.getItem('valgen_validTimeStamp'));
		const sessionTime = localStorage.getItem('valgen_sessionTimeOut');
		console.log(currentTime.diff(validTime, 'minutes'));
		if (currentTime.diff(validTime, 'minutes') > parseInt(sessionTime)) {
			this.alertSession('Session TimeOut');
		} else {
			localStorage.setItem('valgen_validTimeStamp', moment().format('YYYY-MM-DD, h:mm:ss'));
		}
	}

	async alertSession(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						this.sessionLogout();
					}
				}
			]
		});
		await alert.present();
	}

	public sessionLogout() {
		const url = localStorage.getItem('valgen_settingUrl');
		localStorage.clear();
		localStorage.setItem('valgen_settingUrl', url);
		// this._localpdb.deleteDatabase().subscribe(
		// 	(data) => {
		// 		console.log('delete', data);
		// 	}
		// );
		this._router.navigate(['login']);
	}
	async loadingController(message) {
		this.loading = await this._loadingCtrl.create({
			message: message,
			spinner: 'lines'
		});
		await this.loading.present();
	}

	async alertPopup(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
	}
	public tableDetailsBtnClick() {
		this.tableDetailsButton = true;
		this.captureDataButton = false;

	}
	public captureDataBtnClick() {
		this.captureDataButton = true;
		this.tableDetailsButton = false;
		this._router.navigate([`/menu/home/elog/elogTask/${this.taskId}/detail/captureSolution`]);
	}
	public goBack() {
		this._router.navigate([`/menu/home/elog/elogTask/0`]);
	}
}
