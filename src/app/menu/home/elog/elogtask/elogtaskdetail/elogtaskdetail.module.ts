import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AngularEditorModule } from '@kolkov/angular-editor';
import { ElogtaskdetailComponent } from './elogtaskdetail.component';

import { CaptureformComponent } from './captureform/captureform.component';
import { CleaningsolutionComponent } from './cleaningsolution/cleaningsolution.component';
import { CommonPipesModule } from '../../../../../common/pipe';

const routes = [
	{ path: '', component: ElogtaskdetailComponent },
    { path: 'captureSolution', component: CaptureformComponent },
    { path: 'cleaningSolution', component: CleaningsolutionComponent }
]

@NgModule({
    imports: [
        CommonModule,
        CommonPipesModule,
		IonicModule,
		FormsModule, 
        ReactiveFormsModule,
        AngularEditorModule,
		RouterModule.forChild(routes)
    ],
    declarations: [
        ElogtaskdetailComponent,
        CaptureformComponent,
        CleaningsolutionComponent
    ],
    exports: [
		ElogtaskdetailComponent,
	]
})
export class ElogtaskdetailModule {
    static routes = routes;
}
