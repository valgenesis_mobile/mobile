import { Component, OnInit } from '@angular/core';
import { ActionSheetController, AlertController, Platform } from '@ionic/angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ElogPouchdbProvider } from '../../../../../../common/elog-pouchdb';
import * as moment from 'moment';
//import { AlertController, ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { HomeService } from '../../../../home.service';
declare let jQuery: any;
import * as momenttz from 'moment-timezone';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import {LoadingService} from '../../../../../../loading.service';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import * as watermark from 'watermarkjs';

import { DatePipe } from '@angular/common';

declare var $: any;


@Component({
	templateUrl: 'cleaningsolution.component.html',
	styleUrls: ['cleaningsolution.component.scss']
})
export class CleaningsolutionComponent implements OnInit {

	public showNewSection = false;
	public sectionList: any;
	public sectionData: any;
	public FormSectionId;
	public rowId;
	public taskId;
	public siteList: any;
	public newSectionDataArray = [];
	public newSectionData: any;
	public rowIndexPresent: boolean = false;
	public tempHeaderSection = [];
	public checkBoxList = [];
	public allEvidence = [];
	private localFile = {};
	public imageFileData: any;
	public modalImageName: any;
	public EvidenceEnable: boolean = false;
	public elogAttach: any;
	public sectionName:any;
	public partId: any;
	public logDataSectionID:any;
	public fileForm: FormGroup;
	public fileNameChange:any;
	public base64textString:any;
	public uploadData: any;
	public siteId:any;
	public activityName:any;
	public reason:any;
	public sectionDetailData:any;
	public disableEvidence:boolean = false;
	public LOGSECTIONDATA:any;
	public evidenceReq:boolean = false;
	public reasonReq:boolean = false;
	public verifiedReq:boolean = false;
	public evidenceDisplayReq:boolean = false;
	public reasonDisplayReq:boolean = false;
	public verifiedDisplayReq:boolean = false;
	public fileErrorMsg:any;
	public totalEvidenceNo:any;
	public allowSaveSection:boolean = true;
	public hideDate:boolean = true;
	public hideTime:boolean = true;
	public hideDateTime:boolean = true;
	public appPlatform:any;
	public scrimgId: any;
	public originalImage: any;
	public watermarkedImg: any;
	public watermarkFont:any;
	public blobImage: any;
	public isDesktopApp:boolean = false;
	public editorConfig: AngularEditorConfig = {
		enableToolbar: false,
		showToolbar: false,
		editable: true,
		minHeight: '5rem',
	};
	public nonEditorConfig: AngularEditorConfig = {
		enableToolbar: false,
		showToolbar: false,
		editable: false,
		minHeight: '5rem',
	};


	constructor(private _router: Router, public _elogpdb: ElogPouchdbProvider, private route: ActivatedRoute,
		private _homeService: HomeService, public _alertCtrl: AlertController,private datePipe: DatePipe,
		public _actionSheetCtrl: ActionSheetController, private _camera: Camera, public loadingService: LoadingService, private platform: Platform) {
			

	}
    get f() { return this.fileForm.controls; }

	ngOnInit() {
		this.route.queryParams.subscribe(params => {
			this.FormSectionId = params['FormSectionId'];
			this.rowId = Number(params['rowId']);
			this.taskId = params['taskId'];
			this.logDataSectionID = Number(params['logDataSectionID']);
			this.partId = Number(params['partId']);
			this.activityName = params['activity'];
		});
		this.getElogTaskDetailSection();
		this.getLocalImageDocument();
		this.fileForm = new FormGroup({
			imageInput: new FormControl('', Validators.required)
		});
		this.siteId = localStorage.getItem('valgen_siteId');
		this.appPlatform = localStorage.valgen_myplatform;
		console.log("this.appPlatform  ", this.appPlatform)
		if(this.appPlatform === 'Electron')
		{
			this.isDesktopApp = true;
		}else if(this.appPlatform === 'undefined')
		{
			this.platform.ready().then(() => {
				//	console.log("this.platform  ", this.platform)
		
					if (this.platform.is('android')) {
						localStorage.setItem('valgen_myplatform', 'Android');
						
					}
					else if (this.platform.is('ios')) {
						localStorage.setItem('valgen_myplatform', 'IOS');
						
					}
					else if (this.platform.is('electron')) {
						localStorage.setItem('valgen_myplatform', 'Electron');
						this.isDesktopApp = true;
						
					}
					else {
						localStorage.setItem('valgen_myplatform', 'Desktop');
						
					}
					this.appPlatform = localStorage.valgen_myplatform;
					//console.log("this.appPlatform   ", this.appPlatform)
		
		
				});
		}
		this.hideDate = true;
		console.log("this.hideDate  ", this.hideDate)
	this.hideTime = true;
	this.hideDateTime = true;
	}
	ionViewWillEnter() {
		
		this.getSiteDetails();
		this.appPlatform = localStorage.valgen_myplatform;
		console.log("this.appPlatform  ion view", this.appPlatform) 
		if(this.appPlatform === 'Electron')
		{
			this.isDesktopApp = true;
		}else if(this.appPlatform === 'undefined')
		{
			this.platform.ready().then(() => {
				//	console.log("this.platform  ", this.platform)
		
					if (this.platform.is('android')) {
						localStorage.setItem('valgen_myplatform', 'Android');
						
					}
					else if (this.platform.is('ios')) {
						localStorage.setItem('valgen_myplatform', 'IOS');
						
					}
					else if (this.platform.is('electron')) {
						localStorage.setItem('valgen_myplatform', 'Electron');
						this.isDesktopApp = true;
						
					}
					else {
						localStorage.setItem('valgen_myplatform', 'Desktop');
						
					}
					this.appPlatform = localStorage.valgen_myplatform;
					//console.log("this.appPlatform   ", this.appPlatform)
		
		
				});
		}
  
		this.hideDate = true;
		console.log("this.hideDate  ionview", this.hideDate)
	this.hideTime = true;
	this.hideDateTime = true;
	}

	onChildClickFunction(event: Event) {
		event.stopPropagation();
		const target = event.target || event.currentTarget;
		const attachmentFile = this.localFile[target['attributes'].class.value];
		this.openModal(attachmentFile.FileContent, attachmentFile.FileName)
	}

	public goBack() {
		// this._router.navigate([`/menu/home/elog/elogTask/${this.taskId}/detail/captureSolution`]);
		this._router.navigate([`/menu/home/elog/elogTask/${this.taskId}/detail`]);
	}

	public openModal(fileContent, name) {	
		jQuery("body").addClass("modal-open");
		jQuery('#evidenceModal').modal();
		this.modalImageName = name;		
		this.imageFileData = `data:image/png;base64,${fileContent}`;
	}

	public closeModal() {
		// console.log('close');
		jQuery("body").removeClass("modal-open");
		jQuery('#imageModal').modal('hide');
	}

	public openViewEvidenceModal(){
		this.allEvidence = [];
		this.sectionList.displayData.forEach((section) => {
			if (section.fieldlabel === "Evidence") {
				this.allEvidence = section.FileStream;
			}

		})
	}
  public deleteAttachment(i, list){
	 
	  this.openViewEvidenceModal();
	  this.sectionList.displayData.forEach((section) => {
		 
		if (section.fieldlabel === "Evidence") {
		
			section.FileStream.forEach((evilist, index) => {
				if (evilist == list) {
					section.FileStream.splice(index, 1);
					
				}
			})
		}

	})

  }


	public getLocalImageDocument() {
		//this.allEvidence = [];
		const detail_id = `${this.taskId}-${this.FormSectionId}-${this.logDataSectionID}`;
		if (Object.keys(this.localFile).length === 0) {
			this._elogpdb.getElogTaskDetailSection(detail_id).subscribe(
				(sectionData) => {
					if (sectionData.LMWorkSheetFormField.length > 0) {
						sectionData.LMWorkSheetFormField.forEach((sData) => {
							if (sData.fieldlabel === 'Evidence') {
								if (sData.FileStream != null) {
									sData.FileStream.forEach((file) => {
										console.log("File  ", file)
										//this.allEvidence.push(file);
										this.localFile[file.FileName] = file;
										const aTag = document.getElementsByClassName(`${file.FileName}`);
										if (aTag.length) {
											aTag[0].addEventListener('click', this.onChildClickFunction.bind(this));
										}
									});
								}
							}
						})
					}
					console.log("this.allEvidence  ", this.allEvidence)
				})
				
		}
	}

	


	async pictureActionSheet() {
		
		this.takePicture(this._camera.PictureSourceType.CAMERA);
	}


	async takePictureElectron() {
		const image = await Plugins.Camera.getPhoto({
		  quality: 100,
		 // allowEditing: true,
		  resultType: CameraResultType.DataUrl,
		  source: CameraSource.Camera,
		  correctOrientation :true
		});
	//	console.log("Image  ", image)
	    const imageData = image.dataUrl;
	//	this.photo = this.sanitizer.bypassSecurityTrustResourceUrl(image && (image.dataUrl));
	var base64Img = imageData.replace("data:image/jpeg;base64,", "");
	var base64Img = imageData.replace("data:image/png;base64,", "");
		console.log("this.photo  ", base64Img)
		this.scrimgId = 1;
		const dataTime = `Date:${moment()}`;
		const fileName = `IMG_${this.sectionName} ${this.rowId}_${parseInt(this.totalEvidenceNo) + 1}`;
	//	this._camera.getPicture().then((imageData) => {
			this.totalEvidenceNo = parseInt(this.totalEvidenceNo) + 1;
			console.log("this.totalEvidenceNo   ", this.totalEvidenceNo)
			const sequenceID = `IMGSCR_${this.rowId}_${this.scrimgId}`;
			console.log("this.FormSectionId  ", this.FormSectionId)
			//this.uploadImageToLocal(base64Img, this.taskId, this.FormSectionId, this.rowId, fileName);		
			this.cameraImage(fileName, imageData)
			
	  }



	public takePicture(sourceType) {
		const options: CameraOptions = {
			quality: 40,
			destinationType: this._camera.DestinationType.DATA_URL,
			sourceType: sourceType,
			encodingType: this._camera.EncodingType.JPEG,
			mediaType: this._camera.MediaType.PICTURE
		}  
		const fileName = `IMG_${this.sectionName} ${this.rowId}_${parseInt(this.totalEvidenceNo) + 1}`;
		this.scrimgId = 1;
		this._camera.getPicture(options).then((imageData) => {
			
			this.totalEvidenceNo = parseInt(this.totalEvidenceNo) + 1;
			const sequenceID = `IMGSCR_${this.rowId}_${this.scrimgId}`;
			this.cameraImage(fileName, imageData)
			//this.uploadImageToLocal(imageData, this.taskId, this.FormSectionId, this.rowId, fileName);		
		},
			(err) => {
				console.log(err);
			});
	}

	public cameraImage(sequenceID, imageData) {
		//const sequenceID = `IMGSCR_${this.rowId}_${scrimgId}`;
		const fileName = `${sequenceID}.jpg`;
		const dataTime = `Date:${moment()}`;
		console.log("this.appPlatform  came   ", this.appPlatform)
		if(this.appPlatform === 'Electron'){
			this.originalImage = imageData;
			this.watermarkFont = '20px Josefin Slab';			
		}
		else
		{
			 this.originalImage = 'data:image/jpeg;base64,' + imageData;
			//this.originalImage =  imageData;
			this.watermarkFont = '30px Josefin Slab';
		}
		console.log("this.originalImage  ", this.originalImage)
		fetch(this.originalImage)
		.then(res => res.blob())
		.then(blob => {
		  this.blobImage = blob;
		  this.watermarkImage(fileName,sequenceID, this.watermarkFont);
		//this.imageWithWaterMark(fileName,sequenceID, this.originalImage, columnId);
		});

	}

	watermarkImage(fileName,sequenceID, fontsize) {
		
		var day = new Date()
        var dayWrapper = moment(day); 
        var imageTime = dayWrapper.format("YYYY-MM-DD, h:mm:ss"); 
		const ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
		
		
			if(this.appPlatform === 'Electron'){
				var rotate = function(target) {
					var context = target.getContext('2d');		
					var text = `Date:` + imageTime + '(' + ClientZone + ')';
					var metrics = context.measureText(text);
					if( localStorage.valgen_WatermarkPosition === "L    "){
						var x = (target.width ) - (metrics.width - 100);
					}
					else{
						var x = (target.width ) - (metrics.width + 450);
					}
				
				
				var y = (target.height / 2) + 100 ;
				context.translate(x, y);
				context.globalAlpha = 1;
				context.fillStyle = '#0000ff';
				context.font = fontsize;
				context.rotate(-90 * Math.PI / 180);
				context.fillText(text, 0, 0);
				return target;
			  };
			}
			else{
				var rotate = function(target) {
					var context = target.getContext('2d');		
					var text = `Date:` + imageTime + '(' + ClientZone + ')';
					var metrics = context.measureText(text);
					if( localStorage.valgen_WatermarkPosition === 'L    '){
						var x = (target.width ) - (metrics.width + 24);
					}
					else{
						var x = (target.width ) - (metrics.width + 1050);
						
					}
				
				var y = (target.height / 2) + 400 ;
				context.translate(x, y);
				context.globalAlpha = 1;
				context.fillStyle = '#0000ff';
				context.font = fontsize;
				context.rotate(-90 * Math.PI / 180);
				context.fillText(text, 0, 0);
				return target;
			  };
			}

		
		watermark([this.blobImage])
		.image(rotate)
		  .then(img => {			
			var imgData = img.src;			
			this.watermarkedImg = imgData.replace("data:image/png;base64,","");
			this.imageWithWaterMark(fileName,sequenceID, this.watermarkedImg);
		  });
	
	  }


	  public imageWithWaterMark(fileName,sequenceID, watermarkedImg){

		if(this.appPlatform === 'Electron'){
			const uploadData = {
				ExUploadFileEntity: [
					{
						
						AttachmentName: fileName,
						SequenceID: sequenceID,
						FileContent: watermarkedImg,
						CreatedBy: parseInt(localStorage.getItem('valgen_userId')),
						CreatedOn: moment().format(localStorage.getItem('valgen_DataValue'))
					}
				]
			};
			this.uploadImageToLocal(watermarkedImg,this.taskId, this.FormSectionId,this.rowId, fileName);
			this.openViewEvidenceModal();
			//this.uploadImageToLocal(uploadData.ExUploadFileEntity[0], fileName, this.exeId, this.tableId, this.rowId, columnId);
		}
		else{
			const uploadData = {
				ExUploadFileEntity: [
					{
						
						AttachmentName: fileName,
						SequenceID: sequenceID,
						FileContent: watermarkedImg,
						CreatedBy: parseInt(localStorage.getItem('valgen_userId')),
						CreatedOn: moment().format(localStorage.getItem('valgen_DataValue'))
					}
				]
			};
			this.uploadImageToLocal(watermarkedImg,this.taskId, this.FormSectionId,this.rowId, fileName);
			this.openViewEvidenceModal();
		}
	  }


	public uploadImageToLocal(imageData,taskId,FormSectionId ,rowId, fileName) {
		
		var extn = fileName.split(".").pop();
		this.sectionList.displayData.forEach((section) => {
			if (section.fieldlabel === "Evidence") {
				if(section.FileStream && section.FileId){
					
				}
				else{
					section.FileStream = [];
					section.FileId =[];
				}
				const fileStream =	 
				{
					FileContent: imageData,
					FileName: fileName,
					ContentType: null,
					FileSize: 0,
					CreatedBy:parseInt(localStorage.getItem('valgen_userId')),
					UserName: localStorage.getItem('valgen_userName'),
					CreatedOn: moment().format(localStorage.getItem('valgen_DataValue')),
					FolderName: null,
					FileType: null,
					DocExtension: extn,
					RowId : rowId
				}

				const FileId = {
					FileContent: null,
					FileName: fileName,
					ContentType: null,
					FileSize: 0,
					CreatedBy: parseInt(localStorage.getItem('valgen_userId')),
					UserName: null,
					CreatedOn: moment().format(localStorage.getItem('valgen_DataValue')),
					FolderName: null,
					FileType: null,
					DocExtension: extn,
				}
			
				section.Value = section.Value + `<a target="_blank" href="javascript:void(0)" class='${fileName}'>[${fileName}]</a>`;
				section.FileStream.push(fileStream);
				section.FileId.push(FileId);
				this.elogAttach = "";
				section.FileStream.forEach((evidence) => {
				// this.elogAttach = this.elogAttach + `<a  (click)="imageOpen(evidence)" class='${evidence.FileName}'>[${evidence.FileName}]</a>`;
				this.elogAttach = this.elogAttach + `<a target="_blank" href="javascript:void(0)" class='${evidence.FileName}'>[${evidence.FileName}]</a>`;

				})
				console.log("this.sectionList  56565656", this.sectionList)
				this.EvidenceEnable = true;
				this.getLocalImageDocument();
			}
			else{

				
			}
			
		})
	}
	
		
	public imageOpen(evidence){
		console.log("Evid  ", evidence)
	}

	public chooseFileModal() {
		jQuery('#FileModal').modal('show');
		
	}

	areaChange() {    //remove emoji in input field
		jQuery('#areainput').keyup(function () {
			this.value = this.value.replace(/(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g, '');
		});
		jQuery('#filename').keyup(function(){
			this.value = this.value.replace(/(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g, '');
		});	
	}

	focusOutFunction()
	{

	}
	focusChange()
	{
		
	}

	


	public filechange(event: any) {				 
		var file = event.target.files[0];	
		console.log("file  ", file)	
		if(file.type == "image/jpeg" || file.type == "image/png" || file.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.type == "application/pdf" || file.type == "application/msword" || file.type == "application/vnd.ms-excel")
		{
		if(file.size < 999999){
		this.fileErrorMsg ='';
		var name = event.target.files[0].name;		
		this.fileForm.controls['imageInput'].setValue(file ? file.name : '');
		if (event.target.files.length > 0) {
			var extn = name.split(".").pop();
			//var isNumber = rxjsNumeric.isNumeric(extn)		
			this.fileNameChange = `FILE_${this.sectionName} ${this.rowId}_${parseInt(this.totalEvidenceNo) + 1}` + "." + extn;		
			const fileReader = new FileReader();
			fileReader.onload = this.handleReaderLoaded.bind(this);
			fileReader.readAsBinaryString(file);
			this.totalEvidenceNo = this.totalEvidenceNo + 1;
		}
	}
	else{
		this.fileErrorMsg = "Select file less the 1 MB";		
	}
}
else{
	this.fileErrorMsg = "Select only allowed file format";	
}
	}



	handleReaderLoaded(event) {
	var file = event.target.result;
	const sequenceID = this.newGuid();
	this.base64textString= btoa(file);
	this.uploadData = this.base64textString;			
	}

	public submitFile(){
		this.uploadImageToLocal(this.uploadData, this.taskId, this.FormSectionId, this.rowId, this.fileNameChange);
		this.openViewEvidenceModal();
		jQuery('#FileModal').modal('hide');
		this.fileForm.reset();
	}
	public viewEvidence(ev){
	}



	public getElogTaskDetailSection() {
		const id = `${this.taskId}-${this.FormSectionId}`;
		const detail_id =  `${this.taskId}-${this.FormSectionId}-${this.logDataSectionID}`;
		let logSectionDetail = {
			"headers": [],
			"displayData": []
		};
		this.sectionDetailData =[];
		this._elogpdb.getElogTaskHeaderSection(id).subscribe(
			(data) => {
				console.log("getElogTaskHeaderSection  ", data)
				const headers = [];
				const tempHeaders = [];
				this.sectionName = data.SectionName;
				this.tempHeaderSection = data.LMWorkSheetFormField;
				data.LMWorkSheetFormField.forEach((header, index) => {
					if(header.EntityPartID == this.partId){
					if (header.ControlType === 'Drop Down' || header.ControlType === 'Date' || header.ControlType === 'Text Box' || header.ControlType === 'Check Box' || header.ControlType === 'Radio Button' || header.ControlType === 'Text Area' || header.ControlType === 'Time' || header.ControlType === 'DateTime') {
						headers.push(header);
					}
					else
					{
						// header.Value = '';
						// header.RowIndex = this.rowId;
						// logSectionDetail.displayData.push(header);
					}
				}
				});

				logSectionDetail.headers = headers;
				this._elogpdb.getElogTaskDetailSection(detail_id).subscribe(
					(sectionData) => {
						this.sectionData = sectionData;
						console.log("this.sectionData    ", this.sectionData )
						if(sectionData.ReferenceType == "Header" || sectionData.ReferenceType == "Footer"){
							this.disableEvidence = true;  // to disable the evidence tab
						}
						if (sectionData.LMWorkSheetFormField.length > 0) {
							
							sectionData.LMWorkSheetFormField.forEach((sData) => {
							if(!this.rowIndexPresent){
								if (parseInt(sData.RowIndex) === parseInt(this.rowId)) {
									this.rowIndexPresent = true; 									
								}					
							}
							})
                            console.log("this.rowIndexPresent  ", this.rowIndexPresent)
							if (this.rowIndexPresent) {   // For Edit Function																	
								sectionData.LMWorkSheetFormField.forEach((sData) => {
									//console.log(" sData sData ", sData)
									if (parseInt(sData.RowIndex) === parseInt(this.rowId)) {
										logSectionDetail.displayData.push(sData);
										if (sData.fieldlabel === 'Evidence') {											
											if(sData.FileStream){
												this.totalEvidenceNo = sData.FileStream.length;
											}
											else{
												this.totalEvidenceNo = 0;
											}											
											if (sData.Value != '') {
												this.elogAttach = "";
												if(sData.FileStream != null){												
												sData.FileStream.forEach((evidence) => {
													this.elogAttach = this.elogAttach + `<a target="_blank" href="javascript:void(0)" class='${evidence.FileName}'>[${evidence.FileName}]</a>`;
												})
												this.EvidenceEnable = true;
											}
											}
											else{
												sData.Value = '';
												sData.FileStream = [];
												this.totalEvidenceNo = 0;
											}											
										}								
									}
								})


								logSectionDetail.displayData.forEach((display) => {  // assigning display data to headers data
									logSectionDetail.headers.forEach((head) => {
										if (display.fieldid === head.fieldid) {
											head.Value = display.Value;
											head.RowIndex = display.RowIndex;
										}
									})
								})
							}
							else {
								console.log("dddddddd")
								this.tempHeaderSection.forEach((tempsec) => {  // If row not present in sectionDetails array
									tempsec.Value = '';
									tempsec.RowIndex = this.rowId;
								})
								logSectionDetail.displayData = this.tempHeaderSection;
							}
						}

						// if SectionDetails.LMWorkSheetFormField  length is 0
						else {
							console.log("this.tempHeaderSection  ", this.tempHeaderSection)
							this.tempHeaderSection.forEach((tempsec) => {
								tempsec.Value = '';
								tempsec.RowIndex = this.rowId;
							})
							logSectionDetail.displayData = this.tempHeaderSection;
						}

						this.showNewSection = true;
						this.sectionList = logSectionDetail;	
						console.log("this.sectionList  ", this.sectionList)					
						this.sectionList.headers.forEach((checkValue) =>{
								if(checkValue.ControlType === "Radio Button"){
									 const tempListItemArray = checkValue.ListItems;
									 checkValue.ListItems = [];
									 tempListItemArray.forEach((listitem, index) => {
                                          const tempListItem = {
											  id: index,
											  name: listitem,
											  checked: false
										  }
									 checkValue.ListItems.push(tempListItem);
								   })
                                   this.sectionList.displayData.forEach((checkData) => {
												if(checkData.fieldid == checkValue.fieldid)
												{
													checkValue.ListItems.forEach((list) =>{
														if(checkData.Value == list.name){
															list.checked = true;
														}
													})
												}
											})
								}

								if(checkValue.ControlType === "Check Box"){
									const tempListItemArray = checkValue.ListItems;
									checkValue.ListItems = [];
									tempListItemArray.forEach((listitem, index) => {
										 const tempListItem = {
											 id: index,
											 name: listitem,
											 checked: false
										 }
										 checkValue.ListItems.push(tempListItem);
								  })

								  this.sectionList.displayData.forEach((checkData) => {
											   if(checkData.fieldid == checkValue.fieldid)
											   {
												   checkValue.ListItems.forEach((list) =>{
													   if(checkData.Value == list.name){
														   list.checked = true;
													   }
												   })
											   }
										   })
							   }


							   this.sectionList.displayData.forEach((evidence) => {
							    if (evidence.fieldlabel === 'Evidence') {
									if(evidence.FileStream){
										this.totalEvidenceNo = evidence.FileStream.length;
									    this.allEvidence = evidence.FileStream;
											
								
										
									}
									else{
										this.totalEvidenceNo = 0;
									}
								}
							})					
							})
							sectionData.LMWorkSheetFormField = logSectionDetail.displayData;
					},
					(err) => {
						console.log('data', err);
					});
			},
			(error) => {
				console.log('data', error);
			}
		);
	}


	radioChange(e, radio, fieldid) {
		this.sectionList.displayData.forEach((section) => {			
			if (section.fieldid == fieldid) {				
				section.Value =radio.name;
			}
		})

		this.sectionList.headers.forEach((section) => {			
			if (section.fieldid == fieldid) {			
				section.Value =radio.name;
			}
		})
	}

	checkChange(e, check, fieldid) {

		// console.log("Event ", e.detail.checked)

		// if(e.detail.checked == true){        This is for MultiSection Option 
		// 	this.checkBoxList.push(check);
		// 	console.log("checkBoxList ", this.checkBoxList);
		// }
		// else
		// {
		//   for(var i=0; i< this.checkBoxList.length; i++){
		// 	  if(this.checkBoxList[i] === check){
		// 	   this.checkBoxList.splice(i,1);
		// 	  }
		//   }
		//   console.log("FilterList else ", this.checkBoxList);
		// }

		this.sectionList.displayData.forEach((section) => {
			if (section.fieldid == fieldid) {
				section.Value = check.name;
			}
		})

		this.sectionList.headers.forEach((section) => {
			if (section.fieldid == fieldid) {
				section.Value = check.name;
			}
		})
	}

	async editElogData() {
		const alert = await this._alertCtrl.create({
			header: 'Reason for Edit',
			backdropDismiss: false,
			inputs: [
				{
					name: 'Reason',
					type: 'text',
					placeholder: 'Reason for Edit'
				}
			],
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel'
				},
				{
					text: 'Save',
					handler: (data) => {					
						this.saveElogData(data.Reason);
					}
				}
				
			]
		});
		await alert.present();
	}
	

	public saveElogData(reason?) {
		this.reason = reason;
		for(let i=0; i< this.sectionList.headers.length; i++){

			if(this.sectionList.headers[i].Value != "")
			{
                this.allowSaveSection = true;
			}
			else
			{
				this.allowSaveSection = false;	
				return false;
			}
		}
		if(this.allowSaveSection){
			this.saveSection();
		}
		else{
			this.alertPopup('Enter All Fields');
		}
		
	}
	
	public formatAMPM(date) {
		console.log("Timee" , date)
		
		//console.log("Time    ", this.datePipe.transform(date, 'HH:mm'))
		var hours = date.substring(0,2);
		var minutes = date.substr(date.length - 2); ;
		var ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		hours = hours ? hours : 12;
		minutes = minutes < 10 ? '0'+minutes : minutes;
		var strTime = hours + ':' + minutes + ' ' + ampm;
		console.log("strTime   ", strTime)
		return strTime;
		
  }

  public convertTo24Hour(time) {
	var hours = Number(time[0]);
	var tempminutes = time[1].split(' ');
	var minutes = Number(tempminutes[0])
	var AMPM = tempminutes[1];
	if(AMPM == "PM" && hours<12) hours = hours+12;
	if(AMPM == "AM" && hours==12) hours = hours-12;
	var sHours = hours.toString();
	var sMinutes = minutes.toString();
	if(hours<10) sHours = "0" + sHours;
	if(minutes<10) sMinutes = "0" + sMinutes;
	var strTime = sHours + ':' + sMinutes;
    return strTime;
}

	public saveSection(){
		console.log("save section   ", this.sectionList)
				const detail_id =  `${this.taskId}-${this.FormSectionId}-${this.logDataSectionID}`;
				this._elogpdb.getElogTaskDetailSection(detail_id).subscribe(
					(LOGData) => {
						this.LOGSECTIONDATA = LOGData;
						this.LOGSECTIONDATA.LMWorkSheetFormField.forEach((logData) =>{
							if (parseInt(logData.RowIndex) === parseInt(this.rowId)) {
								if(logData.fieldlabel == 'Evidence'){
									this.evidenceReq = true;
								}
								if(logData.fieldlabel == "Reason"){
									 this.reasonReq = true;
								 } 
								if(logData.fieldlabel == "VerifiedBy"){
									 this.verifiedReq = true;
							}
						}
						})
		
				if (this.rowIndexPresent) {   // edit function 
					this.tempHeaderSection.forEach((tempHeader) => { // In already Entered Data, if Evidence not there insert over here
					if(!this.evidenceReq){
						if(tempHeader.fieldlabel === 'Evidence'){
							tempHeader.Value ='';
							tempHeader.RowIndex = this.rowId;
							tempHeader.FileStream = [];
							tempHeader.FileId = [];
							this.LOGSECTIONDATA.LMWorkSheetFormField.push(tempHeader);	
						} 
					}
					if(!this.reasonReq){
					if( tempHeader.fieldlabel === "Reason"){
							tempHeader.Value ='';
							tempHeader.RowIndex = this.rowId;
							this.LOGSECTIONDATA.LMWorkSheetFormField.push(tempHeader);
						} 
					}
					if(!this.verifiedReq){
					if(tempHeader.fieldlabel === "VerifiedBy"){				
							tempHeader.Value ='';
							tempHeader.RowIndex = this.rowId;
							this.LOGSECTIONDATA.LMWorkSheetFormField.push(tempHeader);					
						}
					}
						
					})
		
					
					this.sectionList.headers.forEach((header) => {
						this.sectionList.displayData.forEach((display) => {
							if (header.fieldid === display.fieldid) {
								display.Value = header.Value;
								if (display.ControlType === "Date") {
									console.log("this.hideDate  ", this.hideDate)
									if(this.hideDate){
										console.log("Without change2")
										display.Value = ((header.Value));
										header.Value = display.Value;
									}
									else{

										console.log("With change2")
									display.Value = moment((header.Value)).format("DD/MM/YYYY");
									header.Value = display.Value;
									}
									// const dateValue =  ((header.Value).toDate);
									// console.log("this.datePipe.transform ", this.datePipe.transform(header.Value,"dd/MM/yyyy"))
									// display.Value = this.datePipe.transform(header.Value,"dd/MM/yyyy");
								}
								if (display.ControlType === "DateTime") {
									if(this.hideDate){
										//console.log("Without change1")
										display.Value = ((header.Value));
										header.Value = display.Value;
									}
									else{

									//console.log("With change1")
									display.Value = moment((header.Value)).format("DD/MM/YYYY hh:mm A");
									header.Value = display.Value;
									}
									// display.Value = moment((header.Value).toDate).format("DD/MM/YYYY hh:mm A");
								}
								if (display.ControlType === "Time") {
									// display.Value = this.formatAMPM((header.Value));
									display.Value = ((header.Value));
									// display.Value = (header.Value).toDate).format("hh:mm A");
								}
							}
							 if(this.reason){
							if(display.fieldlabel === "Reason"){
								display.Value = this.reason;			
							}
						   }
			
							if (display.fieldlabel === "PerformedBy") {
								display.Value = localStorage.getItem('valgen_userElogName') + "  " + moment().format(localStorage.getItem('valgen_DataValue')) + " (" + (momenttz.tz(momenttz.tz.guess()).zoneAbbr()) + ")";
							}
						});
					});
		
					this.sectionList.displayData.forEach((secDisplay) => {
						this.LOGSECTIONDATA.LMWorkSheetFormField.forEach((secData) => {
							if (secData.RowIndex == secDisplay.RowIndex) {
								if (secData.fieldid == secDisplay.fieldid) {
									secData.FileStream = secDisplay.FileStream;
									secData.Value = secDisplay.Value;
								}
							}
						})
					})
				}
				else {					
					this.sectionList.headers.forEach((header) => {
						this.sectionList.displayData.forEach((display) => {
							if (header.fieldid === display.fieldid) {
								display.Value = header.Value;
								if (display.ControlType === "Date") {
									console.log("this.hideDate  ", this.hideDate)
									// const dateValue =  ((header.Value).toDate);
									// console.log("this.datePipe.transform ", this.datePipe.transform(header.Value,"dd/MM/yyyy"))
									// display.Value = this.datePipe.transform(header.Value,"dd/MM/yyyy");
									if(this.hideDate){
										console.log("Without change1")
										display.Value = ((header.Value));
										header.Value = display.Value;
									}
									else{

									console.log("With change1")
									display.Value = moment((header.Value)).format("DD/MM/YYYY");
									header.Value = display.Value;
									}
									// display.Value = moment((header.Value).toDate).format("DD/MM/YYYY");
								}
								if (display.ControlType === "DateTime") {
									if(this.hideDate){
										//console.log("Without change1")
										display.Value = ((header.Value));
										header.Value = display.Value;
									}
									else{

									//console.log("With change1")
									display.Value = moment((header.Value)).format("DD/MM/YYYY hh:mm A");
									header.Value = display.Value;
									}
									// display.Value = moment((header.Value).toDate).format("DD/MM/YYYY hh:mm A");
								}	
								if (display.ControlType === "Time") {
									// display.Value = moment((header.Value).toDate).format("hh:mm A");
									// display.Value = this.formatAMPM((header.Value));
									display.Value = ((header.Value));
								}							
							}
							
						if(this.reason){							
							if(display.fieldlabel === "Reason"){
								display.Value = this.reason;			
							}
						}			
							if (display.fieldlabel === "PerformedBy") {
								display.Value = localStorage.getItem('valgen_userElogName') + "  " + moment().format(localStorage.getItem('valgen_DataValue')) + " (" + (momenttz.tz(momenttz.tz.guess()).zoneAbbr()) + ")";
							}
						});
					});
		
					this.sectionList.displayData.forEach((secDisplay) => {
						this.LOGSECTIONDATA.LMWorkSheetFormField.push(secDisplay);
					})
				}
				const id = `${this.taskId}-${this.FormSectionId}-${this.logDataSectionID}`;
				this._elogpdb.updateElogTaskDetailSection(id, this.LOGSECTIONDATA)
					.subscribe(
						(data) => { 
							this._router.navigate([`/menu/home/elog/elogTask/${this.taskId}/detail/captureSolution`], { queryParams: { formsectionid: this.FormSectionId, partId: this.partId, logDataSectionID: this.logDataSectionID } });
						},
						(error) => {
							console.log(error);
						}
					);
				})
	}
	public changeDate(list){
		console.log("Dateeeee ", list)
		// this.sectionList.headers.forEach((header) => {
		// 	this.sectionList.displayData.forEach((display) => {
		// 		if (header.fieldid === display.fieldid) {
		// 			display.Value = header.Value;
		// 			if (display.ControlType === "Date") {
		// 				display.Value = moment((header.Value)).format("DD/MM/YYYY");
		// 				// const dateValue =  ((header.Value).toDate);
		// 				// console.log("this.datePipe.transform ", this.datePipe.transform(header.Value,"dd/MM/yyyy"))
		// 				// display.Value = this.datePipe.transform(header.Value,"dd/MM/yyyy");
		// 			}
		// 			// if (display.ControlType === "DateTime") {
		// 			// 	display.Value = moment((header.Value).toDate).format("DD/MM/YYYY hh:mm A");
		// 			// }
		// 			// if (display.ControlType === "Time") {
		// 			// 	// display.Value = this.formatAMPM((header.Value));
		// 			// 	display.Value = ((header.Value));
		// 			// 	// display.Value = (header.Value).toDate).format("hh:mm A");
		// 			// }
		// 		}
		// 	})
		// })
	}

	public dateChange(list){
		console.log("Date6666666666666666666666666666666666  ", list)
		 this.hideDate = false;
	}

	public timeChange(list){	
		console.log(" list  ", list)	
		this.hideTime = false;
   }

   public dateTimeChange(list){	
	this.hideDateTime = false;
}

	public reset(){
		this.sectionList.headers.forEach((list) =>{
			list.Value = "";
			if(list.ControlType == "Check Box" || list.ControlType == "Radio Button")
			{  				
				list.ListItems.forEach((item) => {
					item.checked= false;
				})
			}
			
		})


		
		this.sectionList.displayData.forEach((section) => {
			console.log("Section   out ", section)
		  if (section.fieldlabel === "Evidence") {
			console.log("section   ", section)
			 console.log("section.FileStream   ", section.FileStream)
			  section.FileStream = [];
			  section.FileId =[];
			  section.Value = '';
			  this.openViewEvidenceModal();
		  }
  
	  })
	}

	public getSiteDetails() {
		const userName = localStorage.getItem('valgen_userName');
		this._homeService.getSiteDetails().subscribe((data) => {
			const siteList = [];
			const siteId = localStorage.getItem('valgen_siteId');
			this.siteId = localStorage.getItem('valgen_siteId');
			if (data.TotalCount !== 0) {
				data.Result.forEach((list) => {
					siteList.push(list);
				})
				this.siteList = siteList;
			} else {
				this.alertPopup('No Site List Found !');
			}
		}, (err) => {
			
		}
		);

		this._homeService.getRoleProfile().subscribe((data) => {
			if (data.TransactionId === 0) {
			}
		})
	}



	public checkAuthorization(siteId) {
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			if (data.TransactionId === 0) {
				this.loadingService.pushAllLocalDataToServer(siteId);
			} else {
				this.alertPopup(data.TransactionMessage);
				console.log(" My error")
			}
		}, (err) => {
		
		}
		);
	}

	public checkSessionTimeOut() {
		const currentTime = moment(moment().format('YYYY-MM-DD, h:mm:ss'));//now
		const validTime = moment(localStorage.getItem('valgen_validTimeStamp'));
		const sessionTime = localStorage.getItem('valgen_sessionTimeOut');
		if (currentTime.diff(validTime, 'minutes') > parseInt(sessionTime)) {
			this.alertSession('Session TimeOut');
		} else {
			localStorage.setItem('valgen_validTimeStamp', moment().format('YYYY-MM-DD, h:mm:ss'));
		}
	}

	async alertSession(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						this.sessionLogout();
					}
				}
			]
		});
		await alert.present();
	}

	public sessionLogout() {
		const url = localStorage.getItem('valgen_settingUrl');
		localStorage.clear();
		localStorage.setItem('valgen_settingUrl', url);
		this._router.navigate(['login']);
	}

	async alertPopup(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
	}

	public newGuid() {
		const val = Math.floor(1000 + Math.random() * 9000);
		return val;
	}
}
