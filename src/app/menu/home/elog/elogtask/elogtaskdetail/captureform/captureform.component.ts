import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { ElogPouchdbProvider } from '../../../../../../common/elog-pouchdb';
import { LocalPouchdbProvider } from '../../../../../../common/local-pouchdb';
import { AlertController } from '@ionic/angular';
import { LoadingService } from '../../../../../../loading.service';
import { HomeService } from '../../../../home.service';
declare let jQuery: any;

@Component({
	templateUrl: 'captureform.component.html',
	styleUrls: ['captureform.component.scss']
})
export class CaptureformComponent  {

	private taskId: string;
	public taskDetailSection: any;
	private maxRowCount = 0;
	public siteList: any;
	public sectionFormId: any;
	public partId: any;
	public logDataSectionID: any;
	public sectionName: any;
	public sectionHead = [];
	public sectionFoot = [];
	public headerFormSectionId: any;
	public footerFormSectionId: any;
	public sectionHeaderDetails = [];
	public sectionFooterDetails = [];
	public loadSection: boolean;
	public loadHeader: any;
	public loadFooter: any;
	public headerSectionId: any;
	public footerSectionId: any;
	public historyViewData: any;
	public historyHeader: any;
	public historyDetail: any;
	public historyData: any;
	public imageFileData: any;
	public modalImageName: any;
	public siteId:any;


	constructor(private _router: Router, public _elogpdb: ElogPouchdbProvider, private route: ActivatedRoute,
		private _homeService: HomeService, public _alertCtrl: AlertController, public loadingService: LoadingService) {
		this.loadSection = false;
		this.loadHeader = false;
		this.loadFooter = false;
		this.getParamValues();
	}

	private getParamValues() {
		this.route.params.subscribe((params) => {
			this.taskId = params['id'];
		});
		this.route.queryParams.subscribe((params) => {
			this.sectionFormId = Number(params['formsectionid']);
			this.partId = Number(params['partId']);
			this.logDataSectionID = Number(params['logDataSectionID']);
		});
	}

	// <span *ngIf="header.ControlType != 'DateTime' || header.ControlType != 'Date' || header.ControlType != 'Time' "> {{header.fieldlabel.replace('amp;','')}}</span>
	// <span *ngIf="header.ControlType == 'DateTime'"> {{header.fieldlabel | date: 'dd/MM/yyyy hh:mm a'}}</span>
	// <span *ngIf="header.ControlType == 'Date'"> {{header.fieldlabel | date: 'dd/MM/yyyy'}}</span>
	// <span *ngIf="header.ControlType == 'Time'"> {{header.fieldlabel | date: 'hh:mm a'}}</span>

	ionViewWillEnter() {
		this.loadingService.present('Loading');
		this.getCaptureDetail();
		this.getSectionHeaderFooter();
		this.loadingService.dismiss();
		this.getSiteDetails();
	}
	public goBack() {
		this._router.navigate([`/menu/home/elog/elogTask/${this.taskId}/detail`]);
	}

	public getCaptureDetail() {
		const id = `${this.taskId}-${this.sectionFormId}-${this.logDataSectionID}`;
		this._elogpdb.getElogTaskLogSectionbyID(id).subscribe(
			(data) => {
				const section = data;
				const section_id = `${section.taskId}-${section.FormSectionId}`;
				const section_id_detail = `${section.taskId}-${section.FormSectionId}-${section.LMlogDataSectionID}`;
				const logSectionDetail = [];
				// Table Header label
				this._elogpdb.getElogTaskHeaderSection(section_id).subscribe(
					(hData) => {
					//	console.log("hData ", hData)
						data.headers = [];
						data.displayData = [];
						let maxHeaderCount = 0;
						hData.LMWorkSheetFormField.forEach((head) => {
							if (head.EntityPartID == this.partId) {
								data.headers.push(head);
							}
						})
						this.sectionName = hData.SectionName;
						let headerMap = {};
						data.headers.forEach((header, index) => {
							if (header.EntityPartID == this.partId) {
								headerMap[header.fieldid] = index;  // arranging field id in an array
								maxHeaderCount = index;   // total no of header								
							}
						});
						if (hData.SectionRowCount) {
							for (let i = 0; i < hData.SectionRowCount; i++) {
								let headersArray = [];
								for (let j = 0; j <= maxHeaderCount; j++) {
									headersArray.push('')
								}
								data.displayData.push(headersArray);
							}
						}

						this._elogpdb.getElogTaskDetailSection(id).subscribe(
							(sectionData) => {
								
								if (sectionData.LMWorkSheetFormField.length > 0) {
									sectionData.LMWorkSheetFormField.forEach((col) => {
										let index = headerMap[col.fieldid];
										data.displayData[col.RowIndex][index] = col;
									});
									this.loadSection = true;
								}
							},
							(err) => {
								console.log('data', err);
							});
						logSectionDetail.push(data);
					},
					(err) => {
					}
				);
				console.log("logSectionDetail  ", logSectionDetail)
				this.taskDetailSection = logSectionDetail;
				if (this.taskDetailSection.length > 0) {
					localStorage.taskDetailSection = JSON.parse(this.taskDetailSection);
				}
			},
			(err) => {
				console.log('data', err);
			}
		);
		// For getting header and Footers

	}
	public getSectionHeaderFooter() {
		this.sectionHead = [];
		this.sectionFoot = [];
		this._elogpdb.getAllElogTaskHeaderDetail(this.taskId).subscribe(
			(sectionHF) => {
				let maxHeaderCountHead = 0;
				let maxHeaderCountFoot = 0;
				sectionHF.forEach((HFValue) => {
					if (HFValue.SectionName == "") {
						let headerMapHead = {};
						let headerMapFoot = {};
						HFValue.LMWorkSheetFormField.forEach((secHF, index) => {
							if (secHF.ContentType == 'Header') {
								secHF.Value = '';
								this.headerSectionId = HFValue.FormSectionId;
								this.headerFormSectionId = `${this.taskId}-${HFValue.FormSectionId}-${HFValue.LogDataSectionId}`;
								this.sectionHead.push(secHF);
							}
							else if (secHF.ContentType == 'Footer') {
								secHF.Value = '';
								this.footerSectionId = HFValue.FormSectionId;
								this.footerFormSectionId = `${this.taskId}-${HFValue.FormSectionId}-${HFValue.LogDataSectionId}`;
								this.sectionFoot.push(secHF);
							}
						})
					}
				})

				if (this.headerFormSectionId) {
					this._elogpdb.getElogTaskDetailSection(this.headerFormSectionId).subscribe(
						(sectionhead) => {
							if (sectionhead.ReferenceType === 'Header') {
								if (sectionhead.LMWorkSheetFormField.length > 0) {
									this.sectionHead.forEach((head) => {
										sectionhead.LMWorkSheetFormField.forEach((col) => {
											if (head.fieldid == col.fieldid) {
												head.Value = col.Value;
											}
										})
										this.loadHeader = true;
									});
								}
							}
						})
				}
				else {
				}
				if (this.footerFormSectionId) {
					this._elogpdb.getElogTaskDetailSection(this.footerFormSectionId).subscribe(
						(sectionfoot) => {
							if (sectionfoot.ReferenceType === 'Footer') {
								if (sectionfoot.LMWorkSheetFormField.length > 0) {
									this.sectionFoot.forEach((head) => {
										sectionfoot.LMWorkSheetFormField.forEach((col) => {
											if (head.fieldid == col.fieldid) {
												head.Value = col.Value;
											}
										})
										this.loadFooter = true;
									});
								}
							}
						})
				}
			})
	}

	editHeaderDetails(data) {
		const navigationExtras: NavigationExtras = {
			queryParams: {
				taskId: this.taskId,
				FormSectionId: this.headerSectionId,
				rowId: 0,
				logDataSectionID: 0,
				partId: 0,
				activity: 'add'
			}
		};
		this._router.navigate([`/menu/home/elog/elogTask/${this.taskId}/detail/cleaningSolution`], navigationExtras);
	}

	editFooterDetails(data) {
		const navigationExtras: NavigationExtras = {
			queryParams: {
				taskId: this.taskId,
				FormSectionId: this.footerSectionId,
				rowId: 0,
				logDataSectionID: 0,
				partId: 0,
				activity: 'add'
			}
		};		
		this._router.navigate([`/menu/home/elog/elogTask/${this.taskId}/detail/cleaningSolution`], navigationExtras);
	}


	editSection(rowData, index, FormSectionId, activity) {
		const navigationExtras: NavigationExtras = {
			queryParams: {
				taskId: this.taskId,
				FormSectionId: rowData.FormSectionId,
				rowId: index,
				logDataSectionID: rowData.LMlogDataSectionID,
				partId: this.partId,
				activity: activity
			}
		};
		for (let i = 0; i < rowData.length; i++) {
			if (rowData.displayData[i] == index) {
				for (let j = 0; j < rowData.displayData[i].length; j++) {
					if (rowData.displayData[i][j] == "") {
					}
					else {
						this._router.navigate([`/menu/home/elog/elogTask/${this.taskId}/detail/cleaningSolution`], navigationExtras);
					}
				}
			}
		}
		this._router.navigate([`/menu/home/elog/elogTask/${this.taskId}/detail/cleaningSolution`], navigationExtras);
	}

	viewHistory(rowData, index, FormSectionId) {
		this.historyViewData = [];
		this.historyHeader = [];
		this.historyDetail = [];
		const id = `${this.taskId}-${rowData.FormSectionId}-${rowData.LMlogDataSectionID}`;
		this._elogpdb.getElogHistory(id).subscribe(
			(historyData) => {
				historyData.LMWorkSheetFormField.forEach((history) => {
					if (history.RowNo == index) {
						this.historyViewData.push(history);
					}
				})
				for (var i in this.historyViewData[0]) {
					this.historyHeader.push(i);
				}
				this.historyDetail = [];
				for (let key of this.historyViewData) {
					this.historyData = [];
					for (var i in key) {
						this.historyData.push(key[i]);
					}
					this.historyDetail.push(this.historyData);
				}
				this.openModalHistory();				
			})
	}
	public openModalHistory() {
		jQuery('#viewHistoryModal').modal('show');
		}

		public closeHistoryModal() {
			jQuery('#viewHistoryModal').modal('hide');
			}

	public openImageModal(fileContent, name) {
		jQuery("body").addClass("modal-open");
		jQuery('#imageModal').modal();
		this.modalImageName = name;
		this.imageFileData = `data:image/png;base64,${fileContent.FileContent}`;
	}

	public closeImageModal() {
		jQuery("body").removeClass("modal-open");
		jQuery('#imageModal').modal('hide');
	}

	cleaningSolution() {
		// this._router.navigate([`/menu/home/elog/elogTask/${this.taskId}/detail/cleaningSolution`]);
	}

	public getSiteDetails() {
		const userName = localStorage.getItem('valgen_userName');
		// this.userName = {
		// 	userName
		// };
	//	const encryptValue = this._encrDecr.encryptURL(this.userName);
		this._homeService.getSiteDetails().subscribe((data) => {
			const siteList = [];
			const siteId = localStorage.getItem('valgen_siteId');
			this.siteId = localStorage.getItem('valgen_siteId');
			if (data.TotalCount !== 0) {
				data.Result.forEach((list) => {
					siteList.push(list);
				})
				this.siteList = siteList;
			} else {
				this.alertPopup('No Site List Found !');
			}
		}, (err) => {
			
		}
		);

		this._homeService.getRoleProfile().subscribe((data) => {
			if (data.TransactionId === 0) {
			}
		})
	}



	public checkAuthorization(siteId) {
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			if (data.TransactionId === 0) {
				this.loadingService.pushAllLocalDataToServer(siteId);
			} else {
				this.alertPopup(data.TransactionMessage);
				console.log(" My error")
			}
		}, (err) => {
		
		}
		);
	}

	async alertPopup(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
	}

}
