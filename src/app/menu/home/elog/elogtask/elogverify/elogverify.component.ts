import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ElogPouchdbProvider } from '../../../../../common/elog-pouchdb';
import { AlertController } from '@ionic/angular';
import async from 'async';
import * as momenttz from 'moment-timezone';
import * as moment from 'moment';
import { Toast } from '@ionic-native/toast/ngx';
import { ElogtaskComponent } from '../elogtask.component';
import { LoadingService } from '../../../../../loading.service';
import { ElogModeService } from '../../../../../common/elogmode.service';
import { HomeService } from '../../../home.service';
declare let jQuery: any;

@Component({
	selector: 'app-elogverify',
	templateUrl: './elogverify.component.html',
	styleUrls: ['./elogverify.component.scss'],
	providers: [ElogtaskComponent]
})
export class ElogverifyComponent implements OnInit {

	private taskId: string;
	public elogTaskDetail: any;
	public elogTaskLogSection: any;
	public siteList: any;
	public tableDetailsButton: boolean = true;
	public captureDataButton: boolean = false;
	public openSection: boolean = false;
	public siteId: any;
	public userId: any;
	public userName: any;
	public section: any;
	public loadDetails: any;
	public loadSection: any;
	public loadSectionData: any;
	public SecHeader: any;
	public SecDetail: any;
	public enableAccept:any;
	public imageFileData: any;
	public modalImageName: any;

	constructor(private _router: Router, public _elogpdb: ElogPouchdbProvider,
		private route: ActivatedRoute,  public _alertCtrl: AlertController, private toast: Toast, public elogtaskcom: ElogtaskComponent,
		public loadingService: LoadingService,  private _homeService: HomeService,public _elogModeService: ElogModeService) {
		this.getParamValues();
		this.siteId = localStorage.getItem('valgen_siteId');
		this.userId = localStorage.getItem('valgen_userId');
		this.userName = localStorage.getItem('valgen_userName');
		this.loadDetails = false;
		this.loadSection = false;
		this.loadSectionData = false;
		this.enableAccept = false;
		

	}

	ionViewWillEnter() {
	// this.checkSessionTimeOut();
	}

	private getParamValues() {
		this.route.params.subscribe((params) => {
			this.taskId = params['id'];
		});
	}

	ngOnInit() {
		this.getElogTaskDetail();   // above label part in html
		this.getElogTaskLogSection();  // section list part in html
		this.tableDetailsButton = true;
		this.captureDataButton = false;
	}

	private getElogTaskDetail() {
		this.elogTaskDetail = 	{
			"Code": "",
"Title": "",
"LogFormId": "",
"LogType": "",
"Entity": "",
		}
		this.loadingService.present('Loading.....');
		this._elogModeService.taskDetails(this.taskId).subscribe(
			(data) => {
				if (data.TransactionCode === 'SUCCESS') {
					this.elogTaskDetail = data.Result[0];
					this.loadDetails = true;
				}
			})
	}


	private getElogTaskLogSection() {

		this._elogModeService.taskLogSection(this.taskId).subscribe(
			(data) => {
				if (data.TransactionCode === 'SUCCESS') {
					this.elogTaskLogSection = data.Result;
					console.log("this.elogTaskLogSection   ", this.elogTaskLogSection)
					if(this.elogTaskLogSection[0].EnableAccept == 1){
						this.enableAccept = true;
					}
					this.loadSection = true;
					this.loadingService.dismiss();
				}
			})

	}

	public openImageModal(fileContent, name) {
		console.log("fileContent   ", fileContent)
		jQuery("body").addClass("modal-open");
		jQuery('#imageModal').modal();
		this.modalImageName = name;
		this.imageFileData = `data:image/png;base64,${fileContent.FileContent}`;
	}

	public closeImageModal() {
		jQuery("body").removeClass("modal-open");
		jQuery('#imageModal').modal('hide');
	}


	onCaptureSolution(formsectionid,LMlogDataSectionID, ind) {
		this.loadingService.present('Loading.....');
			for (var i = 0; i < this.elogTaskLogSection; i++) {
			this.elogTaskLogSection[i].open = false;
		}
		this.elogTaskLogSection.forEach((section, index) => {
			if (ind === index) {
				this.section = section;
				//this.section.headers.LMWorkSheetFormField = [];
				section.open = !section.open;
			} else {
				section.open = false;
			}
		});

		const logSectionDetail = [];
		let headerMap = {};
		this._elogModeService.taskLogSectionHeader(this.taskId).subscribe(
			(data) => {			
				const LogSectionHeader = data;
				if (data.length > 0) {
					this._elogModeService.taskLogSectionDetails(this.taskId).subscribe(
						(data) => {
							if (data.length > 0) {
								LogSectionHeader.forEach((sectionHeader) => {
									if (sectionHeader.LogDataSectionId == LMlogDataSectionID) {
										this.SecHeader = sectionHeader;
										this.section.displayData = [];
										let maxHeaderCount = 0;									
										this.section.headers = this.SecHeader;
										this.section.headers.LMWorkSheetFormField.forEach((header, index) => {											
											headerMap[header.fieldid] = index;
											maxHeaderCount = index;																				 
										});
										if (this.SecHeader.SectionRowCount) {
											for (let i = 0; i < this.SecHeader.SectionRowCount; i++) {
												let headersArray = [];
												for (let j = 0; j <= maxHeaderCount; j++) {
													headersArray.push('')
												}
												this.section.displayData.push(headersArray);
											}
										}
									}
								})

								data.forEach((sectionDetail) => {
									if (sectionDetail.FormSectionId == formsectionid) {
										this.SecDetail = sectionDetail;
										this.SecDetail.LMWorkSheetFormField.forEach((col) => {
											let index = headerMap[col.fieldid];
											if(col.fieldlabel == "S.No")
											{
												col.Value = parseInt(col.Value) + 1;
											}
											this.section.displayData[col.RowIndex][index] = col;
										});
									}
								})
								this.loadSectionData = true;
								logSectionDetail.push(data);
								this.loadingService.dismiss();
							}
							else {
								this.loadingService.dismiss();
							}
						})
				}
				else {
					this.loadingService.dismiss();
				}
			})

	}

	public verifyTask() {
		this.loadingService.present('Loading.....');
		const ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
		const ClientOffset = new Date().getTimezoneOffset();
		const uploadData =
			{
				LMLogFormExceutionDetails: [
					{
						LMEntityFormMappingID: this.taskId,
						Comments: "",
						ESigned: 0,
						SiteId: this.siteId,
						UserID: this.userId,
						TransferTask: 4,
						TransferTo: this.userId,
						ClientOffset: ClientOffset,
						ClientZone: ClientZone,
						UnitID: "LM054",
						VerifiedBy: localStorage.getItem('valgen_userElogName') + "  " + moment().format(localStorage.getItem('valgen_DataValue')) + " (" + (momenttz.tz(momenttz.tz.guess()).zoneAbbr()) + ")",
					}

				]
			}

		this._elogModeService.FormVerification(uploadData).subscribe(
			(data) => {
				if (data.TransactionId == 0) {
					async.series([
						(done) => {
							this._elogpdb.deleteLocalData(this.taskId, done);
						},
						(done) => {
							this._elogpdb.deleteElogTask(this.taskId, done);
						}
					], () => {
						this.elogtaskcom.pullAllFromServer();
						setTimeout(() => {
							this.loadingService.dismiss();
							this._router.navigate([`/menu/home/elog/elogTask/0`]);
						}, 700)

						this.toast.show("Verified Successfully", '5000', 'bottom').subscribe(
							toast => {
							}
						);
					});
				}
				else {
					this.elogtaskcom.pullAllFromServer();
					this.loadingService.dismiss();
					this._router.navigate([`/menu/home/elog/elogTask/0`]);
					this.toast.show("Verify Error", '5000', 'bottom').subscribe(
						toast => {
							
						}
					);

				}
			}, (err) => {
				console.log(err)
			}
		)
	}

	async verfyRejectComments() {
		const alert = await this._alertCtrl.create({
			header: 'Comments',
			backdropDismiss: false,
			inputs: [
				{
					name: 'Comments',
					type: 'text',
					placeholder: 'Enter Comments'
				}
			],
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel'
				},
				{
					text: 'Save',
					handler: (data) => {
						this.rejectverifyTask(data);

					}
				}				
			]
		});
		await alert.present();
	}



	public rejectverifyTask(data) {
		this.loadingService.present('Loading.....');
		const ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
		const ClientOffset = new Date().getTimezoneOffset();
		const uploadData =
			{
				LMLogFormExceutionDetails: [
					{
						LMEntityFormMappingID: this.taskId,
						Comments: data.Comments,
						ESigned: 0,
						SiteId: this.siteId,
						UserID: this.userId,
						TransferTask: 5,
						TransferTo: this.userId,
						ClientOffset: ClientOffset,
						ClientZone: ClientZone,
						UnitID: "LM054",
						VerifiedBy: localStorage.getItem('valgen_userElogName') + "  " + moment().format(localStorage.getItem('valgen_DataValue')) + " (" + (momenttz.tz(momenttz.tz.guess()).zoneAbbr()) + ")",
					}
				]
			}

		this._elogModeService.FormVerification(uploadData).subscribe(
			(data) => {
				if (data.TransactionId == 0) {
					async.series([
						(done) => {
							this._elogpdb.deleteLocalData(this.taskId, done);
						},
						(done) => {
							this._elogpdb.deleteElogTask(this.taskId, done);
						}
					], () => {
						this.elogtaskcom.pullAllFromServer();
						this.loadingService.dismiss();
						this._router.navigate([`/menu/home/elog/elogTask/0`]);
						this.toast.show("Verified Successfully", '5000', 'bottom').subscribe(
							toast => {
							}
						);

					});
				}
				else {
					this.elogtaskcom.pullAllFromServer();
					this.loadingService.dismiss();
					this._router.navigate([`/menu/home/elog/elogTask/0`]);
					this.toast.show("Verified Error", '5000', 'bottom').subscribe(
						toast => {
							
						}
					);
				}
			}, (err) => {
				console.log(err)
			}
		)
	}

	public checkAuthorization(siteId) {
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			if (data.TransactionId === 0) {
				this.loadingService.pushAllLocalDataToServer(siteId);
			} else {
				this.alertPopup(data.TransactionMessage);
			}
		}, (err) => {
		}
		);
	}

	async alertPopup(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: ['OK']
		});
		await alert.present();
	}

	public acceptTask(){
		this.loadingService.present('Loading.....');
		const ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
		const ClientOffset = new Date().getTimezoneOffset();
		const uploadData =
		{
			LMLogFormExceutionDetails: [
			{
				UnitId : "LM054",
				LMEntityFormMappingID :parseInt(this.taskId),
				UserID : parseInt(this.userId),
				UserType : 16,
				ClientOffset : ClientOffset,
				ClientZone : ClientZone,
		
			}
			]
		}
		
		this._elogModeService.FormAccept(uploadData).subscribe(
			(data) => {
				
				if(data.TransactionCode === 'SUCCESS'){

					this.enableAccept = false;
				}
				else{
					this.toast.show("Verified Error", '5000', 'bottom').subscribe(
						toast => {
							
						}
					);

				}
			})
	}
}






