import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ElogPouchdbProvider } from '../../../../../common/elog-pouchdb';
import { LocalPouchdbProvider } from '../../../../../common/local-pouchdb';
import { AlertController } from '@ionic/angular';
import async from 'async';
import * as momenttz from 'moment-timezone';
import { Toast } from '@ionic-native/toast/ngx';
import {ElogtaskComponent} from '../elogtask.component';
import { LoadingService } from '../../../../../loading.service';
import { ElogModeService } from '../../../../../common/elogmode.service';
import { HomeService } from '../../../home.service';
declare let jQuery: any;

@Component({
  selector: 'app-elogapprove',
  templateUrl: './elogapprove.component.html',
  styleUrls: ['./elogapprove.component.scss'],
  providers: [ElogtaskComponent]
})
export class ElogapproveComponent implements OnInit {

 
  private taskId: string;
  public elogTaskDetail: any;
  public elogTaskLogSection: any;
  public siteList: any;
  public tableDetailsButton: boolean = true;
  public captureDataButton: boolean = false;
  public openSection:boolean = false;
  public siteId:any;
  public userId:any;
  public userName:any;
	public section:any;
	public loadDetails: any;
	public loadSection: any;
	public loadSectionData: any;
	public SecHeader: any;
	public SecDetail: any;
	public enableAccept:any;
	public entityId:any;
	public imageFileData: any;
	public modalImageName: any;
	

  constructor( private _router: Router, public _elogpdb: ElogPouchdbProvider, 
  private route: ActivatedRoute, public _alertCtrl: AlertController, 
	private _localpdb: LocalPouchdbProvider,private toast: Toast, public elogtaskcom: ElogtaskComponent,
	public loadingService: LoadingService,private _homeService: HomeService, public _elogModeService: ElogModeService) {
      this.getParamValues();
      this.siteId = localStorage.getItem('valgen_siteId');
      this.userId = localStorage.getItem('valgen_userId');
			this.userName = localStorage.getItem('valgen_userName');
			this.loadDetails = false;
			this.loadSection = false;
			this.loadSectionData = false;
			this.enableAccept = false;
  }

ionViewWillEnter() {
     // this.checkSessionTimeOut();
}

  private getParamValues() {
  this.route.params.subscribe((params) => {
    console.log('Params', params)
    this.taskId = params['id'];
  });
  this.route.queryParams.subscribe((params) => {
    console.log('Params', params)
    this.entityId = params['EntityId'];
  });
  console.log('taskId', this.taskId);
  }
  
  ngOnInit() {
      this.getElogTaskDetail();   // above label part in html
  this.getElogTaskLogSection();  // section list part in html
  this.tableDetailsButton = true;
    this.captureDataButton = false;
  }
  
  // private getElogTaskDetail() {
  // this._elogpdb.getElogTaskDetail(this.taskId).subscribe(
  //   (data) => {
  //             console.log(data, 'getElogTaskDetail');
  //             this.elogTaskDetail = data;
  //   },
  //   (err) => {
  //     console.log('data', err);
  //   }
  // );
  // }


  // private getElogTaskLogSection() {
	// 	this._elogpdb.getElogTaskLogSection(this.taskId).subscribe(
	// 		(data) => {
  //               console.log(data, 'getElogTaskLogSection');
  //               this.elogTaskLogSection = data;
	// 		},
	// 		(err) => {
	// 			console.log('data', err);
	// 		}
	// 	);
  // }


  // onCaptureSolution (formsectionid, partId, ind) {
	// //	this._router.navigate([`/menu/home/elog/elogTask/${this.taskId}/detail/captureSolution`], { queryParams: { formsectionid: formsectionid , partId:partId} });
	//    for(var i=0;i<this.elogTaskLogSection;i++){
	// 	   this.elogTaskLogSection[i].open = false;
	//    }
	   
	//    console.log("this.elogTaskLogSection  ", this.elogTaskLogSection);
	// 	this.elogTaskLogSection.forEach((section, index) => {
	// 		if (ind === index) {
  //               this.section = section;
	// 			section.open = !section.open;
	// 		} else {
	// 			section.open = false;
	// 		}
	// 	});
	
	
	//    let data = this.section;
	//    let sectionId = `${this.taskId}-${formsectionid}`;
	// 	console.log("Section Id", sectionId);
	//    const logSectionDetail = [];
	// 					this._elogpdb.getElogTaskHeaderSection(sectionId).subscribe(
	// 						(hData) => {
	// 							console.log(hData);
	// 							data.displayData = [];
	// 							let maxHeaderCount = 0;
	// 							data.headers = hData;
	// 							let headerMap = {};
	// 							data.headers.LMWorkSheetFormField.forEach((header, index) => {
	// 								headerMap[header.fieldid] = index;
	// 								maxHeaderCount = index;
	// 							});
	// 							if (hData.SectionRowCount) {
	// 								for (let i = 0; i < hData.SectionRowCount; i++) {
	// 									let headersArray = [];
	// 									for (let j = 0; j <= maxHeaderCount; j++) {
	// 										headersArray.push('')
	// 									}
	// 									data.displayData.push(headersArray);
	// 								}
	// 							}
								
	// 							this._elogpdb.getElogTaskDetailSection(this.section._id).subscribe(
	// 							(sectionData) => {
	// 								console.log(sectionData);
	// 								sectionData.LMWorkSheetFormField.forEach((col) => {
	// 									let index = headerMap[col.fieldid];
	// 									data.displayData[col.RowIndex][index] = col;
	// 								});
	// 								console.log('adsasdsdsdadasdas  ', data.displayData);
	// 							},
	// 							(err) => {
	// 								console.log('data', err);
	// 							});
								
	// 							logSectionDetail.push(data);
	// 						},
	// 						(err) => {
	// 							console.log('data', err);
	// 						}
	// 					);
			
  //     //  this._router.navigate([`/menu/home/elog/elogTask/${this.taskId}/detail/captureSolution`]);
	// }

	public openImageModal(fileContent, name) {
		console.log("fileContent   ", fileContent)
		jQuery("body").addClass("modal-open");
		jQuery('#imageModal').modal();
		this.modalImageName = name;
		this.imageFileData = `data:image/png;base64,${fileContent.FileContent}`;
	}

	public closeImageModal() {
		jQuery("body").removeClass("modal-open");
		jQuery('#imageModal').modal('hide');
	}
	
	private getElogTaskDetail() {
		// this._elogpdb.getElogTaskDetail(this.taskId).subscribe(
		//   (data) => {
		//             console.log(data, 'getElogTaskDetail');
		//             this.elogTaskDetail = data;
		//   },
		//   (err) => {
		//     console.log('data', err);
		//   }
		// );

		this.loadingService.present('Loading.....');
		this._elogModeService.taskDetails(this.taskId).subscribe(
			(data) => {
				console.log("_elogModeService  ", data)
				if (data.TransactionCode === 'SUCCESS') {
					this.elogTaskDetail = data.Result[0];
					this.loadDetails = true;
					console.log("this.elogTaskDetail ", this.elogTaskDetail)

				}
			})
	}


	private getElogTaskLogSection() {

		this._elogModeService.taskLogSection(this.taskId).subscribe(
			(data) => {
				console.log(data, "5555555")
				if (data.TransactionCode === 'SUCCESS') {
					this.elogTaskLogSection = data.Result;
					if(this.elogTaskLogSection[0].EnableAccept == 1){
						this.enableAccept = true;
					}
					this.loadSection = true;
					this.loadingService.dismiss();
				}
				else {
					console.log(data.TransactionMessage);
				}
			})

	}


	onCaptureSolution(formsectionid, partId, ind) {

		this.loadingService.present('Loading.....');
		//	this._router.navigate([`/menu/home/elog/elogTask/${this.taskId}/detail/captureSolution`], { queryParams: { formsectionid: formsectionid , partId:partId} });
		for (var i = 0; i < this.elogTaskLogSection; i++) {
			this.elogTaskLogSection[i].open = false;
		}

		console.log("this.elogTaskLogSection  ", this.elogTaskLogSection);
		this.elogTaskLogSection.forEach((section, index) => {
			if (ind === index) {
				this.section = section;
				section.open = !section.open;
			} else {
				section.open = false;
			}
		});



		const logSectionDetail = [];
		let headerMap = {};



		this._elogModeService.taskLogSectionHeader(this.taskId).subscribe(
			(data) => {

				const LogSectionHeader = data;
				console.log("data  ", data)
				if (data.length > 0) {
					this._elogModeService.taskLogSectionDetails(this.taskId).subscribe(
						(data) => {
							if (data.length > 0) {

								LogSectionHeader.forEach((sectionHeader) => {
									if (sectionHeader.FormSectionId == formsectionid) {
										this.SecHeader = sectionHeader;

										this.section.displayData = [];
										let maxHeaderCount = 0;
										this.section.headers = this.SecHeader;

										this.section.headers.LMWorkSheetFormField.forEach((header, index) => {
											headerMap[header.fieldid] = index;
											maxHeaderCount = index;
										});
										if (this.SecHeader.SectionRowCount) {
											for (let i = 0; i < this.SecHeader.SectionRowCount; i++) {
												let headersArray = [];
												for (let j = 0; j <= maxHeaderCount; j++) {
													headersArray.push('')
												}
												this.section.displayData.push(headersArray);
											}
										}
									}
								})

								data.forEach((sectionDetail) => {
									if (sectionDetail.FormSectionId == formsectionid) {
										this.SecDetail = sectionDetail;
										this.SecDetail.LMWorkSheetFormField.forEach((col) => {
											let index = headerMap[col.fieldid];
											this.section.displayData[col.RowIndex][index] = col;
											this.loadingService.dismiss();
										});
									}
								})
								this.loadSectionData = true;
								logSectionDetail.push(data);
								
							}
							else {
								console.log(data.TransactionMessage)
								this.loadingService.dismiss();
							}
						})


				}
				else {

					console.log(data.TransactionMessage)
					this.loadingService.dismiss();
				}
			})

	}
  
public verifyTask(){
 // console.log(uploadData, 'pppppppppppppppppppppppppp')
 const ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
		const ClientOffset = new Date().getTimezoneOffset();
const uploadData =
 {
  
   
	LMLogFormExceutionDetails: [
		{
						UnitId : "LM054",
						LMFormDetailId : parseInt(this.taskId),
						UserType : 5,
						Comments : 0,
						CreatedBy : this.userId,
						ESigned : 0,
						ClientOffset : ClientOffset,
						ClientZone : ClientZone,
						SiteId : this.siteId,
						TransferTask : 13
	
		}
		]
  
}




  this._elogModeService.FormApprove(uploadData).subscribe(
    (data) => {
      console.log(data)

      if(data.TransactionId == 0){
		async.series([
			// (done) => {
			// 	this._elogpdb.deleteLocalData(this.taskId, done);
			// },
			// (done) => {
			// 	this._elogpdb.updateElogTaskById(this.taskId, this.entityId,this.siteId,'statusPull').subscribe(
			// 		(ldata) => {	
			// 			done();
			// 		},
			// 		(err) => {
			// 			done();
			// 		}
			// 	);
			// },
			(done) => {
				this._elogpdb.deleteElogTask(this.taskId, done);
				
				
			}
		], () => {
		
			this.elogtaskcom.pullAllFromServer();
			// setTimeout(() => {
				this._router.navigate([`/menu/home/elog/elogTask/0`]);
			//   },7000)
			
			this.toast.show("Approved Successfully", '5000', 'bottom').subscribe(
			  toast => {
		
			  }
			  );

		});
       
       
	  }
	  else{
		this.elogtaskcom.pullAllFromServer();

        this._router.navigate([`/menu/home/elog/elogTask/0`]);
        this.toast.show("Verified Error", '5000', 'bottom').subscribe(
          toast => {
           
          //  this._router.navigate([`/menu/home/elog/elogTask/0`]);
          }
          );

	  }
    }, (err) => {
      console.log(err)
    }
  )
}
async verfyRejectComments() {
	const alert = await this._alertCtrl.create({
		header: 'Comments',
		backdropDismiss: false,
		inputs: [
			{
				name: 'Comments',
				type: 'text',
				placeholder: 'Enter Comments'
			}
		],
		buttons: [
			{
				text: 'Cancel',
				role: 'cancel'
			},
			{
				text: 'Save',
				handler: (data) => {
					this.rejectverifyTask(data);

				}
			}
			
		]
	});
	await alert.present();
}


public rejectverifyTask(data){

	const ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
		const ClientOffset = new Date().getTimezoneOffset();
const uploadData =
 {
 
LMLogFormExceutionDetails: [
	{
		  UnitId : "LM054",
		  LMFormDetailId : parseInt(this.taskId),
		  UserType : 5,
		  Comments : data.Comments,
		  CreatedBy : 168,
		  ESigned : 0,
		  ClientOffset : ClientOffset,
		  ClientZone : ClientZone,
		  SiteId : this.siteId,
		  TransferTask : 12

	}
	]
  
}



  this._elogModeService.FormApproveReject(uploadData).subscribe(
    (data) => {
     

      if(data.TransactionId == 0){
          async.series([
			// (done) => {
			// 	this._elogpdb.deleteLocalData(this.taskId, done);
			// },
			// (done) => {
			// 	this._elogpdb.updateElogTaskById(this.taskId,this.entityId, this.siteId,'statusPull').subscribe(
			// 		(ldata) => {	
			// 			done();
			// 		},
			// 		(err) => {
			// 			done();
			// 		}
			// 	);
			// },
			(done) => {
				this._elogpdb.deleteElogTask(this.taskId, done);
				
				
			}
		], () => {
			
			this.elogtaskcom.pullAllFromServer();
			this._router.navigate([`/menu/home/elog/elogTask/0`]);
			this.toast.show("Verified Successfully", '5000', 'bottom').subscribe(
			  toast => {
		
			  }
			  );

		});
 
	  }
	   else
	   {
		this.elogtaskcom.pullAllFromServer();
        this._router.navigate([`/menu/home/elog/elogTask/0`]);
        this.toast.show("Verified Error", '5000', 'bottom').subscribe(
          toast => {
            console.log(toast);
          //  this._router.navigate([`/menu/home/elog/elogTask/0`]);
          }
          );
	   }

    }, (err) => {
      console.log(err)
    }
  )

}

public checkAuthorization(siteId) {

	console.log("Home site called....")
	this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
		if (data.TransactionId === 0) {
			this.loadingService.pushAllLocalDataToServer(siteId);
		} else {
			this.alertPopup(data.TransactionMessage);
		}
	}, (err) => {
		console.log(err, 'getSiteAuthorization')
	}
	);
}

async alertPopup(message) {
	const alert = await this._alertCtrl.create({
		header: 'Alert',
		message: message,
		backdropDismiss: false,
		buttons: ['OK']
	});
	await alert.present();
}

public acceptTask(){
	this.loadingService.present('Loading.....');
	const ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
	const ClientOffset = new Date().getTimezoneOffset();
	const uploadData =
	{
		LMLogFormExceutionDetails: [
		{
			UnitId : "LM054",
			LMEntityFormMappingID :parseInt(this.taskId),
			UserID : parseInt(this.userId),
			UserType : 5,
			ClientOffset : ClientOffset,
			ClientZone : ClientZone,
	
		}
		]
	}
	

	this._elogModeService.FormAccept(uploadData).subscribe(
		(data) => {
			console.log(data);
			if(data.TransactionCode === 'SUCCESS'){

				this.enableAccept = false;
			}
			else{
				this.toast.show("Verified Error", '5000', 'bottom').subscribe(
					toast => {
						
					}
				);

			}
			

		})
}


}
