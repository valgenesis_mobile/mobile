import { Injectable } from '@angular/core';
import { HttpService } from '../../../../common/http.service';
import { EncrDecrService } from '../../../../common/encr-decr.service';

@Injectable()
export class ElogTaskService {
	constructor(private _httpService: HttpService,  private _encrDecr: EncrDecrService) {	
	}

	public getElogTaskList(userId, siteId) {

		const data = {
			UserID : userId,
			SiteId : siteId

		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Task/GetUsertask`,
			body : encryptValue
		};
		return this._httpService.post(info);
	}

}
