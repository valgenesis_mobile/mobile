import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController, PopoverController, AlertController } from '@ionic/angular';
import { Network } from '@ionic-native/network/ngx';
import * as moment from 'moment';
import { ElogPouchdbProvider } from '../../../../common/elog-pouchdb';
import { EncrDecrService } from '../../../../common/encr-decr.service';
import async from 'async';
import { HomeService } from '../../home.service';
import { ElogModeService } from '../../../../common/elogmode.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { LoadingService } from '../../../../loading.service';
import { ElogFilterComponent } from '../elog-filter/elog-filter.component';
import { formatDate } from '@angular/common';
import 'rxjs/add/operator/map';
import { OrderPipe } from 'ngx-order-pipe';
import { IonSearchbar } from '@ionic/angular';


declare let jQuery: any;

@Component({
	templateUrl: 'elogtask.component.html',
	styleUrls: ['elogtask.component.scss']
})
export class ElogtaskComponent implements OnInit {

	public elogTaskList: any = [];
	public tempelogTaskList: any = [];
	public loading: any;
	public searchTerm = '';
	public siteList: any;
	public allData = [];
	public logBookButton: boolean = false;
	public taskListButton: boolean = true;
	public logFormId: any;
	public sectionList: any;
	public selectSectionId = [];
	public userName: any;
	public userElogName: any;
	public applyButtonEnable: boolean = false;
	public verifyFormList: any;
	public verifyFormArray = [];
	public elogTaskListLength: any;
	public noofNewTask: any;
	public enableSearchbar: boolean = false;
	public qrCodeId: any;
	public SectionLocked: boolean = false;
	public elogTaskLogSection: any = [];
	public tempsectionList: any = [];
	public siteId: any;
	public count: any;
	public disableMoreData: boolean;
	public entityID: any;
	public filterArray: any;
	public tempExecutionList: any;
	public appPlatform: any;
	public userId:any;

	constructor(private _router: Router, private route: ActivatedRoute,
		public _loadingCtrl: LoadingController, public _alertCtrl: AlertController,
		public _elogpdb: ElogPouchdbProvider, private _network: Network, private _homeService: HomeService,
		 private orderPipe: OrderPipe, private _encrDecr: EncrDecrService,
		public popoverCtrl: PopoverController, public _elogModeService: ElogModeService, private barcode: BarcodeScanner
		, public loadingService: LoadingService) {
		this.userName = localStorage.getItem('valgen_userName');
		this.userElogName = localStorage.getItem('valgen_userElogName');
		this.appPlatform = localStorage.valgen_myplatform;
		this.count = 15;
		this.disableMoreData = false;
		this.getParamValues();
	}

	ngOnInit() {
		jQuery(document).ready(function () {
			jQuery('[data-toggle="tooltip"]').tooltip();
		});
	}
	@ViewChild(IonSearchbar) myInput: IonSearchbar;
	ionViewWillEnter() {
		this.siteId = localStorage.getItem('valgen_siteId');
		this.userId = localStorage.getItem('valgen_userId');
		this.checkLocalElogTaskList();
		this.getSiteDetails();
		this.taskListButton = true;
		this.logBookButton = false;
		this.enableSearchbar = false;
	}
	
	private getParamValues() {
		this.route.params.subscribe((params) => {			
			this.qrCodeId = params['id'];
		});		
	}

	setFilteredItems() {
		this.elogTaskList = this.allData.filter((location) => {
			return (location.doc.FormName.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1) || (location.doc.LogFormId.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1);
		});

		if (this.elogTaskList.length <= 0) {
			this.elogTaskListLength = true;
		} else {
			this.elogTaskListLength = false;
		}
		this.noofNewTask = 0;
		this.elogTaskList.forEach((exe) => {
			if (exe.doc.IsNew == 1) {
				this.noofNewTask = this.noofNewTask + 1;
			}
		})
	}

	public toolView() {
		jQuery('[data-toggle="tooltip"]').tooltip();
	}

	closeVerifyModal() {

	}


	public getSearchbar() {
		this.searchTerm = '';
		this.enableSearchbar = !this.enableSearchbar;
	
		setTimeout(() => { this.myInput.setFocus(); 
			}, 500);
	}
	public doRefresh(e) {
		this.pullAllFromServer();

		setTimeout(() => {
			e.target.complete();

		}, 500);
	}
	logScrolling(eve) {
		if (eve.detail.scrollTop > 70) {
			document.getElementById('searchBar').style.position = 'fixed';
			document.getElementById('searchBar').style.zIndex = '99999';
			document.getElementById('searchBar').style.backgroundColor = '#ccc';
			document.getElementById('searchBar').style.top = '0px';
			document.getElementById('searchBar').style.marginTop = '60px';
		} else {
			document.getElementById('searchBar').style.position = 'unset';
			document.getElementById('searchBar').style.zIndex = 'unset';
			document.getElementById('searchBar').style.backgroundColor = 'unset';
			document.getElementById('searchBar').style.top = 'unset';
			document.getElementById('searchBar').style.marginTop = 'unset';
		}
	}

	public goHome() {
		this._router.navigate(['/menu/home']);
	}



	private checkLocalElogTaskList() {
		if (this._network.type === 'none' || navigator.onLine === false) {
			this.alertController('Check Internet Connection!');
			this.getLocalElogTaskList();
		} else {

			const disconnectSubscription = this._network.onDisconnect().subscribe(() => {
				this.alertController('Check Internet Connection!');
			});
			this.pullAllFromServer();
		}
	}


	public pullAllFromServer() {
		this.loadingService.showLoading10("Loading");
		if (this._network.type === 'none' || navigator.onLine === false) {
			this.alertController('Check Internet Connection!');
		} else {

			const disconnectSubscription = this._network.onDisconnect().subscribe(() => {
				this.alertController('Check Internet Connection!');
			});
			const userId = localStorage.getItem('valgen_userId');
			const siteId = localStorage.getItem('valgen_siteId');
			this._elogModeService.getElogTaskList(userId, siteId).subscribe(
				(data) => {
					if (data.TotalCount != 0) {
						const elogTaskList = [];
						data.Result.forEach((list) => {
							if (moment().format('MM DD YYYY') == moment(list.TargetDate).format('MM DD YYYY')) {
								list.checkDateStatus = 'Today';
								elogTaskList.push(list);
							} else {
								const date = moment(list.TargetDate).format('DD MMMM')
								list.checkDateStatus = date;
								elogTaskList.push(list);
							}
						});

						this._elogpdb.addElogTaskList(elogTaskList).subscribe(
							(ldata) => {								
								this.loadingService.dismiss();
								this.getLocalElogTaskList();								
								disconnectSubscription.unsubscribe();
							},
							(err) => {								
								disconnectSubscription.unsubscribe();
								this.loadingService.dismiss();
							}
						);
					} else {
						disconnectSubscription.unsubscribe();
						this.loadingService.dismiss();
						this.elogTaskListLength = true;
						//this.alertController(data.TransactionMessage);
						console.log(" My error")
					}
				},
				(error) => {
					this.loadingService.dismiss();
				//	this.alertController(error);
					console.log(" My error")
				}
			);
		}
	}

	public qrCodeScan() {
		this.barcode.scan().then(barcodeData => {
			const qrcodeId = (barcodeData.text).substring(0, 3);
			this.qrCodeId = qrcodeId;
			this.elogTaskList.forEach((tasklist) => {
				if (this.qrCodeId === tasklist.doc.EntityId) {					
					this.tempelogTaskList.push(tasklist);				
				}
			})		
			this.elogTaskList = this.tempelogTaskList;
			if (this.elogTaskList.length == 1) {
				this.sectionListPopup(this.elogTaskList[0].doc.ID, this.elogTaskList[0].doc);
			}
		}).catch(err => {
			console.log('Error', err);
		});

	}

	private  getLocalElogTaskList() {
		this.tempelogTaskList = [];
		this.elogTaskList = [];
		this.loadingService.showLoading10("Loading");
		this._elogpdb.getAllElogTaskList().subscribe(
			(data) => {
				this.allData = data;
				this.allData.forEach((list) => {
					if (list.doc.SiteId == this.siteId && list.doc.UserID == this.userId) {
						this.elogTaskList.push(list);
					}
				})
				this.loadingService.dismiss();
				if (this.elogTaskList.length <= 0) {
					this.elogTaskListLength = true;
				} else {
					this.elogTaskListLength = false;
				}
				this.noofNewTask = 0;
				this.elogTaskList.forEach((exe) => {
					if (exe.doc.IsNew == 1) {
						this.noofNewTask = this.noofNewTask + 1;
					}
				})
				if (this.qrCodeId > 0) {
					this.elogTaskList.forEach((qrvalue) => {
						if (this.qrCodeId === qrvalue.doc.EntityId) {
							this.tempelogTaskList.push(qrvalue);
						}
					})			
					this.elogTaskList = this.tempelogTaskList;
					if (this.elogTaskList.length <= 0) {
						this.elogTaskListLength = true;
					} else {
						this.elogTaskListLength = false;
					}
					this.noofNewTask = 0;
					this.elogTaskList.forEach((exe) => {
						if (exe.doc.IsNew == 1) {
							this.noofNewTask = this.noofNewTask + 1;
						}
					})					
					if (this.elogTaskList.length == 1) {
						this.sectionListPopup(this.elogTaskList[0].doc.ID, this.elogTaskList[0].doc);
					}
				}
			},
			(err) => {
				console.log('data', err);
			}
		);
	}
	public sectionListPopup(id, list) {
		this._elogModeService.SectionDependency(id,list.FormID).subscribe(
			(data) => {
              console.log("Dependecy    ", data)
			})
		this.loadingService.present("Loading");
		this.tempsectionList = [];
		this.logFormId = id;
		this.entityID = list.EntityId;
		if (list.Activity === "Verify Log Data" || list.Activity === "Approve Log Data") {
			if (this._network.type === 'none' || navigator.onLine === false) {
				this.loadingService.dismiss();
				this.alertController('Check Internet Connection!');
			} else {
				const disconnectSubscription = this._network.onDisconnect().subscribe(() => {
					this.alertController('Check Internet Connection!');
				});
				this.pullVerifyFormFromServer(this.logFormId);
				this.loadingService.dismiss();
			}
		}

		else if (list.Activity === "Capture Log Data" || list.Activity === "Reinitiate Log Data") {
			this.applyButtonEnable = false;
			this.logFormId = id;
			if (this._network.type === 'none' || navigator.onLine === false) {
				this.loadingService.dismiss();
				this.alertController('Check Internet Connection!');
			} else {
				const disconnectSubscription = this._network.onDisconnect().subscribe(() => {
					this.loadingService.dismiss();
					this.alertController('Check Internet Connection!');
				});
				this._elogModeService.taskLogSection(id).subscribe(
					(data) => {
						this.sectionList = data.Result;
						if (this.sectionList.length > 0) {
							this.sectionList.forEach((seclist) => {
								if (seclist.LockedStatus === 'UnLocked') {
									this.SectionLocked = true;
								}

								if (seclist.LockedStatus === 'Locked' && seclist.LockedBy === this.userElogName) {
									this.applyButtonEnable = true;
									this.tempsectionList.push(seclist);
								}
							})
							if (this.SectionLocked) {
								this.SectionLocked = false;
								this.loadingService.dismiss();
								this.openModalSectionList();
								disconnectSubscription.unsubscribe();
							}
							else {
								if (this.tempsectionList.length == 1) {  // If only one section and that has locked by logged in user, the captureForm will be called directly
									this.qrCodeId = 0;
									this.pullFromServer();
									this._elogpdb.getElogTaskLogSection(this.logFormId).subscribe(
										(data) => {
											setTimeout(() => {
												this.loadingService.dismiss();
												this._router.navigate([`/menu/home/elog/elogTask/${this.logFormId}/detail/captureSolution`], { queryParams: { formsectionid: data[0].FormSectionId, partId: data[0].LmEntityPartID, logDataSectionID: data[0].LMlogDataSectionID } });
												this.tempsectionList = [];
											}, 500);
										}
									);

								}
								else if (this.tempsectionList.length == 0) {  // if all the section has been locked by another user, the popup will display the list of section and the aplly button will be disabled
									this.loadingService.dismiss();
									this.openModalSectionList();
									this.tempsectionList = [];
								}
								else {  // if there are two or more section which has locked by logged in user, it will go to the detail page with sections downloaded
									this.qrCodeId = 0;
									this.pullFromServer();
									this.loadingService.dismiss();
									setTimeout(() => {
										this._router.navigate([`/menu/home/elog/elogTask/${this.logFormId}/detail`]);
										this.tempsectionList = [];
									}, 1000)
								}
							}
						}
					})
			}
		}



	}


	public elogTaskDetail(taskId, syncState, list) {
		if (list.Activity == 'Capture Log Data' || list.Activity == "Reinitiate Log Data") {
			if (syncState) {
				this.qrCodeId = 0;
				this._router.navigate([`/menu/home/elog/elogTask/${taskId}/detail`], { queryParams: { EntityId: list.EntityId } });
			}
			else {
				this.alertController('Data not Downloaded!');
			}
		}
		else if (list.Activity == "Verify Log Data" && list.GroupAlert == 0) {
			if (this._network.type === 'none' || navigator.onLine === false) {
				this.alertController('Check Internet Connection!');
			} else {
				const disconnectSubscription = this._network.onDisconnect().subscribe(() => {
					this.alertController('Check Internet Connection!');
				});
				this._router.navigate([`/menu/home/elog/elogTask/elogverify/${taskId}`]);
			}
		}
		else if (list.Activity == "Verify Log Data" && list.GroupAlert == 1) {
			if (this._network.type === 'none' || navigator.onLine === false) {
				this.alertController('Check Internet Connection!');
			} else {
				const disconnectSubscription = this._network.onDisconnect().subscribe(() => {
					this.alertController('Check Internet Connection!');
				});
				this._router.navigate([`/menu/home/elog/elogTask/elogformlist/${taskId}/0`]);
			}
		}
		else if (list.Activity == "Approve Log Data" && list.GroupAlert == 0) {
			if (this._network.type === 'none' || navigator.onLine === false) {
				this.alertController('Check Internet Connection!');
			} else {
				const disconnectSubscription = this._network.onDisconnect().subscribe(() => {
					this.alertController('Check Internet Connection!');
				});
				this._router.navigate([`/menu/home/elog/elogTask/elogapprove/${taskId}`], { queryParams: { EntityId: list.EntityId } });
			}
		}
		else if (list.Activity == "Approve Log Data" && list.GroupAlert == 1) {
			if (this._network.type === 'none' || navigator.onLine === false) {
				this.alertController('Check Internet Connection!');
			} else {
				const disconnectSubscription = this._network.onDisconnect().subscribe(() => {
					this.alertController('Check Internet Connection!');
				});
				this._router.navigate([`/menu/home/elog/elogTask/elogformlist/${taskId}/1`]);
			}
		}
	}

	public lockSection(sectionId, partid) {
		this.applyButtonEnable = false;
		if (this.selectSectionId.indexOf(sectionId) == -1) {
			this.selectSectionId.push(sectionId);
		}
		console.log("this.logFormId   ", this.logFormId)
		console.log("sectionId  ", sectionId)
		console.log("partid   ", partid)
		this._elogModeService.checkLockStatus(this.logFormId, sectionId, partid).subscribe(
			(data) => {
				this._elogModeService.taskLogSection(this.logFormId).subscribe(
					(data) => {
						this.sectionList = data.Result;
						console.log("this.sectionList  ", this.sectionList)
						this.sectionList.forEach((seclist) => {
							if (seclist.LockedStatus === 'Locked' && seclist.LockedBy === this.userElogName) {
								console.log("seclist  ", seclist)
								this.applyButtonEnable = true;
							}
						})				
					})
			})
	}

	public lockForm(formId, form) {

		this.verifyFormArray.push(formId);
	}

	public pullVerifyFormFromServer(formId?) {

	}

	public pullFromServer() {
		async.series([
			(done) => {
				this._elogpdb.addSingleElogTaskList(this.logFormId, this.entityID, this.siteId, false, 1).subscribe(  // for new download 3rd parameter is 1
					(ldata) => {
						this.getLocalElogTaskList();
						done();
					},
					(err) => {				
						done();
					}
				);
			}
		], () => {			
			this.closeSectionModal();			
		});

	}

	public getSiteDetails() {
		const userName = localStorage.getItem('valgen_userName');
		this.userName = {
			userName
		};
		const encryptValue = this._encrDecr.encryptURL(this.userName);
		this._homeService.getSiteDetails().subscribe((data) => {
			const siteList = [];
			const siteId = localStorage.getItem('valgen_siteId');
			this.siteId = localStorage.getItem('valgen_siteId');
			if (data.TotalCount !== 0) {
				data.Result.forEach((list) => {
					siteList.push(list);
				})
				this.siteList = siteList;
			} else {
				this.alertPopup('No Site List Found !');
			}
		}, (err) => {
			
		}
		);

		this._homeService.getRoleProfile().subscribe((data) => {
			if (data.TransactionId === 0) {
			}
		})
	}

	public checkAuthorization(siteId) {
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			if (data.TransactionId === 0) {
				this.loadingService.pushAllLocalDataToServer(siteId);
			} else {
				this.alertPopup(data.TransactionMessage);
				console.log(" My error")
			}
		}, (err) => {
		
		}
		);
	}

	async alertPopup(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: ['OK']
		});
		await alert.present();
	}

	public openModalSectionList() {
		jQuery('#sectionListModal').modal('show');
	}

	public closeSectionModal() {
		jQuery('#sectionListModal').modal('hide');
	}

	public pushToServer(taskId, list) {
		if (this._network.type === 'none' || navigator.onLine === false) {
			this.alertController('Check Internet Connection!');
		} else {
			this.loadingController('Syncing..');
			const disconnectSubscription = this._network.onDisconnect().subscribe(() => {
				this.loading.dismiss();
				this.alertController('Check Internet Connection!');
			});		
			this._elogpdb.pushLocalData(taskId).subscribe((data) => {
				if (data == 'success') {
					async.series([
						(done) => {
							this._elogpdb.deleteLocalData(taskId, done);
						}
					], () => {
						disconnectSubscription.unsubscribe();
						this.loading.dismiss();
					});
					this.loading.dismiss();
					this.checkLocalElogTaskList();
				}
				else {
					this.loading.dismiss();
					this.checkLocalElogTaskList();
				}

			});
		}
	}

	async taskInfo(list, ev?: any) {
		let popover = await this.popoverCtrl.create({
			component: ElogFilterComponent,
			event: ev,
			animated: true,
			showBackdrop: true,
			componentProps: {
				"paramID": 123,
				"paramTitle": "TaskInfo",
				"paramDetails": list
			}
		});
		popover.present();
		popover.onDidDismiss().then((data) => {

		})
	}

	async elogFilter(ev?: any) {      // sorting
		this.getLocalElogTaskList();
		let popover = await this.popoverCtrl.create({
			component: ElogFilterComponent,
			event: ev,
			animated: true,
			showBackdrop: true,
			componentProps: {
				"paramID": 123,
				"paramTitle": "Filter"
			}
		});
		popover.present();
		popover.onDidDismiss().then((data) => {
			this.filterArray = JSON.parse(data.role);
			this.tempExecutionList = [];
			let now = new Date();
			const todayDate = formatDate(now, 'MM/dd/yyyy hh:mm:ss a', 'en-US', '+0530');
			for (let i = 0; i < this.filterArray.length; i++) {
				if (this.filterArray[i] == 1) {
					for (let j = 0; j < this.elogTaskList.length; j++) {
						if (this.elogTaskList[j].doc.Activity === "Capture Log Data") {
							this.tempExecutionList.push(this.elogTaskList[j]);
						}
					}
				}
				else if (this.filterArray[i] == 2) {
					for (let j = 0; j < this.elogTaskList.length; j++) {
						if (this.elogTaskList[j].doc.Activity === "Verify Log Data") {
							this.tempExecutionList.push(this.elogTaskList[j]);
						}
					}
				}
				else if (this.filterArray[i] == 3) {
					for (let j = 0; j < this.elogTaskList.length; j++) {
						if (this.elogTaskList[j].doc.Activity === "Approve Log Data") {
							this.tempExecutionList.push(this.elogTaskList[j]);
						}
					}
				}
				else if (this.filterArray[i] == 4) {
					for (let j = 0; j < this.elogTaskList.length; j++) {
						if (this.elogTaskList[j].doc.TaskOrigin === "Reinitiate Log Data") {
							this.tempExecutionList.push(this.elogTaskList[j]);
						}
					}
				}

				if (this.tempExecutionList.length > 0) {
					this.elogTaskList = this.tempExecutionList;
				}
				else {
					this.elogTaskList = [];
				}
			}

			if (data.data == 1) {
				this.elogTaskList = this.orderPipe.transform(this.elogTaskList, 'doc.CreatedOn', true);
			}
			else if (data.data == 2) {
				this.elogTaskList = this.orderPipe.transform(this.elogTaskList, 'doc.FormName');
			}
		});


	}

	public checkSessionTimeOut() {
		const currentTime = moment(moment().format('YYYY-MM-DD, h:mm:ss'));//now
		const validTime = moment(localStorage.getItem('valgen_validTimeStamp'));
		const sessionTime = localStorage.getItem('valgen_sessionTimeOut');
		if (currentTime.diff(validTime, 'minutes') > parseInt(sessionTime)) {
			this.alertSession('Session TimeOut');
		} else {
			localStorage.setItem('valgen_validTimeStamp', moment().format('YYYY-MM-DD, h:mm:ss'));
		}
	}

	async alertSession(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						this.sessionLogout();
					}
				}
			]
		});
		await alert.present();
	}

	public sessionLogout() {
		const url = localStorage.getItem('valgen_settingUrl');
		localStorage.clear();
		localStorage.setItem('valgen_settingUrl', url);
		this._router.navigate(['login']);
	}

	async alertController(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
	}

	async loadingController(message) {
		this.loading = await this._loadingCtrl.create({
			message: message,
			spinner: 'lines'
		});
		await this.loading.present();
	}
	async dismiss() {
		this.loading = false;
		return await this.loading.dismiss().then();
	}
	public logBookBtnClick() {
		this.logBookButton = true;
		this.taskListButton = false;
		this._router.navigate([`/menu/home/elog/viewLog/enterEntity`]);
	}
	public taskListBtnClick() {
		this.taskListButton = true;
		this.logBookButton = false;


	}
	public goBack() {
		this._router.navigate([`/menu/home/elog`]);
	}
}
