import { Injectable } from '@angular/core';
import { HttpService } from '../../common/http.service';
import { EncrDecrService } from '../../common/encr-decr.service';
@Injectable()
export class HomeService {

	private userId: any;
	public siteId:any;
	private userName = {};

	constructor(private _httpService: HttpService,  private _encrDecr: EncrDecrService) {
		this.userId = localStorage.getItem('valgen_userId');
		this.siteId = localStorage.getItem('valgen_siteId');
	}

	public executionList() {
		const siteId = localStorage.getItem('valgen_siteId');
		const data = {
			UserID : this.userId,
			SiteID : siteId
		};
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/BindElectronicTask`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

	public tableList(ExeID) {
		const data = {
			ExeID : ExeID
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/BindDocumentTableList`,
			body: encryptValue
		};
		
		return this._httpService.post(info);
	}

	public mappingDetail(ExeID, tableId) {
		const data = {
			UserID: this.userId,
			ExeID : ExeID,
			TableID: tableId
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/BindMappingDetails`,
			body : encryptValue
		};
		return this._httpService.post(info);
	}

	public tableDetails(ExeID) {
		const data = {
			UserID: this.userId,
			ExeID : ExeID
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/BindExecutedTableDetails`,
			body : encryptValue
		};
		return this._httpService.post(info);
	}

	public documentDetails(ExeID) {
		const data = {
			ExeID : ExeID
		}
		console.log("ListData  ", JSON.stringify(data));
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/BindDocumentStream`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

	public UpdateExeTableDetail(data: any) {
		const info = {
			url: `Execution/UpdateExeTableDetail`,
			body: data
		};
		return this._httpService.post(info);
	}

	public UpdateDocumentData(exeId, data: any) {
		const info = {
			url: `Execution/UpdateSyncExecutionStatus`,
			body: {
				exeId: parseInt(exeId),                 //INT
				userId: parseInt(this.userId),                 //INT
				docContent: data, //Byte[]            
			}
		};
		console.log("UpdateSyncExecutionStatus   ", info);
		return this._httpService.post(info);
	}

	public UpdateUploadFiles(data) {
		const info = {
			url: `Execution/UpdateUploadFiles`,
			body: data
		};
		return this._httpService.post(info);
	}

	public uploadFileName(exeId, tableId, rowId) {
		const data = {
			ExeID : exeId,
			TableID : tableId,
			RowId : rowId,
			Type: 'attachment'
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/GetUploadAttachUrl`,
			body : encryptValue
			// ?exeId=${exeId}&tableID=${tableId}&rowId=${rowId}&type=attachment
		};
		return this._httpService.post(info);
	}
	

	public getSiteDetails() {
		const userName = localStorage.getItem('valgen_userName');
		this.userName = {
			userName
		};
		const encryptValue = this._encrDecr.encryptURL(this.userName);
		const info = {
			url: `Login/LoadSiteMenu`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

	public getSiteAuthorization(siteId) {
		const data = {
			UserID : this.userId,
			SiteID : siteId
		};
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Login/GetSiteAccessRights`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}


	public getRoleProfile() {
		const siteId = localStorage.getItem('valgen_siteId');
		const data = {
			UserID : this.userId,
			SiteID : siteId
		};
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `RoleProfile/GetRoleProfile`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

	public UserLogOff(){

		const data = {
			userName : localStorage.valgen_userName
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Login/UserLogOff`,
			body : encryptValue
			// ?exeId=${exeId}&tableID=${tableId}&rowId=${rowId}&type=attachment
		};
		return this._httpService.post(info);
	}
}
