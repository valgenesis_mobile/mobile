import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, AlertController,Platform, PopoverController,MenuController } from '@ionic/angular';
import * as moment from 'moment';
import { HomeService } from './home.service';
import { LocalPouchdbProvider } from '../../common/local-pouchdb';
import { ElogPouchdbProvider } from '../../common/elog-pouchdb';
import { Events,IonRouterOutlet} from '@ionic/angular';
import { MenuComponent } from '../../menu/menu.component';
import { EncrDecrService } from '../../common/encr-decr.service';
import {LoadingService } from '../../loading.service';
import { UserIdleService } from 'angular-user-idle';
import { Idle } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';



@Component({
	selector: 'app-home',
	templateUrl: 'home.page.html',
	styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

	public executionList: any;
	public loading: any;
	public searchTerm = '';
	public vlmsMenu: any;
	public eLogMenu: any;
	public siteList: any;
	public siteId: any;
	public button1Class:boolean =true;
	public button2Class:boolean =false;
	public allData = [];
	public userName = {};
	public watchTime = true;
	public appPlatform:any;
	public loggedInUser: string;
	public UserRole:string;

	timeInSeconds:any;
	time:any;
	runTimer:any;
	hasStarted:any;
	hasFinished:any;
	remainingTime:any;;
	displayTime:any;



  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;

	constructor(private _router: Router, public _loadingCtrl: LoadingController, public _alertCtrl: AlertController, 
		private _homeService: HomeService,private _platform: Platform,public _localpdb: LocalPouchdbProvider, public _elogPdb: ElogPouchdbProvider
		, public events: Events, public menucom: MenuComponent, private _encrDecr: EncrDecrService
		, public loadingService: LoadingService, private userIdle: UserIdleService,private routerOutlet: IonRouterOutlet,public menu: MenuController,public modalController: PopoverController,
		private idle: Idle, private keepalive: Keepalive) {
			this.appPlatform = localStorage.valgen_myplatform;

			this._platform.backButton.subscribeWithPriority(1, () => {
			
			
			});
			  }
			
	ionViewWillEnter() {	
		this.vlmsMenu = localStorage.getItem('valgen_vlmsMenu');
		this.eLogMenu = localStorage.getItem('valgen_eLogMenu');	
		this.button1Class = true;
		this.button2Class = false;
		this.getSiteDetails();
		this.menucom.ngOnInit();	
		if (localStorage.getItem('valgen_eLogMenu') == 'false') {
			this._router.navigate([`/menu/home/task`]);
		}
		else if (localStorage.getItem('valgen_vlmsMenu') == 'false') {
			this._router.navigate([`/menu/home/elog`]);
		}

		this.menu.swipeEnable(true);
		 this.routerOutlet.swipeGesture = false;	
	
	}
	ngOnInit() {
		this.getSiteDetails();
	}

	siteClick(){
		console.log("CLICKEDDDDDDDDDD");
		this.getSiteDetails();
	}


	menuclick(){
		console.log("menu click")
		this.menucom.ngOnInit();
	}


	homeListClick (path) {
		this._router.navigate([`/menu/home/${path}`]);
        localStorage.removeItem("taskFilter");
	}

	public getSiteDetails() {
		const userName = localStorage.getItem('valgen_userName');
		this.userName = {
			userName
		};
		this._homeService.getSiteDetails().subscribe((data) => {
			const siteList = [];
			this.siteId = localStorage.getItem('valgen_siteId');
			if(data.TotalCount !== 0) {
				data.Result.forEach((list) => {
						siteList.push(list);
				})
				this.siteList = siteList;
			} else {
				this.alertPopup('No Site List Found !');
			}
			}, (err) => {
			}
		);

		this._homeService.getRoleProfile().subscribe((data) => {			
			if (data.TransactionId === 0) {
			}
		})
	}

	public checkAuthorization(siteId) {
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			if (data.TransactionId === 0) {
				this.loadingService.pushAllLocalDataToServer(siteId);
			} else {
				this.alertPopup(data.TransactionMessage);
			}
			}, (err) => {
			}
		);
	}

	async alertPopup(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: ['OK']
		});
		await alert.present();
	}

	dashBtnClick()
	{
		this.button1Class = true;
		this.button2Class = false;
		this._router.navigate([`/menu/home`]);
	}
	taskBtnClick(path)
	{ 
		this.button2Class = true;
		this.button1Class = false;
		this._router.navigate([`/menu/home/${path}`]);
	}
}
