import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbPaginationModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { HomePage } from './home.page';
import { HomeService } from './home.service';

import { CommonPipesModule } from '../../common/pipe';
import { LocalDbServiceModule } from '../../common/local-pouchdb';
import { ElogDbServiceModule } from '../../common/elog-pouchdb';

import {Keepalive} from '@ng-idle/keepalive';


const route = [
	{ path: '',  component: HomePage },
	{ path: 'task', loadChildren: './task/task.module#TaskModule' },
	{ path: 'elog', loadChildren: './elog/elog.module#ElogModule' }
]
@NgModule({
	imports: [
		LocalDbServiceModule,
		ElogDbServiceModule,
		CommonModule,
		FormsModule,
		CommonPipesModule,
		IonicModule,
		NgbPaginationModule,
		NgbAlertModule,
		RouterModule.forChild(route)
	],
	
	declarations: [
		HomePage,
	],
	providers: [
		HomeService,
		Keepalive
	]
})
export class HomePageModule { }
