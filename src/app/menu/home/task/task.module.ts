import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';

import { TaskComponent } from './task.component';
import { TaskService } from './task.service';

import { CommonPipesModule } from '../../../common/pipe';
import { LocalDbServiceModule } from '../../../common/local-pouchdb';


const route = [

	{ path: '',  component: TaskComponent },
	{ path: ':ExeID', loadChildren: './taskExecution/taskExecution.module#TaskExecutionModule' }
]
@NgModule({
	imports: [
		LocalDbServiceModule,
		CommonModule,
		FormsModule,
		CommonPipesModule,
		IonicModule,
		NgbPaginationModule,
		NgbAlertModule,
		RouterModule.forChild(route)
	],
	
	declarations: [
		TaskComponent,
	],
	providers: [
		TaskService
	]
})
export class TaskModule { }
