import { Injectable } from '@angular/core';
import { HttpService } from '../../../common/http.service';
import { EncrDecrService } from '../../../common/encr-decr.service';
@Injectable()
export class TaskService {

	private userId: any;

	constructor(private _httpService: HttpService,  private _encrDecr: EncrDecrService) {
		//this.userId = '';
		this.userId = localStorage.getItem('valgen_userId');
		console.log("this.userId ",localStorage.getItem('valgen_userId') )
	}

	public executionList(userid, siteid) {
		//const siteId = localStorage.getItem('valgen_siteId');
		const data = {
			UserID : userid,
			SiteID : siteid
		};
		console.log("DATAAAAAAAA", data)
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/BindElectronicTask`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

	public reviewedList(userid, siteid) {
		//const siteId = localStorage.getItem('valgen_siteId');
		// const data = {
		// 	UserID : userid,
		// 	SiteID : siteid
		// };
		 console.log("reviewedList")
		// const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/BindReviewedElectronicTask?userId=${userid}&siteId=${siteid}`,
			//body: encryptValue
		};
		return this._httpService.get(info);
	}

	public peerList(userid, siteid) {
		console.log("peerList")
		const info = {
			url: `Execution/BindPeerElectronicTask?userId=${userid}&siteId=${siteid}`,
			//body: encryptValue
		};
		return this._httpService.get(info);
	}

	public tableList(ExeID) {
		const data = {
			ExeID : ExeID
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/BindDocumentTableList`,
			body: encryptValue
		};
		
		return this._httpService.post(info);
	}

	public mappingDetail(ExeID, tableId) {
		const data = {
			UserID: this.userId,
			ExeID : ExeID,
			TableID: tableId
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/BindMappingDetails`,
			body : encryptValue
		};
		return this._httpService.post(info);
	}

	public tableDetails(ExeID) {
		const data = {
			UserID: this.userId,
			ExeID : ExeID
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/BindExecutedTableDetails`,
			body : encryptValue
		};
		return this._httpService.post(info);
	}

	public documentDetails(ExeID) {
		const data = {
			ExeID : ExeID
		}
		console.log("ListData  ", JSON.stringify(data));
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/BindDocumentStream`,
			body : encryptValue
		};
		return this._httpService.post(info);
		
	}

	public UpdateExeTableDetail(data: any) {
		console.log("UpdateExeTableDetail  ", data)
		const info = {
			url: `Execution/UpdateExeTableDetail`,
			body: data
		};
		return this._httpService.post(info);
	}

	public UpdateDocumentData(exeId, data: any) {
		console.log("UpdateSyncExecutionStatus  ", data);
		const info = {
			url: `Execution/UpdateSyncExecutionStatus`,
			body: {
				exeId: parseInt(exeId),                 //INT
				userId: parseInt(this.userId),                 //INT
				docContent: data, //Byte[]            
			}
		};
		
		return this._httpService.post(info);
	}

	public UpdateUploadFiles(data) {
		console.log("UpdateUploadFiles  ", data)
		const info = {
			url: `Execution/UpdateUploadFiles`,
			body: data
		};
		return this._httpService.post(info);
	}
	public uploadFileName(exeId, tableId, rowId) {
		const data = {
			ExeID : exeId,
			TableID : tableId,
			RowId : rowId,
			Type: 'attachment'
		}
		console.log("GetUploadAttachUrl  ", data)
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/GetUploadAttachUrl`,
			body : encryptValue
			// ?exeId=${exeId}&tableID=${tableId}&rowId=${rowId}&type=attachment
		};
		return this._httpService.post(info);
	}
}
