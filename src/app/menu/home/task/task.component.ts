import { Component, ViewChild } from '@angular/core';
import { formatDate } from '@angular/common';
import { Router } from '@angular/router';
import { LoadingController, AlertController, PopoverController,MenuController } from '@ionic/angular';
import { Network } from '@ionic-native/network/ngx';
import { TaskService } from './task.service';
import { LocalPouchdbProvider } from '../../../common/local-pouchdb';
import { LoadingService } from '../../../loading.service';
import * as moment from 'moment';
import { HomeService } from '../home.service';
import { TaskFilterComponent } from '../../../task-filter/task-filter.component';
import { OrderPipe } from 'ngx-order-pipe';
import { Subscription } from 'rxjs';
import { Platform } from '@ionic/angular';
import { IonSearchbar } from '@ionic/angular';
import { Toast } from '@ionic-native/toast/ngx';
import { MenuComponent } from '../../../menu/menu.component';
//import { LoginComponent } from '../../../login/login.component';
import async from 'async';

@Component({
	templateUrl: 'task.component.html',
	styleUrls: ['task.component.scss']
	//providers: [LoginComponent]
})
export class TaskComponent {

	public executionList: any;
	
	public loading: any;
	public searchTerm = '';
	public siteList: any;
	public siteId: any;
	public button1Class: boolean = false;
	public button2Class: boolean = true;
	public enableSearchbar = false;
	public allData = [];
	public searchList =[];
	public noofNewTask: number;
	public executionListLength = false;
	public order: string = 'doc.TargetDate';
	public filterArray: any;
	public tempExecutionList: any;
	private onPauseSubscription: Subscription;
	private onResumeSubscription: Subscription;
	public appPlatform: any;
	public filterAvailable: boolean = false;

	constructor(private _taskService: TaskService, private _router: Router,
		public _localpdb: LocalPouchdbProvider, public _loadingCtrl: LoadingController, public _alertCtrl: AlertController,
		private _network: Network, private _homeService: HomeService, public popoverCtrl: PopoverController, private orderPipe: OrderPipe,
		private loadingService: LoadingService, public platform: Platform,public menu: MenuController, private toast: Toast, public menucom: MenuComponent) {
		this.executionList = [];
		this.onPauseSubscription = platform.pause.subscribe(() => {
		});
		this.onResumeSubscription = platform.resume.subscribe(() => {
		});
		this.appPlatform = localStorage.valgen_myplatform;
	}
	// @ViewChild('searchId', {static: false}) searchId:IonSearchbar;
	@ViewChild(IonSearchbar) myInput: IonSearchbar;
	ionViewWillEnter() {
		this.checkLocalExecutionList();
		this.loadingService.reset();
		this.getSiteDetails();
		this.filterAvailable = false;
		localStorage.removeItem("taskFilter");
		this.menu.swipeEnable(true);
		this.executionListLength = false;
	}

	// setFilteredItems() {
	// 	this.searchList = [];
	// 	console.log("search filter ", this.executionList)
		
	// 	// this.allData.forEach((task) => {
	// 	// 	if (task.doc.SiteID == localStorage.getItem('valgen_siteId') && task.doc.Userid == localStorage.getItem('valgen_userId')) {
	// 	// 		this.searchList.push(task);
	// 	// 	}
	// 	// })
	// 	this.executionList = this.executionList.filter((location) => {
	// 		return (location.doc.Title.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1) || location.doc.ValDocCode.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;

	// 	});

	
	// 	if (this.executionList.length <= 0) {
	// 		this.executionListLength = true;
	// 	} else {
	// 		this.executionListLength = false;
	// 	}
	// 	this.noofNewTask = 0;
	// 	this.executionList.forEach((exe) => {
	// 		if (exe.doc.NewTaskStatus === 1) {
	// 			this.noofNewTask = this.noofNewTask + 1;
	// 		}
	// 	})
	// }

	setFilteredItems() {
		this.searchList = [];
		
		
		
		if(this.filterAvailable == true){
		
		this.tempExecutionList.forEach((task) => {
		if (task.doc.SiteID == localStorage.getItem('valgen_siteId') && task.doc.Userid == localStorage.getItem('valgen_userId')) {
		
		this.searchList.push(task);
		
		}
		
		})
		
		}else{
		this.allData.forEach((task) => {
		if (task.doc.SiteID == localStorage.getItem('valgen_siteId') && task.doc.Userid == localStorage.getItem('valgen_userId')) { 
		this.searchList.push(task); 
		}
		
		}) 
		}
		
		this.executionList = this.searchList.filter((location) => {
		
		return (location.doc.Title.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1) || location.doc.ValDocCode.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
		
		});
		
		
		if (this.executionList.length <= 0) {
		this.executionListLength = true;
		} else {
		this.executionListLength = false;
		}
		this.noofNewTask = 0;
		this.executionList.forEach((exe) => {
		if (exe.doc.NewTaskStatus === 1) {
		this.noofNewTask = this.noofNewTask + 1;
		}
		})
		}
	   

	logScrolling(eve) {

	}
	async taskFilter(ev?: any) {    // sorting
		this.enableSearchbar = false;
		let popover = await this.popoverCtrl.create({
			component: TaskFilterComponent,
			event: ev,
			animated: true,
			showBackdrop: true,
			componentProps: {
				"paramID": 123,
				"paramTitle": "Filter"
			}
		});
		popover.present();
		popover.onDidDismiss().then((data) => {
			if (data.data === 'closePopover') {
				if(localStorage.taskFilter.length == 2){
					this.filterAvailable=false;
					localStorage.removeItem('taskFilter');
					this.loadingService.showLoading('Loading......');

					this.getLocalExecutionList();
				}
			 }
			else {
				this.filterArray = JSON.parse(data.role);
				console.log(" this.filterArray  ", this.filterArray)
				this.tempExecutionList = [];
				this.executionList = JSON.parse(localStorage.executionList);
				console.log("Filerrrrrr", this.executionList)
				this.executionListLength = false;
				if (this.filterArray.length > 0) {
					this.filterAvailable = true;
				}
				else {
					this.filterAvailable = false;
				}
				let now = new Date();
				const todayDate = formatDate(now, 'MM/dd/yyyy hh:mm:ss a', 'en-US', '+0530');
				for (let i = 0; i < this.filterArray.length; i++) {
					if (this.filterArray[i].id == 1) {
						for (let j = 0; j < this.executionList.length; j++) {
							if (this.executionList[j].doc.TaskOrigin == 1) {
								this.tempExecutionList.push(this.executionList[j]);
							}
						}
					}
					 if (this.filterArray[i].id == 2) {
						for (let j = 0; j < this.executionList.length; j++) {
							if (this.executionList[j].doc.TaskOrigin == 2) {
								this.tempExecutionList.push(this.executionList[j]);
							}
						}
					}
					 if (this.filterArray[i].id == 3) {
						for (let j = 0; j < this.executionList.length; j++) {
							if (this.executionList[j].doc.TaskOrigin == 3) {
								this.tempExecutionList.push(this.executionList[j]);
							}
						}
					}
					 if (this.filterArray[i].id == 4) {
						for (let j = 0; j < this.executionList.length; j++) {
							if (this.executionList[j].doc.TaskOrigin == 4) {
								this.tempExecutionList.push(this.executionList[j]);
							}
						}
					}
					 if (this.filterArray[i].id == 7) {
						for (let j = 0; j < this.executionList.length; j++) {

							if (todayDate < this.executionList[j].doc.TargetDate) {
								this.tempExecutionList.push(this.executionList[j]);
							}
						}

					}
					if (this.filterArray[i].id == 5) {
						for (let j = 0; j < this.executionList.length; j++) {
							if (this.executionList[j].doc.TaskOrigin == 5) {
								this.tempExecutionList.push(this.executionList[j]);
							}
						}
					}
					if (this.filterArray[i].id == 6) {
						for (let j = 0; j < this.executionList.length; j++) {
							if (this.executionList[j].doc.TaskOrigin == 6) {
								this.tempExecutionList.push(this.executionList[j]);
							}
						}
					}
				
				}

				if (this.tempExecutionList.length > 0) {
                    console.log("this.tempExecutionList   ", this.tempExecutionList)
						this.executionList = this.tempExecutionList;
					}
					else {
						this.executionList = [];
						this.executionListLength = true;
					}

				if (data.data == 1) {
					this.executionList = this.orderPipe.transform(this.executionList, 'doc.TargetDate', true);
				}
				else if (data.data == 2) {
					this.executionList = this.orderPipe.transform(this.executionList, 'doc.Title');
				}
				this.noofNewTask = 0;
				this.executionList.forEach((exe) => {
					if (exe.doc.NewTaskStatus === 1) {
						this.noofNewTask = this.noofNewTask + 1;
					}
				})
			}

		});
	}

    public menuClicked(){
		this.menucom.ngOnInit();
 this.menu.swipeEnable(true);
	}

	public pushToServer(exeId) {
		this.enableSearchbar = false;
		this.loadingService.present("Syncing");
		if (this._network.type === 'none' || navigator.onLine === false) {
			this.loadingService.dismiss();
			if (this.appPlatform == "Electron") {
				this.toastController("Check Internet Connection");
			}
			else {
			this.toast.show('Check Internet Connection', '10000', 'bottom').subscribe(
				toast => {
					console.log(toast);
				}
			);
		}
		} else {
			const disconnectSubscription = this._network.onDisconnect().subscribe(() => {
				if (this.appPlatform == "Electron") {
					this.toastController("Check Internet Connection");
				}
				else {
				this.toast.show('Check Internet Connection', '10000', 'bottom').subscribe(
					toast => {
						console.log(toast);
					}
				);
			}
			});
			async.series([
				(done) => {
					this._localpdb.pushLocalData(exeId, done);
				},
				(done) => {
					this._localpdb.deleteLocalData(exeId, done);
				},
				(done) => {
					this._localpdb.updateTaskById(exeId, 'statusPull').subscribe(
						(ldata) => {
							this.getLocalExecutionList();
							this.loadingService.dismiss();
							done();
							if (this.appPlatform == "Electron") {
								this.toastController("Task Uploaded successfully");
							}
							else {
							this.toast.show('Task Uploaded successfully', '10000', 'bottom').subscribe(
								toast => {
									
								}
							);
						}
						},
						(err) => {
							console.log(err);
							this.loadingService.dismiss();
							if (this.appPlatform == "Electron") {
								this.toastController("Task is not uploaded due to server error");
							}
							else {
							this.toast.show('Task is not uploaded due to server error', '10000', 'bottom').subscribe(
								toast => {
									
								});
							}
							done();
						}
					);
				}
			], () => {
				disconnectSubscription.unsubscribe();
				this.loadingService.dismiss();
			});
		}
	}

	public pullFromServer(exeId, Mapped) {
		this.enableSearchbar = false;
		this.loadingService.showLoading10("Downloading...");
		if (this._network.type === 'none' || navigator.onLine === false) {
			this.loadingService.dismiss();
			if (this.appPlatform == "Electron") {
				this.toastController("Check Internet Connection");
			}
			else {
			this.toast.show('Check Internet Connection', '10000', 'bottom').subscribe(
				toast => {
					console.log(toast);
				}
			);
		}
		} else {

			const disconnectSubscription = this._network.onDisconnect().subscribe(() => {
				if (this.appPlatform == "Electron") {
					this.toastController('Check Internet Connection');
				}
				else {
				this.toast.show('Check Internet Connection', '10000', 'bottom').subscribe(
					toast => {
						console.log(toast);
					}
				);
			}
			});
			if (Mapped == 0) {
				async.series([
					(done) => {
						this._localpdb.addSingleExecutionList(exeId).subscribe(
							(ldata) => {
								this.getLocalExecutionList();
								done();
								if (this.appPlatform == "Electron") {
									this.toastController('Task downloaded successfully');
								}
								else {
								this.toast.show('Task downloaded successfully', '10000', 'bottom').subscribe(
									toast => {
										console.log(toast);
									}
								);
							}
								this.loadingService.dismiss();
							},
							(err) => {
								done();
								console.log(err)
								this.loadingService.dismiss();
							}
						);
					}
				], () => {
					disconnectSubscription.unsubscribe();
					this.loadingService.dismiss();
					if (this.appPlatform == "Electron") {
						this.toastController('Task is not downloaded.');
					}
					else {
					this.toast.show('Task is not downloaded.', '10000', 'bottom').subscribe(
						toast => {
							
						});
					}
				});
			}
			else {
				this.loadingService.dismiss();
				this.alertController('Mapping details are not configured..');

			}
		}
	}

	public pullAllFromServer() {
		this.loadingService.showLoading10('Loading.....');
		if (this._network.type === 'none' || navigator.onLine === false) {
			if (this.appPlatform == "Electron") {
				this.toastController('Check Internet Connection');
			}
			else {
			this.toast.show('Check Internet Connection', '5000', 'bottom').subscribe(
				toast => {
					console.log(toast);
				}
			);
		}
		} else {

			const disconnectSubscription = this._network.onDisconnect().subscribe(() => {
				if (this.appPlatform == "Electron") {
					this.toastController('Check Internet Connection');
				}
				else {
				this.toast.show('Check Internet Connection', '5000', 'bottom').subscribe(
					toast => {
						console.log(toast);
					}
				);
			}
			});
			const siteId = localStorage.getItem('valgen_siteId');
			const userId = localStorage.getItem('valgen_userId');


			this._taskService.reviewedList(userId, siteId).subscribe(
				(data) => {
					console.log("reviewedList   ", data)
					if (data.TotalCount != undefined) {
						if (data.TotalCount !== 0) {

							this._localpdb.addExecutionList(data.Result).subscribe(
								(ldata) => {
									console.log("reviewedList  local", ldata)
									// this.getLocalExecutionList();	
									// disconnectSubscription.unsubscribe();
									// this.loadingService.dismiss();
								},
								(err) => {
									disconnectSubscription.unsubscribe();
									this.loadingService.dismiss();
								}
							);
						} else {
							disconnectSubscription.unsubscribe();
							this.executionListLength = true;
							this.loadingService.dismiss();
						}
					}
                   

				})

				this._taskService.peerList(userId, siteId).subscribe(
					(data) => {
						console.log("peerList   ", data)
						if (data.TotalCount != undefined) {
							if (data.TotalCount !== 0) {
	
								this._localpdb.addExecutionList(data.Result).subscribe(
									(ldata) => {
										// console.log("peerlist  local", ldata)
										// this.getLocalExecutionList();	
										// disconnectSubscription.unsubscribe();
										// this.loadingService.dismiss();
									},
									(err) => {
										disconnectSubscription.unsubscribe();
										this.loadingService.dismiss();
									}
								);
							} else {
								//this.getLocalExecutionList();	
								
								disconnectSubscription.unsubscribe();
								this.executionListLength = true;
								this.loadingService.dismiss();
							}
						}
	
	
	
					})
	
			this._taskService.executionList(userId, siteId).subscribe(
				(data) => {
					console.log("reviewedList   3333  ", data)				
					if (data.TotalCount == undefined) {
						//this.getLocalExecutionList();
						disconnectSubscription.unsubscribe();
						this.loadingService.dismiss();
					}

					if (data.TotalCount === 0) {
						
						disconnectSubscription.unsubscribe();
						this.loadingService.dismiss();
					}

					if (data.TotalCount != undefined) {
						if (data.TotalCount !== 0) {

							this._localpdb.addExecutionList(data.Result).subscribe(
								(ldata) => {
									this.getLocalExecutionList();	
									disconnectSubscription.unsubscribe();
									this.loadingService.dismiss();
								},
								(err) => {
									disconnectSubscription.unsubscribe();
									this.loadingService.dismiss();
								}
							);
						} else {
							disconnectSubscription.unsubscribe();
							this.executionListLength = true;
							this.loadingService.dismiss();
						}
					}
				}
			);

				}
		this.loadingService.dismiss();
	}

	private checkLocalExecutionList() {
		if(localStorage.executionList){
         this.executionList = JSON.parse(localStorage.executionList);
		}
		
				if (this.executionList.length == 0) {
					this.pullAllFromServer();
				}else{
					this._localpdb.getAllExecutionList().subscribe(
						(taskData) => {
							console.log(" taskData  taskData", taskData)
							if (taskData.length === 0) {
								this.pullAllFromServer();
							} else {
								this.getLocalExecutionList();
							}
						},
						(err) => {
						}
					);
				}
		
		
	}

	private getLocalExecutionList() {
		this.executionList = [];
		this._localpdb.getAllExecutionList().subscribe(
			(data) => {
				this.allData = data;
				console.log("my task  ", data)
				this.allData.forEach((task) => {
					
					if (task.doc.SiteID == localStorage.getItem('valgen_siteId') && task.doc.Userid == localStorage.getItem('valgen_userId')) {
						this.executionList.push(task);
					}
				})
				localStorage.executionList = JSON.stringify(this.executionList);

				if (this.executionList.length == 0) {
					this.pullAllFromServer();
				}
				else {
					if (this.executionList.length <= 0) {
						this.executionListLength = true;
					} else {
						this.executionListLength = false;
					}
					this.noofNewTask = 0;
					this.executionList.forEach((exe) => {
						if (exe.doc.NewTaskStatus === 1) {
							this.noofNewTask = this.noofNewTask + 1;
						}
					})
				}
			},
			(err) => {

			}
		);
	}

	doRefresh(event) {

		this.filterAvailable = false;
		localStorage.removeItem("taskFilter");
		if (this._network.type === 'none' || navigator.onLine === false) {

			setTimeout(() => {
				event.target.complete();
				this.pullAllFromServer();
			}, 50);
		}
		else {
			this._localpdb.deleteTaskList();
			setTimeout(() => {
				event.target.complete();
				this.pullAllFromServer();
			}, 50);
		}

	}

	public taskDetails(ExeID, syncState) {
		this.enableSearchbar = false;
		if(navigator.onLine == true){
		if (syncState) {
			this._router.navigate([`/menu/home/task/${ExeID}`]);
		} else {
			this.alertController('Task not Downloaded!');
		}
	}else{
		this.alertController('Please check Internet connection.')
	}
	}

	public getSiteDetails() {
		this._homeService.getSiteDetails().subscribe((data) => {
			const siteList = [];
			const siteId = localStorage.getItem('valgen_siteId');
			this.siteId = localStorage.getItem('valgen_siteId');
			if (data.TotalCount !== 0) {
				data.Result.forEach((list) => {
					siteList.push(list);
				})
				this.siteList = siteList;
			} else {
				this.alertController('No Site List Found !');
			}
		});
	}

	public checkAuthorization(siteId) {
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			if (data.TransactionId === 0) {
				this.loadingService.pushAllLocalDataToServer(siteId);
			} else {
				this.alertController(data.TransactionMessage);
			}
		}, (err) => {
			console.log(err, 'getSiteAuthorization')
		}
		);
	}



	public checkSessionTimeOut() {
		const currentTime = moment(moment().format('YYYY-MM-DD, h:mm:ss'));//now
		const validTime = moment(localStorage.getItem('valgen_validTimeStamp'));
		const sessionTime = localStorage.getItem('valgen_sessionTimeOut');
		if (currentTime.diff(validTime, 'minutes') > parseInt(sessionTime)) {
			this.alertSession('Session TimeOut');
		} else {
			localStorage.setItem('valgen_validTimeStamp', moment().format('YYYY-MM-DD, h:mm:ss'));
		}
	}

	async alertSession(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						this.sessionLogout();
					}
				}
			]
		});
		await alert.present();
	}

	public sessionLogout() {
		const url = localStorage.getItem('valgen_settingUrl');
		localStorage.clear();
		localStorage.setItem('valgen_settingUrl', url);
		this._router.navigate(['login']);
	}

	async alertController(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
	}
	async toastController(message) {
		const alert = await this._alertCtrl.create({
			//header: 'Alert',
			message: message
		});
		await alert.present();
		setTimeout(() => alert.dismiss(), 2000);
	}

	async loadingController(message) {
		this.loading = await this._loadingCtrl.create({
			message: message,
			spinner: 'lines'
		});
		await this.loading.present();
	}
	public dashBtnClick() {
		this.button1Class = true;
		this.button2Class = false;
		this._router.navigate([`/menu/home`]);
	}
	public taskBtnClick() {
		this.button2Class = true;
		this.button1Class = false;
	}
	public getSearchbar() {
		this.searchTerm = '';
		this.enableSearchbar = !this.enableSearchbar;
	
		setTimeout(() => { this.myInput.setFocus(); 
			}, 500);
			//this.getLocalExecutionList();
		
	}
	public goHome() {
		this._router.navigate(['/menu/home']);
	}

}
