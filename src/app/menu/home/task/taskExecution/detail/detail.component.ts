import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HomeService } from '../../../home.service';

import { LocalPouchdbProvider } from '../../../../../common/local-pouchdb';
import {LoadingService} from '../../../../../loading.service';
import { AlertController } from '@ionic/angular';
import * as moment from 'moment';
import async from 'async';

@Component({
	templateUrl: 'detail.component.html',
	styleUrls: ['detail.component.scss']
})
export class DetailComponent implements OnInit {
	private exeId: string;
	public taskDetail: any;
	public showMore: boolean = false;
	button1Class:boolean =true;
	button2Class:boolean =false;
	button3Class:boolean = false;
	public taskTitle:string;
	public siteList: any;
	public siteId: any;
	public viewmore:boolean = false;
	public appPlatform:any;

	constructor(private _router: Router, private route: ActivatedRoute, private _localpdb: LocalPouchdbProvider, 
		public _alertCtrl: AlertController, private _homeService: HomeService, public loadingService: LoadingService) {
		this.getParamValues();
		this.appPlatform = localStorage.valgen_myplatform;
	}

	ionViewWillEnter() {
       // this.checkSessionTimeOut();
    }

	private getParamValues() {
		this.route.parent.params.subscribe((params) => {
			// console.log('Params', params)
			this.exeId = params['ExeID'];
		});
		console.log('Exe Id', this.exeId);
	}

	public ngOnInit() {
		this.getTaskDetails(this.exeId);
		this.getSiteDetails();
	}

	public getSiteDetails() {
		this._homeService.getSiteDetails().subscribe((data) => {
			const siteList = [];
			const siteId = localStorage.getItem('valgen_siteId');
			this.siteId = localStorage.getItem('valgen_siteId');
			if(data.TotalCount !== 0) {
				data.Result.forEach((list) => {
						siteList.push(list);
				})
				this.siteList = siteList;
			} else {
				this.alertController('No Site List Found !');
			}
			}, (err) => {
				console.log(err, 'getSiteDetails')
			}
		);
	}

	public checkAuthorization(siteId) {
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			if (data.TransactionId === 0) {
				this.loadingService.pushAllLocalDataToServer(siteId);
			} else {
				this.alertController(data.TransactionMessage);
			}
			}, (err) => {
				console.log(err, 'getSiteAuthorization')
			}
		);
	}

	goBack(){
		this._router.navigate([`/menu/home/task`]);
		}

	private getTaskDetails(execId) {
		this._localpdb.getTaskById(execId).subscribe(
			(data) => {
				console.log(data, 'getDocumentDetails');
				this.taskDetail = data;
				this.taskTitle = data.ValDocCode;
			},
			(err) => {
				console.log('data', err);
			}
		);
	}

	public checkSessionTimeOut() {
		const currentTime = moment(moment().format('YYYY-MM-DD, h:mm:ss'));//now
		const validTime = moment(localStorage.getItem('valgen_validTimeStamp'));
		const sessionTime = localStorage.getItem('valgen_sessionTimeOut');
		console.log(currentTime.diff(validTime, 'minutes'));
		if(currentTime.diff(validTime, 'minutes') > parseInt(sessionTime)) {
			this.alertSession('Session TimeOut');
		} else {
			localStorage.setItem('valgen_validTimeStamp', moment().format('YYYY-MM-DD, h:mm:ss'));
		}
	}

	public viewMore(){
		this.viewmore = !this.viewmore;
	}
	
	async alertSession(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						this.sessionLogout();
					}
				}
			]
		});
		await alert.present();
	}

	public sessionLogout () {
		const url = localStorage.getItem('valgen_settingUrl');
		localStorage.clear();
		localStorage.setItem('valgen_settingUrl', url);
		// this._localpdb.deleteDatabase().subscribe(
		// 	(data) => {
		// 		console.log('delete', data);
		// 	}
		// );
		this._router.navigate(['login']);
	}
	async alertController(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
	}

	taskdetailsBtnClick()
	{
		this.button1Class = true;
		this.button2Class = false;
		this.button3Class = false;
		
	}
	tablesBtnClick()
	{
		this.button2Class = true;
		this.button1Class = false;
		this.button3Class = false;
		this._router.navigate([`/menu/home/task/${this.exeId}`]);
	}
	documentBtnClick(event)
	{
		this.button2Class = false;
		this.button1Class = false;
		this.button3Class = true;
		this._router.navigate([`/menu/home/task/${this.exeId}/${event}`]);

	}
}
