import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalPouchdbProvider } from '../../../../../common/local-pouchdb';
import { LoadingService } from '../../../../../loading.service';
import { AlertController } from '@ionic/angular';
import { HomeService } from '../../../home.service';
declare let jQuery: any;
import { Base64 } from 'js-base64';
@Component({
	templateUrl: 'document.component.html',
	styleUrls: ['document.component.scss']
})
export class DocumentComponent implements OnInit {
	private ExeId: string;
	public documentHtmlString: string;
	private localFile = {};
	public imageFileData: any;
	public modalImageName: any;
	public button1Class: boolean = false;
	public button2Class: boolean = false;
	public button3Class: boolean = true;
	public taskDetail: any;
	public taskTitle: string;
	public siteList: any;
	public siteId: any;
	public appPlatform: any;

	constructor(private _router: Router, private route: ActivatedRoute, private _localpdb: LocalPouchdbProvider,
		public _alertCtrl: AlertController, private _homeService: HomeService, public loadingService: LoadingService) {
		this.getParamValues();
		this.appPlatform = localStorage.valgen_myplatform;
	}

	ionViewWillEnter() {
	}

	public ngOnInit() {
		this.getDocumentDetails(this.ExeId);
		this.getLocalImageDocument(this.ExeId);
		this.getTaskDetails(this.ExeId);
		this.getSiteDetails();
	}
	private getParamValues() {
		this.route.parent.params.subscribe((params) => {
			this.ExeId = params['ExeID'];
		});
	}
	goBack(){
		this._router.navigate([`/menu/home/task`]);
		}

	private getDocumentDetails(execId) {
		this._localpdb.getDocumentDetails(execId)
			.subscribe(
				(data) => {
					this.documentHtmlString = this.base64ToArrayBuffer(data.buffer);
					jQuery(document).ready(function () {
						jQuery('a[target!="_blank"]').removeAttr("href");
					})
					const firstvariable = "https";
					const secondvariable = ".jpg" || ".png" || ".pdf";
					var getFromBetween = {
						results: [],
						string: "",
						getFromBetween: function (sub1, sub2) {
							if (this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
							var SP = this.string.indexOf(sub1) + sub1.length;
							var string1 = this.string.substr(0, SP);
							var string2 = this.string.substr(SP);
							var TP = string1.length + string2.indexOf(sub2);
							return this.string.substring(SP, TP);
						},
						removeFromBetween: function (sub1, sub2) {
							if (this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
							var removal = sub1 + this.getFromBetween(sub1, sub2) + sub2;
							this.string = this.string.replace(removal, "");
						},
						getAllResults: function (sub1, sub2) {
							// first check to see if we do have both substrings
							if (this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return;
							// find one result
							var result = this.getFromBetween(sub1, sub2);
							// push it to the results array
							this.results.push(result);
							// remove the most recently found one from the string
							this.removeFromBetween(sub1, sub2);
							// if there's more substrings
							if (this.string.indexOf(sub1) > -1 && this.string.indexOf(sub2) > -1) {
								this.getAllResults(sub1, sub2);
							}
							else return;
						},
						get: function (string, sub1, sub2) {
							this.results = [];
							this.string = string;
							this.getAllResults(sub1, sub2);
							return this.results;
						}
					};
				},
				(err) => {
					console.log('data', err);
				}
			);
	}

	private base64ToArrayBuffer(buffer) {
		return Base64.decode(buffer);
	}

	onChildClickFunction(event: Event) {
		event.stopPropagation();
		const target = event.target || event.currentTarget;
		const attachmentFile = this.localFile[target['attributes'].class.value];
		this.openModal(attachmentFile._id, attachmentFile.AttachmentName)
	}

	public openModal(fileId, name) {
		jQuery("body").addClass("modal-open");
		jQuery('#imageModal').modal();
		this.modalImageName = name;
		this._localpdb.getImagebyId(fileId).subscribe(
			(data) => {
				this.imageFileData = `data:image/png;base64,${data.FileContent}`;
			},
			(err) => {
				console.log('data', err);
			}
		);
	}

	public closeModal() {
		jQuery("body").removeClass("modal-open");
		jQuery('#imageModal').modal('hide');
	}

	public getSiteDetails() {
		this._homeService.getSiteDetails().subscribe((data) => {
			const siteList = [];
			const siteId = localStorage.getItem('valgen_siteId');
			this.siteId = localStorage.getItem('valgen_siteId');
			if (data.TotalCount !== 0) {
				data.Result.forEach((list) => {
					siteList.push(list);
				})
				this.siteList = siteList;
			} else {
				this.alertController('No Site List Found !');
			}
		}, (err) => {
			console.log(err, 'getSiteDetails')
		}
		);
	}

	public checkAuthorization(siteId) {
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			if (data.TransactionId === 0) {
				this.loadingService.pushAllLocalDataToServer(siteId);
			} else {
				this.alertController(data.TransactionMessage);
			}
		}, (err) => {
			console.log(err, 'getSiteAuthorization')
		}
		);
	}


	private getLocalImageDocument(execId) {
		if (Object.keys(this.localFile).length === 0) {
			this._localpdb.getImageDoc(execId).subscribe(
				(data) => {
					console.log(data, 'getImageDoc');
					data.forEach((file) => {
						this.localFile[file._id] = file;
						const aTag = document.getElementsByClassName(`${file._id}`);
						if (aTag.length) {
							aTag[0].addEventListener('click', this.onChildClickFunction.bind(this));
						}
					});
				},
				(err) => {
					console.log('data', err);
				}
			);
		}
	}

	private getTaskDetails(execId) {
		this._localpdb.getTaskById(execId).subscribe(
			(data) => {
				console.log(data, 'getDocumentDetails');
				this.taskDetail = data;
				this.taskTitle = data.ValDocCode;
			},
			(err) => {
				console.log('data', err);
			}
		);
	}

	async alertController(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
	}
	taskdetailsBtnClick(event) {
		this.button1Class = true;
		this.button2Class = false;
		this.button3Class = false;
		this._router.navigate([`/menu/home/task/${this.ExeId}/${event}`]);

	}
	tablesBtnClick() {
		this.button2Class = true;
		this.button1Class = false;
		this.button3Class = false;
		this._router.navigate([`/menu/home/task/${this.ExeId}`]);
	}
	documentBtnClick() {
		this.button2Class = false;
		this.button1Class = false;
		this.button3Class = true;
	}

}
