import { Component, OnInit, AfterViewInit, ViewChild, NgZone,ElementRef   } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ActionSheetController, AlertController, Platform,MenuController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { LocalPouchdbProvider } from '../../../../../common/local-pouchdb';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IOSFilePicker } from '@ionic-native/file-picker/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { MediaCapture, MediaFile, CaptureError, CaptureVideoOptions } from '@ionic-native/media-capture/ngx';
import { EncrDecrService } from '../../../../../common/encr-decr.service';
import { HomeService } from '../../../home.service';
import { Toast } from '@ionic-native/toast/ngx';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Base64 } from 'js-base64';
import { Device } from '@ionic-native/device/ngx';
import async from 'async';
import * as moment from 'moment';
import * as momenttz from 'moment-timezone';
import { LoadingService } from '../../../../../loading.service';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import * as rxjsNumeric from "rxjs/util/isNumeric"
import { IonRouterOutlet } from '@ionic/angular';
import { DesktopCapturer, Screen } from "electron";
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
//import {electron} from 'electron';

import * as watermark from 'watermarkjs';


declare var $: any;
declare let jQuery: any;
declare var screenShot: any;
//const electron = require('electron');


@Component({
	templateUrl: 'execution.component.html',
	styleUrls: ['execution.component.scss']
})
export class ExecutionComponent implements OnInit, AfterViewInit {

	public tempimage: any;
	private exeId: string;
	public tableList = [];
	public tableData: any;
	public tableId = -1;
	public rowId = -1;
	public selectedMenu: any;
	private documentHtmlString: string;
	public tableMappingDetails = [];
	public documentTableDetails = [];
	public executableColumnOnly = [];
	public nonExecutableColumn = [];
	public columnTypeArray = [1, 2, 4, 5, 8, 9, 10];
	public resultCheckArray = [3, 11];
	public executableColumnArray = [1, 9];
	public nonexecutableColumnArray = [4, 8];
	public EsignVerifyArray = [12, 13, 14, 15];
	public statusArray = ['Pass', 'Fail', 'N/A'];
	private latestTableRowDetails = [];
	public electronicExec: any;
	public editActivated = false;
	public showRecordButton = false;
	public result = -1;
	public maxRowIndex = -1;
	public activity: any;
	public dateTime: any;
	public loggedIn: any;
	public userName: any;
	public selectedUser: any;
	public password: any;
	public reason = '';
	public statusEdit = false;
	public textboxLength = false;
	public tableLength: any;
	public showEsignPopup = false;
	public showVerifyPopup = false;
	public alertBox = false;
	private localFile = {};
	public imageFileData: any;
	public modalImageName: any;
	public verifyUserList: any;
	public selectedUserName: any;
	public showExecutableText: any;
	public button1Class: boolean = true;
	public button2Class: boolean = false;
	public passButton: boolean = false;
	public failButton: boolean = false;
	public naButton: boolean = false;
	public siteList: any;
	public editNonExe = false;
	public btncolor: any = 'light';
	public isSave=false;
	public oldelectronicExec: any;
	public userID: any;
	public columnid: any;
	public uploadData: any;
	public fileForm: FormGroup;
	public base64textString: any;
	public fileNameChange: any;
	public originalImage: any;
	public blobImage: any;
	public scrimgId: any;
	public snapImageId: any;
	public passFailButton: any;
	public EsignRowReq: boolean = false;
	public EsignTableReq: boolean = false;
	public VerifyRowReq: boolean = false;
	public VerifyTableReq: boolean = false;
	public EVcolumn: boolean = false;
	public selectedUserNameTable: any;
	public selectedUserTable: any;
	public tableRow:any;
	public pTagClass:any;
	public spanTagClass:any;
	public appPlatform:any;
	public enableEvidence:boolean = false;
	public appFileFormat:any;
	public fileErrorMsg:any = '';
	public showActualButton:boolean = false;
	public disableAttachEvidence = false;
	public isDesktopApp:boolean = false;
	public EsignVerifyData:any;
	public selectFileError:boolean = false;
	public verifyRowButton:boolean = false;
	public verifyTableButton:boolean = false;
	public siteId:any;
	public proceedExecution:boolean = true;
	public verifyInExecutable:boolean = false;
	public EsignVerbiageText:any;
	public evidenceList:any;
	public evidenceListShow:boolean = false;
	public doc:any;
	public allScreenList: any;
    public fileContent: any;
	public screenCaptureData = -1;
	public watermarkedImg: any;
	public watermarkFont:any;
	public evidenceListOld:any;
	public desktopCapture: DesktopCapturer;
	public electronScreen:Screen;
    // private fileTransfer: FileTransferObject; 

	public editorConfig: AngularEditorConfig = {
		enableToolbar: false,
		showToolbar: false,
		editable: true,
		minHeight: '5rem',

	};

	public nonEditorConfig: AngularEditorConfig = {
		enableToolbar: false,
		showToolbar: false,
		editable: false,
		minHeight: '5rem',
	};


	photo: SafeResourceUrl;
//public desktopCapture = electron.desktopCapturer;
//public  electronScreen = electron.remote.screen;

	@ViewChild('waterMarkedImage') waterMarkImage: ElementRef;


	constructor(private _homeService: HomeService, private _router: Router, private _route: ActivatedRoute, private _localpdb: LocalPouchdbProvider,
		private _camera: Camera, public _actionSheetCtrl: ActionSheetController, public _alertCtrl: AlertController, private _encrDecr: EncrDecrService,
		private _platform: Platform, private toast: Toast, private _fileChooser: FileChooser, private _filePicker: IOSFilePicker, 
		private mediaCapture: MediaCapture, public loadingService: LoadingService, private device: Device,  private platform: Platform,
		 public ngZone: NgZone,private routerOutlet: IonRouterOutlet,public menu: MenuController, private sanitizer: DomSanitizer,
		 ) {
		this.getParamValues();
		this.userID = localStorage.getItem('valgen_userId');
		this.appPlatform = localStorage.valgen_myplatform;
		console.log('Device UUID is: ' + this.device.uuid);
		// console.log(" desktopCapture  ", this.desktopCapture)
		
		//localStorage.getItem('valgen_esign_username') != null ? this.userName = localStorage.getItem('valgen_esign_username') : this.userName = '';

	
		// this._platform.backButton.subscribeWithPriority(1, () => {
			
			
		//  });
		
	
	}

	
	get f() { return this.fileForm.controls; }
	@ViewChild('form') form;
	ionViewWillEnter() {
		this.evidenceList = [];
		this.evidenceListShow = false;
		this.editNonExe = false;
		this.executableColumnOnly = [];
		this.nonExecutableColumn = [];	
		this.appPlatform = localStorage.valgen_myplatform;
		console.log("this.appPlatform  ", this.appPlatform)
		if(this.appPlatform === 'Electron')
		{
			this.isDesktopApp = true;
		}
		else if(this.appPlatform === 'undefined')
		{
			this.platform.ready().then(() => {
				//	console.log("this.platform  ", this.platform)
		
					if (this.platform.is('android')) {
						localStorage.setItem('valgen_myplatform', 'Android');
						
					}
					else if (this.platform.is('ios')) {
						localStorage.setItem('valgen_myplatform', 'IOS');
						
					}
					else if (this.platform.is('electron')) {
						localStorage.setItem('valgen_myplatform', 'Electron');
						this.isDesktopApp = true;
						
					}
					else {
						localStorage.setItem('valgen_myplatform', 'Desktop');
						
					}
					this.appPlatform = localStorage.valgen_myplatform;
					//console.log("this.appPlatform   ", this.appPlatform)
		
		
				});
		}
	
	}



	public updateStatus(value) {
		this.result = value;
		switch (value) {
			case 1:
				this.passButton = true;
				this.failButton = false;
				this.naButton = false;
				break;

			case 2:
				this.passButton = false;
				this.failButton = true;
				this.naButton = false;
				break;

			case 3:
				this.passButton = false;
				this.failButton = false;
				this.naButton = true;
				break;
		}
	}

	private getParamValues() {
		this.showEsignPopup = false;
		this.showVerifyPopup = false;
		this._route.parent.params.subscribe((params) => {
			this.exeId = params['ExeID'];
		});
		this._route.queryParams.subscribe((params) => {
			if (params['rowId']) {
				this.rowId = Number(params['rowId']);
			}
			if (params['tableId']) {
				this.tableId = Number(params['tableId']);
			}
			if (params['tableLength']) {
				this.tableLength = Number(params['tableLength']);
			}
			if (this.rowId !== -1 && this.tableId !== -1) {
				this.getTableData();
			}
			if (this.rowId + 1 === this.tableLength) {   // last row in table
				this.showEsignPopup = true;
				this.showVerifyPopup = true;
			}
		});
	}

	ngOnInit() {
		this.appPlatform = localStorage.valgen_myplatform;
		this.evidenceList = [];
		this.evidenceListShow = false;	
		this.evidenceListOld =[];		
		this.getSiteDetails();
		this.getTableList();
		this.getLocalImageDocument(this.exeId);	
		this.executableColumnOnly = [];
		this.nonExecutableColumn = [];
		this.menu.swipeEnable(true); 
		localStorage.removeItem('electronicExec');
		this.fileForm = new FormGroup({
			fileName: new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(100)])),
			imageInput: new FormControl('', Validators.required)
		});
		console.log("this.appPlatform   oninit ", this.appPlatform)
		if(this.appPlatform === 'Electron')
		{
			this.isDesktopApp = true;
		}
		else if(this.appPlatform === 'undefined')
		{
			this.platform.ready().then(() => {
				//	console.log("this.platform  ", this.platform)
		
					if (this.platform.is('android')) {
						localStorage.setItem('valgen_myplatform', 'Android');
						
					}
					else if (this.platform.is('ios')) {
						localStorage.setItem('valgen_myplatform', 'IOS');
						
					}
					else if (this.platform.is('electron')) {
						localStorage.setItem('valgen_myplatform', 'Electron');
						this.isDesktopApp = true;
						
					}
					else {
						localStorage.setItem('valgen_myplatform', 'Desktop');
						
					}
					this.appPlatform = localStorage.valgen_myplatform;
					//console.log("this.appPlatform   ", this.appPlatform)
		
		
				});
		}
		this.doc = "https://vg.valgenesis.us/VGV4PSG/fs-vgv4v1psg/Docs/executionfile.pdf";
	
	}


	

	onChildClickFunction(event: Event) {
		event.stopPropagation();
		const target = event.target || event.currentTarget;
		const attachmentFile = this.localFile[target['attributes'].class.value];
		console.log(attachmentFile);
		this.openModal(attachmentFile._id, attachmentFile.AttachmentName);
	}

	public openModal(fileId, name) {
		jQuery("body").addClass("modal-open");
		jQuery('#imageModal').modal();
		this.modalImageName = name;
		this._localpdb.getImagebyId(fileId).subscribe(
			(data) => {
				this.imageFileData = `data:image/png;base64,${data.FileContent}`;
			},
			(err) => {
				console.log('data', err);
			}
		);
	}

	public closeModal() {
		jQuery("body").removeClass("modal-open");
		jQuery('#imageModal').modal('hide');
		//$("#file").remove();
	}
     public viewFile(){
		//const browser = this.iab.create('https://vg.valgenesis.us/VGV4PSG/fs-vgv4v1psg/Docs/executionfile.pdf', '_system', this.options);
		
	 }




	public openViewEvidenceModal(fileId?, name?) {		
		this.modalImageName = name;
		this._localpdb.getImageDoc(this.exeId).subscribe(
			(data) => {
				this.evidenceList = [];
				this.evidenceListOld =[];
				data.forEach((img) => {
					if(img.tableId === this.tableId && img.rowId === this.rowId){
						var extn = img.AttachmentName.split(".").pop();
						img.Extension = extn;
						if(img.isNew == 0 ){
							this.evidenceListOld.push(img);
						}else
						{
							this.evidenceList.push(img);
						}                      
					}
				})
				if(this.evidenceList.length > 0) {
					 this.evidenceListShow = true;						 
				}
				else{
					this.evidenceListShow = false;					
				}
				jQuery("body").addClass("modal-open");
				jQuery('#viewevidence').modal();
			})	
	}

	

	// const url = 'http://www.example.com/file.pdf';
	// fileTransfer.download(url, this.file.dataDirectory + 'file.pdf').then((entry) => {
	//   console.log('download complete: ' + entry.toURL());
	// }, (error) => {
	//   // handle error
	// });
     
	// public download(fileName, filePath) {  
	// 	//here encoding path as encodeURI() format.  
	// 	let url = encodeURI(filePath);  
	// 	//here initializing object.  
	// 	this.fileTransfer = this.transfer.create();  
	// 	// here iam mentioned this line this.file.externalRootDirectory is a native pre-defined file path storage. You can change a file path whatever pre-defined method.  
	// 	fileTransfer.download(url, this.file.externalRootDirectory + fileName, true).then((entry) => {  
	// 		//here logging our success downloaded file path in mobile.  
	// 		console.log('download completed: ' + entry.toURL());  
	// 	}, (error) => {  
	// 		//here logging our error its easier to find out what type of error occured.  
	// 		console.log('download failed: ' + error);  
	// 	});  
	// }


	// downloadFile(fileName, filePath) {
		// Some Loading

	//   }

	public closeViewEvidenceModal() {
		jQuery("body").removeClass("modal-open");
		jQuery('#viewevidence').modal('hide');
	}

	private getLocalImageDocument(execId) {
		this.evidenceList =[];
		if (Object.keys(this.localFile).length === 0) {
			this._localpdb.getImageDoc(execId).subscribe(
				(data) => {
					data.forEach((file) => {
						this.localFile[file._id] = file;
						const aTag = document.getElementsByClassName(`${file._id}`);
						if (aTag.length) {
							aTag[0].addEventListener('click', this.onChildClickFunction.bind(this));
						}
						if(file.tableId === this.tableId && file.rowId === this.rowId){
						   this.evidenceList.push(file);
						}
					});

					if(this.evidenceList.length > 0 ){
						this.evidenceListShow = true;
				   }
				   else{
					   this.evidenceListShow = false;
				   }
				   console.log("this.evidenceListShow 2222 ", this.evidenceListShow)
				},
				(err) => {
					console.log('data', err);
				}
			);
		}
	}

	public viewAttachment(file)
	{
		console.log("fiiiiiiiiif  ", file)
		if(file.Extension == 'jpeg' || file.Extension === 'png' || file.Extension === 'jpg'){
			this.openModal(file._id, file.AttachmentName);
		}
		else{
			console.log("exte", file.Extension)
		}
		
	}

	public deleteAttachment(ind, list) {
		localStorage.electronicExec = JSON.stringify(this.electronicExec);
		var executlist = this.executableColumnOnly[0].value;
		var els = document.getElementsByTagName("a");
		for (var i = 0, l = els.length; i < l; i++) {
			var el = els[i];
			if (el.getAttribute("class") === list._id) {
				var executlist1 = executlist.replace(el.innerHTML, '');
				this.executableColumnOnly[0].value = executlist1;
			}
		}
		this.evidenceList.forEach((evilist, index) => {
			if (evilist._id == list._id) {
				this.evidenceList.splice(index, 1);
			}
		})
		if (this.evidenceList.length > 0) {
			this.evidenceListShow = true;
		}
		else {
			this.evidenceListShow = false;
		}
		this._localpdb.updateImageLocal(list,list._id);		
	}

	isEmptyOrSpaces(str) {
		return str === null || str.match(/^ *$/) !== null;
	}

	onChangeTime(data): void {
		if (data.length > 0) {
			if (data.replace(/&nbsp;/gi, '') === '') {
				this.textboxLength = false;
			} else {
				this.textboxLength = true;
			}
		} else {
			this.textboxLength = false;
		}
	}

	changeEditFunc() {
		this.editNonExe = !this.editNonExe;
		$('#copytoactual_id').css('opacity','0.3');
		$('#saveImg').css('opacity','0.3');
		
		this.editNonExe ? this.result = 0 : this.result = -1;
	}

	public saveNonExec(editStatus) {
		this.editNonExe = !this.editNonExe;
	
		this.reasonNonExecEditSheet();

	}



	private getTableData() {
		this.cleanUp();
		async.parallel([
			(done) => {
				this.getDocumentDetails(this.exeId, done);
			},
			(done) => {
				this.tableBindMappingDetails(this.tableId, done);
			},
			(done) => {
				this.getTableById(this.tableId, done);
			},
			(done) => {
				this.getLatestTableRowDetails(done);
			}
		], () => {
			this.bindExecutedTableDetails(this.tableData.HtmlDocTableId, this.rowId);
			this.getRowDetail(this.documentTableDetails, this.rowId);
		});
	}

	private cleanUp() {
		this.executableColumnOnly = [];
		this.nonExecutableColumn = [];
		this.editActivated = false;
		this.showRecordButton = false;
		this.result = -1;
		this.passButton = false;
		this.failButton = false;
		this.naButton = false;
		$('.outerPass').css({ 'background': '#fff' });
		$('.outerPass').css({ 'color': '#000' });
		$('.outerFail').css({ 'background': '#fff' });
		$('.outerFail').css({ 'color': '#000' });
		$('.outerNA').css({ 'background': '#fff' });
		$('.outerNA').css({ 'color': '#000' });
	}

	ngAfterViewInit() {
		$(document).ready(() => {
			$('.outerPass').on('click', function () {
				$(this).css('background', '#679436');
				$(this).css('color', '#fff');

				$('.outerFail').css({ 'background': '#fff' });
				$('.outerFail').css({ 'color': '#000' });
				$('.outerNA').css({ 'background': '#fff' });
				$('.outerNA').css({ 'color': '#000' });
			});

			$('.outerFail').on('click', function () {
				$(this).css('background', '#d11115');
				$(this).css('color', '#fff');

				$('.outerPass').css({ 'background': '#fff' });
				$('.outerPass').css({ 'color': '#000' });
				$('.outerNA').css({ 'background': '#fff' });
				$('.outerNA').css({ 'color': '#000' });
			});

			$('.outerNA').on('click', function () {
				$(this).css('background', '#cccccc');
				$(this).css('color', '#fff');

				$('.outerPass').css({ 'background': '#fff' });
				$('.outerPass').css({ 'color': '#000' });
				$('.outerFail').css({ 'background': '#fff' });
				$('.outerFail').css({ 'color': '#000' });
			});
		});
	}

	areaChange() {    //remove emoji in input field

		$('#areainput').focusin(function(){
		})

		
		$('#areainput').keydown(function(){
		})
		$('#areainput').keyup(function () {
			this.value = this.value.replace(/(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g, '');
		});
		$('#filename').keyup(function(){
			this.value = this.value.replace(/(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g, '');
	
		});

		this.isSave=true;
		$('#saveImg').css('opacity','1');
		
	}
	focusChange(){
	}

	focusOutFunction(){
	}


	public executableButton(columnid,elec) {
		
		this.showActualButton = false;
		//this.evidenceList = [];
		if(elec.ColumnType == 10 || elec.ColumnType == 2){
			this.showActualButton = true;
			this.evidenceListShow = false;	
			this.disableAttachEvidence = false;
		}

		if ((elec.ColumnType == 1) ||(elec.ColumnType == 9)){
			this.disableAttachEvidence = true;
			//this.evidenceListShow = true;	
			console.log(this.evidenceList)
		
			this._localpdb.getImageDoc(this.exeId).subscribe(
				(data) => {
				   
					console.log("DATA  Evidence  ", data)
					this.evidenceList = [];
					
					data.forEach((img) => {
						if(img.tableId === this.tableId && img.rowId === this.rowId){
							var extn = img.AttachmentName.split(".").pop();
							img.Extension = extn;
						   this.evidenceList.push(img);
						}
					})	
					
				})
               console.log("this.evidenceList   ", this.evidenceList)
				if(this.evidenceList.length > 0) {
					this.evidenceListShow = true;		
			   }else{
				this.evidenceListShow = false;	
			   }

				
			
		}
		// else{
		// 	this.disableAttachEvidence = false;
		// 	this.evidenceListShow = false;
		// }

		this.showExecutableText = columnid;
		this.btncolor = 'success';
	}

	public onRowChangeCheck(value, add) {
		this.editNonExe = false;
		//!this.textboxLength ? this.onRowChanged(value, add) : this.alertCheckEditValid(value, add);
		this.onRowChanged(value, add);
	}

	public onRowChanged(value, add) {
		// console.log("value  ", value)
		// console.log("add   ", add)
		// console.log("this.rowId   ", this.rowId)
		console.log("this.evidenceListShow 66666  ", this.evidenceListShow)
		this.showEsignPopup = false;
		this.showVerifyPopup = false;
		localStorage.removeItem('scrimgId');
		this.textboxLength = false;
		let rowId = value;
		if (add) {
			console.log('TTTTTTTTTTTTTT')
			if(rowId != 0){
			rowId += this.rowId;
			console.log('YYYYYY'+rowId)
			this.rowId = this.rowId + 1;
			}else{
				this.rowId = this.maxRowIndex;
			}
					
		}
		// console.log("this.tableId  ", this.tableId)
		 console.log("rowId   ", rowId)
		 console.log("this.maxRowIndex   ", this.maxRowIndex)
		
	
		if (rowId <= this.maxRowIndex && rowId > 0) {
			this._localpdb.updateTaskListTableRow(this.exeId, this.tableId, rowId).subscribe(
				(updateTable) => {
					if(localStorage.valgen_SequentialRowExecutionRequired == 1){

						this._localpdb.getImageDoc(this.exeId).subscribe(
							(data) => {
							   
								console.log("DATA  Evidence  ", data)
								this.evidenceList = [];
								data.forEach((img) => {
									if(img.tableId === this.tableId && img.rowId === this.rowId){
										var extn = img.AttachmentName.split(".").pop();
										img.Extension = extn;
									   this.evidenceList.push(img);
									}
								})
				
								console.log(this.evidenceList);
				
								if(this.evidenceList.length > 0) {
									 this.evidenceListShow = true;	
									 
								}
								else{
									this.evidenceListShow = false;
									
								}
								// jQuery("body").addClass("modal-open");
								// jQuery('#viewevidence').modal();
				                console.log("this.evidenceListShow 444  ", this.evidenceListShow)
				
							})

					this._localpdb.checkTaskListTableDetail(this.exeId, this.tableId, rowId).subscribe(
						(checkListDetail) => {
							 console.log("checkListDetail2222  ", checkListDetail)

							if(checkListDetail.length > 0){
								 console.log("checkListDetail   ", checkListDetail)

								this._router.navigate([`/menu/home/task/${this.exeId}/execution`],
								{ queryParams: { tableId: this.tableId, rowId: rowId, tableLength: this.tableLength } });

							}
							else
							{
								if(rowId == 1){
									this._router.navigate([`/menu/home/task/${this.exeId}/execution`],
								{ queryParams: { tableId: this.tableId, rowId: rowId, tableLength: this.tableLength } });

								}
								else{
									//this.alertPopup('Execute in Sequential Order Only, Select the Previous Row.');
								}
								
							}
						})
					}
					else{
						this._router.navigate([`/menu/home/task/${this.exeId}/execution`],
						{ queryParams: { tableId: this.tableId, rowId: rowId, tableLength: this.tableLength } });
					}
					
				},
				(err) => {
					// console.log('updateTable', err);
				}
			);
			if (rowId + 1 === this.tableLength) {   // last row in table
				this.showEsignPopup = true;
				this.showVerifyPopup = true;
			}
		} else {
			let tableIndex = -1;
			this.rowId = this.rowId + 1;
			this.tableList.forEach((table, index) => {
				if (table.TableID === this.tableId) {
					tableIndex = index;
				}
			});
			if (tableIndex !== -1) {
				if (add) {
					tableIndex += value;
				}

				this._localpdb.checkTaskListTableDetail(this.exeId, this.tableId, -2).subscribe(
					(checkListDetail) => {
						if (checkListDetail.length) {
							if (tableIndex < this.tableList.length && tableIndex >= 0) {
								this.alertAllNonExecRow(this.exeId, this.tableList[tableIndex].TableID);
							} else if (tableIndex === this.tableList.length) {
								this.alertAllNonExeRowToDoc(this.exeId);
							}
						} else {
							if (tableIndex < this.tableList.length && tableIndex >= 0) {
								this.alertAllExecRow(this.exeId, this.tableList[tableIndex].TableID);
							} else if (tableIndex === this.tableList.length) {
								this.alertAllExeRowToDoc(this.exeId);
							}
						}
					},
					(err) => {
						console.log('checkListDetail', err);
					}
				);
			}
		}
	}

	public CurrentRowChanged(value, add){
		var rowId = value + this.rowId;
		console.log("row  ", rowId)
		this.loadingService.showLoading10('Loading.....');
		this._router.navigate([`/menu/home/task/${this.exeId}/execution`],
		{ queryParams: { tableId: this.tableId, rowId: rowId, tableLength: this.tableLength } });
 
         setTimeout(() => {
			var rowId1 = 0 + this.rowId - 1;
			console.log("Rowid ",rowId1)
	
			this._router.navigate([`/menu/home/task/${this.exeId}/execution`],
			{ queryParams: { tableId: this.tableId, rowId: rowId1, tableLength: this.tableLength } });
			this.loadingService.dismiss();							
		 }, 100);
	


	}

	async alertAllNonExecRow(exeId, tableIndex) {
		this._router.navigate([`/menu/home/task/${exeId}/execution`],
									{ queryParams: { tableId: tableIndex, rowId: 1 } });
		// const alert = await this._alertCtrl.create({
		// 	header: 'Info!',
		// 	message: 'You are navigating to the next table!  Please read all test instructions and pre-requisites before executing',
		// 	backdropDismiss: false,
		// 	buttons: [
		// 		{
		// 			text: 'Yes',
		// 			handler: () => {

		// 				this._localpdb.updateTaskListTableRow(exeId, tableIndex, 0).subscribe(
		// 					(updateTable) => {
		// 						this.executableColumnOnly = [];
		// 						this.nonExecutableColumn = [];
		// 						this._router.navigate([`/menu/home/task/${exeId}/execution`],
		// 							{ queryParams: { tableId: tableIndex, rowId: 0 } });
		// 					},
		// 					(err) => {
		// 						console.log('updateTable', err);
		// 					}
		// 				);
		// 			}
		// 		},
		// 		{
		// 			text: 'No',
		// 			role: 'cancel'
		// 		}
		// 	]
		// });
		// await alert.present();
	}

	async alertAllNonExeRowToDoc(exeId) {
		this._router.navigate([`/menu/home/task/${exeId}/document`]);
		// const alert = await this._alertCtrl.create({
		// 	header: 'Info!',
		// 	message: 'You are navigating to the next table!  Please read all test instructions and pre-requisites before executing',
		// 	backdropDismiss: false,
		// 	buttons: [
		// 		{
		// 			text: 'Yes',
		// 			handler: () => {
		// 				this._router.navigate([`/menu/home/task/${exeId}/document`]);
		// 			}
		// 		},
		// 		{
		// 			text: 'No',
		// 			role: 'cancel'
		// 		}
		// 	]
		// });
		// await alert.present();
	}

	async alertAllExecRow(exeId, tableIndex) {
		this._router.navigate([`/menu/home/task/${exeId}/execution`],{ queryParams: { tableId: tableIndex, rowId: 1 } });
		// const alert = await this._alertCtrl.create({
		// 	header: 'Info!',
		// 	message: 'You are navigating to the next table!  Please read all test instructions and pre-requisites before executing',
		// 	backdropDismiss: false,
		// 	buttons: [
		// 		{
		// 			text: 'Yes',
		// 			handler: () => {
		// 				this._localpdb.updateTaskListTableRow(exeId, tableIndex, 0).subscribe(
		// 					(updateTable) => {
		// 						this._router.navigate([`/menu/home/task/${exeId}/execution`],
		// 							{ queryParams: { tableId: tableIndex, rowId: 0 } });
		// 					},
		// 					(err) => {
		// 						console.log('updateTable', err);
		// 					}
		// 				);
		// 			}
		// 		},
		// 		{
		// 			text: 'No',
		// 			role: 'cancel'
		// 		}
		// 	]
		// });
		// await alert.present();
	}

	async alertAllExeRowToDoc(exeId) {
		this._router.navigate([`/menu/home/task/${exeId}/document`]);
		// const alert = await this._alertCtrl.create({
		// 	header: 'Info!',
		// 	message: 'You are navigating to the next table!  Please read all test instructions and pre-requisites before executing',
		// 	backdropDismiss: false,
		// 	buttons: [
		// 		{
		// 			text: 'Yes',
		// 			handler: () => {
		// 				this._router.navigate([`/menu/home/task/${exeId}/document`]);
		// 			}
		// 		},
		// 		{
		// 			text: 'No',
		// 			role: 'cancel'
		// 		}
		// 	]
		// });
		// await alert.present();
	}

	async alertCheckEditValid(value, add) {
		const alert = await this._alertCtrl.create({
			header: 'Info!',
			message: 'Execution Not Saved, Do You want continue with next Execution?',
			backdropDismiss: false,
			buttons: [
				{
					text: 'No',
					role: 'cancel'
				},
				{
					text: 'Yes',
					handler: () => {
						this.onRowChanged(value, add);
					}
				}
				
			]
		});
		await alert.present();
	}

	public goBack() {
		this._router.navigate([`/menu/home/task/${this.exeId}`]);
		localStorage.removeItem('electronicExec');
		localStorage.removeItem('scrimgId');
	}
	public goHome() {
		this._router.navigate(['/menu/home/']);
	}



	public resultChecked(expectedChecked, value) {
		this.electronicExec.forEach((col) => {
			if (col.ColumnType === 1 || col.ColumnType === 9) {
				if(col.columnId == this.showExecutableText){
				if (expectedChecked) {
					col.value = `  ${value}`;

					this.textboxLength = true;
				} else {
					col.value = '';
					this.textboxLength = false;
				}
			}
		}
		});
	}

	public onTableChange(table) {
	
		this.executableColumnOnly = [];
		this.nonExecutableColumn = [];
		localStorage.removeItem('scrimgId');
		this._router.navigate([`/menu/home/task/${this.exeId}/execution`], { queryParams: { tableId: table, rowId: 1 } });
	}


	//*********************************  For Sequential Table Code ***********************************






	// public onTableChange(event) {
	// 	console.log("table", this.tableList)
	// 	console.log("event ", event)
	// 	this.executableColumnOnly = [];
	// 	this.nonExecutableColumn = [];
	// 	localStorage.removeItem('scrimgId');
	// 	if(localStorage.valgen_SequentialRowExecutionRequired == 1){
    //              this.tableList.forEach((list) => {
	// 				 if(list.TableID == event.detail.value)
	// 				 {
	// 					 this.tableRow = list;
	// 					 console.log(" this.tableRow ",  this.tableRow)
	// 					 this._localpdb.checkTaskListTableDetail(this.exeId, list.TableID, this.maxRowIndex).subscribe(
	// 						(checkListDetail) => {
	// 							console.log("checkListDetail  ", checkListDetail)
			
	// 							if(checkListDetail.length > 0){
	// 								this._router.navigate([`/menu/home/task/${this.exeId}/execution`], { queryParams: { tableId: event.detail.value, rowId: 1 } });
					
			
	// 							}
	// 							else{
	// 								this._router.navigate([`/menu/home/task/${this.exeId}/execution`],
	// 					{ queryParams: { tableId: (this.tableRow.TableID - 1), rowId: 1 } });
	// 								//this._router.navigate([`/menu/home/task/${this.exeId}/execution`], { queryParams: { tableId: list.TableID, rowId: 1 } });
	// 								this.alertPopup('Execute in Sequential Order Only, Select the MY Table.');
	// 							}
			
	// 						})
	// 				 }
	// 			 })
			
		
	// 	}
	// 	else{

		
	// 	this._router.navigate([`/menu/home/task/${this.exeId}/execution`], { queryParams: { tableId: event.detail.value, rowId: 1 } });
	// 	}
	// }

	private getTableList() {
		this._localpdb.getTableList(this.exeId).subscribe(
			(data) => {
				this.tableList = data;
				console.log(this.tableList);
			},
			(err) => {
				console.log('data', err);
			}
		);
	}

	private getTableById(tableId, done) {
		this._localpdb.getTableById(this.exeId, tableId).subscribe(
			(data) => {
				this.tableData = data;
				done();
			},
			(err) => {
				console.log('data', err);
				done();
			}
		);
	}

	private tableBindMappingDetails(tableId, done) {

		// console.log("getDeviationById    AA   ", this.exeId, this.userID)
		// this.userID = localStorage.getItem('valgen_userId');
		
		// this._localpdb.getDeviationById(this.exeId, this.userID).subscribe(
		// 	(data) => {


		// 		console.log("getDeviationById", data)

		// 	})

		this._localpdb.getTableMapDetatils(this.exeId, tableId).subscribe(
			(data) => {
				this.tableMappingDetails = data;
				this.EsignRowReq = false;
				this.EsignTableReq = false;
				this.VerifyRowReq = false;
				this.VerifyTableReq = false;

				console.log("this.tableMappingDetails  ", this.tableMappingDetails)
				this.tableMappingDetails.forEach((esign) => {
					if (esign.ColumnType === 12) {  // Esign req for row level
						this.EsignRowReq = true;
					}
					else if (esign.ColumnType === 13) { // Esign req for Table level
						this.EsignTableReq = true;
					}
					else if (esign.ColumnType === 14) {   // verification req for row level
						this.VerifyRowReq = true;
					}
					else if (esign.ColumnType === 15 || esign.ColumnType === 7) {    // verification req for Table level
						this.VerifyTableReq = true;
					}
				})

                console.log("this.EsignRowReq  ", this.EsignRowReq , this.EsignTableReq , this.VerifyRowReq , this.VerifyTableReq   )
				if((this.EsignRowReq && this.EsignTableReq) || (this.VerifyRowReq && this.VerifyTableReq))
				{
					this.showRecordButton = false;
					this.alertPopup('Cant Proceed with this execution...');
					this._router.navigate([`/menu/home/task/${this.exeId}`]);
					//alert("Cant Proceed with this execution...")
				}
				done();
			}
		);
	}

	private getDocumentDetails(execId, done) {
		this._localpdb.getDocumentDetails(execId).subscribe(
			(data) => {
				this.documentHtmlString = this.base64ToArrayBuffer(data.buffer);
				done();
			},
			(err) => {
				console.log('data', err);
			}
		);
	}

	private base64ToArrayBuffer(buffer) {
		//return window.atob(buffer);
		return Base64.decode(buffer);
	}

	private bindExecutedTableDetails(htmlTableId, rowId) {
		const id = `#${htmlTableId} tbody`;
		let tableDetails: any;
		$(this.documentHtmlString).find(id).find('tr').each((rowIndex, rowValue) => {
			if (rowIndex === rowId) {
				const tableRows = $(rowValue).find('td');
				const tableRow = [];
				tableRows.each((columnIndex, columnValue) => {
					tableRow.push($(columnValue).html());
				});
				tableDetails = tableRow;
			}
			this.maxRowIndex = rowIndex;
		});
		this.documentTableDetails = tableDetails;
	}

	private getLatestTableRowDetails(done) {
		this._localpdb.getLatestTableDetatils(this.exeId).subscribe(
			(data) => {
				this.latestTableRowDetails = data;
				done();
			}
		);

		this._localpdb.checkTaskListTableDetail(this.exeId, this.tableId, -2).subscribe(
			(checkListDetail) => {

			})
	}

	public getRowData(rowId, ExeTableID) {
		return this.latestTableRowDetails.filter((row) => {
			return (row.ExRowID === rowId && row.ExeTableID === ExeTableID);
		});
	}

	myTrim(x) {
		return x.replace(/^\s+|\s+$/gm,'');
	  }

	public getRowDetail(detail, rowId) {
		this.executableColumnOnly = [];
		this.nonExecutableColumn = [];
		this.editActivated = false;
		this.showRecordButton = true;
		this.passFailButton = false;
		//console.log("Get Row Details", detail);
		const data = [];
		 console.log("Details  ", detail);
		// console.log("this.tableMappingDetails[i]", this.tableMappingDetails);
		//   console.log("this.columnTypeArray", this.columnTypeArray);
		//   	console.log("this.executableColumnArray ", this.executableColumnArray);
		console.log("rowId   ", rowId)
		console.log("this.tableId  ", this.tableId)
		const rowData = this.getRowData(rowId + 1, this.tableId); // get the row details from tableDetails table
		 console.log("RowDATA out  ", rowData); 
		for (let i = 0; i < detail.length; i++) {
			const ColumnType = this.tableMappingDetails[i].ColumnType;
			 console.log("ColumnType ", ColumnType)
			 console.log("this.columnTypeArray.indexOf(ColumnType) ", this.columnTypeArray.indexOf(ColumnType))
			 console.log("this.executableColumnArray.indexOf(ColumnType)  ", this.executableColumnArray.indexOf(ColumnType))
			if (this.columnTypeArray.indexOf(ColumnType) !== -1) {
				if (this.executableColumnArray.indexOf(ColumnType) !== -1) {
					let value = '';
					 console.log("RowDATA  ", rowData);
					for (let j = 0; j < rowData.length; j++) {
						if (rowData[j].ExColumnID === i + 1) {
							value = rowData[j].Content;
							this.showRecordButton = false;
						}
					}
					// Executable Column values   this.tableMappingDetails[i].ColumnID
					data.push({
						'label': this.tableMappingDetails[i].ColumnHeader,
						'value': value.replace(/\&nbsp;/g, ''), 'columnId': i, 'ColumnType': ColumnType, rowId: rowId
					});
			

				} else {
					// console.log("RowDATA 111 ", rowData);
					let value = '';
					if (rowData.length > 0) {
						for (let j = 0; j < rowData.length; j++) {
							if (rowData[j].ExColumnID === i + 1) {
								// /le.log("rowData[j].ExColumnID  ", rowData[j].ExColumnID);
								value = rowData[j].Content;
								this.showRecordButton = false;
								break;
							}
							else {
								// console.log("detail[i]    ", detail[i].text);
								value = $(detail[i]).text();
								// console.log($(detail[i]));
							}
						}
					}
					else {
						// console.log("iiiiiii  ", i)
						// console.log("detail[i] 22222222222   ", detail[i]);
						value = $(detail[i]).text();
						// console.log("Valuedddddd", value)
				        
					}
					// Non Executed column value
					data.push({
						'label': this.tableMappingDetails[i].ColumnHeader,
						'value': value, 'columnId': i, 'ColumnType': ColumnType, rowId: rowId
					});
				}
			}
			// console.log("this.resultCheckArray  ", this.resultCheckArray)
			// check if result type columns available or not ---  this.resultCheckArray = [3, 11]
			if (this.resultCheckArray.indexOf(ColumnType) !== -1) {
				this.passFailButton = true;
			}
		}
		this.electronicExec = data;
		this.oldelectronicExec = this.electronicExec;
		if (localStorage.electronicExec == undefined) {
			localStorage.electronicExec = JSON.stringify(this.electronicExec);
		}
		 console.log(" this.electronicExec ", this.electronicExec);
		for (let i = 0; i < this.electronicExec.length; i++) {
			if (this.electronicExec[i].ColumnType == 1 || this.electronicExec[i].ColumnType == 2 || this.electronicExec[i].ColumnType == 9 || this.electronicExec[i].ColumnType == 10) {
				this.electronicExec[i].id = i;
				this.electronicExec[i].value = this.electronicExec[i].value.replace(/\&nbsp;/g, '');
				this.executableColumnOnly.push(this.electronicExec[i]);
			}
			else {
				this.electronicExec[i].id = i;
				this.nonExecutableColumn.push(this.electronicExec[i]);
			}
			if(this.electronicExec[i].ColumnType != 2 && this.electronicExec[i].ColumnType != 10){
				
				this.textboxLength = true;
				
			}
			if ((this.electronicExec[i].ColumnType == 1) ||(this.electronicExec[i].ColumnType == 9)){
				this.disableAttachEvidence = true;
				//this.evidenceListShow = true;
			}else{
				this.disableAttachEvidence = false;
				this.evidenceListShow = false;
			}
		}

		 console.log("this.executableColumnOnly ", this.executableColumnOnly);
		
		 console.log("this.nonExecutableColumn ", this.nonExecutableColumn);
		for (let j = 0; j < this.executableColumnOnly.length; j++) {
			if (this.executableColumnOnly[j].ColumnType == 2) {   // Executable with sign
				this.showExecutableText = this.executableColumnOnly[j].columnId;
				if(this.showRecordButton ){
					this.executableColumnOnly[j].value = localStorage.valgen_ExecutionComments;
					}
			}
			else if (this.executableColumnOnly[j].ColumnType == 1) {   // comments with sign
				this.showExecutableText = this.executableColumnOnly[j].columnId;
				
			}
			else if (this.executableColumnOnly[j].ColumnType == 9) {
				this.showExecutableText = this.executableColumnOnly[j].columnId;
			}
			else if (this.executableColumnOnly[j].ColumnType == 10) {
				this.showExecutableText = this.executableColumnOnly[j].columnId;
				if(this.showRecordButton ){
				this.executableColumnOnly[j].value = localStorage.valgen_ExecutionComments;
				}
			}

			//console.log(this.executableColumnOnly[j].value.$("<a>").html());



			if((this.executableColumnOnly[j].ColumnType != 1 && this.executableColumnOnly[j].ColumnType != 9 ))
			{
				//this.showRecordButton = true;     || this.executableColumnOnly[j].ColumnType != 2 || this.executableColumnOnly[j].ColumnType != 10
				this.textboxLength = true;
				

			}

		}
		// console.log($(detail[detail.length - 1]));
		const myclass = $(detail[detail.length - 1]);
		 console.log(myclass);
		 console.log(myclass[0].className);
		this.pTagClass = myclass[0].className;
		if(myclass[0].children.length > 0){

			this.spanTagClass = myclass[0].children[0].className;
		}
		
		// console.log(myclass[0].children[0].className);
	}

	getOldHtml(newHtml, columnId) {
		const oldHtml = this.documentTableDetails[columnId];
		//console.log("OLD HTML ", oldHtml)
		const checkAtag = $(oldHtml).find('a');
		if (checkAtag.length) {
			const div = document.createElement('div');
			//console.log(div)
			div.innerHTML = oldHtml;
			const elements = div.getElementsByTagName('a');
			//console.log(elements)
			elements[0].parentNode.removeChild(elements[0]);
			// const result = div.innerHTML;
			const result = oldHtml;
			//console.log(result)
			return `${newHtml} <p class=${this.pTagClass}><b><u>Old Value</u></b></p><strike>${result}</strike>`;
		} else {
			
			const result = oldHtml;
			//console.log(result)
			return `${newHtml} <p class=${this.pTagClass}><b><u>Old Value</u></b></p><strike>${result}</strike>`;
		}
	}

	updateHtmlDoc(value: any, columnId: any, rowId: any) {
		//console.log("this.tableData  ", this.tableData)
		const id = `#${this.tableData.HtmlDocTableId} tbody`;
		const div = document.createElement('html');
		//console.log("this.documentHtmlString  ", this.documentHtmlString)
		div.innerHTML = this.documentHtmlString;

		const row = $(div).find(id).find('tr').eq(rowId);
		//console.log("Row  ", row)
		const col = row.find('td').eq(columnId);
		//console.log("Col ", col)
		col.html(value);
		const doc = this.addHtmlTag($(div).html());
	//console.log("doc ", doc)
		return doc;
	}

	bufferToBase64(data) {
		// return btoa(data.replace(/[\u00A0-\u2666]/g, function (c) {
		// 	return '&#' + c.charCodeAt(0) + ';';
		// }));
		return Base64.encode(data);
	}

	// base64 = btoa(str.replace(/[\u00A0-\u2666]/g, function(c) {
	// 	return '&#' + c.charCodeAt(0) + ';';
	// }));

	async reasonEditSheet() {
	//	console.log('enter');
		const alert = await this._alertCtrl.create({
			header: 'Reason for Edit',
			backdropDismiss: false,
			inputs: [
				{
					name: 'Reason',
					type: 'text',
					placeholder: 'Reason for Edit',
					id: 'editReasonId'
				}
			],
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel'
				},
				{
					text: 'Save',
					handler: (data) => {
						//to see the object
						// this.electronicSignature(data.Reason, true);
						// this.recordListLocal(data.Reason, true);
						

						 if(data.Reason.trim() != ""){
							this.checkTaskEsign(data.Reason, true);
						 }
						 else
						 {  if (this.appPlatform === "Electron") {
							this.toastController("Enter Reason");
						}
						else {
							this.toast.show("Enter Reason", '10000', 'bottom').subscribe(
								toast => {
								}
							);
						}
							return false;
						 }
						
					}
				}
			
			]
		});
		await alert.present()
		.then(() => {
			document.getElementById('editReasonId').focus();
		  })
	}

	async reasonNonExecEditSheet() {
		const alert = await this._alertCtrl.create({
			header: 'Reason for Edit',
			backdropDismiss: false,
			inputs: [
				{
					name: 'Reason',
					type: 'text',
					placeholder: 'Reason for Edit',
					id: 'editReasonId'
				}
			],
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel'
				},
				{
					text: 'Save',
					handler: (data) => {
						if(data.Reason.trim() != ""){
							this.recordListLocalNonExec(data.Reason);
						 }
						 else
						 { if (this.appPlatform === "Electron") {
							this.toastController("Enter Reason");
						}
						else {
							this.toast.show("Enter Reason", '10000', 'bottom').subscribe(
								toast => {
								}
							);
						}
							return false;
						 }

					}
				}
			
			]
		});
		await alert.present()
		.then(() => {
			document.getElementById('editReasonId').focus();
		  })
	}

	public checkTaskEsign(reason?, status?) {
		this.proceedExecution = true;
		this.electronicExec.forEach((exec) => {
           if(exec.ColumnType == 9 || exec.ColumnType == 1){
			  exec.value = exec.value.replace(/\&nbsp;/g, '');
			  exec.value = exec.value.trim();
			  if(exec.value == ''){
				  this.proceedExecution = false;
			  }  
		  }
		})


		if(this.proceedExecution)  // Check for all Executable Columns has value
		{


           this._localpdb.getTaskById(this.exeId).subscribe(
			(data) => {

				// console.log("getTaskById  ", data)
				this.EsignVerifyData = data;
				//console.log("this.showEsignPopup  ", this.showEsignPopup)
				// console.log("this.EsignRowReq  ", this.EsignRowReq)
				// console.log("this.EsignTableReq ", this.EsignTableReq)
				// console.log("this.VerifyRowReq  ", this.VerifyRowReq)
                // console.log("this.VerifyTableReq  ", this.VerifyTableReq)
				if (data.EsignReq === 3) {
					if (this.showEsignPopup) {       // Check it is an last row in a table
						if (this.EsignTableReq) {    // Esign req for Table level (ColumnType === 13)
							this.electronicSignature(reason, status);
						}
						else {
							if (this.VerifyRowReq) {  // Verify req for Table level (ColumnType === 15)			
								this.verifyRowButton = true;
							}
							else if(this.VerifyTableReq){
								this.verifyTableButton = true;
							}
							else if(this.EsignRowReq){
								this.electronicSignature(reason, status);
							}
							else{
                                this.recordListLocal(reason, status);
							}
							
						}
					} else {
						if (this.EsignRowReq) {    // Esign req for Row level (ColumnType === 12)
							this.electronicSignature(reason, status);
						}
						else {
							if (this.VerifyRowReq) {
								this.verifyRowButton = true;				
								
							}
							else{
                                this.recordListLocal(reason, status);
							}
							
						}
					}
				}
				else {
					if (data.EsignReq === 1 && data.EsignType === 1) {
						if (this.showEsignPopup) { // Check for last Row i.e Table level
							this.electronicSignature(reason, status);
						} else {
							this.recordListLocal(reason, status);
						}
					} else if (data.EsignReq === 1 && data.EsignType === 2) {  // Row Level
						this.electronicSignature(reason, status);
					}
					else {  // other conditions i.e EsignReq == 0 or any other number
						this.recordListLocal(reason, status);
					}
				}



				
			},
			(err) => {
				console.log('data', err);
			}
		);

		}
		else{
			this.alertPopup('Enter all Executable column');

		}
		
	}

	public electronicSignature(reason?, editStatus?) {
		$('#electronicSignature').modal('show');
		var eSignVerbiage = localStorage.getItem('valgen_EsignVerbiageReq');
		if( eSignVerbiage == 'True'){
            this.EsignVerbiageText = localStorage.getItem('valgen_EsignVerbiageText');
		}
		else{
			this.EsignVerbiageText = "";
		}
		this.reason = reason;
		this.statusEdit = editStatus;
		this.activity = 'Electronic Execution';
		this.dateTime = moment().format('MM/DD/YYYY h:mm:ss a') + '(' + momenttz.tz(momenttz.tz.guess()).zoneAbbr() + ')';
		this.loggedIn = `${localStorage.getItem('valgen_firstName')} ${localStorage.getItem('valgen_lastName')}`;
	}


	public checkElectSign(username, password) {
		// console.log("this.EsignVerifyData ", this.EsignVerifyData)
		const encryptValue = this._encrDecr.encryptCheck(password);
		if (username.toLowerCase() === localStorage.getItem('valgen_userName').toLowerCase()) {
			localStorage.setItem('valgen_esign_username', username);
			if (encryptValue) {
				$('#electronicSignature').modal('hide');
				this.password = '';
				this.alertBox = false;
				if (this.EsignVerifyData.EvidenceRequired === 3) {   //  verification Req or not for this task							
					if (this.VerifyRowReq) {
							this.verifyRowButton = true;	  // Enable the Verify Button	
						}	
						else if(this.VerifyTableReq){
							this.verifyTableButton = true;
						}
						else
						{
							// No Verify, only Esign means below is called
							 if (this.showEsignPopup) {       // Check it is an last row in a table
									if (this.EsignTableReq) { 
										this.EVcolumn = true;  
								        this.recordListLocal(this.reason, this.statusEdit, this.EVcolumn);
									}
									else if (this.EsignRowReq) { 
										this.recordListLocal(this.reason, this.statusEdit, this.EVcolumn);
									}
								}
								else{
									if (this.EsignRowReq) { 
										this.recordListLocal(this.reason, this.statusEdit, this.EVcolumn);
									}
								}
						}
				}
				else {
					if (this.EsignVerifyData.EvidenceRequired === 1 && this.EsignVerifyData.EvidenceType === 1) { // table
						if (this.showVerifyPopup) {
							this.executionVerificationByUserForTable(this.reason, status);
							if(!this.VerifyTableReq)
							{
								this.verifyInExecutable = true;
							}
						} else {
							this.recordListLocal(this.reason, status);
						}
					}
					else if (this.EsignVerifyData.EvidenceRequired === 1 && this.EsignVerifyData.EvidenceType === 2) { // row
						this.executionVerificationByUser(this.reason, status);
						if(!this.VerifyTableReq)
							{
								this.verifyInExecutable = true;
							}
					}
					else{
						this.recordListLocal(this.reason, status);
					}
				}
			} else {
				this.alertBox = true;
				this.password='';
				setTimeout(()=> this.alertBox = false,2500);
			}
		} else {
			this.alertBox = true;
			this.password='';
			setTimeout(()=> this.alertBox = false,2500);
		}
	}

	public closeEsignModal() {
		this.password='';
		$('#electronicSignature').modal('hide');
		$('#executionVerificationByUser').modal('hide');
	}
	
	public verifyRowEnable(){
		// console.log("Verify Enabled");
		this.executionVerificationByUser(this.reason, status);
	
	}
	public verifyTableEnable(){
		this.verifyTableButton = false;
		this.executionVerificationByUserForTable(this.reason, status);

	}
	// executionVerificationByUser
	public executionVerificationByUser(reason?, editStatus?) {
		this.verifyUserList = [];
		this.reason = reason;
		this.statusEdit = editStatus;
		this.activity = 'Verification Execution Task';
		this.dateTime = moment().format('MM/DD/YYYY h:mm:ss a') + '(' + momenttz.tz(momenttz.tz.guess()).zoneAbbr() + ')';
		this._localpdb.getAllVerificationUsers(this.exeId).subscribe(
			(data) => {
				data.forEach((verify) => {
					if (this.EsignVerifyData.EvidenceRequired === 3) {
					if(verify.DevTableID == this.tableId){
                        this.verifyUserList.push(verify);
					}
				}
				else{
					this.verifyUserList.push(verify);
				}
				})
				
				$('#executionVerificationByUser').modal('show');
			}
		);
	}

	public checkVerificationByUser(selectedUser, password) {
		const devTableId = localStorage.DevTableId;
		this._localpdb.getVerifyUserById(this.exeId, selectedUser, devTableId).subscribe(
			(data) => {
				// console.log("Dataaaa  ", data)
				const encryptValue = this._encrDecr.encryptSelectedUserCheck(data, password);
				if (encryptValue) {
					$('#executionVerificationByUser').modal('hide');
					this.password = '';
					this.selectedUser = '';
					this.selectedUserName = '';
					this.alertBox = false;
				//   console.log("this.showVerifyPopup  ", this.showVerifyPopup)
				//   console.log("this.VerifyTableReq   ", this.VerifyTableReq)
					if (this.showVerifyPopup) {

						if (this.VerifyTableReq) {
									this.verifyTableButton = true;	
									this.verifyRowButton = false;
									// console.log("this.verifyTableButton   ", this.verifyTableButton)					
									
								}
								else{
									this.verifyRowButton = false;
									this.recordListLocal(this.reason, status);
								}	
					}
					else {						
						this.verifyRowButton = false;					
						this.recordListLocal(this.reason, this.statusEdit, this.EVcolumn, false, data.FullName);				
					}


				} else {
					this.alertBox = true;
					this.password='';
					setTimeout(()=> this.alertBox = false,2500);
				}
			}
		);
	}

	public onChangeOfVerifyUser(userId) {
		this.verifyUserList.forEach((user) => {
			if (user.UserID == userId) {
				this.selectedUserName = user.FullName;
				localStorage.selectedUserName = this.selectedUserName;
				localStorage.DevTableId = user.DevTableID;
			}
		})
	}

	public onChangeOfVerifyUserTable(userId) {
		this.selectedUserName = "";
		this.verifyUserList.forEach((user) => {
			if (user.UserID == userId) {
				this.selectedUserNameTable = user.FullName;
				localStorage.DevTableId = user.DevTableID;
			}
		})
	}

	public closeVerificationByUser() {
		$('#executionVerificationByUser').modal('hide');
	}

	// verification for table level
	public executionVerificationByUserForTable(reason?, editStatus?) {
		this.verifyUserList = [];
		this.reason = reason;
		this.statusEdit = editStatus;
		this.activity = 'Verification Execution Task';
		this.dateTime = moment().format('MM/DD/YYYY h:mm:ss a') + '(' + momenttz.tz(momenttz.tz.guess()).zoneAbbr() + ')';

		this._localpdb.getAllVerificationUsers(this.exeId).subscribe(
			(data) => {
				data.forEach((verify) => {
					if (this.EsignVerifyData.EvidenceRequired === 3) {
						if(verify.DevTableID == this.tableId){
							this.verifyUserList.push(verify);
						}
					}
					else{
						this.verifyUserList.push(verify);
					}
				})
				$('#executionVerificationByUserForTable').modal('show');
		// this._localpdb.getAllVerificationUsers(this.exeId).subscribe(
		// 	(data) => {
		// 		this.verifyUserList = data;
				
			}
		);
	}

	public checkVerificationByUserForTable(selectedUser, password) {
		const devTableId = localStorage.DevTableId;
		this._localpdb.getVerifyUserById(this.exeId, selectedUser,devTableId).subscribe(
			(data) => {
				const encryptValue = this._encrDecr.encryptSelectedUserCheck(data, password);
				if (encryptValue) {
					$('#executionVerificationByUserForTable').modal('hide');
					this.password = '';
					this.selectedUserTable = '';
					this.selectedUserNameTable = '';
					this.alertBox = false;
					localStorage.selectedUserNameTable = data.FullName;
					localStorage.verifycolumn = true;
					this.VerifyRowReq = false;
					this.EVcolumn = true;
					//if (!this.EsignRowReq && !this.EsignTableReq && !this.VerifyTableReq && !this.VerifyRowReq) {
						this.recordListLocal(this.reason, this.statusEdit, this.EVcolumn, true, data.FullName);
					//}

				} else {
					this.alertBox = true;
					this.password='';
					setTimeout(()=> this.alertBox = false,
					2500);
				}
			}
		);
	}



	public closeVerificationByUserForTable() {

this.password='';
		$('#executionVerificationByUserForTable').modal('hide');
	}


	recordListLocalNonExec(reason) {
		this.editNonExe = !this.editNonExe;
		const ExecutionDocument = [];
		let doc: any;
		this.electronicExec.forEach((col) => {
			let tempCol = col;
			let ColumnText = '';
			const userName = localStorage.getItem('valgen_userName');
			const firstName = localStorage.getItem('valgen_firstName');
			const lastName = localStorage.getItem('valgen_lastName');
			const timeStamp = moment().format(localStorage.getItem('valgen_DataValue'));
			const timeZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
			this.oldelectronicExec = JSON.parse(localStorage.electronicExec);
			for (let i = 0; i < this.nonexecutableColumnArray.length; i++) {
				if (col.ColumnType === this.nonexecutableColumnArray[i]) {
					for (let j = 0; j < this.oldelectronicExec.length; j++) {
						if (col.columnId == this.oldelectronicExec[j].columnId) {
							if (col.value != this.oldelectronicExec[j].value) {
							 	// console.log(this.pTagClass);
		                        // console.log(this.spanTagClass);
								// ColumnText = `<p class=${this.pTagClass}><span class=${this.spanTagClass}>${col.value}</span></p><p class=${this.pTagClass}><span class=${this.spanTagClass} style="text-decoration: underline;font-weight: bold;">Reason:</span><span class=${this.spanTagClass}>${reason}</span></p>`;
								// ColumnText = `<p ><span  style="text-decoration: underline;font-weight: bold;">New Value</span></p>${ColumnText}`;
								// ColumnText = `${ColumnText}<p class=${this.pTagClass}><b><u>Edited By:</u></b> <p class=${this.pTagClass}> ${firstName} ${lastName} (${userName})-${timeStamp}-(${timeZone})</p></p>`;
								// ColumnText = this.getOldHtml(ColumnText, col.columnId);
								// console.log("COLUMN sa ", ColumnText)
								ColumnText = `<p class=${this.pTagClass}>${col.value}</p>`;
								ColumnText = `<p class=${this.pTagClass}><b><u>Reason for Edit:</u></b><br>${reason}</p><p class=${this.pTagClass} ><b><u>New Value:-</u></b></p>${ColumnText}`;
								ColumnText = `${ColumnText}<p class=${this.pTagClass}><b><u>Edited By:</u></b> <p class=${this.pTagClass}> ${firstName} ${lastName} (${userName})-${timeStamp}-(${timeZone})</p></p>`;
								ColumnText = this.getOldHtml(ColumnText, col.columnId);
								 console.log("COLUMN sa ", ColumnText)
								const updatedColumn = {
									ExecutionID: Number(this.exeId),
									ExeTableID: this.tableId,
									ExRowID: col.rowId + 1,
									ExColumnID: col.columnId + 1,
									ExResult: this.result,
									Content: col.value, 
									Version: 1,
									Reason: '',
									Flag: 1,
									CreatedBy: this.userID,
									CreatedOn: moment().format(localStorage.getItem('valgen_DataValue')),
									OriginalExeBy: this.userID,
									ClientOffset: new Date().getTimezoneOffset(),
									ClientZone: momenttz.tz(momenttz.tz.guess()).zoneAbbr()
								};
								ExecutionDocument.push(updatedColumn);
								doc = this.updateHtmlDoc(ColumnText, col.columnId, col.rowId);
								this.documentHtmlString = doc;
								break;
							}
							else {
								ColumnText = `<p class=${this.pTagClass}>${col.value}</p>`;
								const updatedColumn = {
									ExecutionID: Number(this.exeId),
									ExeTableID: this.tableId,
									ExRowID: col.rowId + 1,
									ExColumnID: col.columnId + 1,
									ExResult: this.result,
									Content: col.value,
									Version: 1,
									Reason: '',
									Flag: 1,
									CreatedBy: this.userID,
									CreatedOn: moment().format(localStorage.getItem('valgen_DataValue')),
									OriginalExeBy: this.userID,
									ClientOffset: new Date().getTimezoneOffset(),
									ClientZone: momenttz.tz(momenttz.tz.guess()).zoneAbbr()
								};
								ExecutionDocument.push(updatedColumn);
								doc = this.updateHtmlDoc(ColumnText, col.columnId, col.rowId);
								this.documentHtmlString = doc;
								break;
							}

						}

					}
				}
				else if (col.ColumnType === 5) {
					ColumnText = `<p class=${this.pTagClass}>${col.value}</p>`;
					const updatedColumn = {
						ExecutionID: Number(this.exeId),
						ExeTableID: this.tableId,
						ExRowID: col.rowId + 1,
						ExColumnID: col.columnId + 1,
						ExResult: this.result,
						Content: col.value,
						Version: 1,
						Reason: '',
						Flag: 1,
						CreatedBy: this.userID,
						CreatedOn: moment().format(localStorage.getItem('valgen_DataValue')),
						OriginalExeBy: this.userID,
						ClientOffset: new Date().getTimezoneOffset(),
						ClientZone: momenttz.tz(momenttz.tz.guess()).zoneAbbr()
					};
					ExecutionDocument.push(updatedColumn);
					doc = this.updateHtmlDoc(ColumnText, col.columnId, col.rowId);
					this.documentHtmlString = doc;
					break;
				}
			}
		});
		localStorage.removeItem('electronicExec');


		async.parallel([
			(done) => {
				this._localpdb.updateLatestTableDetails(ExecutionDocument, this.exeId).subscribe(
					(tableDetail) => {
						done();
					}
				);
			},
			(done) => {
				const htmlBase64 = this.bufferToBase64(doc);
				this._localpdb.updateDocumentDetail(htmlBase64, this.exeId).subscribe(
					(data) => {
						done();
					}
				);

			},
			(done) => {
				this._localpdb.updateTaskById(this.exeId, 'statusPush').subscribe(
					(data) => {
						done();
					},
					(err) => {
						console.log('updateTaskById', err);
					}
				);
			}
		], () => {
			this.CurrentRowChanged(1, true);
		});
		this.executableColumnOnly = [];
		this.nonExecutableColumn = [];
		this.passButton = false;
		this.failButton = false;
		this.naButton = false;
	}

	htmlconvertImage(ColumnText)
	{
		const div = document.createElement('div');
						console.log(div)
						div.innerHTML = ColumnText;
						const elements = div.getElementsByTagName('a');
						console.log(elements)
						return elements;
	}



      updateRecordedImage(htmlAtag){
					console.log(htmlAtag)
					// if(htmlAtag.length > 0){
					// 	for(var i=0;i< htmlAtag.length; i++){
					// 		for(var j=0;j< this.evidenceList.length;j++){
					// 			if(this.evidenceList[j]._id == htmlAtag[i].className)
					// 			{
					// 				this._localpdb.updateRecordedImageLocal(this.evidenceList[j], this.evidenceList[j]._id);
					// 			}
					// 		}
					// 	}
					// }
				}

	recordListLocal(reason?, editStatus?, ESigncolumn?, Verifycolumn?, verifyName?) {
		// console.log(reason,editStatus   , ESigncolumn      , Verifycolumn     , verifyName);
		const ExecutionDocument = [];
		let doc: any;
		const timeStamp = moment().format(localStorage.getItem('valgen_DataValue'));
		const timeZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
		// let ColumnText = `<p><b><u>Verified By:</u></b> ${localStorage.selectedUserName} -${timeStamp}-(${timeZone})</p>`;
		// console.log("this.electronicExec  ", this.electronicExec)
		this.electronicExec.forEach((col) => {
			// console.log("Column  ", col)
			if (col.ColumnType === 9 || col.ColumnType === 10) {
				let ColumnText = '';
				if (editStatus) {
					 if (col.ColumnType === 9) {
						if(this.verifyInExecutable)
						{
							ColumnText = `<p class=${this.pTagClass}><b><u>New Value:-</u></b></p><p class=${this.pTagClass}>${col.value}</p><br><p class=${this.pTagClass}><b><u>Verified By:</u></b><p class=${this.pTagClass}> ${localStorage.selectedUserNameTable} -${timeStamp}-(${timeZone})</p></p>`;
							var htmlAtag = this.htmlconvertImage(ColumnText);
			                 this.updateRecordedImage(htmlAtag);
						}
						else{
						ColumnText = `<p class=${this.pTagClass}><b><u>New Value:-</u></b></p><p class=${this.pTagClass}>${col.value}</p>`;
						var htmlAtag = this.htmlconvertImage(ColumnText);
						this.updateRecordedImage(htmlAtag);
						}
					 }
					 else{
						ColumnText = `<p class=${this.pTagClass}><b><u>New Value:-</u></b></p><p class=${this.pTagClass}>${col.value}</p>`;  // for comment
						
					 }
					
					}
					else {
						ColumnText = `<p class=${this.pTagClass}>${col.value}</p>`;  // First Time Execute
						var htmlAtag = this.htmlconvertImage(ColumnText);
			                 this.updateRecordedImage(htmlAtag);
					}
					if (!this.showRecordButton) {
						ColumnText = `<p class=${this.pTagClass}><b><u>Reason for Edit:</u></b><p class=${this.pTagClass}>${reason}</p></p><p class=${this.pTagClass}>${ColumnText}</p>`;
						var htmlAtag = this.htmlconvertImage(ColumnText);
						this.updateRecordedImage(htmlAtag);
						ColumnText = this.getOldHtml(ColumnText, col.columnId);
					}
				
				const updatedColumn = {
					ExecutionID: Number(this.exeId),
					ExeTableID: this.tableId,
					ExRowID: col.rowId + 1,
					ExColumnID: col.columnId + 1,
					ExResult: this.result,
					Content: col.value,
					Version: 1,
					Reason: '',
					Flag: 1,
					CreatedBy: this.userID,
					CreatedOn: moment().format(localStorage.getItem('valgen_DataValue')),
					OriginalExeBy: this.userID,
					ClientOffset: new Date().getTimezoneOffset(),
					ClientZone: momenttz.tz(momenttz.tz.guess()).zoneAbbr()
				};
				ExecutionDocument.push(updatedColumn);
				doc = this.updateHtmlDoc(ColumnText, col.columnId, col.rowId);
				this.documentHtmlString = doc;
				//console.log("col.ColumnType == 9 ", doc)
			}

			if (col.ColumnType === 1 || col.ColumnType === 2) {

				const userName = localStorage.getItem('valgen_userName');
				const firstName = localStorage.getItem('valgen_firstName');
				const lastName = localStorage.getItem('valgen_lastName');
				const timeStamp = moment().format(localStorage.getItem('valgen_DataValue'));
				const timeZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();

				let ColumnText = '';
				if (editStatus) {
					if (col.ColumnType === 1) {
						if(this.verifyInExecutable)
						{
							ColumnText = `<p class=${this.pTagClass}><b><u>New Value:-</u></b></p><p class=${this.pTagClass}>${col.value}</p><br><p class=${this.pTagClass}><b><u>Executed By:</u></b><p class=${this.pTagClass}> ${firstName} ${lastName} (${userName})-${timeStamp}-(${timeZone})</p></p><br><p class=${this.pTagClass}><b><u>Verified By:</u></b><p class=${this.pTagClass}> ${localStorage.selectedUserNameTable} -${timeStamp}-(${timeZone})</p></p>`;
							var htmlAtag = this.htmlconvertImage(ColumnText);
							this.updateRecordedImage(htmlAtag);
						}
						else{
							ColumnText = `<p class=${this.pTagClass}><b><u>New Value:-</u></b></p><p class=${this.pTagClass}>${col.value}</p><br><p class=${this.pTagClass}><b><u>Executed By:</u></b><p class=${this.pTagClass}> ${firstName} ${lastName} (${userName})-${timeStamp}-(${timeZone})</p></p>`;
							var htmlAtag = this.htmlconvertImage(ColumnText);
							this.updateRecordedImage(htmlAtag);
						}
					}
					else {
						ColumnText = `<p class=${this.pTagClass}><b><u>New Value:-</u></b></p><p class=${this.pTagClass}>${col.value}</p><br><p class=${this.pTagClass}><b><u>Executed By:</u></b><p class=${this.pTagClass}> ${firstName} ${lastName} (${userName})-${timeStamp}-(${timeZone})</p></p>`;
						//this.htmlconvertImage(ColumnText);
					}
				} else {
					ColumnText = `<p class=${this.pTagClass}>${col.value}</p><br><p class=${this.pTagClass}><b><u>Executed By:</u></b><p class=${this.pTagClass}> ${firstName} ${lastName} (${userName})-${timeStamp}-(${timeZone})</p></p>`;
					var htmlAtag = this.htmlconvertImage(ColumnText);
			                 this.updateRecordedImage(htmlAtag);
				}
				if (!this.showRecordButton) {
					ColumnText = `<p class=${this.pTagClass}><b><u>Reason for Edit:</u></b><p class=${this.pTagClass}>${reason}</p></p><p class=${this.pTagClass}>${ColumnText}</p>`;
					var htmlAtag = this.htmlconvertImage(ColumnText);
			                 this.updateRecordedImage(htmlAtag);
					ColumnText = this.getOldHtml(ColumnText, col.columnId);
				}
				const updatedColumn = {
					ExecutionID: Number(this.exeId),
					ExeTableID: this.tableId,
					ExRowID: col.rowId + 1,
					ExColumnID: col.columnId + 1,
					ExResult: this.result,
					Content: col.value,
					Version: 1,
					Reason: '',
					Flag: 1,
					CreatedBy: this.userID,
					CreatedOn: moment().format(localStorage.getItem('valgen_DataValue')),
					OriginalExeBy: this.userID,
					ClientOffset: new Date().getTimezoneOffset(),
					ClientZone: momenttz.tz(momenttz.tz.guess()).zoneAbbr()
				};
				ExecutionDocument.push(updatedColumn);
				doc = this.updateHtmlDoc(ColumnText, col.columnId, col.rowId);
				this.documentHtmlString = doc;
				// console.log("col.ColumnType == 9 ", updatedColumn)
			}
		});

		for (let i = 0; i < this.tableMappingDetails.length; i++) {
			const column = this.tableMappingDetails[i];
			if (column.ColumnType === 3) {  //  3 is Result column & 2 is Comment column
				const userName = localStorage.getItem('valgen_userName');
				const firstName = localStorage.getItem('valgen_firstName');
				const lastName = localStorage.getItem('valgen_lastName');
				const timeStamp = moment().format(localStorage.getItem('valgen_DataValue'));
				const timeZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
				let ColumnText = `<p class=${this.pTagClass}>${this.statusArray[this.result - 1]}</p><br><p class=${this.pTagClass}><b><u>Executed By:</u></b><p class=${this.pTagClass}> ${firstName} ${lastName} (${userName})-${timeStamp}-(${timeZone})</p></p>`;
				if (!this.showRecordButton) {
					ColumnText = `<p class=${this.pTagClass}><b><u>New Value:-</u></b></p><p class=${this.pTagClass}>${ColumnText}</p>`;
					ColumnText = this.getOldHtml(ColumnText, i);
				}
				ExecutionDocument.push({
					ExecutionID: Number(this.exeId),
					ExeTableID: this.tableId,
					ExRowID: this.rowId + 1,
					ExColumnID: column.ColumnID,
					ExResult: this.result,
					Content: `~${this.result}~`,
					ColumnText,
					Version: 1,
					Reason: '',
					Flag: 1,
					CreatedBy: this.userID,
					CreatedOn: moment().format(localStorage.getItem('valgen_DataValue')),
					OriginalExeBy: this.userID,
					ClientOffset: new Date().getTimezoneOffset(),
					ClientZone: momenttz.tz(momenttz.tz.guess()).zoneAbbr()
				});
				doc = this.updateHtmlDoc(ColumnText, i, this.rowId);
				this.documentHtmlString = doc;
				//console.log("col.ColumnType == 3 ", doc)
			}

			if (column.ColumnType === 11) {     //  11 is ResultWithout Sign column & 10 is CommentWithout Sign column
				let ColumnText = `<p class=${this.pTagClass}>${this.statusArray[this.result - 1]}</p>`;
				if (!this.showRecordButton) {
					ColumnText = `<p class=${this.pTagClass}><b><u>New Value:-</u></b></p><p class=${this.pTagClass}>${ColumnText}</p>`;
					ColumnText = this.getOldHtml(ColumnText, i);
				}
				ExecutionDocument.push({
					ExecutionID: Number(this.exeId),
					ExeTableID: this.tableId,
					ExRowID: this.rowId + 1,
					ExColumnID: column.ColumnID,
					ExResult: this.result,
					Content: `~${this.result}~`,
					ColumnText,
					Version: 1,
					Reason: '',
					Flag: 1,
					CreatedBy: this.userID,
					CreatedOn: moment().format(localStorage.getItem('valgen_DataValue')),
					OriginalExeBy: this.userID,
					ClientOffset: new Date().getTimezoneOffset(),
					ClientZone: momenttz.tz(momenttz.tz.guess()).zoneAbbr()
				});
				doc = this.updateHtmlDoc(ColumnText, i, this.rowId);
				this.documentHtmlString = doc;
				//console.log("col.ColumnType == 3 ", doc)
			}

			if (column.ColumnType === 6) {   // 6 is signature  
				const userName = localStorage.getItem('valgen_userName');
				const firstName = localStorage.getItem('valgen_firstName');
				const lastName = localStorage.getItem('valgen_lastName');
				const timeStamp = moment().format(localStorage.getItem('valgen_DataValue'));
				const timeZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
				let ColumnText = `<p class=${this.pTagClass}><b><u>Executed By:</u></b> <p class=${this.pTagClass}>${firstName} ${lastName} (${userName})-${timeStamp}-(${timeZone})</p></p>`;
				if (!this.showRecordButton) {
					ColumnText = `<p class=${this.pTagClass}><b><u>New Value:-</u></b></p><p class=${this.pTagClass}>${ColumnText}</p>`;
					ColumnText = this.getOldHtml(ColumnText, i);
				}
				ExecutionDocument.push({
					ExecutionID: Number(this.exeId),
					ExeTableID: this.tableId,
					ExRowID: this.rowId + 1,
					ExColumnID: column.ColumnID,
					ExResult: this.result,
					Content: ColumnText,
					Version: 1,
					Reason: '',
					Flag: 1,
					CreatedBy: this.userID,
					CreatedOn: moment().format(localStorage.getItem('valgen_DataValue')),
					OriginalExeBy: this.userID,
					ClientOffset: new Date().getTimezoneOffset(),
					ClientZone: momenttz.tz(momenttz.tz.guess()).zoneAbbr()
				});
				doc = this.updateHtmlDoc(ColumnText, i, this.rowId);
				this.documentHtmlString = doc;
				//console.log("col.ColumnType == 6 ", doc)
			}
			
			if (column.ColumnType === 7) {  //  7 is VerifiedBy column
				// localStorage.selectedUserName
				const userName = localStorage.getItem('valgen_userName');
				const firstName = localStorage.getItem('valgen_firstName');
				const lastName = localStorage.getItem('valgen_lastName');
				const timeStamp = moment().format(localStorage.getItem('valgen_DataValue'));
				const timeZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
				let ColumnText = `<p class=${this.pTagClass}><b><u>Verified By:</u></b><p class=${this.pTagClass}> ${localStorage.selectedUserNameTable} -${timeStamp}-(${timeZone})</p></p>`;
				if (!this.showRecordButton) {
					ColumnText = `<p class=${this.pTagClass}><b><u>New Value:-</u></b></p><p class=${this.pTagClass}>${ColumnText}</p>`;
					ColumnText = this.getOldHtml(ColumnText, i);
				}
				ExecutionDocument.push({
					ExecutionID: Number(this.exeId),
					ExeTableID: this.tableId,
					ExRowID: this.rowId + 1,
					ExColumnID: column.ColumnID,
					ExResult: this.result,
					Content: `~${this.result}~`,
					ColumnText,
					Version: 1,
					Reason: '',
					Flag: 1,
					CreatedBy: this.userID,
					CreatedOn: moment().format(localStorage.getItem('valgen_DataValue')),
					OriginalExeBy: this.userID,
					ClientOffset: new Date().getTimezoneOffset(),
					ClientZone: momenttz.tz(momenttz.tz.guess()).zoneAbbr()
				});
				doc = this.updateHtmlDoc(ColumnText, i, this.rowId);
				this.documentHtmlString = doc;
				localStorage.removeItem('selectedUserNameTable');
				localStorage.removeItem('verifycolumn');
				//console.log("col.ColumnType == 3 ", doc)
			}

			if (column.ColumnType === 12) {   //  12 is EsignRow 
				const userName = localStorage.getItem('valgen_userName');
				const firstName = localStorage.getItem('valgen_firstName');
				const lastName = localStorage.getItem('valgen_lastName');
				const timeStamp = moment().format(localStorage.getItem('valgen_DataValue'));
				const timeZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
				let ColumnText = `<p class=${this.pTagClass}><b><u>Executed By:</u></b> <p class=${this.pTagClass}>${firstName} ${lastName} (${userName})-${timeStamp}-(${timeZone})-(Esign)</p></p>`;
				if (!this.showRecordButton) {
					ColumnText = `<p class=${this.pTagClass}><b><u>New Value:-</u></b></p><p class=${this.pTagClass}>${ColumnText}</p>`;
					ColumnText = this.getOldHtml(ColumnText, i);
				}
				ExecutionDocument.push({
					ExecutionID: Number(this.exeId),
					ExeTableID: this.tableId,
					ExRowID: this.rowId + 1,
					ExColumnID: column.ColumnID,
					ExResult: this.result,
					Content: ColumnText,
					Version: 1,
					Reason: '',
					Flag: 1,
					CreatedBy: this.userID,
					CreatedOn: moment().format(localStorage.getItem('valgen_DataValue')),
					OriginalExeBy: this.userID,
					ClientOffset: new Date().getTimezoneOffset(),
					ClientZone: momenttz.tz(momenttz.tz.guess()).zoneAbbr()
				});
				doc = this.updateHtmlDoc(ColumnText, i, this.rowId);
				this.documentHtmlString = doc;
				//console.log("col.ColumnType == 6 ", doc)
			}

			 if (ESigncolumn) {
				if (column.ColumnType === 13) {         // 13 is EsignTable
					const userName = localStorage.getItem('valgen_userName');
					const firstName = localStorage.getItem('valgen_firstName');
					const lastName = localStorage.getItem('valgen_lastName');
					const timeStamp = moment().format(localStorage.getItem('valgen_DataValue'));
					const timeZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
					let ColumnText = `<p class=${this.pTagClass}><b><u>Executed By:</u></b><p class=${this.pTagClass}> ${firstName} ${lastName} (${userName})-${timeStamp}-(${timeZone})-(Esign)</p></p>`;
					if (!this.showRecordButton) {
						ColumnText = `<p class=${this.pTagClass}><b><u>New Value:-</u></b></p><p class=${this.pTagClass}>${ColumnText}</p>`;
						ColumnText = this.getOldHtml(ColumnText, i);
					}
					ExecutionDocument.push({
						ExecutionID: Number(this.exeId),
						ExeTableID: this.tableId,
						ExRowID: this.rowId + 1,
						ExColumnID: column.ColumnID,
						ExResult: this.result,
						Content: ColumnText,
						Version: 1,
						Reason: '',
						Flag: 1,
						CreatedBy: this.userID,
						CreatedOn: moment().format(localStorage.getItem('valgen_DataValue')),
						OriginalExeBy: this.userID,
						ClientOffset: new Date().getTimezoneOffset(),
						ClientZone: momenttz.tz(momenttz.tz.guess()).zoneAbbr()
					});
					doc = this.updateHtmlDoc(ColumnText, i, this.rowId);
					this.documentHtmlString = doc;
					// console.log("col.ColumnType == 6 ", doc)
				}

			 }

			if (column.ColumnType === 14) {  //  3 is Result column & 2 is Comment column
				// localStorage.selectedUserName
				const userName = localStorage.getItem('valgen_userName');
				const firstName = localStorage.getItem('valgen_firstName');
				const lastName = localStorage.getItem('valgen_lastName');
				const timeStamp = moment().format(localStorage.getItem('valgen_DataValue'));
				const timeZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
				let ColumnText = `<p class=${this.pTagClass}><b><u>Verified By:</u></b><p class=${this.pTagClass}> ${localStorage.selectedUserName} -${timeStamp}-(${timeZone})</p></p>`;
				if (!this.showRecordButton) {
					ColumnText = `<p class=${this.pTagClass}><b><u>New Value:-</u></b></p><p class=${this.pTagClass}>${ColumnText}</p>`;
					ColumnText = this.getOldHtml(ColumnText, i);
				}
				ExecutionDocument.push({
					ExecutionID: Number(this.exeId),
					ExeTableID: this.tableId,
					ExRowID: this.rowId + 1,
					ExColumnID: column.ColumnID,
					ExResult: this.result,
					Content: `~${this.result}~`,
					ColumnText,
					Version: 1,
					Reason: '',
					Flag: 1,
					CreatedBy: this.userID,
					CreatedOn: moment().format(localStorage.getItem('valgen_DataValue')),
					OriginalExeBy: this.userID,
					ClientOffset: new Date().getTimezoneOffset(),
					ClientZone: momenttz.tz(momenttz.tz.guess()).zoneAbbr()
				});
				doc = this.updateHtmlDoc(ColumnText, i, this.rowId);
				this.documentHtmlString = doc;
				//console.log("col.ColumnType == 3 ", doc)
			}

			if (localStorage.verifycolumn) {

				if (column.ColumnType === 14) {  //  3 is Result column & 2 is Comment column
					// localStorage.selectedUserName
					const userName = localStorage.getItem('valgen_userName');
					const firstName = localStorage.getItem('valgen_firstName');
					const lastName = localStorage.getItem('valgen_lastName');
					const timeStamp = moment().format(localStorage.getItem('valgen_DataValue'));
					const timeZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
					let ColumnText = `<p class=${this.pTagClass}><b><u>Verified By:</u></b><p class=${this.pTagClass}> ${localStorage.selectedUserName} -${timeStamp}-(${timeZone})</p></p>`;
					if (!this.showRecordButton) {
						ColumnText = `<p class=${this.pTagClass}><b><u>New Value:-</u></b></p><p class=${this.pTagClass}>${ColumnText}</p>`;
						ColumnText = this.getOldHtml(ColumnText, i);
					}
					ExecutionDocument.push({
						ExecutionID: Number(this.exeId),
						ExeTableID: this.tableId,
						ExRowID: this.rowId + 1,
						ExColumnID: column.ColumnID,
						ExResult: this.result,
						Content: `~${this.result}~`,
						ColumnText,
						Version: 1,
						Reason: '',
						Flag: 1,
						CreatedBy: this.userID,
						CreatedOn: moment().format(localStorage.getItem('valgen_DataValue')),
						OriginalExeBy: this.userID,
						ClientOffset: new Date().getTimezoneOffset(),
						ClientZone: momenttz.tz(momenttz.tz.guess()).zoneAbbr()
					});
					doc = this.updateHtmlDoc(ColumnText, i, this.rowId);
					this.documentHtmlString = doc;
					//console.log("col.ColumnType == 3 ", doc)
					localStorage.removeItem('selectedUserName');
				}




				if (column.ColumnType === 15) {  //  3 is Result column & 2 is Comment column
					// localStorage.selectedUserName
					const userName = localStorage.getItem('valgen_userName');
					const firstName = localStorage.getItem('valgen_firstName');
					const lastName = localStorage.getItem('valgen_lastName');
					const timeStamp = moment().format(localStorage.getItem('valgen_DataValue'));
					const timeZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
					let ColumnText = `<p class=${this.pTagClass}><b><u>Verified By:</u></b><p class=${this.pTagClass}> ${localStorage.selectedUserNameTable} -${timeStamp}-(${timeZone})</p></p>`;
					if (!this.showRecordButton) {
						ColumnText = `<p class=${this.pTagClass}><b><u>New Value:-</u></b></p><p class=${this.pTagClass}>${ColumnText}</p>`;
						ColumnText = this.getOldHtml(ColumnText, i);
					}
					ExecutionDocument.push({
						ExecutionID: Number(this.exeId),
						ExeTableID: this.tableId,
						ExRowID: this.rowId + 1,
						ExColumnID: column.ColumnID,
						ExResult: this.result,
						Content: `~${this.result}~`,
						ColumnText,
						Version: 1,
						Reason: '',
						Flag: 1,
						CreatedBy: this.userID,
						CreatedOn: moment().format(localStorage.getItem('valgen_DataValue')),
						OriginalExeBy: this.userID,
						ClientOffset: new Date().getTimezoneOffset(),
						ClientZone: momenttz.tz(momenttz.tz.guess()).zoneAbbr()
					});
					doc = this.updateHtmlDoc(ColumnText, i, this.rowId);
					this.documentHtmlString = doc;
					localStorage.removeItem('selectedUserNameTable');
					localStorage.removeItem('verifycolumn');
					//console.log("col.ColumnType == 3 ", doc)
				}
			}
		}
		async.parallel([
			(done) => {
				this._localpdb.updateLatestTableDetails(ExecutionDocument, this.exeId).subscribe(
					(tableDetail) => {
						done();
						// console.log("updateLatestTableDetails  ", tableDetail)
					}
				);
			},
			(done) => {
				this._localpdb.updateTableStatus(this.exeId, this.tableId).subscribe(
					() => {
						done();
					}
				);
			},
			(done) => {
				//console.log("localStorage.scrimgId  ", localStorage.scrimgId)
				if (localStorage.scrimgId == undefined) {  // if camera not used, Image not taken
					done();
				} else {     // if camera used
					this.snapImageId = parseInt(localStorage.scrimgId);

					const snapId = `${this.exeId}-${this.tableId}-${this.rowId}`;
					this._localpdb.getExecutionSnapDetails(snapId).subscribe(      // search for record in ExecutionSnapDetailsDB
						(data) => {							
							this._localpdb.updateExecutionSnapDetails(this.exeId, this.tableId, this.rowId, this.snapImageId).subscribe(
								(data) => {
									done();
									localStorage.removeItem('scrimgId');
								}
							)

						},
						(err) => {     // if record not found in ExecutionSnapDetailsDB, insert new row in DB
							const inputData = [{
								ExecutionId: parseInt(this.exeId),
								RowId: this.rowId,
								TableId: this.tableId,
								scrimgId: this.snapImageId,
								status: "0"
							}];

							this._localpdb.addExecutionSnapDetails(inputData).subscribe((data) => {
								done();
								localStorage.removeItem('scrimgId');
							})

						}
					);
				}


			},
			(done) => {
				const htmlBase64 = this.bufferToBase64(doc);
				this._localpdb.updateDocumentDetail(htmlBase64, this.exeId).subscribe(
					(data) => {
						done();
					}
				);

			},
			(done) => {
				this._localpdb.updateTaskById(this.exeId, 'statusPush').subscribe(
					(data) => {
						done();
					},
					(err) => {
						console.log('updateTaskById', err);
					}
				);
			}
		], () => {
			this.onRowChanged(1, true);
			// if (this.appPlatform == "Electron") {
			// 	this.toastController("Execution Saved Successfully");
			// }
			// else {
			this.toast.show("Execution Saved Successfully.", '5000', 'bottom').subscribe(
				toast => {
					// console.log(toast);
				}
			);
		// }
		});
		this.executableColumnOnly = [];
		this.nonExecutableColumn = [];
		this.passButton = false;
		this.failButton = false;
		this.naButton = false;
	}

	addHtmlTag(doc) {
		return `<!DOCTYPE html ><html xmlns="http://www.w3.org/1999/xhtml">${doc}</html>`
	}

	async pictureActionSheet(columnId) {

		this.takePicture(this._camera.PictureSourceType.CAMERA, columnId);
		// const actionSheet = await this._actionSheetCtrl.create({
		// 	header: 'Select Image Source',
		// 	buttons: [
		// 		{
		// 			text: 'Load from Library',
		// 			handler: () => {
		// 				this.takePicture(this._camera.PictureSourceType.PHOTOLIBRARY, columnId);
		// 			}
		// 		},
		// 		{
		// 			text: 'Use Camera',
		// 			handler: () => {
		// 				this.takePicture(this._camera.PictureSourceType.CAMERA, columnId);
		// 			}
		// 		},
		// 		{
		// 			text: 'Cancel',
		// 			role: 'cancel'
		// 		}
		// 	]
		// });
		// await actionSheet.present();
	}

	async takePictureElectron(columnId) {
		const image = await Plugins.Camera.getPhoto({
		  quality: 100,
		  allowEditing: true,
		  resultType: CameraResultType.DataUrl,
		  source: CameraSource.Camera,
		  correctOrientation :true
		});
	//	console.log("Image  ", image)
		const imageData = image.dataUrl;
		var base64Img = imageData.replace("data:image/jpeg;base64,", "");
	var base64Img = imageData.replace("data:image/png;base64,", "");
		this.photo = this.sanitizer.bypassSecurityTrustResourceUrl(image && (image.dataUrl));
		console.log("this.photo  ", base64Img)
		const dataTime = `Date:${moment()}`;
			this.scrimgId = 1;
			const snapId = `${this.exeId}-${this.tableId}-${this.rowId}`;
			if (localStorage.scrimgId != undefined) {
				this.scrimgId = parseInt(localStorage.scrimgId) + 1;
				localStorage.scrimgId = this.scrimgId;
				this.cameraImage(this.scrimgId, columnId, imageData);
			} else {
				this._localpdb.getExecutionSnapDetails(snapId).subscribe(
					(data) => {
					//	console.log("getExecutionSnapDetails ", data)
						this.scrimgId = parseInt(data.scrimgId) + 1;
						localStorage.scrimgId = (this.scrimgId);
						this.cameraImage(this.scrimgId, columnId, imageData);
					},
					(err) => {
					//	console.log("getExecutionSnapDetails  error   ", err)
						this.scrimgId = 1;
						localStorage.scrimgId = this.scrimgId;
						this.cameraImage(this.scrimgId, columnId, imageData);
					}
				);
			}
	  }

	public takePicture(sourceType, columnId) {
		const options: CameraOptions = {
			quality: 40,
			destinationType: this._camera.DestinationType.DATA_URL,
			sourceType: sourceType,
			encodingType: this._camera.EncodingType.JPEG,
			mediaType: this._camera.MediaType.PICTURE
		}
		this._camera.getPicture(options).then((imageData) => {
			//console.log("imageData       "+imageData);
			const dataTime = `Date:${moment()}`;
			this.scrimgId = 1;
			const snapId = `${this.exeId}-${this.tableId}-${this.rowId}`;
		//	this.originalImage = 'data:image/jpeg;base64,' + imageData;
			if (localStorage.scrimgId != undefined) {
				this.scrimgId = parseInt(localStorage.scrimgId) + 1;
				localStorage.scrimgId = this.scrimgId;
				this.cameraImage(this.scrimgId, columnId, imageData);
			} else {
				this._localpdb.getExecutionSnapDetails(snapId).subscribe(
					(data) => {
					//	console.log("getExecutionSnapDetails ", data)
						this.scrimgId = parseInt(data.scrimgId) + 1;
						localStorage.scrimgId = (this.scrimgId);
						this.cameraImage(this.scrimgId, columnId, imageData);
					},
					(err) => {
					//	console.log("getExecutionSnapDetails  error   ", err)
						this.scrimgId = 1;
						localStorage.scrimgId = this.scrimgId;
						this.cameraImage(this.scrimgId, columnId, imageData);
					}
				);
			}
		},
			(err) => {
				console.log(err);
			});
	}

 

	public cameraImage(scrimgId, columnId, imageData) {
		const sequenceID = `IMGSCR_${this.tableId}_${this.rowId}_${scrimgId}`;
		const fileName = `${sequenceID}.jpg`;
		const dataTime = `Date:${moment()}`;
		console.log("this.appPlatform  camera   ", this.appPlatform)
		if(this.appPlatform === 'Electron'){
			this.originalImage = imageData;
			this.watermarkFont = '20px Josefin Slab';			
		}
		else
		{
			this.originalImage = 'data:image/jpeg;base64,' + imageData;
			// this.originalImage =  imageData;
			this.watermarkFont = '40px Josefin Slab';
		}
		console.log("this.originalImage  ", this.originalImage)
		fetch(this.originalImage)
		.then(res => res.blob())
		.then(blob => {
		  this.blobImage = blob;
		 this.watermarkImage(fileName,sequenceID, columnId, this.watermarkFont);
		// this.imageWithWaterMark(fileName,sequenceID, this.originalImage, columnId);
		});

	}

	watermarkImage(fileName,sequenceID, columnId, fontsize) {
		
		var day = new Date()
        var dayWrapper = moment(day); 
        var imageTime = dayWrapper.format("YYYY-MM-DD, h:mm:ss"); 
		const ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
		console.log("this.appPlatform  watermark", this.appPlatform)
		
			if(this.appPlatform === 'Electron'){
				var rotate = function(target) {
					var context = target.getContext('2d');		
					var text = `Date:` + imageTime + '(' + ClientZone + ')';
					var metrics = context.measureText(text);
					if( localStorage.valgen_WatermarkPosition === "L    "){
						var x = (target.width ) - (metrics.width - 100);
					}
					else{
						var x = (target.width ) - (metrics.width + 450);
					}
				
				
				var y = (target.height / 2) + 100 ;
				context.translate(x, y);
				context.globalAlpha = 1;
				context.fillStyle = '#0000ff';
				context.font = fontsize;
				context.rotate(-90 * Math.PI / 180);
				context.fillText(text, 0, 0);
				return target;
			  };
			}
			else{
				var rotate = function(target) {
					var context = target.getContext('2d');		
					var text = `Date:` + imageTime + '(' + ClientZone + ')';
					var metrics = context.measureText(text);
					if( localStorage.valgen_WatermarkPosition === 'L    '){
						var x = (target.width ) - (metrics.width + 24);
					}
					else{
						var x = (target.width ) - (metrics.width + 1050);
						
					}
				
				var y = (target.height / 2) + 400 ;
				context.translate(x, y);
				context.globalAlpha = 1;
				context.fillStyle = '#0000ff';
				context.font = fontsize;
				context.rotate(-90 * Math.PI / 180);
				context.fillText(text, 0, 0);
				return target;
			  };
			}

		
		watermark([this.blobImage])
		.image(rotate)
		  .then(img => {			
			var imgData = img.src;			
			this.watermarkedImg = imgData.replace("data:image/png;base64,","");
			this.imageWithWaterMark(fileName,sequenceID, this.watermarkedImg, columnId);
		  });
	
	  }


	  public imageWithWaterMark(fileName,sequenceID, watermarkedImg, columnId){

		if(this.appPlatform === 'Electron'){
			const uploadData = {
				ExUploadFileEntity: [
					{
						ExecutionID: parseInt(this.exeId),
						AttachmentName: fileName,
						SequenceID: sequenceID,
						FileContent: watermarkedImg,
						CreatedBy: parseInt(localStorage.getItem('valgen_userId')),
						CreatedOn: moment().format(localStorage.getItem('valgen_DataValue'))
					}
				]
			};
			this.uploadImageToLocal(uploadData.ExUploadFileEntity[0], fileName, this.exeId, this.tableId, this.rowId, columnId);
		}
		else{
			const uploadData = {
				ExUploadFileEntity: [
					{
						ExecutionID: parseInt(this.exeId),
						AttachmentName: fileName,
						SequenceID: sequenceID,
						FileContent: watermarkedImg,
						CreatedBy: parseInt(localStorage.getItem('valgen_userId')),
						CreatedOn: moment().format(localStorage.getItem('valgen_DataValue'))
					}
				]
			};
			this.uploadImageToLocal(uploadData.ExUploadFileEntity[0], fileName, this.exeId, this.tableId, this.rowId, columnId);
		}
	  }

	public uploadImageToLocal(uploadData, fileName, exeId, tableId, rowId, columnId) {
		uploadData.tableId = tableId;
		uploadData.rowId = rowId;
		this._localpdb.addImageLocal(uploadData).subscribe(
			(data) => {
				this.updateImageLocal( fileName, data.id, columnId)
			},
			(err) => {
				console.log('data', err);
			}
		);
	}

	public updateImageLocal(fileName, id, columnId) {
		this.executableColumnOnly.forEach((col) => {
			if ((col.ColumnType === 9 || col.ColumnType === 1) && col.columnId === columnId) {
				col.value = `${col.value}<a target="_blank" href="javascript:void(0)" class="${id}">[${fileName}]</a>`;
			//	this.getLocalImageDocument(this.exeId);
			};
		});
	}

	public uploadImageToServer(uploadData, fileName) {
		this._homeService.UpdateUploadFiles(uploadData).subscribe((data) => {
			this._homeService.uploadFileName(this.exeId, this.tableId, this.rowId).subscribe(
				(fileData) => {
					this.updateFileData(fileData.Result, fileName);
				});
		});
	}

	public updateFileData(data, fileName) {
		if (data.length) {
			data = data[0];
			this.electronicExec.forEach((col) => {
				if (col.ColumnType === 9 || col.ColumnType === 1) {
					col.value = `${col.value}<a href=${data.HLink}${fileName}>[${fileName}]</a>`
				};
			});
		}
	}

	public includeAttachment(i, list, columnId){
		
		this.updateImageLocal(list.AttachmentName, list._id,columnId);
	}
	public videoCapture() {
		let options: CaptureVideoOptions = { limit: 1 };
		this.mediaCapture.captureVideo(options)
			.then(
				(data: MediaFile[]) => {
					// console.log("Video", data)
					let file = data[0];
					var name = data[0].name;
					if (data.length > 0) {

					}
				},
				(err: CaptureError) => console.error("video err", err)
			);
	}

	public chooseFileModal(columnid) {	
		this.fileForm.reset();
		this.appFileFormat = localStorage.valgen_FileFormat;	
		jQuery('#FileModal').modal('show');
		this.columnid = columnid;		
	}


	public filechange(event: any) {				 
		var file = event.target.files[0];	
		console.log("file  ", file)	
		if(file.type == "image/jpeg" || file.type == "image/png" || file.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.type == "application/pdf" || file.type == "application/msword" || file.type == "application/vnd.ms-excel")
		{
		if(file.size < 999999){
		this.fileErrorMsg ='';
		var name = event.target.files[0].name;		
		this.fileForm.controls['imageInput'].setValue(file ? file.name : '');
		if (event.target.files.length > 0) {
			var extn = name.split(".").pop();
			var isNumber = rxjsNumeric.isNumeric(extn)		
			this.fileNameChange = this.fileForm.value.fileName + "." + extn;		
			const fileReader = new FileReader();
			fileReader.onload = this.handleReaderLoaded.bind(this);
			fileReader.readAsBinaryString(file);
		}
	}
	else{
		this.fileErrorMsg = "Select file less the 1 MB";		
	}
}
else{
	this.fileErrorMsg = "Select only allowed file format";	
}
	}

	handleReaderLoaded(event) {
		var file = event.target.result;
		const sequenceID = this.newGuid();
		this.base64textString = btoa(file);
		this.uploadData = {
			ExUploadFileEntity: [
				{
					ExecutionID: parseInt(this.exeId),
					AttachmentName: this.fileNameChange,
					SequenceID: sequenceID,
					FileContent: this.base64textString,
					CreatedBy: parseInt(localStorage.getItem('valgen_userId')),
					CreatedOn: moment().format(localStorage.getItem('valgen_DataValue'))
				}
			]
		};

	}
	public submitFile() {
		this.fileForm.reset();
		this.evidenceListShow = true;
		this.fileForm.controls['imageInput'].setValue('');
		this.uploadImageToLocal(this.uploadData.ExUploadFileEntity[0], this.uploadData.ExUploadFileEntity[0].AttachmentName, this.exeId, this.tableId, this.rowId, this.columnid);
		jQuery('#FileModal').modal('hide');

	}


	// handleReaderLoaded(event) {
	// 	var file = event.target.result;
	// 	const sequenceID = this.newGuid();
	// 	const fileName = `${sequenceID}.jpg`;
	// 	this.base64textString = btoa(file);
	// 	this.originalImage = 'data:image/jpeg;base64,' + this.base64textString;
	// 	fetch(this.originalImage)
	// 	.then(res => res.blob())
	// 	.then(blob => {
	// 	  this.blobImage = blob;
	// 	  this.watermarkImage(this.originalImage ,sequenceID,"","");
	// 	});
		

	// }

	// public uploadDataExUploadFile(fileName,sequenceID,watermarkedImg){

	// this.uploadData = {
	// 	ExUploadFileEntity: [
	// 		{
	// 			ExecutionID: parseInt(this.exeId),
	// 			AttachmentName: this.fileNameChange,
	// 			SequenceID: sequenceID,
	// 			FileContent: this.watermarkedImg,
	// 			CreatedBy: parseInt(localStorage.getItem('valgen_userId')),
	// 			CreatedOn: moment().format(localStorage.getItem('valgen_DataValue'))
	// 		}
	// 	]
	//   };
	// }

	//   public submitFile(){

	//   this.fileForm.reset();
	// 	this.evidenceListShow = true;
	// 	this.fileForm.controls['imageInput'].setValue('');
	// 	this.uploadImageToLocal(this.uploadData.ExUploadFileEntity[0], this.uploadData.ExUploadFileEntity[0].AttachmentName, this.exeId, this.tableId, this.rowId, this.columnid);
	// 	jQuery('#FileModal').modal('hide');
    //  }

	
	public fileChooseFromDevice() {

		if (this._platform.is('android')) {
			this._fileChooser.open().then(uri => {
			}).catch(e =>
				console.log(e)
			);
		} else if (this._platform.is('ios')) {
			this._filePicker.pickFile().then(uri => {
			}).catch(err =>
				console.log('Error', err)
			);
		}
	}

	public checkSessionTimeOut() {
		const currentTime = moment(moment().format(localStorage.getItem('valgen_DataValue')));//now
		const validTime = moment(localStorage.getItem('valgen_validTimeStamp'));
		const sessionTime = localStorage.getItem('valgen_sessionTimeOut');
		if (currentTime.diff(validTime, 'minutes') > parseInt(sessionTime)) {
			this.alertSession('Session TimeOut');
		} else {
			localStorage.setItem('valgen_validTimeStamp', moment().format(localStorage.getItem('valgen_DataValue')));
		}
	}

	async alertSavedSuccessfully() {
		const alert = await this._alertCtrl.create({
			header: 'Info!',
			message: 'Execution Saved Successfully.',
			backdropDismiss: false,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						this.onRowChanged(1, true);
					}
				}
			]
		});
		await alert.present();
	}

	async alertSession(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						this.sessionLogout();
					}
				}
			]
		});
		await alert.present();
	}

	public sessionLogout() {
		const url = localStorage.getItem('valgen_settingUrl');
		localStorage.clear();
		localStorage.setItem('valgen_settingUrl', url);
		// this._localpdb.deleteDatabase().subscribe(
		// 	(data) => {
		// 		console.log('delete', data);
		// 	}
		// );
		this._router.navigate(['login']);
	}

	public newGuid() {
		const val = Math.floor(1000 + Math.random() * 9000);
		return val;
	}

	async alertPopup(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
	}
	executionBtnClick() {
		this.button1Class = true;
		this.button2Class = false;
	}
	documentBtnClick(event) {
		this.button2Class = true;
		this.button1Class = false;
		this._router.navigate([`/menu/home/task/${this.exeId}/${event}`]);
		localStorage.removeItem('electronicExec');
	}

	public checkAuthorization(siteId) {
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			if (data.TransactionId === 0) {
				this.loadingService.pushAllLocalDataToServer(siteId);
			} else {
				this.alertPopup(data.TransactionMessage);
			}
			}, (err) => {
				console.log(err, 'getSiteAuthorization')
			}
		);
	}
	public getSiteDetails() {
		this._homeService.getSiteDetails().subscribe((data) => {
			const siteList = [];
			const siteId = localStorage.getItem('valgen_siteId');
			this.siteId = localStorage.getItem('valgen_siteId');
			
			if(data.TotalCount !== 0) {
				data.Result.forEach((list) => {
						siteList.push(list);
				})
				this.siteList = siteList;
			} else {
				this.alertPopup('No Site List Found !');
			}
			}, (err) => {
				console.log(err, 'getSiteDetails')
			}
		);
	}

	public openModalSiteList() {
		jQuery('#siteListModalTask').modal('show');
		this.alertPopup('No Site List Found !');
	}

	public closeSiteModal() {
		jQuery('#siteListModalTask').modal('hide');
	}



	//****************************************** ScreenShot*********************************************/

	public determineScreenShotSize () {
		// const screenSize = this.electronScreen.getPrimaryDisplay().workAreaSize;
		// const maxDimension = Math.max(screenSize.width, screenSize.height);
	 
		// return {
		// 	width: maxDimension * window.devicePixelRatio,
		// 	height: maxDimension * window.devicePixelRatio
		// };
		return {
			width: 1700,
			height: 700
		};
	}
	 
	public screenShot(callback) {
		const thumbSize = this.determineScreenShotSize();
		//console.log("electron  ", electron)
		console.log('kkkkkkkkkkkkkkkkkkkkkkkkk', thumbSize);
		let options = { types: ['window', 'screen'], thumbnailSize: thumbSize }
		//DesktopCapturer, Screen
		//this.desktopCapture.getSources()
		console.log(this.desktopCapture);
		this.desktopCapture.getSources(options).then( async sources => {
			console.log('ggggggggggggggggggggggggggggggg  ', sources);
			// if(error) return console.log(error);
			let returnArray = [];
			sources.forEach(function(source) {
				let base64 = this.arrayBufferToBase64(source.thumbnail.toPNG());
				returnArray.push({image: `data:image/png;base64,${base64}`, name: source.name, id: source.id});
			});
			console.log('sssssssssssssssssssssssssssss  ', returnArray);
			callback(returnArray);
		});
		
      
		
	}
	 
	public arrayBufferToBase64( buffer ) {
		var binary = '';
		var bytes = new Uint8Array( buffer );
		var len = bytes.byteLength;
		for (var i = 0; i < len; i++) {
			binary += String.fromCharCode( bytes[ i ] );
		}
		return window.btoa( binary );
	}
	 
	
	

	screenCapture(screenCaptureData) {
		this.screenCaptureData = screenCaptureData;
		//console.log(screenShot)
      /*  this.screenShot((data) => {
            this.ngZone.run(() => {
				this.allScreenList = data;
				console.log("this.allScreenList  , ", this.allScreenList)
            })
            this.openScreenCaptureModal();
		});*/
		
		 screenShot((data) => {
             this.ngZone.run(() => {
                 this.allScreenList = data;
             })
             this.openScreenCaptureModal();
         });
    }

    public openScreenCaptureModal() {
        jQuery('#screenCaptureWindows').modal('show');
    }

    public closeScreenCaptureModal() {
        jQuery('#screenCaptureWindows').modal('hide');
	}
	
	async toastController(message) {
		const alert = await this._alertCtrl.create({
			//header: 'Alert',
			message: message
		});
		await alert.present();
		setTimeout(() => alert.dismiss(), 2000);
	}

    public selectedScreen(data) {

        if (this.screenCaptureData !== -1) {
            this.fileContent = data.image.replace('data:image/png;base64,', '');
            this.scrimgId = 1;
            const snapId = `${this.exeId}-${this.tableId}-${this.rowId}`;

            if (localStorage.scrimgId != undefined) {
                this.scrimgId = parseInt(localStorage.scrimgId) + 1;
                localStorage.scrimgId = this.scrimgId;
                this.cameraImage(this.scrimgId, this.screenCaptureData, this.fileContent);
            } else {
                this._localpdb.getExecutionSnapDetails(snapId).subscribe(
                    (data) => {
                        this.scrimgId = parseInt(data.scrimgId) + 1;
                        localStorage.scrimgId = (this.scrimgId);
                        this.cameraImage(this.scrimgId, this.screenCaptureData, this.fileContent);
                        this.screenCaptureData = -1;
                    },
                    (err) => {
                        this.scrimgId = 1;
                        localStorage.scrimgId = this.scrimgId;
                        this.cameraImage(this.scrimgId, this.screenCaptureData, this.fileContent);
                        this.screenCaptureData = -1;
                    }
                );
            }
            this.closeScreenCaptureModal();
        }
    }
}


// Columntypes	 Values
// Executable	1
// Comments	2
// Result 	3
// NonExecutable	4
// Reference	5
// Signature	6
// VerifiedBy	7
// ExpectedResult	8
// ExecutablewithoutSignature	9
// CommentswithoutSignature	10
// ResultwithoutSignature	11
