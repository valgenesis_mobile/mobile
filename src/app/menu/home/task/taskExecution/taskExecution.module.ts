// Angular Imports
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { IOSFilePicker } from '@ionic-native/file-picker/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { MediaCapture } from '@ionic-native/media-capture/ngx';
// This Module's Components
import { TaskExecutionComponent } from './taskExecution.component';
import { DetailComponent } from './detail/detail.component';
import { TablesComponent } from './tables/tables.component';
import { ExecutionComponent } from './execution/execution.component';
import { DocumentComponent } from './document/document.component';

import { LocalDbServiceModule } from '../../../../common/local-pouchdb';
import { CommonPipesModule } from '../../../../common/pipe';

const routes = [
	{
		path: '', component: TaskExecutionComponent, children: [
			{ path: '', redirectTo: 'table', pathMatch: 'full' },
			{ path: 'detail', component: DetailComponent, pathMatch: 'full' },
			{ path: 'table', component: TablesComponent, pathMatch: 'full' },
			{ path: 'execution', component: ExecutionComponent, pathMatch: 'full' },
			{ path: 'document', component: DocumentComponent, pathMatch: 'full' },
		]
	},
]

@NgModule({
	imports: [
		CommonPipesModule,
		LocalDbServiceModule,
		IonicModule,
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		AngularEditorModule,
		RouterModule.forChild(routes)
	],
	declarations: [
		TaskExecutionComponent,
		DetailComponent,
		TablesComponent,
		ExecutionComponent,
		DocumentComponent,
		
	],
	exports: [
		TaskExecutionComponent,
		ReactiveFormsModule
	],
	providers: [
		IOSFilePicker,
		FileChooser,
		MediaCapture
	]
})
export class TaskExecutionModule {
	static routes = routes;

}
