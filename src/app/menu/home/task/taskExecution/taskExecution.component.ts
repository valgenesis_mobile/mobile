import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { LocalPouchdbProvider } from '../../../../common/local-pouchdb';
import { AlertController } from '@ionic/angular';
import {LoadingService} from '../../../../loading.service';
import * as moment from 'moment';
import  async from 'async';
import { HomeService } from '../../home.service';
declare let jQuery: any;

@Component({
	templateUrl: 'taskExecution.component.html',
	styleUrls: ['taskExecution.component.scss']
})
export class TaskExecutionComponent implements AfterViewInit, OnInit {
	private exeId: string;
	public activeTab = 'table';
	public siteList: any;

	constructor(private _router: Router, private route: ActivatedRoute, private _location: Location,
		public _localpdb: LocalPouchdbProvider, private _homeService: HomeService, public _alertCtrl: AlertController, public loadingService: LoadingService) {
		this.getParamValues();
	}

	ionViewWillEnter() {
     //   this.checkSessionTimeOut();
    }

	private getParamValues() {
		this.route.params.subscribe((params) => {
			// console.log('Params', params)
			this.exeId = params['ExeID'];
		});
		console.log('Exe Id', this.exeId);
	}

	ngAfterViewInit(): void {
		this.changeActiveNavigationItem(this._location);
	}

	changeActiveNavigationItem(location): void {
		// console.log('changeActiveNavigationItem', location.path().split('?')[0], location.path().split('?')[0].split('/').pop());
		this.activeTab = location.path().split('?')[0].split('/').pop();
		// let $newActiveLink = this.$el.find('a[href="' + location.path().split('?')[0] + '"]');

		// this.$el.find('.active').removeClass('active');

		// $newActiveLink.addClass('active');
	}

	ngOnInit(): void {
		this._router.events.subscribe((event) => {
			if (event instanceof NavigationEnd) {
				this.changeActiveNavigationItem(this._location);
			}
		});
	}

	public segmentChanged(event) {
		// console.log(event);
		if(event === 'execution') {
			this._localpdb.getTaskById(this.exeId).subscribe(
				(data) => {
					console.log(data, 'get task list');
					this._router.navigate([`/menu/home/task/${this.exeId}/${event}`], { queryParams: { tableId: data.tableId, rowId: data.rowId } });
				},
				(err) => {
					console.log('get task list', err);
				}
			);
		} else {
			this._router.navigate([`/menu/home/task/${this.exeId}/${event}`]);
		}
	}

	public getSiteDetails() {
		this._homeService.getSiteDetails().subscribe((data) => {
			console.log('getSiteDetails', data);
			const siteList = [];
			const siteId = localStorage.getItem('valgen_siteId');
			if(data.TotalCount !== 0) {
				data.Result.forEach((list) => {
					if(list.SiteID != siteId) {
						siteList.push(list);
					}
				})
				this.siteList = siteList;
				this.openModalSiteList();
			} else {
				this.alertPopup('No Site List Found !');
			}
			}, (err) => {
				console.log(err, 'getSiteDetails')
			}
		);
	}

	public openModalSiteList() {
		jQuery('#siteListModalTaskExe').modal('show');
	}

	public closeSiteModal() {
		console.log('close');
		jQuery('#siteListModalTaskExe').modal('hide');
	}

	public checkAuthorization(siteId) {
		jQuery('#siteListModal').modal('hide');
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			console.log('getSiteAuthorization', data);
			if (data.TransactionId === 0) {
				this.loadingService.pushAllLocalDataToServer(siteId);
			} else {
				this.alertPopup(data.TransactionMessage);
			}
			}, (err) => {
				console.log(err, 'getSiteAuthorization')
			}
		);
	}


	public checkSessionTimeOut() {
		const currentTime = moment(moment().format('YYYY-MM-DD, h:mm:ss'));//now
		const validTime = moment(localStorage.getItem('valgen_validTimeStamp'));
		const sessionTime = localStorage.getItem('valgen_sessionTimeOut');
		console.log(currentTime.diff(validTime, 'minutes'));
		if(currentTime.diff(validTime, 'minutes') > parseInt(sessionTime)) {
			this.alertSession('Session TimeOut');
		} else {
			localStorage.setItem('valgen_validTimeStamp', moment().format('YYYY-MM-DD, h:mm:ss'));
		}
	}
	
	async alertSession(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						this.sessionLogout();
					}
				}
			]
		});
		await alert.present();
	}

	public sessionLogout () {
		const url = localStorage.getItem('valgen_settingUrl');
		localStorage.clear();
		localStorage.setItem('valgen_settingUrl', url);
		// this._localpdb.deleteDatabase().subscribe(
		// 	(data) => {
		// 		console.log('delete', data);
		// 	}
		// );
		this._router.navigate(['login']);
	}

	async alertPopup(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
	}
}
