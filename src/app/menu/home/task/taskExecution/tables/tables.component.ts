import { Component, OnInit, ElementRef, AfterViewInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalPouchdbProvider } from '../../../../../common/local-pouchdb';
import { LoadingService } from '../../../../../loading.service'
import { AlertController,MenuController } from '@ionic/angular';
import * as moment from 'moment';
import { HomeService } from '../../../home.service';
import {EncrDecrService} from '../../../../../common/encr-decr.service';
// import { Base64 } from '@ionic-native/base64/ngx';
import { Base64 } from 'js-base64';
import { IonSearchbar} from '@ionic/angular';
import { Pipe, PipeTransform } from '@angular/core';
import { OrderPipe } from 'ngx-order-pipe';


import * as $ from "jquery";
import async from 'async';
import { of } from 'rxjs';
declare let jQuery: any;

@Component({
	templateUrl: 'tables.component.html',
	styleUrls: ['tables.component.scss']
})

@Pipe({ name: 'OrderBy' })


export class TablesComponent implements OnInit, AfterViewInit {

	private exeId: string;
	public tableList = [];
	public selectedMenu: any;
	private documentHtmlString: string;
	private localFile = {};
	public imageFileData:any;
	public modalImageName: any;
	button1Class:boolean =false;
	button2Class:boolean =true;
	button3Class:boolean = false;
	showActionButton:boolean = false;
	public selectedTable= [];
	public enableSearchbar = false;
	allData = [];
	public searchTerm = '';
	public noTableExist:boolean = false;
	public taskDetail:any;
	public taskTitle:string;
	public siteList: any;
	public siteId: any;
	public noofCompletedTable:any;
	public previousTable:any;
	public appPlatform:any;
	public tableMappingDetails = [];
	private latestTableRowDetails = [];
	public executableColumnOnly = [];
	public nonExecutableColumn = [];
	public tableId:any;
	public columnTypeArray = [1, 2, 4, 5, 8, 9, 10];
	public resultCheckArray = [3, 11];
	public executableColumnArray = [1, 9];
	public nonexecutableColumnArray = [4, 8];
	public tableMap:any;

	constructor(private _router: Router, private _route: ActivatedRoute, private _localpdb: LocalPouchdbProvider,
		private elRef: ElementRef, public _alertCtrl: AlertController, public loading: LoadingService,
		 private _homeService: HomeService,public menu: MenuController, private endecry: EncrDecrService) {
		this.getParamValues();
		this.tableList = [];
		this.appPlatform = localStorage.valgen_myplatform;
	}

	private getParamValues() {
		this._route.parent.params.subscribe((params) => {
			this.exeId = params['ExeID'];
		});
		console.log('Exe Id', this.exeId);
	}
    @ViewChild(IonSearchbar) myInput: IonSearchbar;
	ionViewWillEnter() {
		this.loading.reset();
    }

	ngOnInit() {
		
		this.getSiteDetails();
		this.menu.swipeEnable(true);
		async.parallel([
			(done) => {
				this.loading.present('Loading..');
				this.getTableList(done);
				this.loading.dismiss();
			},
			(done) => {
				this.getDocumentDetails(this.exeId, done);
				this.getTaskDetails(this.exeId);
			}
		], () => {
			this.tableList.forEach((table, index) => {
				this.bindExecutedTableDetails(table.HtmlDocTableId, index);
			});
			console.log('Done');
		});
	}

	ngAfterViewInit() {
		// this.elRef.nativeElement.querySelector('button');
	}
	goBack(){
		this._router.navigate([`/menu/home/task`]);
		}

	onChildClickFunction(event: Event) {
		event.stopPropagation();
		const target = event.target || event.currentTarget;
		const attachmentFile = this.localFile[target['attributes'].class.value];
		console.log('kkkkkkkkkk ', target['attributes'], target['attributes'].class.value, attachmentFile);
		this.openModal(attachmentFile._id, attachmentFile.AttachmentName)
	}
	setFilteredItems() {
		this.tableList = this.allData.filter((location) => {
			return location.TableTitle.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
		});
		if(this.tableList.length == 0){
			this.noTableExist = true;
		}
		else{
			this.noTableExist = false;
		}
		this.noofCompletedTable = 0;
				this.tableList.forEach((exe) => {
					if (exe.tableStatus === 'COMPLETED') {
						this.noofCompletedTable = this.noofCompletedTable + 1;
					}
				})
	}

	public openModal(fileId, name) {
		//jQuery("body").addClass("modal-open");
		jQuery('#imageModal').modal('show');
		this.modalImageName = name;
		this._localpdb.getImagebyId(fileId).subscribe(
			(data) => {
				this.imageFileData = `data:image/png;base64,${data.FileContent}`;
			},
			(err) => {
				console.log('data', err);
			}
		);
	}

	public closeModal() {
		//jQuery("body").removeClass("modal-open");
		jQuery('#imageModal').modal('hide');
	}

	public getSiteDetails() {
		this._homeService.getSiteDetails().subscribe((data) => {
			const siteList = [];
			const siteId = localStorage.getItem('valgen_siteId');
			this.siteId = localStorage.getItem('valgen_siteId');
			if(data.TotalCount !== 0) {
				data.Result.forEach((list) => {
						siteList.push(list);
				})
				this.siteList = siteList;
			//	console.log("this.siteList   ", this.siteList);
			} else {
				this.alertController('No Site List Found !');
			}
			}, (err) => {
			//	console.log(err, 'getSiteDetails')
			}
		);
	}

	public checkAuthorization(siteId) {
		this._homeService.getSiteAuthorization(siteId).subscribe((data) => {
			console.log("checkAuthorization ", data)
			if (data.TransactionId === 0) {
				this.loading.pushAllLocalDataToServer(siteId);
			} else {
				this.alertController(data.TransactionMessage);
			}
			}, (err) => {
				console.log(err, 'getSiteAuthorization')
			}
		);
	}

	

	private getLocalImageDocument(execId) {
		if (Object.keys(this.localFile).length === 0) {
			this._localpdb.getImageDoc(execId).subscribe(
				(data) => {
				//	console.log(data, this.tableList, 'getLocalDocument');
					data.forEach((file) => {
						this.localFile[file._id] = file;
						const aTag = document.getElementsByClassName(`${file._id}`);
						if (aTag.length) {
							aTag[0].addEventListener('click', this.onChildClickFunction.bind(this));
						}
					});
				},
				(err) => {
					console.log('data', err);
				}
			);
		}
	}

	private getDocumentDetails(execId, done) {
		this._localpdb.getDocumentDetails(execId).subscribe(
			(data) => {
				console.log(data, 'getDocumentDetails');
				this.documentHtmlString = this.base64ToArrayBuffer(data.buffer);
				//Base64.encode(data.buffer);
			//	console.log(Base64.decode(data.buffer))
				// this.base64.encodeFile(data.buffer).then((base64File: string) => {
				// 	console.log(base64File);
				//   }, (err) => {
				// 	console.log(err);
				//   });
				done();
			},
			(err) => {
				console.log('data', err);
				done();
			}
		);
	}

	private base64ToArrayBuffer(buffer) {
		// return window.atob(buffer);
		return Base64.decode(buffer);
	
	}

	private getTableList(done) {
		this._localpdb.getTableList(this.exeId).subscribe(
			(data) => {
				console.log(data, 'get exe list');
				if(data.length == 0){
					this.noTableExist = true;
				}
				this.tableList = data;
				this.noofCompletedTable = 0;
				this.tableList.forEach((exe) => {
					if (exe.tableStatus === 'COMPLETED') {
						this.noofCompletedTable = this.noofCompletedTable + 1;
					}
				})
				this.allData = data;
				done();
			},
			(err) => {
				console.log('data', err);
				done();
			}
		);
	}

	public openPage(page, index) {
		if (this.selectedMenu) {
			this.selectedMenu = 0;
		} else {
			this.selectedMenu = index;
		}
	}

	private bindExecutedTableDetails(htmlTableId, index) {
		const id = `#${htmlTableId} tbody`;
		const tableDetails = [];
		$(this.documentHtmlString).find(id).find('tr').each((rowIndex, rowValue) => {
			const tableRows = $(rowValue).find('td');
			const tableRow = [];
			tableRows.each((columnIndex, columnValue) => {
				tableRow.push($(columnValue).html());
			});
			tableDetails.push(tableRow);
		});
		this.tableList[index].documentTableDetails = tableDetails;
	//	console.log("tableDetails   ", tableDetails);
		this.selectedTable.push(tableDetails);
	}


	private tableBindMappingDetails(tableId) {
	   console.log("this.exeId   ", this.exeId)
	   console.log(" tableId  ", tableId)
		this._localpdb.getTableMapDetatils(this.exeId, tableId).subscribe(
			(data) => {
				this.tableMappingDetails = data;
				console.log("this.tableMappingDetails  ", this.tableMappingDetails)		
			}
		);
	}

	private getLatestTableRowDetails() {
		this._localpdb.getLatestTableDetatils(this.exeId).subscribe(
			(data) => {
				this.latestTableRowDetails = data;
				
			}
		);

	}

		public getRowData(rowId, ExeTableID) {
			return this.latestTableRowDetails.filter((row) => {
				return (row.ExRowID === rowId && row.ExeTableID === ExeTableID);
			});
		}

	public getRowDetail(table, tableIndex, rowId, length, docDetails) {
		// console.log('asdasdsadsadsadsadsad  ', table, rowId , tableIndex, docDetails);
		// const rowdata = this.tableList[tableIndex].documentTableDetails[rowId];
		this.tableId = table.TableID;
		
		 this.tableBindMappingDetails(table.TableID);
		 this.getLatestTableRowDetails();
		
		console.log("table ", table)
		console.log("tableIndex  ", tableIndex)
		console.log("docDetails  ", docDetails)
		console.log("rowId  ", rowId)
		if(localStorage.valgen_SequentialRowExecutionRequired == 1){
		// 	if(tableIndex == 0){

			
			if(rowId === 0){ }  // Header of table when clicked it does nothing
			else if(rowId === 1){
			this._router.navigate([`/menu/home/task/${this.exeId}/execution`], { queryParams: { tableId: table.TableID, rowId: rowId, tableLength: length } });
			}
			else
			{
				this.executableColumnOnly = [];
				this.nonExecutableColumn = [];
				
				let detail = docDetails[rowId - 1];
				const data = [];
				 setTimeout(() => {
					 for(let k=0; k < this.tableMappingDetails.length; k++){
						 if(this.tableMappingDetails[k].ColumnType == 1 || this.tableMappingDetails[k].ColumnType == 9)
						 {  let value = '';
							this.tableMap = k;
							value = detail[this.tableMap];						
							data.push({
									'label': this.tableMappingDetails[k].ColumnHeader,
									'value': value.replace(/<[^>]*>?/gm, ''), 'columnId': k, 'ColumnType': this.tableMappingDetails[k].ColumnType , rowId: rowId
								});					
						 }
						 
					 }
			
				
				for (let i = 0; i < data.length; i++) {
					if (data[i].ColumnType == 1 || data[i].ColumnType == 2 || data[i].ColumnType == 9 || data[i].ColumnType == 10) {
						data[i].id = i;
						data[i].value = data[i].value.replace(/\&nbsp;/g, '');
						this.executableColumnOnly.push(data[i]);
					}
                 
				}
				
				if(this.executableColumnOnly[0].value != ''){
					this._router.navigate([`/menu/home/task/${this.exeId}/execution`], { queryParams: { tableId: table.TableID, rowId: rowId, tableLength: length } });
				}
				else
				{
					//this.alertController('Execute in Sequential Order Only, Select the First Row.');
				}
			}, 1000)				
			}

		}
		else
		{

		
		if(rowId === 0){ }  // Header of table when clicked it does nothing
		else{
		this._router.navigate([`/menu/home/task/${this.exeId}/execution`], { queryParams: { tableId: table.TableID, rowId: rowId, tableLength: length } });
		}
	}
	}
	

	public toggleSection(i) {
		this.enableSearchbar = false;
		console.log("TableList  ", this.tableList);
		this.tableList.forEach((table, index) => {
			if (i === index) {
				table.open = !table.open;
			} else {
				table.open = false;
			}
		});
		this.getLocalImageDocument(this.exeId);
	}

	private getTaskDetails(execId) {
		this._localpdb.getTaskById(execId).subscribe(
			(data) => {
			//	console.log(data, 'getDocumentDetails');
				this.taskDetail = data;
				this.taskTitle = data.ValDocCode;
			},
			(err) => {
				console.log('data', err);
			}
		);
	}

	public checkSessionTimeOut() {
		const currentTime = moment(moment().format('YYYY-MM-DD, h:mm:ss'));//now
		const validTime = moment(localStorage.getItem('valgen_validTimeStamp'));
		const sessionTime = localStorage.getItem('valgen_sessionTimeOut');
	//	console.log(currentTime.diff(validTime, 'minutes'));
		if(currentTime.diff(validTime, 'minutes') > parseInt(sessionTime)) {
			this.alertSession('Session TimeOut');
		} else {
			localStorage.setItem('valgen_validTimeStamp', moment().format('YYYY-MM-DD, h:mm:ss'));
		}
	}
	
	async alertSession(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						this.sessionLogout();
					}
				}
			]
		});
		await alert.present();
	}

	public sessionLogout () {
		const url = localStorage.getItem('valgen_settingUrl');
		localStorage.clear();
		localStorage.setItem('valgen_settingUrl', url);
		// this._localpdb.deleteDatabase().subscribe(
		// 	(data) => {
		// 		console.log('delete', data);
		// 	}
		// );
		this._router.navigate(['login']);
	}

	async alertController(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
	}

	taskdetailsBtnClick(event)
	{
		this.button1Class = true;
		this.button2Class = false;
		this.button3Class = false;
		this._router.navigate([`/menu/home/task/${this.exeId}/${event}`]);
	}
	tablesBtnClick()
	{
		this.button2Class = true;
		this.button1Class = false;
		this.button3Class = false;
	}
	documentBtnClick(event)
	{
		this.button2Class = false;
		this.button1Class = false;
		this.button3Class = true;
		this._router.navigate([`/menu/home/task/${this.exeId}/${event}`]);

	}
	public getSearchbar()
	{
	   this.enableSearchbar = !this.enableSearchbar;
	 
	   setTimeout(() => { this.myInput.setFocus(); }, 150);
	}

}

