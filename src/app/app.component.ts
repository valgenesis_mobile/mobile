import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Events } from '@ionic/angular';
import { LocalPouchdbProvider } from './common/local-pouchdb';
import { ElogPouchdbProvider } from './common/elog-pouchdb';
import { LoadingService } from './loading.service';
import { LoginService } from './login/login.service';
import { MenuComponent } from './menu/menu.component';
import { Device } from '@ionic-native/device/ngx';


declare let PouchDB: any


@Component({
	selector: 'app-root',
	templateUrl: 'app.component.html',
	providers: [LoginService]
})
export class AppComponent {
	public isLogin: boolean = false;
	homeclass: boolean = false;
	profileclass: boolean = false;
	settingsclass: boolean = false;
	logoutclass: boolean = false;
	public taskListDb;
	constructor(
		private platform: Platform,
		private splashScreen: SplashScreen, private statusBar: StatusBar, public loading: LoadingService,
		private router: Router, public events: Events, public _localpdb: LocalPouchdbProvider, public _alertCtrl: AlertController,
		public menucom: MenuComponent, public cdRef: ChangeDetectorRef, public _elogpdb: ElogPouchdbProvider, 
		private _loginService: LoginService, private device: Device) {
		this.initializeApp();

		events.subscribe('tabShow', () => {
			this.isLogin = true;
			this.homeclass = true;
			this.profileclass = false;
			this.settingsclass = false;
			this.logoutclass = false;
		});
		events.subscribe('tabClose', () => {
			this.isLogin = false;
		});
	}

	ngAfterViewInit() {
		this.cdRef.detectChanges();
	}

	initializeApp() {
		this.platform.ready().then(() => {
		   this.platform.backButton.subscribe((event) => {		
		  });		 
			this.statusBar.styleDefault();
			console.log("this.platform   ", this.platform)
			if (this.platform.is('android')) {
				this.statusBar.styleLightContent();		
				console.log("his.device  ", this.device)		
				localStorage.valgen_deviceId = this.device.uuid;
			}
			else if (this.platform.is('ios')) {				
				localStorage.valgen_deviceId = this.device.uuid;
			}
			else if (this.platform.is('electron')) {				
				localStorage.valgen_deviceId = this.device.uuid;
				console.log("this.platform    electron  ", this.platform)
			}
			else
			{
				localStorage.valgen_deviceId = this.device.uuid;
			}			
			this.splashScreen.hide();
		});
	}
	homeClick() {
		this.router.navigate(['menu', 'home']);
		this.homeclass = true;
		this.profileclass = false;
		this.settingsclass = false;
		this.logoutclass = false;
	}
	profileClick() {
		this.router.navigate(['menu', 'profile']);
		this.profileclass = true;
		this.homeclass = false;
		this.settingsclass = false;
		this.logoutclass = false;
	}
	settingsClick() {
		this.router.navigate(['menu', 'setting']);
		this.profileclass = false;
		this.homeclass = false;
		this.settingsclass = true;
		this.logoutclass = false;
	}

	async alertController(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
	}


}
