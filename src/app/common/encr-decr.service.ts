import { Injectable } from '@angular/core';
// import { LocalPouchdbProvider } from './local-pouchdb';
declare let CryptoJS: any;
import * as moment from 'moment';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';


@Injectable({
	providedIn: 'root'
})

export class EncrDecrService {
	key:any;
	loginEncrypt: any;
	constructor(private _router: Router,public _alertCtrl: AlertController) { 
		if(localStorage.getItem('valgen_AESKey'))
		this.key = CryptoJS.enc.Base64.parse(localStorage.getItem('valgen_AESKey'));
	 }

	// The set method is use for encrypt the value.
	encryptCheck(password) {
		const key = CryptoJS.enc.Base64.parse('Xt+UXcaToi+D5n7+0zc3xw==');
		const iv = CryptoJS.enc.Base64.parse(localStorage.getItem('valgen_AESIV'));
		const encrypted = CryptoJS.AES.encrypt(password, key, { iv: iv });
		const deencrypted = CryptoJS.AES.decrypt(localStorage.getItem('valgen_EsignPassword'), key, { iv: iv });
		console.log("deencrypted   ", deencrypted)
		let check = false;
		if (encrypted.toString() === localStorage.getItem('valgen_EsignPassword')) {
			check = true;
		}
		return check;
	}

	encryptSelectedUserCheck(data, password) {
		const key = CryptoJS.enc.Base64.parse(data.AESKey);
		const iv = CryptoJS.enc.Base64.parse(data.AESIV);
		const encrypted = CryptoJS.AES.encrypt(password, key, { iv: iv });
		let check = false;
		if (encrypted.toString() === data.EsignPassword) {
			check = true;
		}
		return check;
	}
	encrypt(value:any)
	{
		const key = 'EA81AA1D5FC1EC53E84F30AA746139EEBAFF8A9B76638895';
	   return CryptoJS.AES.encrypt(value, key);
        
	}
	decrypt(value:any)
	{  const key = 'EA81AA1D5FC1EC53E84F30AA746139EEBAFF8A9B76638895';
	   return CryptoJS.AES.decrypt(value, key);
	}
	encryptURL(password) {
		const key = 'WHQrVVhjYVRvaStENW43KzB6YzN4dz09';
		const iv = CryptoJS.enc.Base64.parse('87AF7EA221F3FFF5');
		const encrypted = CryptoJS.AES.encrypt(JSON.stringify(password), key, { iv: iv });
		this.loginEncrypt = {
	        "DyanmicValue": encrypted.iv.toString(CryptoJS.enc.Base64),
	        "Core": encrypted.key.toString(CryptoJS.enc.Base64),
	        "Text": encrypted.ciphertext.toString(CryptoJS.enc.Base64)
		};
		
		 const decrypted = CryptoJS.AES.decrypt(encrypted.toString(), key, { iv: iv });
		 var plaintext = decrypted.toString(CryptoJS.enc.Utf8);
		return this.loginEncrypt;
	}

	decryptTest(password, iv) {
		const key1 = 'Xt+UXcaToi+D5n7+0zc3xw==';
		const key = CryptoJS.enc.Base64.parse(key1);
		const iv1 =  iv;
		const text = CryptoJS.enc.Base64.parse(password);
		const decrypted = CryptoJS.AES.decrypt(password, key, { iv: iv });
		return decrypted;
	}

	
}