import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import * as momenttz from 'moment-timezone';
import { EncrDecrService } from '../common/encr-decr.service';



@Injectable()
export class ElogModeService {
	private userId: any;
	constructor(private _httpService: HttpService,  private _encrDecr: EncrDecrService) {
		this.userId = localStorage.getItem('valgen_userId');
	}

	public getElogTaskList(userId, siteId) {
		
		
		const data = {
			UserID : userId,
			SiteId : siteId
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Task/GetUsertask`,
			body : encryptValue
		};
		return this._httpService.post(info);
	}


	public taskDetails(id) {
		const data = {
			LMLogFormInitiateID : id
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			 url: `eLogBook/GetTaskDetails`,
			 body : encryptValue			
		};
		return this._httpService.post(info);
	}


	public SectionDependency(id, FormID) {
		const siteId = localStorage.getItem('valgen_siteId');
		const data = {
			LMLogFormInitiateID : id,
			LMFormId : FormID,
			SiteID  : siteId
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			 url: `eLogBook/GetSectiondependencyDetails`,
			 body : encryptValue			
		};
		return this._httpService.post(info);
	}


	public taskLogSectionHeader(id) {
		const ClientOffset = new Date().getTimezoneOffset();
		const ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
		const siteId = localStorage.getItem('valgen_siteId');
		this.userId = localStorage.getItem('valgen_userId');
		const data = {
			LMLogFormInitiateID : id,
			SiteID : siteId,
			userId : this.userId,
			clientOffset : ClientOffset,
			clientZone : ClientZone
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `eLogBook/GetSectionDetails`,
			body : encryptValue
		};     
		return this._httpService.post(info);
	}

	public taskLogSectionDetails(id) {
		const siteId = localStorage.getItem('valgen_siteId');
		const data = {
			LMLogFormInitiateID : id,
			SiteID : siteId,
		}
		console.log("taskLogSectionDetails  ", data)
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `eLogBook/GetLogFormSectionDetails`,
			body : encryptValue
		};
		return this._httpService.post(info);
	}

	public taskLogHistoryDetails(id) {
		const siteId = localStorage.getItem('valgen_siteId');
		const data = {
			LMLogFormInitiateID : id,
			SiteID : siteId,
			HistoryData: 1
		}
		console.log("taskLogHistoryDetails  ", data)
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `eLogBook/GetLogFormSectionDetails`,
			body : encryptValue
		};
		return this._httpService.post(info);
	}

	public UpdateTaskSectionDetail(data) {
		console.log("SaveSectionDetails  ", data)
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `eLogBook/SaveSectionDetails`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

	public SaveHeaderFooter(data) {
		console.log("SaveHeaderDetails  ", data)
		const encryptValue = this._encrDecr.encryptURL(data);
		
		const info = {
			url: `eLogBook/SaveFormDetails`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

	public ActivityCapture(lmLogFormInitiateID,actionFlag, lmFormSectionId) {
		const ClientOffset = new Date().getTimezoneOffset();
		const ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
		this.userId = localStorage.getItem('valgen_userId');
        const data = {
			LMLogFormInitiateID : lmLogFormInitiateID,
			userId : this.userId,
			ClientOffset : ClientOffset,
			ClientZone : ClientZone,
			actionFlag : actionFlag,
			lmFormSectionId : lmFormSectionId
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `eLogBook/ActivityCapture`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

	public checkLockStatus(taskId, formSectionId,partid) {
		const ClientOffset = new Date().getTimezoneOffset();
		const ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
		this.userId = localStorage.getItem('valgen_userId');
		const data = {
			LMLogFormInitiateID : taskId,
			LMFormSectionId : formSectionId,
			UserID: this.userId,
			PartId : partid,
			ClientOffset : ClientOffset,
			ClientZone : ClientZone
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `eLogBook/InsertLMSectionLock`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}


	public taskLogSection(id) {
		const data = {
			LMLogFormInitiateID : id
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `eLogBook/LogFormSection`,
			body : encryptValue
		};
		return this._httpService.post(info);
	}

	public GetTaskAlertCapture(groupId) {
		const data = {
			GroupID : groupId,
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Task/GetLogDataGroupInfo`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

	public FormVerification(data) {
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `eLogBook/FormVerification`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

	public FormAccept(data) {
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `eLogBook/FormAccept`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

	public FormApprove(data) {
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `eLogBook/FormApprove`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

	public FormApproveReject(data) {
		console.log("uploadData dataaa  ", data)
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `eLogBook/FormApproveReject`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}
}
