import { HttpClient } from '@angular/common/http';
import { Injectable, NgModule } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
declare let PouchDB: any

import { LiveModeService } from './livemode.service';
import async from 'async';
import * as $ from 'jquery';
import { Base64 } from 'js-base64';

@Injectable()
export class LocalPouchdbProvider {
	public accountDb;
	public taskListDb;
	public tableListDb;
	public documentDb;
	public latestTableDetailDb;
	public tableMappingDetailDb;
	public imageDocumentDb;
	public executionVerifyDb;
	public executionSnapDetailsDb;
	public executionDeviationDb;
	private userId: any;
	

	constructor(public http: HttpClient, public _liveModeService: LiveModeService) {
		this.accountDb = new PouchDB('userAccount', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		this.taskListDb = new PouchDB('taskList', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		this.tableListDb = new PouchDB('tableList', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		this.documentDb = new PouchDB('documentDetail', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		this.latestTableDetailDb = new PouchDB('tableDetail', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		this.tableMappingDetailDb = new PouchDB('tableMappingDetail', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		this.imageDocumentDb = new PouchDB('imageDocument', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		this.executionVerifyDb = new PouchDB('executionVerify', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		this.executionSnapDetailsDb = new PouchDB('executionSnapDetails', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		this.executionDeviationDb = new PouchDB('executionDeviation', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		// PouchDB.replicate('taskList', 'http://localhost:5984/taskList', {live: true});
		this.userId = localStorage.getItem('valgen_userId');
	}

	private bufferToBase64(data) {
		// return window.btoa(unescape(data.replace(/[\u00A0-\u2666]/g, function(c) {
		// 	return '&#' + c.charCodeAt(0) + ';';
		// })));
		return Base64.encode(data);
	}
	public pushLocalData(execId, callback) {
		async.series([
			(done) => {
				this.getImageDoc(execId).subscribe(
					(localFileData) => {
					//	console.log('localFileData ', localFileData);
						const uploadData = { ExUploadFileEntity: [] };

						localFileData.forEach((col) => {
							const updatedImageData = {
								ExecutionID: col.ExecutionID,
								AttachmentName: col.AttachmentName,
								SequenceID: col.SequenceID,
								FileContent: col.FileContent,
								CreatedBy: col.CreatedBy,
								CreatedOn: col.CreatedOn
							};
							uploadData.ExUploadFileEntity.push(updatedImageData);
						});

						if (uploadData.ExUploadFileEntity.length) {
							this._liveModeService.UpdateUploadFiles(uploadData).subscribe(
								(data) => {
									//console.log('UpdateUploadFiles', uploadData);
									this._liveModeService.uploadFileName(uploadData.ExUploadFileEntity[0].ExecutionID, 1, 1).subscribe(
										(fileData) => {
											const baseUrl = fileData.Result[0].HLink
											const asyncTask = [];
											localFileData.forEach((col) => {
												asyncTask.push((writeToDoc) => {
													this.getDocumentDetails(`${col.ExecutionID}`).subscribe(
														(docData) => {
															// const documentHtmlString = window.atob(docData.buffer);
															const documentHtmlString = Base64.decode(docData.buffer)
															const div = document.createElement('html');
															div.innerHTML = documentHtmlString;
															let aTag = $(div).find(`.${col._id}`);
															aTag.attr("href", `${baseUrl}${col.AttachmentName}`);
															aTag.removeAttr('class');
															const doc = this.addHtmlTag($(div).html());
															const docBuffer = this.bufferToBase64(doc);
															this.updateDocumentDetail(docBuffer, `${col.ExecutionID}`).subscribe(
																(uData) => {
																	this.deleteImageData(col._id, writeToDoc);
																}
															);
														}
													)
												});
											});
											async.series(asyncTask, () => {
											//	console.log('asdadas');
												done();
											});
										}, (err) => {
											done();
										//	console.log(err, 'uploadFileName')
										}
									);
								}
							);
						} else {
							done();
						}
					},
					(err) => {
						done();
						console.log('data', err);
					}
				);
			},
			(done) => {
				// getLocalLatestTableDetatils
				this.getLocalLatestTableDetatils(execId).subscribe(
					(tableDetail) => {
					//	console.log('getLocalLatestTableDetatils', tableDetail);
						const ExecutionDocument = { ExecutionDocument: [] };
						tableDetail.forEach((col) => {
							const updatedColumn = {
								Executionid: col.ExecutionID,
								Tableid: col.ExeTableID,
								Rowid: col.ExRowID,
								Colid: col.ExColumnID,
								Result: col.ExResult,
								Content: col.Content,
								ColumnText: col.Content,
								Status: col.Flag,
								CreatedBy: col.CreatedBy,
								CreatedOn: col.CreatedOn,
								OriginalExeBy: col.OriginalExeBy,
								ClientOffset: col.ClientOffset.toString(),
								ClientZone: col.ClientZone
							};
							ExecutionDocument.ExecutionDocument.push(updatedColumn);
						});
						console.log('table data ', ExecutionDocument);
						if (ExecutionDocument.ExecutionDocument.length) {
							this._liveModeService.UpdateExeTableDetail(ExecutionDocument).subscribe(
								(data) => {
							//		console.log('UpdateExeTableDetail', data);
									done();
								}
							);
						} else {
							done();
						}
					},
					(err) => {
						console.log('data', err);
						done();
					}
				);
			},

			(done) => {
				// getLocalLatestTableDetatils
				this.getExecutionSnapDetailsAll(execId).subscribe(
					(snapDetail) => {
					//	console.log('getLocalLatestTableDetatils',snapDetail );

						const ExecutionSnapShotSeqlst = {ExecutionSnapShotSeqlst: []};
						snapDetail.forEach((col) => {
							const updatedColumn = {
								Executionid: col.ExecutionId,
								Tableid: col.TableId,
								Rowid: col.RowId,
								scrimgid: col.scrimgId
							};
							ExecutionSnapShotSeqlst.ExecutionSnapShotSeqlst.push(updatedColumn);
						});
						console.log('table data ', ExecutionSnapShotSeqlst);
						if (ExecutionSnapShotSeqlst.ExecutionSnapShotSeqlst.length) {
							this._liveModeService.UpdateExecutionSnapDetails(ExecutionSnapShotSeqlst).subscribe(
								(data) => {
									//console.log('UpdateExecutionSnapDetails', data);
									done();
								},
								(err) => {
									//console.log('error', err)
								}
							);
						} else {
							done();
						}
					},
					(err) => {
					//	console.log('data', err);
						done();
					}
				);
			},
			(done) => {
				// getLocalDocument
				this.getTaskById(execId).subscribe(
					(task) => {
					//	console.log('getTaskByIdhhhhhhhhhhhhhhhhhhhhhhhhhhhhh', task);
						const lastTableId = task.tableId;
						const lastRowId = task.rowId;
						const taskIdSplit = task.TaskID;
						const exExeId = task.ExExecutionid;
						this.getLocalDocument(execId).subscribe(
							(docList) => {
								console.log('getLocalDocument', docList);
								const asyncTask = [];
								docList.forEach(document => {
									asyncTask.push((docUpdateDone) => {
										this._liveModeService.UpdateDocumentData(document._id, document.buffer, lastTableId, lastRowId, taskIdSplit, exExeId).subscribe(
											(data) => {
											//	console.log('UpdateDocumentData', data);
												docUpdateDone();
											}
										);
									});
								});

							
								async.parallel(asyncTask, done);
							},
							(err) => {
								console.log('data', err);
								done();
							}
						);
					},
					(err) => {
						console.log('data', err);
						done();
					}
				);
			}
		], () => {
			callback();
		});
	}

	addHtmlTag(doc) {
		return `<!DOCTYPE html ><html xmlns="http://www.w3.org/1999/xhtml">${doc}</html>`
	}


	public deleteTaskList() {
		this.taskListDb.allDocs({ include_docs: true }, (err, doc) => {
					const taskList = [];
					console.log("doc ", doc)
					doc.rows.forEach((data) => {
						console.log("data  ", data)
						if (data.doc.statusPush === false && data.doc.statusPull === false ) {
							taskList.push(data.doc);
						}
					});

					console.log("tableList   ", taskList)

					if (taskList.length != 0) {
						taskList.forEach((table, index) => {
							this.taskListDb.remove(table);
						});					
					} 
		})
	}

	public deleteLocalData(execId, callback) {
		//console.log(execId);
		async.series([
			(done) => {
				this.tableListDb.allDocs({ include_docs: true }, (err, doc) => {
				//	console.log('asdadadsdsa ', doc);
					const tableList = [];
					doc.rows.forEach((data) => {
						if (data.doc.execId === parseInt(execId)) {
							tableList.push(data.doc);
						}
					//	console.log(tableList);
					});

					if (tableList.length != 0) {
						tableList.forEach((table, index) => {
						//	console.log(table)
							this.tableListDb.remove(table);
						});
						done();
					} else {
						done();
					}
				});
			},
			(done) => {
				this.tableMappingDetailDb.allDocs({ include_docs: true }, (err, doc) => {
				//	console.log('asdadadsdsa ', doc);
					const tableMappingDetail = [];
					doc.rows.forEach((data) => {
						if (data.doc.ExecutionID === parseInt(execId)) {
							tableMappingDetail.push(data.doc);
						}
					});
					if (tableMappingDetail.length != 0) {
						tableMappingDetail.forEach((tableDetail, index) => {
							this.tableMappingDetailDb.remove(tableDetail);
						});
						done();
					} else {
						done();
					}
				});
			},
			(done) => {
				this.documentDb.get(`${execId}`, (err, doc) => {
				//	console.log('asdadadsdsa ', doc, err);
					if (!err && doc) {
						this.documentDb.remove(doc);
						done();
					} else {
						done();
					}
				});
			},
			(done) => {
				this.latestTableDetailDb.allDocs({ include_docs: true }, (err, doc) => {
				//	console.log('asdadadsdsa ', doc);
					const latestTableDetailList = [];
					doc.rows.forEach((data) => {
						if (data.doc.ExecutionID === parseInt(execId)) {
							latestTableDetailList.push(data.doc);
						}
					});
					if (latestTableDetailList.length != 0) {
						latestTableDetailList.forEach((document, index) => {
							this.latestTableDetailDb.remove(document);
						});
						done();
					} else {
						done();
					}
				});
			}
		], () => {
			callback();
		});
	}

	public deleteAllEntiryDb(callback) {
		async.series([
			(done) => {
				this.taskListDb.allDocs({ include_docs: true }, (err, doc) => {
				//	console.log('asdadadsdsa ', doc);
					const taskList = [];
					doc.rows.forEach((data) => {
						taskList.push(data.doc);
					});
					if (taskList.length != 0) {
						taskList.forEach((document, index) => {
							this.taskListDb.remove(document);
						});
						done();
					} else {
						done();
					}
				});
			},
			(done) => {
				this.tableListDb.allDocs({ include_docs: true }, (err, doc) => {
				//	console.log('asdadadsdsa ', doc);
					const tableList = [];
					doc.rows.forEach((data) => {
						tableList.push(data.doc);
					});

					if (tableList.length != 0) {
						tableList.forEach((table, index) => {
						//	console.log(table)
							this.tableListDb.remove(table);
						});
						done();
					} else {
						done();
					}
				});
			},
			(done) => {
				this.tableMappingDetailDb.allDocs({ include_docs: true }, (err, doc) => {
				//	console.log('asdadadsdsa ', doc);
					const tableMappingDetail = [];
					doc.rows.forEach((data) => {
						tableMappingDetail.push(data.doc);
					});
					if (tableMappingDetail.length != 0) {
						tableMappingDetail.forEach((tableDetail, index) => {
							this.tableMappingDetailDb.remove(tableDetail);
						});
						done();
					} else {
						done();
					}
				});
			},
			(done) => {
				this.documentDb.allDocs({ include_docs: true }, (err, doc) => {
				//	console.log('asdadadsdsa ', doc);
					const documentDetail = [];
					doc.rows.forEach((data) => {
						documentDetail.push(data.doc);
					});
					if (documentDetail.length != 0) {
						documentDetail.forEach((document, index) => {
							this.documentDb.remove(document);
						});
						done();
					} else {
						done();
					}
				});
			},
			(done) => {
				this.latestTableDetailDb.allDocs({ include_docs: true }, (err, doc) => {
				//	console.log('asdadadsdsa ', doc);
					const latestTableDetailList = [];
					doc.rows.forEach((data) => {
						latestTableDetailList.push(data.doc);
					});
					if (latestTableDetailList.length != 0) {
						latestTableDetailList.forEach((document, index) => {
							this.latestTableDetailDb.remove(document);
						});
						done();
					} else {
						done();
					}
				});
			},
			(done) => {
				this.imageDocumentDb.allDocs({ include_docs: true }, (err, doc) => {
				//	console.log('asdadadsdsa ', doc);
					const imageDocument = [];
					doc.rows.forEach((data) => {
						imageDocument.push(data.doc);
					});
					if (imageDocument.length != 0) {
						imageDocument.forEach((document, index) => {
							this.imageDocumentDb.remove(document);
						});
						done();
					} else {
						done();
					}
				});
			},
			(done) => {
				this.executionSnapDetailsDb.allDocs({ include_docs: true }, (err, doc) => {
				//	console.log('asdadadsdsa ', doc);
					const executionSnapDetails = [];
					doc.rows.forEach((data) => {
						executionSnapDetails.push(data.doc);
					});
					if (executionSnapDetails.length != 0) {
						executionSnapDetails.forEach((document, index) => {
							this.executionSnapDetailsDb.remove(document);
						});
						done();
					} else {
						done();
					}
				});
			},
			(done) => {
				this.executionVerifyDb.allDocs({ include_docs: true }, (err, doc) => {
				//	console.log('asdadadsdsa ', doc);
					const exeVerifyList = [];
					doc.rows.forEach((data) => {
						exeVerifyList.push(data.doc);
					});
					if (exeVerifyList.length != 0) {
						exeVerifyList.forEach((document, index) => {
							this.executionVerifyDb.remove(document);
						});
						done();
					} else {
						done();
					}
				});
			}
		], () => {
			callback();
		});
	}
	
	public deleteDatabase(): Observable<any> {
		return Observable.create((observer) => {
			async.series([
				(done) => {
					this.accountDb.destroy();
					done();
				},
				(done) => {
					this.taskListDb.destroy();
					done();
				},
				(done) => {
					this.tableListDb.destroy();
					done();
				},
				(done) => {
					this.documentDb.destroy();
					done();
				},
				(done) => {
					this.latestTableDetailDb.destroy();
					done();
				},
				(done) => {
					this.tableMappingDetailDb.destroy();
					done();
				},
				(done) => {
					this.imageDocumentDb.destroy();
					observer.complete();
					done();
				},
				(done) => {
					this.executionVerifyDb.destroy();
					observer.complete();
					done();
				},
				(done) => {
					this.executionSnapDetailsDb.destroy();
					observer.complete();
					done();
				}
			], () => {
				console.log('Completed delete');
			});
		});
	}

	// execution list add
	public addExecutionList(inputData: any): Observable<any> {
		return Observable.create((observer) => {
			const asyncTask = [];
			inputData.forEach((value, index) => {
				asyncTask.push((callback) => {
					const data = JSON.parse(JSON.stringify(value));
					const id = `${data.ExeID}`;

					this.taskListDb.get(id, (err, doc) => {
						data._id = id;
						if (!err && doc) {
							data._rev = doc._rev;
						}
						data.tableId = (doc && doc.tableId) ? doc.tableId : 1;
						data.rowId = (doc && doc.rowId) ? doc.rowId : 1;
						data.statusPull = (doc && doc.statusPull) ? doc.statusPull : false;
						data.statusPush = (doc && doc.statusPush) ? doc.statusPush : false;
						this.taskListDb.put(data, (error, taskData) => {
							callback();
						});
					});
				});
			});
			async.parallel(asyncTask, () => {
				// PouchDB.replicate('taskList', 'http://localhost:5984/taskList', {live: true});
				observer.next();
				observer.complete();
			});
		});
	}

	public addSingleExecutionList(execId: any): Observable<any> {
		return Observable.create((observer) => {
		//	console.log(execId);
			this.taskListDb.get(`${execId}`, (err, doc) => {
			//	console.log('asdasdadasdasda  ', err, doc);
				doc.rowId = 1;
				doc.statusPull = true;
				this.taskListDb.put(doc, (error, taskData) => {
					this.updateTaskDetails(doc.ExeID,doc.TaskID, doc.ExExecutionid, (data) => {
						console.log("addSingleExecutionList   ", data)
						console.log("observer  ",observer)
                        if(data === 'error'){
							observer.error();
						}
						else{
							observer.next();
							observer.complete();
						}
					
						
					});
				});
			});
		});
	}

	public updateTaskListTableRow(execId, tableId, rowId): Observable<any> { 
		return Observable.create((observer) => {
			const id = `${execId}`;
			this.taskListDb.get(id, (err, doc) => {
				doc._id = id;
				doc.tableId = tableId;
				doc.rowId = rowId;
				this.taskListDb.put(doc, (error, data) => {
					if (!error && data) {
						observer.next(data);
						observer.complete();
					} else {
						observer.error(error);
						observer.complete();
					}
				});
			});
		});
	}


	public updateTaskList(execId): Observable<any> { 
		return Observable.create((observer) => {
			const id = `${execId}`;
			this.taskListDb.get(id, (err, doc) => {
				doc.NewTaskStatus = 0;
				this.taskListDb.put(doc, (error, data) => {
					if (!error && data) {
						observer.next(data);
						observer.complete();
					} else {
						observer.error(error);
						observer.complete();
					}
				});
			});
		});
	}



	public updateExecutionSnapDetails(execId, tableId, rowId, snapid): Observable<any> { 
		return Observable.create((observer) => {
			const id = `${execId}-${tableId}-${rowId}`;
			this.executionSnapDetailsDb.get(id, (err, doc) => {
				doc._id = id;
				doc.scrimgId = snapid;
				doc.RowId = rowId;
				doc.ExecutionId = execId;
				doc.TableId = tableId;
				this.executionSnapDetailsDb.put(doc, (error, data) => {
					if (!error && data) {
						observer.next(data);
						observer.complete();
					} else {
						observer.error(error);
						observer.complete();
					}
				});
			});
		});
	}


	public updateTaskDetails(execId,taskId, exExeId, callback) {
		async.parallel([
			(done) => {
				this._liveModeService.tableList(execId,taskId).subscribe(  // get table list from server
					(data) => {
						console.log("tableList  ", data)
						if (data.TransactionCode === 'SUCCESS') {
							this.addTableList(data.Result, execId).subscribe(   // add table list to table
								(tData) => {
									done();
								},
								(err) => {
									done();
								}
							);
						} else {
							done();
						}
					}, (error) =>{
						callback("error");
						console.log("ERRRRRRRRRR", error)

					}
				);
			},
			(done) => {
				this._liveModeService.documentDetails(execId, taskId).subscribe(
					(data) => {
						console.log("documentDetails   ", data);
						if(data.TransactionCode == "SUCCESS"){
						this.addDocumentDetail(data.Result, execId).subscribe(
							(dData) => {
								done();
							},
							(err) => {
								done();
							}
						);
						}
						else{
							done();
							//alert("Data No Found");
						}
					},(error) =>{
						callback("error");
						console.log("error");
					}
				);
			},
			(done) => {
				this._liveModeService.tableDetails(execId, taskId, exExeId).subscribe(
					(data) => {
						console.log("_liveModeService  ", data);
						if (data.Result) {
							this.addLatestTableDetails(data.Result, execId).subscribe(
								(addedData) => {
									done();
								},
								(err) => {
									done();
								}
							);
						} else {
							done();
						}
					},(error) =>{
						callback("error");
						console.log("error");
					}
				);
			},
			(done) => {
				this._liveModeService.GetAttachSnapShotSequence(execId).subscribe(
					(data) => {
						console.log("_liveModeService 444  ", data);
						if (data.Result) {
							this.addExecutionSnapDetails(data.Result).subscribe(
								(addedData) => {
									done();
								},
								(err) => {
							 		done();
								}
							);
						} else {
							done();
						}
					},(error) =>{
						callback("error");
						console.log("error");
					}
				);
			},
			// (done) => {
			// 	this._liveModeService.GetDeviationDetails(execId, taskId).subscribe(
			// 		(data) => {
			// 			if (data.TotalCount != 0) {
			// 				console.log("addExecutionVerifiedByUser", data);
			// 				 this.addDeviationDetails(data, execId).subscribe(
			// 				 	(tData) => {
			// 				 		done();
			// 				 	},
			// 					(err) => {
			// 						done();
			// 					}
			// 				);
			// 			} else {
			// 				done();
			// 			}
			// 		},(error) =>{
			// 			callback("error");
			// 			console.log("error");
			// 		}
			// 	);
			// },
			(done) => {
				this._liveModeService.getExecutionVerifiedByUser(execId).subscribe(
					(data) => {
						if (data.TotalCount != 0) {
							console.log("addExecutionVerifiedByUser", data);
							this.addExecutionVerifiedByUser(data.Result, execId).subscribe(
								(tData) => {
									done();
								},
								(err) => {
									done();
								}
							);
						} else {
							done();
						}
					},(error) =>{
						callback("error");
						console.log("error");
					}
				);
			}
		], () => {
			this._liveModeService.tableStatus(execId).subscribe(
				(data) => {
					console.log("_liveModeService.tableStatus  ",data);
					if (data.TransactionCode === 'SUCCESS') {
						
						this.addTableStatus(data.Result, execId).subscribe(
							(tData) => {
								callback("success");
							},
							(err) => {
								
								callback("dberror");
							}
						);
					} else {
						//console.log("_liveModeService.tableStatus  3333", err)
						callback("fail");
					}
				}, (error) => {
					callback("error " + error);
				}
			);
		},
		
		
		
		
		);
	}


	public getAllExecutionList(): Observable<any> {
		return Observable.create((observer) => {
			this.taskListDb.allDocs({ include_docs: true }, function (err, resp) {
				if (!err && resp) {
					
					observer.next(resp.rows);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public getUnPushedTaskList(): Observable<any> {
		return Observable.create((observer) => {
			this.taskListDb.allDocs({ include_docs: true }, function (err, resp) {
				const taskList = [];
				if (!err && resp) {
					resp.rows.forEach((data) => {
						if (data.doc.statusPush) {
							taskList.push(data.doc);
						}
					});
				}
				if (!err && resp) {
					observer.next(taskList);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public getTaskById(execId: string): Observable<any> {
		return Observable.create((observer) => {
			this.taskListDb.get(`${execId}`, (err, doc) => {
				if (!err && doc) {
					observer.next(doc);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public updateTaskById(execId, state): Observable<any> {
		return Observable.create((observer) => {
			const id = `${execId}`;
			this.taskListDb.get(id, (err, doc) => {
				doc._id = id;
				
				if (state === 'statusPull') {
					doc.statusPull = false;
					doc.statusPush = false;
				}
				if (state === 'statusPush') {
					doc.statusPush = true;
					doc.NewTaskStatus = 0;
					// doc.statusPull = false;
				}

				this.taskListDb.put(doc, (error, data) => {
					if (!error && data) {
						observer.next(data);
						observer.complete();
					} else {
						observer.error(error);
						observer.complete();
					}
				});
			});
		});
	}

	// document add

	public addDocumentDetail(data: any, execId): Observable<any> {
		return Observable.create((observer) => {
			const id = `${execId}`;
			this.documentDb.get(id, (err, doc) => {
				data._id = id;
				console.log(data);
				data.buffer = data.DocumentContent._buffer;
				delete data.DocumentContent;

				if (!err && doc) {
					data._rev = doc._rev;
				}
				data.isLocalUpdate = false;
				this.documentDb.put(data, (error, data) => {
					if (!error && data) {
						observer.next(data);
						observer.complete();
					} else {
						observer.error(error);
						observer.complete();
					}
				});
			});
		});
	}

	public updateDocumentDetail(updateData: any, execId): Observable<any> {
		return Observable.create((observer) => {
			const id = `${execId}`;
			this.documentDb.get(id, (err, doc) => {
				doc._id = id;
				doc.buffer = updateData;
				doc.isLocalUpdate = true;
				doc.ExecutionID = Number(execId);
				this.documentDb.put(doc, (error, data) => {
					if (!error && data) {
						observer.next(data);
						observer.complete();
					} else {
						observer.error(error);
						observer.complete();
					}
				});
			});
		});
	}

	public getDocumentDetails(execId): Observable<any> {
		return Observable.create((observer) => {
			this.documentDb.get(execId, function (err, resp) {
				console.log("resp",resp)
				if (!err && resp) {
					observer.next(resp);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public getExecutionSnapDetailsAll(execId): Observable<any> {
		return Observable.create((observer) => {
			this.executionSnapDetailsDb.allDocs({ include_docs: true }, function (err, resp) {
				const documentList = [];
				resp.rows.forEach((data) => {
					console.log("data  ", data);
					console.log("data   ", data.doc )
					console.log("data  ", data.doc.ExecutionId);
					console.log("execId ", execId);
					if (parseInt(data.doc.ExecutionId) === parseInt(execId)) {
						
					documentList.push(data.doc);
					console.log("documentList  ", documentList);
						
					}
				});
				if (!err && resp) {
					observer.next(documentList);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public getLocalDocument(execId): Observable<any> {
		return Observable.create((observer) => {
			this.documentDb.allDocs({ include_docs: true }, function (err, resp) {
				const documentList = [];
				resp.rows.forEach((data) => {
					if (parseInt(data.id) === parseInt(execId)) {
						// if (data.doc.isLocalUpdate) {
							documentList.push(data.doc);
						// }
					}
				});
				if (!err && resp) {
					observer.next(documentList);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public getImageDoc(execId?): Observable<any> {
		return Observable.create((observer) => {
			this.imageDocumentDb.allDocs({ include_docs: true }, function (err, resp) {
			//	console.log(resp)
				const imageDocument = [];
				resp.rows.forEach((data) => {
					if (execId) {
						if (data.doc.ExecutionID === parseInt(execId)) {
							imageDocument.push(data.doc);
						}
					} else {
						imageDocument.push(data.doc);
					}
				});
				if (!err && resp) {
					observer.next(imageDocument);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public deleteImageData(fileId, done) {
		this.imageDocumentDb.get(fileId, (err, doc) => {
			if (!err && doc) {
				this.imageDocumentDb.remove(fileId, doc._rev, done);
			} else {
				done();
			}
		});
	}

	// public deleteImagebyId(fileId): Observable<any> {
	// 	return Observable.create((observer) => {
	// 	this.imageDocumentDb.get(fileId, (err, doc) => {
	// 		if (!err && doc) {
	// 			this.imageDocumentDb.remove(fileId, doc._rev);
	// 			observer.complete();
	// 		} else {
	// 			observer.error(err);
	// 			observer.complete();
				
	// 		}
	// 	});
	// });
	// }
	public deleteImagebyId(fileId) {
	
		this.imageDocumentDb.get(fileId, (err, doc) => {
			if (!err && doc) {
				this.imageDocumentDb.remove(fileId, doc._rev);
				
			} else {
				
				
			}
		});
	
	}

	public getImagebyId(fileId): Observable<any> {
		return Observable.create((observer) => {
			this.imageDocumentDb.get(fileId, (err, doc) => {
				if (!err && doc) {
					observer.next(doc);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}
	public getExecutionSnapDetails(ExeId): Observable<any> {
		return Observable.create((observer) => {
			this.executionSnapDetailsDb.get(ExeId, (err, doc) => {
				if (!err && doc) {
					observer.next(doc);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}


	// add table list

	public addTableList(inputData: any, execId): Observable<any> {
		return Observable.create((observer) => {
			const asyncTask = [];
			let firstTableId;
			inputData.forEach((value, index) => {
				const data = JSON.parse(JSON.stringify(value));
			//	console.log("addTableList    ", data);
				if (index === 0) {
					firstTableId = data.TableID;
				}
				asyncTask.push((done) => {   // updating Table List
					async.parallel([
						(pdone) => {
							const id = execId + `-${data.TableID}`;
							this.tableListDb.get(id, (err, doc) => {
								//console.log("addTableList   doc  ", doc);
								data._id = id;
								if (!err && doc) {
									data._rev = doc._rev;
								}

								data.execId = execId;
								this.tableListDb.put(data, (error, data1) => {
									pdone();
								});
							});
						},
						(pdone) => {
							this._liveModeService.mappingDetail(execId, data.TableID).subscribe(
								(mapData) => {
									console.log("mapData   ", mapData)
									this.addMappingDetails(mapData.Result, execId, data.TableID).subscribe(
										(mapDetail) => {
											pdone();
											this.tableListDb.allDocs({ include_docs: true }, function (err, resp) {
											//	console.log("getTableList  resp22222222  ",resp )
											})
										},
										(err) => {
											pdone();
										}
									);
								}
							);
						}
					], () => {
						done();
					});
				});
			});
			
			async.parallel(asyncTask, () => {    /// Updating task List
			//	console.log("asyncTask  ", asyncTask);
				if (firstTableId !== undefined) {
					this.taskListDb.get(`${execId}`, (err, doc) => {
						doc.tableId = firstTableId;
						this.taskListDb.put(doc, (error, taskData) => {
							observer.next();
							observer.complete();
						});
					});
				} else {
					observer.next();
					observer.complete();
				}
			});
		});
	}

	public addTableStatus(inputData: any, execId): Observable<any> {
		return Observable.create((observer) => {
			inputData.forEach((value, index) => {
				const data = JSON.parse(JSON.stringify(value));
			//	console.log(data);
				const id = execId +`-${data.DevTableID}`;
				this.tableListDb.get(id, (err, doc) => {
					if (!err && doc) {
						doc.tableStatus = data.Status;
					}
					this.tableListDb.put(doc, (error, taskData) => {
						if (!error && taskData) {
							observer.next();
							observer.complete();
						} else {
							observer.error(error);
							observer.complete();
						}
					});
				});
			});
		});
	}

	public updateTableStatus(execId, tableId): Observable<any> {
		return Observable.create((observer) => {
			
				const id = execId +`-${tableId}`;
				this.tableListDb.get(id, (err, doc) => {
					if (!err && doc) {
						doc.tableStatus = 'WIP';
					}
					this.tableListDb.put(doc, (error, taskData) => {
						if (!error && taskData) {
							observer.next();
							observer.complete();
						} else {
							observer.error(error);
							observer.complete();
						}
					});
				});
			
		});
	}
    
	public getTableList(execId): Observable<any> {   // get all tables in tables page
		return Observable.create((observer) => {
			this.tableListDb.allDocs({ include_docs: true }, function (err, resp) {
				//console.log("getTableList  resp  ",resp )
				const tableList = [];
				resp.rows.forEach((data) => {
				//	console.log("DATA DOC  ", data.doc)
					if (Number(execId) === data.doc.execId) {
						tableList.push(data.doc);
						//console.log("Pouch Table list  ", tableList);
					}
				});
				if (!err && resp) {
					observer.next(tableList);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public getTableById(execId, tableId): Observable<any> {
		return Observable.create((observer) => {
			this.tableListDb.get(`${execId}-${tableId}`, (err, resp) => {
				// const tableList = [];
				// resp.rows.forEach((data) => {
				// 	if (Number(execId) === data.doc.execId) {
				// 		tableList.push(data.doc);
				// 	}
				// });
				if (!err && resp) {
					observer.next(resp);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public addLatestTableDetails(inputData: any, execId): Observable<any> {
		return Observable.create((observer) => {
			const asyncTask = [];
			inputData.forEach((value, index) => {
				asyncTask.push((done) => {
					const data = JSON.parse(JSON.stringify(value));
					const id = execId + `-${data.ExeTableID}-${data.ExRowID}-${data.ExColumnID}`;
					this.latestTableDetailDb.get(id, (err, doc) => {
						data._id = id;
						if (!err && doc) {
							data._rev = doc._rev;
						}
						data.isLocalUpdate = false;
						this.latestTableDetailDb.put(data, (error, data1) => {
							console.log("data11111  ",data1)
							done();
						});
					});
				});
			});
			async.parallel(asyncTask, () => {
				observer.next();
				observer.complete();
			});
		});
	}


	public addExecutionSnapDetails(inputData: any): Observable<any> {
		return Observable.create((observer) => {
			const asyncTask = [];
			inputData.forEach((value, index) => {
			//	console.log("inputData  ", inputData)
				asyncTask.push((done) => {
					const data = JSON.parse(JSON.stringify(value));
					const id = `${data.ExecutionId}-${data.TableId}-${data.RowId}`;
					this.executionSnapDetailsDb.get(id, (err, doc) => {
						data._id = id;
						if (!err && doc) {
							data._rev = doc._rev;
						}
						//data.isLocalUpdate = false;
						this.executionSnapDetailsDb.put(data, (error, data1) => {
						//	console.log("data1", data1);
						//	console.log("error ", error);
							done();
						});
					});
				});
			});
			async.parallel(asyncTask, () => {
				observer.next();
				observer.complete();
			});
		});
	}

	public addExecutionVerifiedByUser(inputData: any, execId): Observable<any> {
		return Observable.create((observer) => {
			const asyncTask = [];
			inputData.forEach((value, index) => {
				asyncTask.push((done) => {
					const data = JSON.parse(JSON.stringify(value));
	                const id = execId + `-${data.UserID}-${data.DevTableID}`;
					this.executionVerifyDb.get(id, (err, doc) => {
						data._id = id;
						if (!err && doc) {
							data._rev = doc._rev;
						}
						data.executionID = execId;
						this.executionVerifyDb.put(data, (error, data1) => {
							done();
						});
					});
				});
			});
			async.parallel(asyncTask, () => {
				observer.next();
				observer.complete();
			});
		});
	}


	public addDeviationDetails(inputData: any, execId): Observable<any> {
		return Observable.create((observer) => {
			const asyncTask = [];
			// inputData.forEach((value, index) => {
				// asyncTask.push((done) => {
					const data = JSON.parse(JSON.stringify(inputData));
					console.log("data", data)
					// const id = execId + `-${data.TableID}-${data.RowId}-${data.DeviationID}`;
					const id = execId + `-${this.userId}`;
					this.executionDeviationDb.get(id, (err, doc) => {
						data._id = id;
						if (!err && doc) {
							data._rev = doc._rev;
						}
						data.executionID = execId;
						this.executionDeviationDb.put(data, (error, data1) => {
							//done();
							observer.next();
				observer.complete();
						});
					// });
				// });
			});
			// async.parallel(asyncTask, () => {
			// 	observer.next();
			// 	observer.complete();
			// });
		});


		// 
	}

	public getAllVerificationUsers(execId): Observable<any> {
		return Observable.create((observer) => {
			this.executionVerifyDb.allDocs({ include_docs: true }, function (err, resp) {
				const verifyUser = [];
				resp.rows.forEach((data) => {
				//	console.log("Verify data", data)
					if (Number(execId) === data.doc.executionID) {
						verifyUser.push(data.doc);
					}
				});
				if (!err && resp) {
					observer.next(verifyUser);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public getVerifyUserById(exeId, userId, devTableId): Observable<any> {
		return Observable.create((observer) => {
			const id = `${exeId}-${userId}-${devTableId}`;
			//console.log(id);	
			this.executionVerifyDb.get(id, (err, doc) => {
				if (!err && doc) {
					observer.next(doc);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public getDeviationById(exeId, userId): Observable<any> {
		return Observable.create((observer) => {
			const id = `${exeId}-${userId}`;
			//console.log(id);	
			this.executionDeviationDb.get(id, (err, doc) => {
				if (!err && doc) {
					observer.next(doc);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}


	public updateLatestTableDetails(inputData: any, execId): Observable<any> {
	//	console.log("inputData   ", inputData);
		return Observable.create((observer) => {
			inputData.forEach((value, index) => {
				const data = JSON.parse(JSON.stringify(value));
				const id = execId + `-${data.ExeTableID}-${data.ExRowID}-${data.ExColumnID}`;
				this.latestTableDetailDb.get(id, (err, doc) => {
					data._id = id;
					if (!err && doc) {
						data._rev = doc._rev;
					}
					data.isLocalUpdate = true;
					this.latestTableDetailDb.put(data, (error, data1) => {
						if (!error && data1) {
							observer.next(data1);
							observer.complete();
						} else {
							observer.error(error);
							observer.complete();
						}
					});
				});
			});
		});
	}

	public getLatestTableDetatils(execId): Observable<any> {
		return Observable.create((observer) => {
			this.latestTableDetailDb.allDocs({ include_docs: true }, function (err, resp) {
				const tableList = [];
				resp.rows.forEach((data) => {
					if (Number(execId) === data.doc.ExecutionID) {
						tableList.push(data.doc);
					}
				});
				if (!err && resp) {
					observer.next(tableList);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public checkTaskListTableDetail(execId, tableId, LocalUpdate): Observable<any> {
		return Observable.create((observer) => {
			this.latestTableDetailDb.allDocs({ include_docs: true }, function (err, resp) {
				//console.log(resp);
			//	console.log("execId  ", execId)
			//	console.log("tableId   ", tableId)
				const checkList = [];
				resp.rows.forEach((data) => {
				//	console.log(data);
					if(LocalUpdate == -2){
						
						if (Number(execId) === data.doc.ExecutionID && Number(tableId) === data.doc.ExeTableID && !data.doc.isLocalUpdate) {
							checkList.push(data.doc);
						}
					}
					else{
						if (Number(execId) === data.doc.ExecutionID && Number(tableId) === data.doc.ExeTableID && Number(LocalUpdate) === data.doc.ExRowID) {
							checkList.push(data.doc);
						}
					
					}
					
				});
				if (!err && resp) {
					observer.next(checkList);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public getLocalLatestTableDetatils(execId): Observable<any> {
		return Observable.create((observer) => {
			this.latestTableDetailDb.allDocs({ include_docs: true }, function (err, resp) {
				const tableList = [];
				resp.rows.forEach((data) => {
					if (Number(execId) === data.doc.ExecutionID) {
						if (data.doc.isLocalUpdate) {
							tableList.push(data.doc);
						}
					}
				});
				if (!err && resp) {
					observer.next(tableList);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	// Table Mapping Detail
	public addMappingDetails(inputData: any, execId, TableID): Observable<any> {
		return Observable.create((observer) => {
			const asyncTask = [];
			inputData.forEach((value, index) => {
				asyncTask.push((done) => {
					const data = JSON.parse(JSON.stringify(value));
					const id = `${execId}-${TableID}-${data.ColumnID}`;
					this.tableMappingDetailDb.get(id, (err, doc) => {
						data._id = id;
						if (!err && doc) {
							data._rev = doc._rev;
						}
						data.eTableID = TableID;
						data.Ad = index;

						this.tableMappingDetailDb.put(data, (error, data1) => {
							done();
						});
					});
				});
			});
			async.parallel(asyncTask, () => {
				observer.next();
				observer.complete();
			});
		});
	}

	public getTableMapDetatils(execId, tableId): Observable<any> {
		return Observable.create((observer) => {
			this.tableMappingDetailDb.allDocs({ include_docs: true }, function (err, resp) {
				const tableList = [];
				//console.log("Reeeeeee", resp)
				resp.rows.forEach((data) => {
					// console.log('adadasdads  ', execId, tableId, data.doc.ExecutionID, data.doc.eTableID,data);
					if (Number(execId) === data.doc.ExecutionID && Number(tableId) === data.doc.eTableID) {
						tableList.push(data.doc);
					//	console.log("tableList 55555   ", tableList)
					}
				});
				if (!err && resp) {
					
					tableList.sort((a,b) =>  a.ColumnID-b.ColumnID );
					
					observer.next(tableList);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}


	public addImageLocal(data: any): Observable<any> {
		return Observable.create((observer) => {
			const id = this.newGuid();
			this.imageDocumentDb.get(id, (err, doc) => {
				data._id = id;
				data.isNew = 1;
				data.isRecorded = 0;
				if (!err && doc) {
					data._rev = doc._rev;
				}
				this.imageDocumentDb.put(data, (error, data) => {
					if (!error && data) {
						observer.next(data);
						observer.complete();
					} else {
						observer.error(error);
						observer.complete();
					}
				});
			});
		});
	}

	public updateRecordedImageLocal(value,ImageId){				
				const data = value;					
				this.imageDocumentDb.get(ImageId, (err, doc) => {
					data._id = ImageId;
					if (!err && doc) {
						data._rev = doc._rev;
					}
					data.isRecorded = 1;
					this.imageDocumentDb.put(data, (error, data1) => {
						if (!error && data1) {					
						} else {							
						}
					});
				});			
	}

	public updateImageLocal(value,ImageId) {					
					const data = (value);	
					console.log(data)				
					this.imageDocumentDb.get(ImageId, (err, doc) => {
						data._id = ImageId;
						if (!err && doc) {
							data._rev = doc._rev;
						}
						data.isNew = 0;
						this.imageDocumentDb.put(data, (error, data1) => {
							if (!error && data1) {							
							} else {							
							}
						});
					});					
		}

	newGuid() {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
			const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
		});
	}
}

// tslint:disable-next-line:max-classes-per-file
@NgModule({
	providers: [LocalPouchdbProvider, LiveModeService],
	// exports: [LocalPouchdbProvider, LiveModeService]
})
export class LocalDbServiceModule {
}
