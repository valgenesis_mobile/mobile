import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class LoginAuthGuardService implements CanActivate {

	constructor(private _router: Router) { }

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		if (!localStorage.getItem('valgen_userId') || localStorage.getItem('valgen_userId') == null) {
			// this._router.navigate(['login']);
			return true;
		} else {
			this._router.navigate(['menu']);
			return false;
		}
	}
}

@Injectable()
export class CheckLoginAuthGuardService implements CanActivate {

	constructor(private _router: Router) { }

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		console.log('jjjjjj ', localStorage.getItem('valgen_apiKey'));
		if (!localStorage.getItem('valgen_userId') || localStorage.getItem('valgen_userId') == null) {
			this._router.navigate(['login']);
			return false;
		} else {
			return true;
		}
	}
}
