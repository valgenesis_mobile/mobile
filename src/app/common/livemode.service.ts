import { Injectable, ɵConsole } from '@angular/core';
import { HttpService } from './http.service';
import { EncrDecrService } from '../common/encr-decr.service';
import * as momenttz from 'moment-timezone';

@Injectable()
export class LiveModeService {

	private userId: any;
	private tableListData: any;
	private ExecutionSnapShotSeqlst:any;

	constructor(private _httpService: HttpService,  private _encrDecr: EncrDecrService) {
		this.userId = localStorage.getItem('valgen_userId');
	}

	public executionList() {
		const siteId = localStorage.getItem('valgen_siteId');
		const data = {
			UserID : this.userId,
			SiteID : siteId
		};
		console.log("data  data  data", data)
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/BindElectronicTask`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

	public tableList(ExeID, taskId) {
		this.tableListData = {
			ExeID : ExeID,
			UserID: this.userId
		};
		console.log("BindDocumentTableList  ",this.tableListData )
		let urlChange = `Execution/BindDocumentTableList`;
		if(taskId != null) {
			this.tableListData = {
				ExeID : ExeID,
				TaskId : taskId, 
				UserID: this.userId
			};
		}
		console.log("this.tableListData  ", this.tableListData);
		const encryptValue = this._encrDecr.encryptURL(this.tableListData);
		const info = {
			url: urlChange,
			body:encryptValue
		};
		console.log("Inforrrr ", info)
		return this._httpService.post(info);
	}

	public tableStatus(ExeID) {
		const data = {
			ExeID : ExeID,
			Mode : 1
		}
		
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/GetExecutionTableStatus`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

	public getExecutionVerifiedByUser(ExeID) {
		const siteId = localStorage.getItem('valgen_siteId');
		const data = {
			ExeID : ExeID,
			SiteID : siteId
		}
		console.log("GetExecutionVerifiedByUsers ", data)
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/GetExecutionVerifiedByUsers`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

	public GetDeviationDetails(ExeID, taskId) {
		const data = {
			UserID: this.userId,
			ExeID : ExeID,
			TaskId : 0
		}

		if (taskId != null) {
			this.tableListData = {
			UserID: this.userId,
			ExeID : ExeID,
			TaskId : taskId
			};
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/GetDeviationDetails`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

	public mappingDetail(ExeID, tableId) {
		const data = {
			UserID: this.userId,
			ExeID : ExeID,
			TableID: tableId
		}
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/BindMappingDetails`,
			body : encryptValue
		};
		return this._httpService.post(info);
		// const info = {
		// 	url: `Execution/BindMappingDetails?userId=${this.userId}&ExeID=${ExeID}&tableID=${tableId}`,
		// };
		// return this._httpService.get(info);
	}

	public GetAttachSnapShotSequence(ExeID) {
		const data = {
			ExeID : ExeID,
		}
		console.log("GetAttachSnapShotSequence   ", data)
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/GetAttachSnapShotSequence`,
			body : encryptValue
		};
		return this._httpService.post(info);
		// const info = {
		// 	url: `Execution/BindMappingDetails?userId=${this.userId}&ExeID=${ExeID}&tableID=${tableId}`,
		// };
		// return this._httpService.get(info);
	}

	public tableDetails(ExeID, taskId, exExeId) {	
		 	
		const ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
		const ClientOffset = new Date().getTimezoneOffset();
		this.tableListData = {
			ExeID : ExeID,
			UserID: this.userId,
			ClientZone:ClientZone,
			ClientOffset:ClientOffset,
			TaskId:0,
			ExExecutionId:0
		};
		
		let urlChange = `Execution/BindExecutedTableDetails`;
		if (taskId != null) {
			this.tableListData = {
				ExeID: ExeID,
				UserID: this.userId,
				ClientZone: ClientZone,
				ClientOffset: ClientOffset,
				ExExecutionId: exExeId,
				TaskId: taskId
			};
		}
		console.log("this.tableListData  ", JSON.stringify(this.tableListData));
		const encryptValue = this._encrDecr.encryptURL(this.tableListData);
		const info = {
			url: urlChange,
			body:encryptValue
		};
		return this._httpService.post(info);
	}

	public documentDetails(ExeID, taskId) {
		this.tableListData = {
			ExeID : ExeID,
			
		};
		console.log("task id  ", ExeID , taskId)
		let urlChange = `Execution/BindDocumentStream`;
		if(taskId != null) {
			this.tableListData = {
				UserID: this.userId,
				TaskId : taskId,
				ExeID : ExeID
			};
			
		}
		console.log(" ", JSON.stringify(this.tableListData));
		const encryptValue = this._encrDecr.encryptURL(this.tableListData);
		const info = {
			url: urlChange,
			body:encryptValue
		};
		return this._httpService.post(info);
	}

	public UpdateExeTableDetail(data: any) {
		console.log("UpdateExeTableDetail  ", data)
		const info = {
			url: `Execution/UpdateExeTableDetail`,
			body: data
		};
		return this._httpService.post(info);
	}

	public UpdateExecutionSnapDetails(data: any) {
	
		const info = {
			url: `Execution/UpdateSnapShotSequencelst`,
			body: data
		};
		return this._httpService.post(info);
	}

	public UpdateDocumentData(exeId, data: any, lastTableId, lastRowId, taskIdSplit, exExeId) {

		let info = {
			url: `Execution/UpdateSyncExecutionStatus`,
			body: {
				exeId: parseInt(exeId),
				userId: parseInt(this.userId),
				docContent: data,
				lastExecutedRow: `${lastTableId}_${lastRowId}`,
				ClientZone: momenttz.tz(momenttz.tz.guess()).zoneAbbr(),
				ClientOffset: new Date().getTimezoneOffset(),
				TaskId: undefined,
				ExExecutionId: undefined
			}
		};
		console.log("UpdateSyncExecutionStatus ",info)
		if (taskIdSplit != null) {
			info.body.TaskId = parseInt(taskIdSplit);
			info.body.ExExecutionId = parseInt(exExeId);
		}
		return this._httpService.post(info);
	}

	// public UpdateDocumentData(exeId, data: any, lastTableId, lastRowId, taskIdSplit, exExeId) {
	// 		if (taskIdSplit != null) {		
	// 		let info = {
	// 			url: `Execution/UpdateSyncExecution?ExeId=${exeId}&userId=${this.userId}&lastExecutedRow=${lastTableId}_${lastRowId}&ClientZone=${momenttz.tz(momenttz.tz.guess()).zoneAbbr()}&ClientOffset=${ new Date().getTimezoneOffset()}&TaskId=${taskIdSplit}`,
	// 			body: {}
	// 		};
	// 		return this._httpService.post(info);
	// 	}
	// 	else{
	// 		let info = {
	// 			url: `Execution/UpdateSyncExecution?ExeId=${exeId}&userId=${this.userId}&lastExecutedRow=${lastTableId}_${lastRowId}&ClientZone=${momenttz.tz(momenttz.tz.guess()).zoneAbbr()}&ClientOffset=${ new Date().getTimezoneOffset()}&TaskId=0`,
	// 			body: {}
	// 		};		
	// 		return this._httpService.post(info);
	// 	}	
	// }

	public UpdateUploadFiles(data) {
	//	console.log("UpdateUploadFiles  ", (JSON.stringify(data)))
		
		const info = {
			url: `Execution/UpdateUploadFiles`,
			body: data
		};
		return this._httpService.post(info);
	}

	public uploadFileName(exeId, tableId, rowId) {
		const data = {
			ExeID : exeId,
			TableID : tableId,
			RowId : rowId,
			Type: 'attachment'
		}
		console.log("GetUploadAttachUrl  ", data)
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `Execution/GetUploadAttachUrl`,
			body : encryptValue
			// ?exeId=${exeId}&tableID=${tableId}&rowId=${rowId}&type=attachment
		};
		return this._httpService.post(info);
	}

}
