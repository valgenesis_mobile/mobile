import { HttpClient } from '@angular/common/http';
import { Injectable, NgModule } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
declare let PouchDB: any
import * as momenttz from 'moment-timezone';
import { ElogModeService } from './elogmode.service';
import async from 'async';

@Injectable()
export class ElogPouchdbProvider {
	public elogTaskDb;
	public elogTaskDetailDb;
	public elogTaskLogSectionDb;
	public elogTaskLogSectionHeaderDb;
	public elogTaskLogSectionDetailDb;
	public elogTaskImageDb;
	public elogHistoryDb;
	public elogUserName: any;
	public siteId:any;
	public secImageData =[];
	public imageFile:any;
	public sectionDetails:any;
	public taskImageData:any;
	public colSection:any;
	public apisuccess:boolean = false;

	constructor(public http: HttpClient, public _elogModeService: ElogModeService) {
		this.elogTaskDb = new PouchDB('elogTask', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		this.elogTaskDetailDb = new PouchDB('elogTaskDetails', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		this.elogTaskLogSectionDb = new PouchDB('elogTaskLogSection', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		this.elogTaskLogSectionHeaderDb = new PouchDB('elogTaskLogSectionHeader', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		this.elogTaskLogSectionDetailDb = new PouchDB('elogTaskLogSectionDetail', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		this.elogTaskImageDb = new PouchDB('elogTaskImage', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		this.elogHistoryDb = new PouchDB('elogHistory', { adapter: 'websql', revs_limit: 1, auto_compaction: true });
		this.elogUserName = localStorage.valgen_userElogName;
		this.siteId = localStorage.valgen_siteId;
	}

	public getAllElogTaskList(): Observable<any> {
		return Observable.create((observer) => {
			this.elogTaskDb.allDocs({ include_docs: true }, function (err, resp) {
				console.log("resp ", resp)
				if (!err && resp) {
					observer.next(resp.rows);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public getUnPushedElogTaskList(): Observable<any> {
		return Observable.create((observer) => {
			this.elogTaskDb.allDocs({ include_docs: true }, function (err, resp) {
				const taskList = [];
				if (!err && resp) {
					resp.rows.forEach((data) => {
						if (data.doc.statusPush) {
							taskList.push(data.doc);
						}
					});
				}
				if (!err && resp) {
					observer.next(taskList);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public addElogTaskList(inputData: any): Observable<any> {
		return Observable.create((observer) => {
			const asyncTask = [];
			inputData.forEach((value, index) => {
				asyncTask.push((callback) => {
					const data = JSON.parse(JSON.stringify(value));
					const id = `${data.ID}-${data.EntityId}-${data.SiteId}`;

					this.elogTaskDb.get(id, (err, doc) => {
						data._id = id;
						if (!err && doc) {
							data._rev = doc._rev;
						}
						data.statusPull = (doc && doc.statusPull) ? doc.statusPull : false;
						data.statusPush = (doc && doc.statusPush) ? doc.statusPush : false;
						this.elogTaskDb.put(data, (error, taskData) => {
							callback();
						});
					});
				});
			});
			async.parallel(asyncTask, () => {
				observer.next();
				observer.complete();
			});
		});
	}

	public addSingleElogTaskList(id: any,entityId, siteId, check, lock): Observable<any> {
		return Observable.create((observer) => {
			if (lock == 1) {   // download new section 
				this.updateTaskDetails(id, entityId,siteId, check, () => {
					observer.next();
					observer.complete();
				});
			}
			else     // download only particular section which is unlocked
			{
				this.updateSectionDetails(id, check, () => {
					observer.next();
					observer.complete();
				});
			}
		});
	}

	public updateTaskDetails(id, entityId,siteId, check, callback) {
		async.parallel([
			(done) => {
				this._elogModeService.taskDetails(id).subscribe(
					(data) => {
						console.log("_elogModeService  ", data)
						if (data.TransactionCode === 'SUCCESS') {
							this.addTaskDetails(data.Result, id).subscribe(
								(tData) => {
									done();
								},
								(err) => {
									done();
								}
							);
						} else {
							done();
						}
					}
				);
			},
			(done) => {
				this._elogModeService.taskLogSection(id).subscribe(
					(data) => {
						if (check == false) {
							this.addTaskLogSection(data.Result, id).subscribe(
								(dData) => {
									done();
								},
								(err) => {
									done();
								}
							);
						}
						else {

							this.addVerifyTaskLogSection(data.Result, id).subscribe(
								(dData) => {
									done();
								},
								(err) => {
									done();
								}
							);

						}


					}
				);
			},
			(done) => {
				this._elogModeService.taskLogSectionHeader(id).subscribe(
					(data) => {
						if (data.length > 0) {

							this.addTaskLogSectionHeader(data, id).subscribe(
								(dData) => {
									done();
								},
								(err) => {
									done();
								}
							);
						}
						else {
							done();
						}
					}, (err) => {

					}
				);
			},
			(done) => {
				this._elogModeService.taskLogSectionDetails(id).subscribe(
					(data) => {
						this.addTaskLogSectionDetails(data, id).subscribe(
							(dData) => {
								 console.log("updateTaskDetails   ", dData);
								done();
							},
							(err) => {
								done();
							}
						);
					}
				);
			},
			(done) => {
				this._elogModeService.taskLogHistoryDetails(id).subscribe(
					(data) => {
						this.addTaskLogHistoryDetails(data, id).subscribe(
							(dData) => {
								// console.log("updateTaskDetails   ", dData);
								done();
							},
							(err) => {
								done();
							}
						);
					}
				);
			}
		], () => {
			this.siteId = localStorage.valgen_siteId;
			this.elogTaskDb.get(`${id}-${entityId}-${this.siteId}`, (err, doc) => {
				doc.statusPull = true;
				this.elogTaskDb.put(doc, (error, taskData) => {
				callback();
				});
			});
		});
	}

	public updateSectionDetails(id, check, callback) {
		async.parallel([
			(done) => {
				this._elogModeService.taskLogSectionHeader(id).subscribe(
					(data) => {
						if (data.length > 0) {
							this.addTaskLogSectionHeader(data, id).subscribe(
								(dData) => {
									done();
								},
								(err) => {
									done();
								}
							);
						}
					}, (err) => {

					}
				);
			},
			(done) => {
				this._elogModeService.taskLogSectionDetails(id).subscribe(
					(data) => {
						this.addTaskLogSectionDetails(data, id).subscribe(
							(dData) => {
								//console.log("updateTaskDetails   ",dData);
								done();
							},
							(err) => {
								done();
							}
						);
					}
				);
			}
		]);
	}

	public addTaskDetails(data: any, taskId): Observable<any> {
		return Observable.create((observer) => {
			data.forEach((value, index) => {
				const data = JSON.parse(JSON.stringify(value));
				const id = `${taskId}`;
				this.elogTaskDetailDb.get(id, (err, doc) => {
					data._id = id;
					data.taskId = id;
					if (!err && doc) {
						data._rev = doc._rev;
					}
					this.elogTaskDetailDb.put(data, (error, data1) => {
						if (!error && data) {
							observer.next(data);
							observer.complete();
						} else {
							observer.error(error);
							observer.complete();
						}
					});
				});
			});
		})
	}

	public addTaskLogSection(inputData: any, taskId): Observable<any> {
		return Observable.create((observer) => {
			const asyncTask = [];
			inputData.forEach((value, index) => {
				if (value.LockedStatus === "Locked" && value.LockedBy === this.elogUserName) {
					asyncTask.push((done) => {
						const data = JSON.parse(JSON.stringify(value));
						const id = `${taskId}-${data.FormSectionId}-${data.LMlogDataSectionID}`;
						this.elogTaskLogSectionDb.get(id, (err, doc) => {
							data._id = id;
							data.taskId = taskId;
							if (!err && doc) {
								data._rev = doc._rev;
							}
							this.elogTaskLogSectionDb.put(data, (error, data1) => {
								done();
							});
						});
					});
				}


			});
			async.parallel(asyncTask, () => {
				observer.next();
				observer.complete();
			});
		});
	}
	public addVerifyTaskLogSection(inputData: any, taskId): Observable<any> {
		return Observable.create((observer) => {
			const asyncTask = [];
			inputData.forEach((value, index) => {
				asyncTask.push((done) => {
					const data = JSON.parse(JSON.stringify(value));
					const id = `${taskId}-${data.FormSectionId}-${data.LMlogDataSectionID}`;
					this.elogTaskLogSectionDb.get(id, (err, doc) => {
						data._id = id;
						data.taskId = taskId;
						if (!err && doc) {
							data._rev = doc._rev;
						}
						this.elogTaskLogSectionDb.put(data, (error, data1) => {
							done();
						});
					});
				});
				// }


			});
			async.parallel(asyncTask, () => {
				observer.next();
				observer.complete();
			});
		});
	}


	public addTaskLogSectionHeader(inputData: any, taskId): Observable<any> {
		return Observable.create((observer) => {
			const asyncTask = [];
			inputData.forEach((value, index) => {
				asyncTask.push((done) => {
					const data = JSON.parse(JSON.stringify(value));
					const id = `${taskId}-${data.FormSectionId}`;
					this.elogTaskLogSectionHeaderDb.get(id, (err, doc) => {
						data._id = id;
						data.taskId = taskId;
						if (!err && doc) {
							data._rev = doc._rev;
						}
						this.elogTaskLogSectionHeaderDb.put(data, (error, data1) => {
							done();
						});
					});
				});
			});
			async.parallel(asyncTask, () => {
				observer.next();
				observer.complete();
			});
		});
	}

	public addTaskLogSectionDetails(inputData: any, taskId): Observable<any> {
		return Observable.create((observer) => {
			const asyncTask = [];
			inputData.forEach((value, index) => {
				asyncTask.push((done) => {
					const data = JSON.parse(JSON.stringify(value));
					const id = `${taskId}-${data.FormSectionId}-${data.LogDataSectionId}`;
					this.elogTaskLogSectionDetailDb.get(id, (err, doc) => {
						data._id = id;
						data.taskId = taskId;
						if (!err && doc) {
							data._rev = doc._rev;
						}
						this.elogTaskLogSectionDetailDb.put(data, (error, data1) => {
							done();
						});
					});
				});
			});
			async.parallel(asyncTask, () => {
				observer.next();
				observer.complete();
			});
		});
	}

	public addTaskLogHistoryDetails(inputData: any, taskId): Observable<any> {
		return Observable.create((observer) => {
			const asyncTask = [];
			inputData.forEach((value, index) => {
				asyncTask.push((done) => {
					const data = JSON.parse(JSON.stringify(value));
					const id = `${taskId}-${data.FormSectionId}-${data.LogDataSectionId}`;
					this.elogHistoryDb.get(id, (err, doc) => {
						data._id = id;
						data.taskId = taskId;
						if (!err && doc) {
							data._rev = doc._rev;
						}
						this.elogHistoryDb.put(data, (error, data1) => {
							done();
						});
					});
				});
			});
			async.parallel(asyncTask, () => {
				observer.next();
				observer.complete();
			});
		});
	}

	
	public deleteElogTask(taskId, callback) {
		async.series([
			(done) => {
					this.elogTaskDb.allDocs({ include_docs: true }, (err, doc) => {
						const elogTask = [];
						doc.rows.forEach((data) => {
							if (data.doc.ID === parseInt(taskId)) {
								elogTask.push(data.doc);
							}
						});
						if (elogTask.length != 0) {
							elogTask.forEach((task, index) => {
								this.elogTaskDb.remove(task);
							});
							done();
						} else {
							done();
						}
					});
				}

			], () => {
				callback();
			})
		}


	public deleteLocalData(taskId, callback) {
		async.series([
			(done) => {
				this.elogTaskDetailDb.allDocs({ include_docs: true }, (err, doc) => {
					const taskDetail = [];
					doc.rows.forEach((data) => {
						if (data.doc.taskId === parseInt(taskId)) {
							taskDetail.push(data.doc);
						}
					});

					if (taskDetail.length != 0) {
						taskDetail.forEach((task, index) => {
							this.elogTaskDetailDb.remove(task);
						});
						done();
					} else {
						done();
					}
				});
			},
			(done) => {
				this.elogTaskDb.allDocs({ include_docs: true }, (err, doc) => {
					const elogTask = [];
					doc.rows.forEach((data) => {
						if (data.doc.ID === parseInt(taskId)) {
							elogTask.push(data.doc);
						}
					});
					if (elogTask.length != 0) {
						elogTask.forEach((task, index) => {
							this.elogTaskDb.remove(task);
						});
						done();
					} else {
						done();
					}
				});
			},
			(done) => {
				this.elogTaskLogSectionDb.allDocs({ include_docs: true }, (err, doc) => {
					const elogTaskSection = [];
					doc.rows.forEach((data) => {
						if (data.doc.taskId === parseInt(taskId)) {
							elogTaskSection.push(data.doc);
						}
					});
					if (elogTaskSection.length != 0) {
						elogTaskSection.forEach((taskDetail, index) => {
							this.elogTaskLogSectionDb.remove(taskDetail);
						});
						done();
					} else {
						done();
					}
				});
			},
			(done) => {
				this.elogTaskLogSectionHeaderDb.allDocs({ include_docs: true }, (err, doc) => {
					const elogTaskSectionHeader = [];
					doc.rows.forEach((data) => {
						if (data.doc.taskId === parseInt(taskId)) {
							elogTaskSectionHeader.push(data.doc);
						}
					});
					if (elogTaskSectionHeader.length != 0) {
						elogTaskSectionHeader.forEach((sectionHeader, index) => {
							this.elogTaskLogSectionHeaderDb.remove(sectionHeader);
						});
						done();
					} else {
						done();
					}
				});
			},
			(done) => {
				this.elogTaskLogSectionDetailDb.allDocs({ include_docs: true }, (err, doc) => {
					const sectionDetail = [];
					doc.rows.forEach((data) => {
						if (data.doc.taskId === parseInt(taskId)) {
							sectionDetail.push(data.doc);
						}
					});
					if (sectionDetail.length != 0) {
						sectionDetail.forEach((document, index) => {
							this.elogTaskLogSectionDetailDb.remove(document);
						});
						done();
					} else {
						done();
					}
				});
			},
			(done) => {
				this.elogHistoryDb.allDocs({ include_docs: true }, (err, doc) => {
					const sectionDetail = [];
					doc.rows.forEach((data) => {
						if (data.doc.taskId === parseInt(taskId)) {
							sectionDetail.push(data.doc);
						}
					});
					if (sectionDetail.length != 0) {
						sectionDetail.forEach((document, index) => {
							this.elogHistoryDb.remove(document);
						});
						done();
					} else {
						done();
					}
				});
			}
		], () => {
			callback();
		});
	}


	public deleteSingleSectionLocalData(taskId, sectionId, logDataSectionID, callback) {
		const id = `${taskId}-${sectionId}`;
		const detail_id = `${taskId}-${sectionId}-${logDataSectionID}`;
		async.series([
			// (done) => {
			// 	this.getElogTaskLogSingleSection(id).subscribe(
			// 		(dData) => {
			// 		console.log('asdadadsdsa111 ', dData);
			// 		this.elogTaskLogSectionDb.remove(dData);
			// 			done();	
			// 	});
			// },
			(done) => {

				this.getElogTaskHeaderSection(id).subscribe(
					(dData) => {
						this.elogTaskLogSectionHeaderDb.remove(dData);
						done();
					});

			},
			(done) => {

				this.getElogTaskDetailSection(detail_id).subscribe(
					(dData) => {
						this.elogTaskLogSectionDetailDb.remove(dData);
						done();
					});

			},
			(done) => {

				this.getElogHistory(detail_id).subscribe(
					(dData) => {
						this.elogHistoryDb.remove(dData);
						done();
					});

			}
		], () => {
			callback();
		});
	}

	public deleteDatabase(): Observable<any> {
		return Observable.create((observer) => {
			async.series([
				(done) => {
					this.elogTaskDb.destroy();
					done();
				},
				(done) => {
					this.elogTaskDetailDb.destroy();
					done();
				},
				(done) => {
					this.elogTaskLogSectionDb.destroy();
					done();
				},
				(done) => {
					this.elogTaskLogSectionHeaderDb.destroy();
					done();
				},
				(done) => {
					this.elogTaskLogSectionDetailDb.destroy();
					done();
				},
				(done) => {
					this.elogHistoryDb.destroy();
					done();
				},
				(done) => {
					this.elogTaskImageDb.destroy();
					done();
				},
			], () => {
			//	console.log('Completed delete');
			});
		});
	}

	public getElogTaskDetail(taskId) {
		return Observable.create((observer) => {
			this.elogTaskDetailDb.get(`${taskId}`, (err, doc) => {
				if (!err && doc) {
					observer.next(doc);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public getElogTaskLogSection(taskId) {
		return Observable.create((observer) => {
			this.elogTaskLogSectionDb.allDocs({ include_docs: true }, function (err, resp) {
				const logSection = [];
				resp.rows.forEach((data) => {
					if (data.doc.taskId === parseInt(taskId)) {
						logSection.push(data.doc);
					}
				});
				if (!err && resp) {
					observer.next(logSection);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public getElogTaskLogSectionbyID(Id) {
		return Observable.create((observer) => {
			this.elogTaskLogSectionDb.get(`${Id}`, (err, doc) => {
				if (!err && doc) {
					observer.next(doc);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});

	}

	public updateTaskLogSection(taskId, formSectionId, logDataSectionID, data): Observable<any> {
		return Observable.create((observer) => {
			const id = `${taskId}-${formSectionId}-${logDataSectionID}`;
			this.elogTaskLogSectionDb.get(id, (err, doc) => {
				doc._id = id;
				doc.LockedStatus = data;
				this.elogTaskLogSectionDb.put(doc, (error, data1) => {
					if (!error && data1) {
						observer.next(data1);
						observer.complete();
					} else {
						observer.error(error);
						observer.complete();
					}
				});
			});
		});
	}
	public getElogTaskLogSingleSection(id) {
		return Observable.create((observer) => {
			this.elogTaskLogSectionDb.get(id, (err, doc) => {
				if (!err && doc) {
					observer.next(doc);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}



	public getElogTaskHeaderSection(id) {
		return Observable.create((observer) => {
			this.elogTaskLogSectionHeaderDb.get(`${id}`, (err, doc) => {
				if (!err && doc) {
					observer.next(doc);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public getElogTaskDetailSection(id) {
		return Observable.create((observer) => {
			this.elogTaskLogSectionDetailDb.get(`${id}`, (err, doc) => {
				if (!err && doc) {
					observer.next(doc);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	
	public getElogHistory(id) {
		return Observable.create((observer) => {
			this.elogHistoryDb.get(`${id}`, (err, doc) => {
				if (!err && doc) {
					observer.next(doc);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public updateElogTaskDetailSection(id, data) {
		return Observable.create((observer) => {
			this.elogTaskLogSectionDetailDb.get(`${id}`, (err, doc) => {
				data._id = id;

				if (!err && doc) {
					data._rev = doc._rev;
				}
				this.elogTaskLogSectionDetailDb.put(data, (error, data1) => {
					if (!error && data1) {
						observer.next(data1);
						observer.complete();
					} else {
						observer.error(error);
						observer.complete();
					}
				});
			});
		});
	}

	public updateElogTaskById(taskId,EntityId,siteId, state) {
		return Observable.create((observer) => {			
			const id = `${taskId}-${EntityId}-${siteId}`;
			this.elogTaskDb.get(`${id}`, (err, doc) => {			
				doc._id = id;
				if (state === 'statusPull') {
					doc.statusPull = true;
				}
				if (state === 'statusPush') {
				   doc.statusPull = false;
				}
				this.elogTaskDb.put(doc, (error, data) => {
					if (!error && data) {
						observer.next(data);
						observer.complete();
					} else {
						observer.error(error);
						observer.complete();
					}
				});
			})
		});
	}

	public getAllElogTaskDetailDetail(taskId): Observable<any> {
		return Observable.create((observer) => {
			this.elogTaskLogSectionDetailDb.allDocs({ include_docs: true }, function (err, resp) {
				const taskSectionDetail = [];
				resp.rows.forEach((data) => {
					if (data.doc.taskId === parseInt(taskId)) {
						taskSectionDetail.push(data.doc);
					}
				});
				if (!err && resp) {
					observer.next(taskSectionDetail);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public getAllElogHistoryDetail(taskId): Observable<any> {
		return Observable.create((observer) => {
			this.elogHistoryDb.allDocs({ include_docs: true }, function (err, resp) {
				const taskSectionDetail = [];
				resp.rows.forEach((data) => {
					if (data.doc.taskId === parseInt(taskId)) {
						taskSectionDetail.push(data.doc);
					}
				});
				if (!err && resp) {
					observer.next(taskSectionDetail);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}


	public getAllElogTaskHeaderDetail(taskId): Observable<any> {
		return Observable.create((observer) => {
			this.elogTaskLogSectionHeaderDb.allDocs({ include_docs: true }, function (err, resp) {
				const taskSectionHeader = [];
				resp.rows.forEach((data) => {
					if (data.doc.taskId === parseInt(taskId)) {
						taskSectionHeader.push(data.doc);
					}
				});
				if (!err && resp) {
					observer.next(taskSectionHeader);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}
	

	public pushLocalData(taskId): Observable<any> {

		return Observable.create((observer) => {
		const ClientOffset = new Date().getTimezoneOffset();
		const ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
		this.siteId = localStorage.valgen_siteId;
		this.imageFile = [];
				this.getElogTaskLogSection(taskId).subscribe((logSection) => {
				
					this.getAllElogTaskDetailDetail(taskId).subscribe(
						(localFileData) => {
							logSection.forEach((logSec) => {
								this.imageFile = [];
								localFileData.forEach((local) => {
									if(local.ReferenceType == 'Header' || local.ReferenceType == 'Footer')							
									{   // Header & Footer Save
										const uploadHeader = {
											LMLogFormInitiateID: taskId,
											CreatedBy: localStorage.getItem('valgen_userId'),
											StartDateTime: "",
											EndDateTime: "",
											HoldTimeReq: 1,
											HoldTime: "1",
											completed: 1,
											dtSection: []
										   };	
										local.LMWorkSheetFormField.forEach((col) => {										
											const header = {
												LMFormSectionId: local.FormSectionId,
												RowId: col.RowIndex,
												FieldId: col.fieldid,
												Value: col.Value
											};
											uploadHeader.dtSection.push(header);
				
										});
										   
										this._elogModeService.SaveHeaderFooter(uploadHeader).subscribe(
											(data) => {
												this.apisuccess = true;
											}, (err) => {
												this.apisuccess = false;									
												observer.complete();
												observer.error();
												observer.next("error");
												
											})	
									 }
                                    else{
									if (logSec.FormSectionId == local.FormSectionId) {
										const uploadData = {
											LMLogFormInitiateID: taskId,
											Code: local.SectionName,
											uniquecode: local.SectionName,
											CreatedBy: localStorage.getItem('valgen_userId'),
											StartDateTime: "2019-06-07 04:24:11.000",
											EndDateTime: "2019-06-07 04:26:11.000",
											HoldTimeReq: 1,
											HoldTime: "1",
											completed: 1,
											ActionFlag: 1,
											SiteId:this.siteId,
											ClientOffset: ClientOffset,
							                ClientZone : ClientZone,
											Section: [{
												LMFormSectionId: local.FormSectionId,
												LMLockSectionId: local.LogDataSectionId,
												SectionDetails: []
											}]
										};
																
											local.LMWorkSheetFormField.forEach((col) => {
												if(col.fieldlabel == "Evidence")
												{
													if(col.FileStream){

														const sectionDetail = {
															RowId: col.RowIndex,
															FieldId: col.fieldid,
															Value: col.Value,
															DocDetails: col.FileStream														
														}													
														uploadData.Section[0].SectionDetails.push(sectionDetail);
													}
													else{
														const sectionDetail = {
															RowId: col.RowIndex,
															FieldId: col.fieldid,
															Value: col.Value											
														}
														uploadData.Section[0].SectionDetails.push(sectionDetail);

													}
												
												}
												else{
													const sectionDetail = {
														RowId: col.RowIndex,
														FieldId: col.fieldid,
														Value: col.Value											
													}
													uploadData.Section[0].SectionDetails.push(sectionDetail);
												}												
											});																				

										if (uploadData.Section[0].SectionDetails.length > 0) {
											this._elogModeService.UpdateTaskSectionDetail(uploadData).subscribe(
												(data) => {
													this.apisuccess = true;
												}, (err) => {
													observer.complete();
													observer.error();
													observer.next("error");
													this.apisuccess = false;
													console.log(err)
												})
												
										}
										else{
											
											this._elogModeService.ActivityCapture(taskId,0,0).subscribe(
												(data) => {
												}, (err) => {
													console.log(err)
												})
										}
									}
								}
								

								})
							})
							
								observer.next("success");
							    observer.complete();						
						}, (err) => {
							observer.complete();
							observer.error();
							observer.next("error");
						}
					);
				})			
	})
	}

	public pushSingleSectionLocalData(taskId, sectionId,logDataSectionID, callback) {
		this.secImageData = [];
		const ClientOffset = new Date().getTimezoneOffset();
		const ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
		this.siteId = localStorage.valgen_siteId;
		async.series([	
			(done) => {
				const formSectionId = `${taskId}-${sectionId}-${logDataSectionID}`;
				this.getElogTaskDetailSection(formSectionId).subscribe(
					(localFileData) => {			
						this.imageFile = [];
						  localFileData.LMWorkSheetFormField.forEach((colNew) => 
								{				  		  	
									 if (colNew.fieldlabel === 'Evidence') {
										 if(colNew.FileStream != null){
									      colNew.FileStream.forEach((file) => {
                                            const imageFileObject =
										{
										   FileName:  file.FileName,
										   FileContent: file.FileContent,
										   CreatedBy:parseInt(localStorage.getItem('valgen_userId')),
					                       UserName: localStorage.getItem('valgen_userName'),
										};
										this.imageFile.push(imageFileObject);
									
										  })	
										}								 		
								   }
								})

						const uploadData = {
							LMLogFormInitiateID: taskId,
							Code: localFileData.SectionName,
							uniquecode: localFileData.SectionName,
							CreatedBy: localStorage.getItem('valgen_userId'),
							StartDateTime: "2019-06-07 04:24:11.000",
							EndDateTime: "2019-06-07 04:26:11.000",
							HoldTimeReq: 1,
							HoldTime: "1",
							completed: 1,
							ActionFlag: 1,
							SiteId:this.siteId,
							ClientOffset: ClientOffset,
							ClientZone : ClientZone,
							Section: [{
								LMFormSectionId: localFileData.FormSectionId,
								LMLockSectionId: localFileData.LogDataSectionId,
								SectionDetails: []
							}]
						};

						if(this.imageFile.length > 0){	
							localFileData.LMWorkSheetFormField.forEach((col) => {
								if(col.fieldlabel == "Evidence")
								{
									const sectionDetail = {
										RowId: col.RowIndex,
										FieldId: col.fieldid,
										Value: col.Value,
										DocDetails: this.imageFile
										
									}
									uploadData.Section[0].SectionDetails.push(sectionDetail);
								}
								else{
									const sectionDetail = {
										RowId: col.RowIndex,
										FieldId: col.fieldid,
										Value: col.Value,
							
									}
									uploadData.Section[0].SectionDetails.push(sectionDetail);
								}								
							});
							
						}

						else
						{
							localFileData.LMWorkSheetFormField.forEach((col) => {
								const sectionDetail = {
									RowId: col.RowIndex,
									FieldId: col.fieldid,
									Value: col.Value
								};
								uploadData.Section[0].SectionDetails.push(sectionDetail);	
							});
						}
                       
                        if(localFileData.LMWorkSheetFormField.length > 0){
							this._elogModeService.checkLockStatus(taskId, localFileData.LogDataSectionId, localFileData.LMWorkSheetFormField[0].EntityPartId).subscribe(
							(data) => {}
						)
						}
						else
						{						
						this._elogModeService.checkLockStatus(taskId, localFileData.LogDataSectionId, 0).subscribe(
							(data) => {}
						)
						}

						if (uploadData.Section[0].SectionDetails.length > 0) {							
							this._elogModeService.UpdateTaskSectionDetail(uploadData).subscribe(
								(data) => {}
							)
						}
						else{
											
							this._elogModeService.ActivityCapture(taskId,0,0).subscribe(
								(data) => {
								}, (err) => {
									console.log(err)
								})
						}
						done();
					// }, 5000)
						
					}, (err) => {
						console.log('data', err);
						done();
					});
			}
		], () => {
			callback();
		});
	}

	public addImageLocal(data: any, taskId): Observable<any> {
		return Observable.create((observer) => {
			const id = this.newGuid();
			this.elogTaskImageDb.get(id, (err, doc) => {
				data._id = id;
				if (!err && doc) {
					data._rev = doc._rev;
				}
				this.elogTaskImageDb.put(data, (error, data) => {
					if (!error && data) {
						observer.next(data);
						observer.complete();
					} else {
						observer.error(error);
						observer.complete();
					}
				});
			});
		});
	}

	public getImagebyId(fileId): Observable<any> {
		return Observable.create((observer) => {
			this.elogTaskImageDb.get(fileId, (err, doc) => {
				if (!err && doc) {
					observer.next(doc);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	public getImageDoc(taskId?): Observable<any> {
		return Observable.create((observer) => {
			this.elogTaskImageDb.allDocs({ include_docs: true }, function (err, resp) {
				const imageDocument = [];
				resp.rows.forEach((data) => {
					if (taskId) {
						if (data.doc.taskId === parseInt(taskId)) {
							imageDocument.push(data.doc);
						}
					} else {
						imageDocument.push(data.doc);
					}
				});
				if (!err && resp) {
					observer.next(imageDocument);
					observer.complete();
				} else {
					observer.error(err);
					observer.complete();
				}
			});
		});
	}

	newGuid() {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
			const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
		});
	}
}

@NgModule({
	providers: [ElogPouchdbProvider, ElogModeService],
	// exports: [LocalPouchdbProvider, LiveModeService]
})
export class ElogDbServiceModule {
}
