import { Injectable, NgModule } from '@angular/core';
import { Http, Headers, Response, RequestOptions, HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable({
	providedIn: 'root'
})
export class HttpService {
	private defaultUrl: string;

	constructor(private http: Http) {
		
	}

	public get(info: any): any {
		console.log('GET  SERVER     ', info);
		if (this.preCheck(info, false)) {
			return this.http.get(info.url, info.options)
				.map(this.extractData)
				.catch(this.handleError);
		} else {
			return Observable.throw('apiKey missing');
		}
	}

	public post(info: any): any {
		console.log('Service', info);
		if (this.preCheck(info, false)) {
			return this.http.post(info.url, info.body, info.options)
				.map(this.extractData)
				.catch(this.handleError);
		} else {
			return Observable.throw('apiKey missing');
		}
	}

	public put(info: any): any {
		if (this.preCheck(info, false)) {
			return this.http.put(info.url, info.body, info.options)
				.map(this.extractData)
				.catch(this.handleError);
		} else {
			return Observable.throw('apiKey missing');
		}
	}

	public delete(info: any): any {
		if (this.preCheck(info, false)) {
			return this.http.delete(info.url, info.options)
				.map(this.extractData)
				.catch(this.handleError);
		} else {
			return Observable.throw('apiKey missing');
		}
	}

	private preCheck(info: any, isXMLHttpRequest: boolean): any {
		const apiKey = '';
		this.defaultUrl = localStorage.getItem('valgen_settingUrl');

		if (info && info.url) {
			if (!isXMLHttpRequest) {
				const headers: Headers = new Headers({ 'Content-Type': 'application/json' });
				const options = new RequestOptions({ headers });
				// if (apiKey) {
				// 	headers.append('api_key', apiKey);
				// }
				// headers.append('clientId', '');

				info.options = options;
			} else {
				info.headers = {};
				// if (apiKey) {
				// 	info.headers['api_key'] = apiKey;
				// }
				// info.headers['clientId'] = '';

			}
			info.url = this.defaultUrl + info.url;
			return true;
		} else {
			return false;
		}
	}

	private handleError(error: Response): any {
		console.error('Error', error.status, error.json());
		const body = error.json();
		if (error.status === 401) {
			localStorage.removeItem('valgen_userId');
		}
		return Observable.throw(body.TransactionMessage);
	}

	private extractData(res: Response): any {
		const body = res.json();
	//	console.log('extractData', body);
		return body || {};
	}

}

// tslint:disable-next-line:max-classes-per-file
@NgModule({
	providers: [HttpService],
	imports: [HttpModule]
})
export class CommonServiceModule {
}
