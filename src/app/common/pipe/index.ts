import { Injectable, NgModule } from '@angular/core';
import { SanitizerPipe, KeysPipe, ReversePipe, SanitizerUrlPipe,TruncatePipe, ArraySortPipe } from './sanitizer.pipe';

@NgModule({
    declarations: [SanitizerPipe, KeysPipe, ReversePipe, SanitizerUrlPipe, TruncatePipe, ArraySortPipe],
    exports: [SanitizerPipe, KeysPipe, ReversePipe, SanitizerUrlPipe, TruncatePipe, ArraySortPipe]
})

export class CommonPipesModule {
}
