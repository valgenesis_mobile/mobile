import { Pipe, PipeTransform, Injectable } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
import * as _ from 'lodash';
@Pipe({
	name: 'sanitizeHtml'
})
@Injectable()
export class SanitizerPipe implements PipeTransform {

	constructor(private _sanitizer: DomSanitizer) { }

	transform(value: any): any {
		return this._sanitizer.bypassSecurityTrustHtml(value);
	}
}

@Pipe({ name: 'sanitizeURL' })
export class SanitizerUrlPipe implements PipeTransform {
	constructor(private _sanitizer: DomSanitizer) { }

	transform(value: any): any {
		return this._sanitizer.bypassSecurityTrustUrl(value);
	}
}

@Pipe({ name: 'keys' })
export class KeysPipe implements PipeTransform {
	transform(value, args: string[]): any {
		let keys = [];
		for (let key in value) {
			keys.push(key);
		}
		return keys;
	}
}


@Pipe({
	name: 'reverse',
	pure: false
})
export class ReversePipe {
	transform(value) {
		return value.slice().reverse();
	}
}
@Pipe({
	name: 'truncate'
  })
  export class TruncatePipe implements PipeTransform {
	transform(value: string, limit = 25, completeWords = false, ellipsis = '...') {
	//   if (completeWords) {
	// 	limit = value.substr(0, limit).lastIndexOf(' ');
	//   }
	const tempvalue = value.split("[").pop();
	  return `${tempvalue.substring(0, tempvalue.length - 1)}`;
	  
	}
  }

  @Pipe({
	name: "sort"
  })
  export class ArraySortPipe  implements PipeTransform {
	transform(array: any, field: string): any[] {
	  if (!Array.isArray(array)) {
		return;
	  }
	  array.sort((a: any, b: any) => {
		if (a[field] < b[field]) {
		  return -1;
		} else if (a[field] > b[field]) {
		  return 1;
		} else {
		  return 0;
		}
	  });
	  return array;
	}
  }
