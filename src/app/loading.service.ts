import { Subscription } from 'rxjs';
import { Injectable } from '@angular/core';
import {LoadingController, PopoverController} from '@ionic/angular';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { UserIdleService } from 'angular-user-idle';
import { Toast } from '@ionic-native/toast/ngx';
import { ModalPage } from './modal/modal.page';

import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
	isLoading = false;
	timeInSeconds:any;
	time:any;
	runTimer:any;
	hasStarted:any;
	hasFinished:any;
	remainingTime:any;;
	displayTime:any;
	count:any
	watchTime= true;
	modal:any;

	idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;

	constructor(public loadingController: LoadingController, private _router: Router,public _alertCtrl: AlertController, 
		private userIdle: UserIdleService,  public popoverCtrl: PopoverController, private toast: Toast,
		public idle: Idle, public keepalive: Keepalive) { }

  async present(message) {
    this.isLoading = true;
    return await this.loadingController.create({
      duration: 5000,
      message:message,
      spinner: 'bubbles'
    }).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss().then();
        }
      });
    });
  }

  async present10(message) {
    this.isLoading = true;
    return await this.loadingController.create({
      //duration: 10000,
      message:message,
      spinner: 'bubbles'
    }).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss().then();
        }
      });
    });
  }



  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then();
  }


  showLoading(message){
	this.present(message);
  }
  showLoading10(message){
	this.present10(message);
  }

  
	public checkSessionTimeOut() {
	// 	setTimeout(() => {
	// 			//this.events.publish('tabShow');
		
	// 	console.log("checkSessionTimeOut ")
	// 	const sessionTime = localStorage.getItem('valgen_sessionTimeOut');
	// 	console.log("sessionTime ", sessionTime)
	// 	const newSessionTime = (parseInt(sessionTime) - 2) * 60;
	// 	// * 60;
	// 	console.log("newSessionTime ", newSessionTime)
    
		
	// 	//this.userIdle.stopWatching();
    // this.userIdle.setConfigValues({idle: newSessionTime, timeout: 2, ping: 2});
	// 	this.userIdle.startWatching();
	// 	if(	this.modal){
	// 	this.modal.onDidDismiss().then((dataReturned) => {
	// 		// this.sessionLogout();
	// 		console.log("dataReturned  ", dataReturned)
	// 		});
	// 	}
	// 	this.userIdle.onTimerStart().subscribe(count => 
	// 		{ 
	// 			console.log("Loading   ",count)
	// 			if(count == null){
	// 				this.userIdle.resetTimer();
	// 			}
	// 		});
	// 	this.userIdle.onTimeout().subscribe(() => {
	// 		this.userIdle.stopWatching();
	// 		 if(this.watchTime){
	// 		 this.watchTime = false;
	// 		this.openModal();
	// 		 }
	// 	});	
	// 	// const currentTime = moment(moment().format('YYYY-MM-DD, h:mm:ss'));//now
	// 	// const validTime = moment(localStorage.getItem('valgen_validTimeStamp'));
	// 	// const sessionTime = localStorage.getItem('valgen_sessionTimeOut');
	// 	// if (currentTime.diff(validTime, 'minutes') > parseInt(sessionTime)) {
	// 	// 	this.alertSession('Session TimeOut service');
	// 	// } else {
	// 	// 	localStorage.setItem('valgen_validTimeStamp', moment().format('YYYY-MM-DD, h:mm:ss'));
	// 	// }

	// }, 1000);

    
      // sets an idle timeout of 5 seconds, for testing purposes.
	  this.idle.setIdle(50000);
	  // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
	  this.idle.setTimeout(5);
	  console.log("Session timeout", DEFAULT_INTERRUPTSOURCES)
	  // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
	  this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
  
	  this.idle.onIdleEnd.subscribe(() => { 
		
	    
		  this.idleState = 'No longer idle.'});
	 
		
		
		
		  this.idle.onTimeout.subscribe(() => {
		this.idleState = 'Timed out!';
		this.timedOut = true;
        this.openModal(this.idleState);
		// const url = localStorage.getItem('valgen_settingUrl');
		// 	localStorage.clear();
		// 	localStorage.setItem('valgen_settingUrl', url);
		// 	this._router.navigate(['login']);
		
	  });
	  this.idle.onIdleStart.subscribe(() => {
		
		this.idleState = 'You\'ve gone idle!'});
	  this.idle.onTimeoutWarning.subscribe((countdown) => {
		
		this.idleState = 'You will time out in ' + countdown + ' seconds!';
	  } );
  
	  // sets the ping interval to 15 seconds
	  this.keepalive.interval(15);
  
	  this.keepalive.onPing.subscribe(() => this.lastPing = new Date());
  
	 
     this.reset();


	}

	reset() {
		this.idle.watch();
		this.idleState = 'Started.';
		this.timedOut = false;
	}

	async alertSession(message) {
		this.userIdle.stopWatching();
    this.userIdle.setConfigValues({idle: 30, timeout: 30, ping: 120});
		this.userIdle.startWatching();
		this.userIdle.onTimerStart().subscribe(count =>{ 
		this.count = count;		
		console.log("Loadin", count)});
		// Start watch when time is up.
		this.userIdle.onTimeout().subscribe(() => {
			const url = localStorage.getItem('valgen_settingUrl');
			localStorage.clear();
            localStorage.setItem('valgen_settingUrl', url);
				
			alert.dismiss();
		//	this.userIdle.stopWatching();
			this._router.navigate(['login']);
		});	
		const alert = await this._alertCtrl.create({
			header:'AL',
			message: message + `${this.displayTime}`,
			backdropDismiss: false,
			buttons: [
				
				{
					text: 'Activate',
					handler: () => {
						localStorage.setItem('valgen_validTimeStamp', moment().format('YYYY-MM-DD, h:mm:ss'));
					}
				},
				{
					text: 'Logout',
					handler: () => {
						this.sessionLogout();
					}
				},

			]
		});
		await alert.present();
	}

	public sessionLogout() {
		const url = localStorage.getItem('valgen_settingUrl');
		localStorage.clear();
			localStorage.setItem('valgen_settingUrl', url);
		
	
		// this._localpdb.deleteDatabase().subscribe();
		this._router.navigate(['login']);
	}

	async openModal(idleState,ev?: any) {
		
		this.modal = await this.popoverCtrl.create({
			component: ModalPage,
			event: ev,
			//cssClass: 'popupClass',
			animated: true,
			showBackdrop: true,
		  backdropDismiss: false,
		  componentProps: {
			"paramTitle": idleState
		  }
		});
		this.modal.present();
	this.modal.onDidDismiss().then((dataReturned) => {
		// this.sessionLogout();
		console.log("dataReturned  ", dataReturned)
		});
	}

	


 /// ***************************** SITE CHANGE ************************************

	public pushAllLocalDataToServer(siteId) {
		localStorage.setItem('valgen_siteId', siteId);
	//	this.alertForSiteChange('Site has been Changed');
		this.toast.show('Site has been Changed', '5000', 'bottom').subscribe(
			toast => {
				console.log(toast);
			}
			);
		this._router.navigate(['/menu/home']);
	}



		
	async alertForSiteChange(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						//this._router.navigate(['menu/home']);
					}
				}
			]
		});
		await alert.present();
	}




}
