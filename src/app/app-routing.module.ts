import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginAuthGuardService, CheckLoginAuthGuardService } from './common/auth/authGuard';

const routes: Routes = [
	{
		path: '', redirectTo: 'login', pathMatch: 'full'
	},
	{
		path: 'login', loadChildren: './login/login.module#LoginModule', canActivate: [LoginAuthGuardService]
	},
	{
		path: 'menu', loadChildren: './menu/menu.module#MenuModule', canActivate: [CheckLoginAuthGuardService]
	},
	{ 
		path: 'modal', loadChildren: './modal/modal.module#ModalPageModule' 
    }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
