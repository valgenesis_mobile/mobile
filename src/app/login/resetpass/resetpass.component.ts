import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {  AlertController } from '@ionic/angular';
// import { EmailComposer } from '@ionic-native/email-composer/ngx';

import { Router } from '@angular/router';
import { EncrDecrService } from '../../common/encr-decr.service';
import { LoginService } from '../login.service';
import { Network } from '@ionic-native/network/ngx';
import { Toast } from '@ionic-native/toast/ngx';
import { Device } from '@ionic-native/device/ngx';


@Component({
  selector: 'app-resetpass',
  templateUrl: './resetpass.component.html',
  styleUrls: ['./resetpass.component.scss'],
})
export class ResetpassComponent implements OnInit {
  
  public resetForm: FormGroup;
  public resetPassForm: FormGroup;
  public resetPasswordShow: boolean = false;
  public resetData:any;
  public appPlatform:any;
  constructor( private _router: Router, private _encrDecr: EncrDecrService
    ,  private network: Network, private _loginService: LoginService, public _alertCtrl: AlertController, private toast: Toast, private device: Device) {
      this.appPlatform = localStorage.valgen_myplatform;
     }
  get f() { return this.resetForm.controls; }
  get m() { return this.resetPassForm.controls; }
  ngOnInit() {
    this.resetForm = new FormGroup({
      MailId: new FormControl('', [Validators.required, Validators.email]),
      UserName: new FormControl('', [Validators.required])
    });
    this.resetPassForm = new FormGroup({
      pass: new FormControl('', [Validators.required]),
      repass: new FormControl('', [Validators.required])
		});
  }

  
	async presentAlert(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert !',
			message: message,
			buttons: ['OK'],
			cssClass: 'profalert'
		});
		return await alert.present();
	}

  emailAuth(){
    console.table(this.resetForm.value); 
    if((this.resetForm.value.MailId).replace(/\s/g, "") != "" && (this.resetForm.value.UserName).replace(/\s/g, "") != "")
			{  
        if(this.device.uuid){
          this.resetForm.value.deviceId = this.device.uuid;
        }
        else{
          this.resetForm.value.deviceId = "This is web";
        }

    const encryptValue = this._encrDecr.encryptURL(this.resetForm.value);
    
    
		if (this.network.type == 'none' || navigator.onLine === false) {
			
      this.presentAlert('Check Internet Connection!');
    

  }
  else{
    this._loginService.login(encryptValue).subscribe(
       (data) => {
         console.log("Login data  ", data)
         if(data.Result == null){
         // this.alertPopup(data.TransactionMessage);
         if (this.appPlatform == "Electron") {
          this.alertController(data.TransactionMessage);
        }
        else {
          this.toast.show(data.TransactionMessage, '5000', 'bottom').subscribe(
            toast => {
              console.log(toast);
            }
          );
        }
         }
         else
         {
           this.resetData = data.Result;
           console.log( data.Result, " this.resetData")
          this.resetPasswordShow = true;
         }
         
       })
      }
    }
      else{
        if (this.appPlatform == "Electron") {
          this.alertController("Enter Required Field");
        }
        else {
        this.toast.show("Enter Required Field", '5000', 'bottom').subscribe(   //  For Whitespace error
          toast => {
            console.log(toast);
          }
        );
      }

      }
   
  }

  public resetPassword(){
    if((this.resetPassForm.value.pass).replace(/\s/g, "") != "" && (this.resetPassForm.value.repass).replace(/\s/g, "") != "")
    {
   console.log(this.resetData)
    const resetForm = this.resetPassForm.value;
  
   if (this.network.type == 'none' || navigator.onLine === false) {
			
    this.presentAlert('Check Internet Connection!');
  

}
else{
   if(resetForm.pass == resetForm.repass){
    this._loginService.resetPassword(this.resetData, resetForm).subscribe(
      (data) => {
        console.log("Reset  ", data)
         if(data.TransactionMessage === "Password reset successfully"){
           this.alertReset(data.TransactionMessage);
         }
         else{
           //this.alertPopup(data.TransactionMessage);
           if (this.appPlatform == "Electron") {
            this.alertController(data.TransactionMessage);
          }
          else {
           this.toast.show(data.TransactionMessage, '5000', 'bottom').subscribe(
            toast => {
              console.log(toast);
            }
          );
        }
         }
      })

   }
  
   else{
   // this.alertPopup();
   if (this.appPlatform == "Electron") {
    this.alertController("Confirm Password not match");
  }
  else {
    this.toast.show("Confirm Password not match", '5000', 'bottom').subscribe(
      toast => {
        console.log(toast);
      }
    );
  }
   }

  }
}

  }

  public newGuid() {
		const val = Math.floor(1000 + Math.random() * 9000);
		return val;
  }
  
  public goHome()
	{
		this._router.navigate(['login']);
  }
  

  async alertReset(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			backdropDismiss: false,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						this._router.navigate(['login']);
					}
				}
			]
		});
		await alert.present();
  }
  
  async alertPopup(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert',
			message: message,
			buttons: ['OK']
		});
		await alert.present();
  }
  

  async alertController(message) {
		const alert = await this._alertCtrl.create({
			//header: 'Alert',
			message: message,
			cssClass: 'login-alert'
		});
		await alert.present();
		setTimeout(() => alert.dismiss(), 2000);
	}


}
