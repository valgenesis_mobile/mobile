import { Injectable } from '@angular/core';
import { HttpService } from '../common/http.service';
import * as moment from 'moment';
import * as momenttz from 'moment-timezone';
import { EncrDecrService } from '../common/encr-decr.service';

@Injectable()
export class LoginService {
    
    constructor(private _httpService: HttpService,  private _encrDecr: EncrDecrService) { }
  
	public login(data) {
		const info = {
			url: `Login/CheckValidateUser`,
			body: data
		};
		return this._httpService.post(info);
    }
    
    public GetExpiryDate(data) {
		const info = {
			url: `Login/GetExpiryDate`,
			body: data
		};
		return this._httpService.post(info);
    }

    public LockAccount(data) {
		const info = {
			url: `Login/LockAccount`,
			body: data
		};
		return this._httpService.post(info);
    }

    public getConfigSettings() {
        const info = {
            url: `Login/GetConfigSettings`,
        };
        return this._httpService.get(info);
    }

    public getRoleProfile(userId, siteId) {
	
		const data = {
			UserID : userId,
			SiteID : siteId
		};
		const encryptValue = this._encrDecr.encryptURL(data);
		const info = {
			url: `RoleProfile/GetRoleProfile`,
			body: encryptValue
		};
		return this._httpService.post(info);
	}

    public resetPassword(data, pass) {
        const ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
		const ClientOffset = new Date().getTimezoneOffset();
        const value = {
			userId : data.UserID,
            userName : data.UserName,
            password : pass.pass,
            reenterPassword: pass.repass,
            pwdLable: 'Password',
            renterLabel: 'Confirm Password',
            clientOffset: ClientOffset,
            clientZone: ClientZone
		}
		const encryptValue = this._encrDecr.encryptURL(value);
        const info = {
            url: `Login/GetPasswordComplexity`,
            body: encryptValue
        };
        return this._httpService.post(info);
    }

}
