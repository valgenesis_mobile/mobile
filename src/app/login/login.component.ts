import { Component, OnInit, ChangeDetectorRef, NgZone } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Platform, AlertController, LoadingController, RouterLinkDelegate } from '@ionic/angular';
import { Events } from '@ionic/angular';
import { LoginService } from './login.service';
import * as moment from 'moment';
import { Network } from '@ionic-native/network/ngx';

import { EncrDecrService } from '../common/encr-decr.service';
import { ElogPouchdbProvider } from '../common/elog-pouchdb';
import { LocalPouchdbProvider } from '../common/local-pouchdb';
import { Toast } from '@ionic-native/toast/ngx';

import { LoadingService } from '../loading.service';
import * as momenttz from 'moment-timezone';
import { Device } from '@ionic-native/device/ngx';



declare var $: any;


@Component({
	selector: 'login',
	templateUrl: 'login.component.html',
	styleUrls: ['login.component.scss']
})
export class LoginComponent implements OnInit {

	public loginData = { user: '', pass: '' };
	public loginForm: FormGroup;
	public passwordType = 'password';
	public passwordIcon = 'eye-off';
	public isLogin: boolean = true;
	public loading: any;
	public loginArray: any;
	public configData: any;
	public testcount: any;
	public enableSecondaryPassword: boolean = false;
	public password_attempt: any = 0;
	public expiryPrimaryMessage: any;
	public appPlatform: any;
	public showToast: boolean = false;
	public usernameRequired: boolean = false;

	staticAlertClosed = false;
	successMessage = '';
	constructor(private _loginService: LoginService, private _router: Router, public _loadingCtrl: LoadingController, private _alertCtrl: AlertController, public events: Events, private network: Network,
		public cdRef: ChangeDetectorRef, private _encrDecr: EncrDecrService,
		public _elogpdb: ElogPouchdbProvider, private toast: Toast, public loadingService: LoadingService,
		public _localpdb: LocalPouchdbProvider, private platform: Platform, public zone: NgZone,
		private device: Device
	) {
		setTimeout(() => {
			this.cdRef.markForCheck();
		}, 2000)

	}
	get f() { return this.loginForm.controls; }
	ngOnInit() {
		this.showToast = false;
		//console.log("this.platform  ", this.platform)

		this.testcount = 0;

		if (this.network.type == 'none') {
			this.presentAlert('Check Internet Connection!');
		}
		this.loginForm = new FormGroup({
			UserName: new FormControl('', [Validators.required]),
			Password: new FormControl('', [Validators.required]),
		});
		const sPassword = localStorage.getItem('valgen_SecondaryPassword');
		const url = localStorage.getItem('valgen_settingUrl');
		const appPlatform = localStorage.getItem('valgen_myplatform');
		localStorage.clear();
		localStorage.setItem('valgen_settingUrl', url);
		localStorage.setItem('valgen_myplatform', appPlatform);
		console.log("url  ", url)
		// if (url == "") {
		// 	this._router.navigate(['login', 'setting']);
		// }
		// else {
		// 	localStorage.setItem('valgen_settingUrl', '');
		// }
		localStorage.setItem('password_attempt', this.password_attempt);
		localStorage.setItem('valgen_SecondaryPassword', sPassword);
		const secondaryPassword = localStorage.valgen_SecondaryPassword;
		if (secondaryPassword === 0) {
			this.enableSecondaryPassword = false;
		}
		else {
			this.enableSecondaryPassword = true;
		}

		//if (!url) {

		// localStorage.setItem('valgen_settingUrl', 'https://demoserver.valgenesis.net/VG4API/api/');
		// localStorage.setItem('valgen_settingUrl', 'https://vgv4test.valgenesis.in/VGV4API/api/');
		// localStorage.setItem('valgen_settingUrl', 'https://vgv4test.valgenesis.in/VGV4APITFS/api/');
		// localStorage.setItem('valgen_settingUrl', 'http://172.16.3.123/VG4API/api/');                    // vasu
		// localStorage.setItem('valgen_settingUrl', 'http://172.16.3.144/VGV4API/api/');                    // Santhosh

		//localStorage.setItem('valgen_settingUrl', 'http://172.16.3.118/WEBAPI/api/');                     // riyas
		// localStorage.setItem('valgen_settingUrl', 'https://demoserver.valgenesis.net/VG4LMAPI/api/');                 
		// localStorage.setItem('valgen_settingUrl', 'https://demoserver.valgenesis.net/VGV4_1API/api/');
		// localStorage.setItem('valgen_settingUrl', 'https://vg.valgenesis.us/VGV4PSGAPI/api/');
		// localStorage.setItem('valgen_settingUrl', 'https://vg.valgenesis.us/VGV4V1API/api/');
		//   https://vg.valgenesis.us/VGV4PSGAPIV3/api/
		//https://vg.valgenesis.us/VGV4V1PRDAPI/api/
		// http://172.16.3.123/VG4API/   https://vgv4test.valgenesis.in/VGV4API/api/	https://demoserver.valgenesis.net/VGV4_1API

		//}

		this.platform.ready().then(() => {
		//	console.log("this.platform  ", this.platform)

			if (this.platform.is('android')) {
				localStorage.setItem('valgen_myplatform', 'Android');
				localStorage.valgen_deviceId = this.device.uuid;
			}
			else if (this.platform.is('ios')) {
				localStorage.setItem('valgen_myplatform', 'IOS');
				localStorage.valgen_deviceId = this.device.uuid;
			}
			else if (this.platform.is('electron')) {
				localStorage.setItem('valgen_myplatform', 'Electron');
				localStorage.valgen_deviceId = 'this.device.uuid';
			}
			else {
				localStorage.setItem('valgen_myplatform', 'Desktop');
				localStorage.valgen_deviceId = 'this.device.uuid';
			}
			this.appPlatform = localStorage.valgen_myplatform;
			//console.log("this.appPlatform   ", this.appPlatform)


		});
	}
	textchange() {
		$('#user_id').keyup(function () {
			this.value = this.value.replace(/(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g, '');
		});

		$('#user_password').keyup(function () {
			this.value = this.value.replace(/(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g, '');
		});
	}



	passwordConfig() {
		if (this.network.type != 'none' || navigator.onLine === false) {
		this._loginService.getConfigSettings().subscribe(
		(data) => {
		// this.configData = response;
		if (data.TransactionCode === 'SUCCESS' && data.TransactionMessage === 'Authentication Successful') {
		localStorage.setItem('valgen_SecondaryPassword', data.Result.OfflineOwnPasswordRequired);
		const secondaryPassword = localStorage.valgen_SecondaryPassword;
		if (secondaryPassword === 0) {
		this.enableSecondaryPassword = false;
		}
		else {
		this.enableSecondaryPassword = true;
		}
		}
		else {
		console.log(data.message);
		}
		})
		}
		else{
		this.presentAlert('Check Internet Connection!');
		}
		
		}


	load() {
		location.reload();
	}

	ngAfterViewInit() {
		const url = localStorage.getItem('valgen_settingUrl');
		console.log("url  after  ", url)
		this.ngOnInit();
		setTimeout(() => {
			this.cdRef.markForCheck();
		}, 2000)
	}

	settingUrl() {
		this._router.navigate(['login', 'setting']);
	}

	hideShowPassword() {
		this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
		this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
	}

	async presentAlert(message) {
		const alert = await this._alertCtrl.create({
			header: 'Alert !',
			message: message,
			buttons: ['OK'],
			cssClass: 'profalert'
		});
		return await alert.present();
	}

	async loadingController(message) {
		this.loading = await this._loadingCtrl.create({
			message: message,
			spinner: 'lines'
		});
		await this.loading.present();
	}


	login(position: any = 'bottom-center') {
		console.log(" navigator.onLine  ", navigator.onLine)
		

		if (this.network.type == 'none' || navigator.onLine === false) {
			
				this.presentAlert('Check Internet Connection!');
			

		}
		else{

			console.log("loginnnn ",this.network )
			if ((this.loginForm.value.UserName).replace(/\s/g, "") != "" && (this.loginForm.value.Password).replace(/\s/g, "") != "") {
				if (localStorage.getItem('valgen_settingUrl')) {
					this.loadingService.showLoading10('Logging in.....');
					this._loginService.getConfigSettings().subscribe(
						(data) => {
							if (data.TransactionCode === 'SUCCESS' && data.TransactionMessage === 'Authentication Successful') {
								localStorage.setItem('valgen_LoginAccountLockout', data.Result.LoginAccountLockout);
							}
						})

					const expiryP = {
						userName: this.loginForm.value.UserName, //username
						mode: "P" //”P” or “O”
						//password: "valgen"
					}
					const expiryO = {
						userName: this.loginForm.value.UserName, //username
						mode: "P" //”P” or “O”
					}


					const encryptValueP = this._encrDecr.encryptURL(expiryP);
					const encryptValueO = this._encrDecr.encryptURL(expiryO);
					this._loginService.GetExpiryDate(encryptValueP).subscribe(
						(data) => {
							if (data.TransactionCode == "SUCCESS") {
								this.expiryPrimaryMessage = data.TransactionMessage;
								this._loginService.GetExpiryDate(encryptValueO).subscribe(
									(data) => {
										if (data.TransactionCode == "SUCCESS") {
											setTimeout(() => {
												if (this.network.type != 'none' || navigator.onLine === false) {
													if (this.device.uuid) {
														this.loginForm.value.deviceId = this.device.uuid;
													}
													else {
														this.loginForm.value.deviceId = "This is web";
													}
													console.log("this.loginForm   ", this.loginForm)
													const encryptValue = this._encrDecr.encryptURL(this.loginForm.value);
													this._loginService.login(encryptValue).subscribe(
														(data) => {
															console.log("Login data  ", data)
															if (data.TransactionCode === 'SUCCESS' && data.TransactionMessage === 'Authentication Successful') {
																if (this.appPlatform == "Electron") {
																	this.alertController(this.expiryPrimaryMessage);
																}
																else {
																	this.toast.show(this.expiryPrimaryMessage, '10000', 'bottom').subscribe(
																		toast => {
																		}
																	);
																}
																localStorage.setItem('valgen_login', JSON.stringify(data.Result));
																localStorage.setItem('valgen_userName', data.Result.UserName);
																localStorage.setItem('valgen_firstName', data.Result.FirstName);
																localStorage.setItem('valgen_lastName', data.Result.LastName);
																localStorage.setItem('valgen_emailId', data.Result.EMailID);
																localStorage.setItem('valgen_roleName', data.Result.RoleName);
																localStorage.setItem('valgen_userId', data.Result.UserID);
																localStorage.setItem('valgen_siteId', data.Result.SiteID);
																localStorage.setItem('valgen_EsignPassword', data.Result.EsignPassword);
																localStorage.setItem('valgen_AESIV', data.Result.AESIV);
																// localStorage.setItem('valgen_vlmsMenu', 'true');
																// localStorage.setItem('valgen_eLogMenu', 'true');
																localStorage.setItem('valgen_userElogName', data.Result.FirstName + ' ' + data.Result.LastName + '[' + data.Result.UserName + ']');
																this._loginService.getRoleProfile(data.Result.UserID, data.Result.SiteID).subscribe(
																	(data) => {
																		data.Result.forEach((role) => {
																			if (role.UnitId === "LM057") {
																				if (role.FullAccess == 1) {
																					localStorage.setItem('valgen_eLogMenu', 'true');
																				}
																				else {
																					localStorage.setItem('valgen_eLogMenu', 'false');
																				}
																			}
																			else if (role.UnitId === "EX000") {
																				if (role.FullAccess == 1) {
																					localStorage.setItem('valgen_vlmsMenu', 'true');
																				}
																				else {
																					localStorage.setItem('valgen_vlmsMenu', 'false');
																				}
																			}
																		})

																		setTimeout(() => {
																			if (localStorage.getItem('valgen_eLogMenu') == 'false') {
																				this._router.navigate([`/menu/home/task`]);
																			}
																			else if (localStorage.getItem('valgen_vlmsMenu') == 'false') {
																				this._router.navigate([`/menu/home/elog`]);
																			}
																			else {
																				this._router.navigate(['menu', 'home']);
																			}

																		}, 1000);

																	}
																)

																this._loginService.getConfigSettings().subscribe(
																	(data) => {

																		console.log("CONFIGGGGG   ", data)
																		if (data.TransactionCode === 'SUCCESS' && data.TransactionMessage === 'Authentication Successful') {
																			localStorage.setItem('valgen_sessionTimeOut', data.Result.SessionTimeOut);
																			localStorage.setItem('valgen_DateFormat', data.Result.DateFormat);
																			localStorage.setItem('valgen_validTimeStamp', moment().format('YYYY-MM-DD, h:mm:ss'));
																			localStorage.setItem('valgen_EsignPerform', data.Result.EsignPerform);
																			localStorage.setItem('valgen_VerificationPerform', data.Result.VerificationPerform);
																			localStorage.setItem('valgen_ExecutionComments', data.Result.ExecutionComments);
																			localStorage.setItem('valgen_SecondaryPassword', data.Result.OfflineOwnPasswordRequired);
																			localStorage.setItem('valgen_FileFormat', data.Result.FileFormat);
																			localStorage.setItem('valgen_EsignVerbiageReq', data.Result.EsignVerbiageReq);
																			localStorage.setItem('valgen_EsignVerbiageText', data.Result.EsignVerbiageText);
																			localStorage.setItem('valgen_WatermarkPosition', data.Result.WatermarkPosition);
																			localStorage.setItem('valgen_SequentialRowExecutionRequired', data.Result.SequentialRowExecutionRequired);
																			if (localStorage.valgen_DateFormat == 1) {
																				localStorage.setItem('valgen_DataValue', 'MM/DD/YYYY h:mm:ss A');
																			}
																			else if (localStorage.valgen_DateFormat == 2) {
																				localStorage.setItem('valgen_DataValue', 'DDMMMYY h:mm:ss A');
																			}
																			else if (localStorage.valgen_DateFormat == 3) {
																				localStorage.setItem('valgen_DataValue', 'DDMMMYYYY h:mm:ss A');
																			}
																			else if (localStorage.valgen_DateFormat == 4) {
																				localStorage.setItem('valgen_DataValue', 'DD-MMM-YYYY h:mm:ss A');
																			}
																			else if (localStorage.valgen_DateFormat == 5) {
																				localStorage.setItem('valgen_DataValue', 'YYYY-MM-DD h:mm:ss A');
																			}
																			else if (localStorage.valgen_DateFormat == 6) {
																				localStorage.setItem('valgen_DataValue', 'YYYY-MMM-DD h:mm:ss A');
																			}
																			this.loadingService.dismiss();
																		} else {
																			this.loadingService.dismiss();
																		}
																	}
																);
															} else {
																this.loadingService.dismiss();
																if (data.TransactionId == -39 || data.TransactionId == -36 || data.TransactionId == -37 || data.TransactionId == -38) {
																	this.password_attempt = parseInt(localStorage.getItem('password_attempt')) + 1;
																	localStorage.setItem('password_attempt', this.password_attempt);
																	if (this.password_attempt == localStorage.valgen_LoginAccountLockout) {
																		const ClientZone = momenttz.tz(momenttz.tz.guess()).zoneAbbr();
																		const ClientOffset = new Date().getTimezoneOffset();
																		const passData = {
																			userName: this.loginForm.value.UserName, //username
																			clientZone: ClientZone, //Client zone
																			clientOffSet: ClientOffset, //Client Offset
																			lockoutAttempt: localStorage.valgen_LoginAccountLockout //Invalid Attempt Count
																		}
																		const encryptValueO = this._encrDecr.encryptURL(passData);
																		this._loginService.LockAccount(encryptValueP).subscribe(
																			(data) => {
																				if (data.TransactionCode == "SUCCESS") {
																					if (this.appPlatform == "Electron") {
																						this.alertController(data.TransactionMessage);
																					}
																					else {


																						this.toast.show(data.TransactionMessage, '5000', 'bottom').subscribe(
																							toast => {
																								console.log(toast);
																							}
																						);
																					}
																				}
																			})
																	}
																}
																if (this.appPlatform == "Electron") {
																	this.alertController(data.TransactionMessage);
																}
																else {
																	this.toast.show(data.TransactionMessage, '5000', 'bottom').subscribe(
																		toast => {
																			console.log(toast);
																		}
																	);
																}
															}
														}, (error) => {
															if (this.appPlatform == "Electron") {
																this.alertController(error);
															}
															else {
																this.toast.show(error, '5000', 'bottom').subscribe(
																	toast => {
																		console.log(toast);
																	}
																);
															}
															this.loadingService.dismiss();
														}
													);
												}
												else {
													this.loadingService.dismiss();
													this.presentAlert('Check Internet Connection!');
												}
											}, 200);

										}
										else {
											if (this.appPlatform == "Electron") {
												this.alertController(data.TransactionMessage);
											}
											else {
												this.toast.show(data.TransactionMessage, '5000', 'bottom').subscribe(   // Secondary or Offline Password Expiry Error
													toast => {

													}
												);
											}
										}
									})
							}
							else {
								if (this.appPlatform == "Electron") {
									this.alertController(data.TransactionMessage);
								}
								else {
									this.toast.show(data.TransactionMessage, '5000', 'bottom').subscribe(   // Primary Password Expiry Error
										toast => {
											console.log(toast);
										}
									);
								}
							}
						})
				}
				else {
					if (this.appPlatform == "Electron") {
						this.alertController("Enter the URL in Settings");
					}
					else {
						this.toast.show("Enter the URL in Settings", '5000', 'bottom').subscribe(
							toast => {
								console.log(toast);
							}
						);
					}
				}
			}
			else {
				if (this.appPlatform == "Electron") {
					this.alertController("Enter the Required Field");
				}
				else {
					this.toast.show("Enter Required Field", '5000', 'bottom').subscribe(
						toast => {
							console.log(toast);
						}
					);
				}
			}
		}
		
	}


	forgotPass() {
		this._router.navigate(['login', 'resetpass']);
	}

	async alertController(message) {
		const alert = await this._alertCtrl.create({
			message: message
		});
		await alert.present();
		setTimeout(() => alert.dismiss(), 2000);
	}


	forgotPass1() {
		this.testcount = this.testcount + 1;
		if (this.testcount >= 5) {
			this.testcount = 0;
			this._localpdb.deleteDatabase().subscribe(
				(data) => {
				});
			this._elogpdb.deleteDatabase().subscribe(
				(data) => {
				});
		}
	}
}
/*  "scripts": {
  //   "ng": "ng",
  //   "start": "ng serve",
  //   "build": "ng build",
  //   "test": "ng test",
  //   "lint": "ng lint",
  //   "e2e": "ng e2e"
  // }
  */