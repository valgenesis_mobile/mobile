import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Platform,LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import {LoginComponent} from '../login.component';
import {LoadingService} from '../../loading.service';
import { Network } from '@ionic-native/network/ngx';

@Component({
    selector: 'settingurl',
    templateUrl: 'settingurl.component.html',
    styleUrls: ['settingurl.component.scss'],
    providers: [LoginComponent]
})
export class SettingurlComponent implements OnInit {

    public settingUrlData = { settingUrl: ''};
	public settingUrlForm: FormGroup;
    public settingUrl: any = "";
    public errorMessage: any = "";
    public configData:any;
    public appPlatform:any;
    constructor(private _router: Router, public http: HttpClient, public loginCmp: LoginComponent, 
         public loadingService: LoadingService, private _network: Network, public _alertCtrl: AlertController, private platform: Platform) {
            this.appPlatform = localStorage.valgen_myplatform;
	}

	ngOnInit() {
        this.settingUrl = "";
        this.settingUrlForm = new FormGroup({
			settingUrl: new FormControl('', [Validators.required])
        });
        console.log("this.platform  ", this.platform)
        console.log("this.settingUrl  ", localStorage.getItem('valgen_settingUrl'))
      if(localStorage.getItem('valgen_settingUrl') == "null"){
        this.settingUrl ="";
      }else{
        this.settingUrl = localStorage.getItem('valgen_settingUrl');
      }
     
       
        
    }

    resetURL(){
        this.settingUrl = "";
        this.errorMessage = "";
    }
    
    setUrl() {
        console.log("this._network.type", navigator.onLine)
        
        if (this._network.type === 'none' || navigator.onLine === false) {
            console.log("this._network  ", this._network)

            this.alertController('Check Internet Connection!');
        } else {
            const disconnectSubscription = this._network.onDisconnect().subscribe(() => {
                this.alertController('Check Internet Connection!');
            });
            this.loadingService.present("loading....");
            if ((this.settingUrlForm.value.settingUrl).replace(/\s/g, "") != "") {
                this.http.get(this.settingUrlForm.value.settingUrl + "test/get")
                    .subscribe((response) => {
                        this.errorMessage = '';
                        localStorage.setItem('valgen_settingUrl', this.settingUrlForm.value.settingUrl);
                        this.loadingService.dismiss();
                        this._router.navigate(['login']);

                    }, (error) => {
                        this.loadingService.dismiss();
                        this.errorMessage = "The URL Does't Exist!!! Please check the URL Entered!!";
                    })
            }
            else {
                this.loadingService.dismiss();
                this.errorMessage = "Enter an Valid URL";
            }

        }
    }

async alertController(message) {
    const alert = await this._alertCtrl.create({
        header: 'Alert',
        message: message,
        buttons: ['OK']
    });
    await alert.present();
}

goBack(){
    this._router.navigate(['/', 'login']);
   }



}
