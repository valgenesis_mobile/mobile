// Angular Imports
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

// This Module's Components
import { IonicModule } from '@ionic/angular';
import { LoginComponent } from './login.component';
import { LoginService } from './login.service';
import { SettingurlComponent } from './settingurl/settingurl.component';

import { ResetpassComponent } from './resetpass/resetpass.component';

const routes: Routes = [
	{
		path: '',
		component: LoginComponent
	},
	{
		path: 'setting',
		component: SettingurlComponent
	},
	{
		path: 'resetpass',
		component: ResetpassComponent
	}
];

@NgModule({
    imports: [
        CommonModule,
		ReactiveFormsModule,
		FormsModule,
		IonicModule,
		RouterModule.forChild(routes)
    ],
    declarations: [
		LoginComponent,
		SettingurlComponent,
		ResetpassComponent
    ],
    providers: [
		LoginService
	]
})
export class LoginModule {

}
