// Modules to control application life and create native browser window
const { app, BrowserWindow, ipcMain } = require('electron')
const url = require('url');
const path = require('path');
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
app.commandLine.appendSwitch('auto');
// app.commandLine.appendSwitch('disable-pinch');
// app.commandLine.appendSwitch('touch-events', 'enabled');
app.disableHardwareAcceleration();

// Disable Confirm, Alert, Prompt Dialogs
// ipcMain.removeAllListeners('ELECTRON_BROWSER_WINDOW_ALERT');
// ipcMain.removeAllListeners('ELECTRON_BROWSER_WINDOW_CONFIRM');
// ipcMain.removeAllListeners('ELECTRON_BROWSER_WINDOW_PROMPT');
// ipcMain.on('ELECTRON_BROWSER_WINDOW_ALERT', (event, message, title) => {
// 	logger.warn(`Alert: ${title} - ${message}`);
// 	event.returnValue = 0; // IMPORTANT!
// });

// ipcMain.on('ELECTRON_BROWSER_WINDOW_CONFIRM', (event, message, title) => {
// 	logger.warn(`Alert: ${title} - ${message}`);
// 	event.returnValue = 0; // IMPORTANT!
// });

// ipcMain.on('ELECTRON_BROWSER_WINDOW_PROMPT', (event, message, title) => {
// 	logger.warn(`Alert: ${title} - ${message}`);
// 	event.returnValue = 0; // IMPORTANT!
// });

function createWindow() {
	// Create the browser window.
	mainWindow = new BrowserWindow({
		width: 1080,
		height: 1920,
		webgl: true,
		webPreferences: {
			nodeIntegration: true
		}
	});

	// and load the index.html of the app.
	mainWindow.loadURL(url.format({
		pathname: path.join(__dirname, 'dist/index.html'),
		protocol: 'file:',
		slashes: true
	}));

	// mainWindow.loadURL(`file://${__dirname}/dist/index.html`);
	// mainWindow.webContents.openDevTools();
	// mainWindow.setFullScreen(true);
	// mainWindow.setKiosk(true);

	// Open the DevTools.
	// mainWindow.webContents.openDevTools()

	// Emitted when the window is closed.
	mainWindow.on('closed', function () {
		// Dereference the window object, usually you would store windows
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		mainWindow = null
	});
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
	// On OS X it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform !== 'darwin') {
		app.quit()
	}
})

app.on('activate', function () {
	// On OS X it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (mainWindow === null) {
		createWindow()
	}
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.