For building

npm i
ionic cordova prepare

For running in browser
ionic serve
ionic cordova run browser

For building android
ionic cordova build android
ionic cordova run android
ionic cordova run android --list
ionic cordova run android -lc --target=ZW2225853D

For release android
ionic cordova build android --prod
ionic cordova build --release android --prod

For building ios
ionic cordova build ios
ionic cordova build ios --prod
ionic cordova build --release ios --prod